/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <debug/sahtrace.h>

#include <dmengine/DM_ENG_RPC.h>
#include <dmxmlparser/ixml.h>
#include <dmcom/dm_com.h>
#include <dmengine/DM_ENG_InformMessageScheduler.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_macros.h>
#include <amxut/amxut_sahtrace.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_util.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>

#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/debugXML.h>

#include "test_full_gpv.h"

xmlDocPtr doc = NULL;
xmlNodePtr gpvr_node = NULL;
xmlXPathContextPtr xpathCtx = NULL;
xmlXPathObjectPtr xpathObj = NULL;

int test_setup(UNUSED void** state) {
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "DM_ENGINE");
    sahTraceAddZone(500, "DM_COM");
    sahTraceOpen("TEST", TRACE_TYPE_STDOUT);

    LIBXML_TEST_VERSION
        doc = xmlParseFile("./files/source.xml");
    assert_non_null(doc);

    xpathCtx = xmlXPathNewContext(doc);
    assert_non_null(xpathCtx);
    xmlChar* xpathExpr = BAD_CAST "/SOAP-ENV:Envelope/SOAP-ENV:Body/cwmp:GetParameterValuesResponse";
    assert_int_equal(xmlXPathRegisterNs(xpathCtx, BAD_CAST "SOAP-ENV", BAD_CAST "http://schemas.xmlsoap.org/soap/envelope/"), 0);
    assert_int_equal(xmlXPathRegisterNs(xpathCtx, BAD_CAST "cwmp", BAD_CAST "urn:dslforum-org:cwmp-1-0"), 0);
    xpathObj = xmlXPathEvalExpression(xpathExpr, xpathCtx);
    assert_non_null(xpathObj);
    xmlNodeSetPtr nodes = xpathObj->nodesetval;
    assert_non_null(nodes);
    assert_int_equal(nodes->nodeNr, 1);
    xmlNodePtr node = nodes->nodeTab[0];
    assert_non_null(node);
    gpvr_node = xmlCopyNode(node, 1);
    /*
        FIXME: Using xml xpath api the gpvr is created with some ns. is that Normal!!

        <cwmp:GetParameterValuesResponse
        xmlns:cwmp="urn:dslforum-org:cwmp-1-0"
        xmlns:soap-enc="http://schemas.xmlsoap.org/soap/encoding/"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

        gpvr shouldn't have ns.

            // node->ns = NULL;
            // xmlDebugDumpDocument(stdout, doc);
            // xmlDebugDumpOneNode(stdout, gpvr_node, 0);
     */
    assert_non_null(gpvr_node);
    return 0;
}

int test_teardown(UNUSED void** state) {
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    sahTraceClose();
    return 0;
}

void test_full_gpv(UNUSED void** state) {
    int retval = 0;
    retval = DM_SUB_GetParameterValuesAll("ACS-ID-423");
    // TODO: test the output xml to be compared with ./files/source.xml
    DM_COM_STOP();
    assert_int_equal(retval, 0);
}

int __wrap_DM_ENG_GetParameterValuesAll(xmlNodePtr node_body);
int __wrap_DM_ENG_GetParameterValuesAll(xmlNodePtr node_body) {
    xmlAddChild(node_body, gpvr_node);
    return 0;
}
