/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <debug/sahtrace.h>

#include <dmengine/DM_ENG_RPC.h>
#include <dmxmlparser/ixml.h>
#include <dmcom/dm_com.h>
#include <dmengine/DM_ENG_InformMessageScheduler.h>

#include "test_change_du_state.h"

int test_setup(UNUSED void** state) {

    sahTraceSetLevel(500);
    sahTraceAddZone(500, "DM_ENGINE");
    sahTraceAddZone(500, "DM_COM");
    sahTraceOpen("TEST", TRACE_TYPE_STDOUT);
    return 0;
}

int test_teardown(UNUSED void** state) {
    sahTraceClose();
    return 0;
}

IXML_Document* test_soap_document_init(const char* xmlFile);
IXML_Document* test_soap_document_init(const char* xmlFile) {
    return ixmlLoadDocument(xmlFile);
}

void test_soap_document_clean(IXML_Document* doc);
void test_soap_document_clean(IXML_Document* doc) {
    ixmlDocument_free(doc);
}

GenericXmlNodePtr test_soap_msg_body(IXML_Document* doc);
GenericXmlNodePtr test_utils_soap_msg_body_get_node(IXML_Document* doc, const char* node_name);

GenericXmlNodePtr test_soap_msg_body(IXML_Document* doc) {
    return xmlGetFirstNodeWithTagName((GenericXmlDocumentPtr) doc, "soapenv:Body");
}

GenericXmlNodePtr test_utils_soap_msg_body_get_node(IXML_Document* doc, const char* node_name) {
    GenericXmlNodePtr bodyNode = test_soap_msg_body(doc);
    return xmlGetFirstNodeWithTagName(xmlNodeToDocument(bodyNode), node_name);
}

void test_valid_ChangeDUState_Install(UNUSED void** state) {
    const char* xmlFile = "files/valid_ChangeDUState_Install.xml";
    int retval = 0;

    IXML_Document* doc = test_soap_document_init(xmlFile);
    retval = DM_SUB_ChangeDUState(test_utils_soap_msg_body_get_node(doc, "cwmp:ChangeDUState"));
    test_soap_document_clean(doc);

    assert_int_equal(retval, 0);
}

void test_valid_ChangeDUState_Install_required_only(UNUSED void** state) {
    const char* xmlFile = "files/valid_ChangeDUState_Install_required_only.xml";
    int retval = 0;

    IXML_Document* doc = test_soap_document_init(xmlFile);
    retval = DM_SUB_ChangeDUState(test_utils_soap_msg_body_get_node(doc, "cwmp:ChangeDUState"));
    test_soap_document_clean(doc);

    assert_int_equal(retval, 0);
}

void test_valid_ChangeDUState_Uninstall(UNUSED void** state) {
    const char* xmlFile = "files/valid_ChangeDUState_Uninstall.xml";
    int retval = 0;

    IXML_Document* doc = test_soap_document_init(xmlFile);
    retval = DM_SUB_ChangeDUState(test_utils_soap_msg_body_get_node(doc, "cwmp:ChangeDUState"));
    test_soap_document_clean(doc);

    assert_int_equal(retval, 0);
}

void test_valid_ChangeDUState_Uninstall_required_only(UNUSED void** state) {
    const char* xmlFile = "files/valid_ChangeDUState_Uninstall_required_only.xml";
    int retval = 0;

    IXML_Document* doc = test_soap_document_init(xmlFile);
    retval = DM_SUB_ChangeDUState(test_utils_soap_msg_body_get_node(doc, "cwmp:ChangeDUState"));
    test_soap_document_clean(doc);

    assert_int_equal(retval, 0);
}

void test_valid_ChangeDUState_Update(UNUSED void** state) {
    const char* xmlFile = "files/valid_ChangeDUState_Update.xml";
    int retval = 0;

    IXML_Document* doc = test_soap_document_init(xmlFile);
    retval = DM_SUB_ChangeDUState(test_utils_soap_msg_body_get_node(doc, "cwmp:ChangeDUState"));
    test_soap_document_clean(doc);

    assert_int_equal(retval, 0);
}

void test_valid_ChangeDUState_Update_required_only(UNUSED void** state) {
    const char* xmlFile = "files/valid_ChangeDUState_Update_required_only.xml";
    int retval = 0;

    IXML_Document* doc = test_soap_document_init(xmlFile);
    retval = DM_SUB_ChangeDUState(test_utils_soap_msg_body_get_node(doc, "cwmp:ChangeDUState"));
    test_soap_document_clean(doc);

    assert_int_equal(retval, 0);
}

/* TODO: move to common an rename it */
static void test_heartbeat_get_servervalue(const char* value, DM_ENG_EntityType type, DM_ENG_SystemParameter_t param, int retval) {
    char* servervalue = strdup(value);
    expect_value(__wrap_DM_ENG_GetManagementServerValue, entity, type);
    expect_value(__wrap_DM_ENG_GetManagementServerValue, parameter, param);
    will_return(__wrap_DM_ENG_GetManagementServerValue, servervalue);
    will_return(__wrap_DM_ENG_GetManagementServerValue, retval);
}


void test_SendDUStateChangeComplete(UNUSED void** state) {
    int retval = 0;
    amxc_var_t results;
    amxc_var_init(&results);
    amxc_var_set_type(&results, AMXC_VAR_ID_HTABLE);

    // TODO: refactor

    amxc_var_t* op1_res1 = NULL;
    op1_res1 = amxc_var_add_key(amxc_htable_t, &results, "ManagementServer.SMM.DUStateChangeComplete.1.Operations.1.Results.1.", NULL);
    amxc_var_add_key(amxc_ts_t, op1_res1, "CompleteTime", NULL);
    amxc_var_add_key(amxc_ts_t, op1_res1, "StartTime", NULL);
    amxc_var_add_key(cstring_t, op1_res1, "CurrentState", "Installed");
    amxc_var_add_key(cstring_t, op1_res1, "DeploymentUnitRef", "");
    amxc_var_add_key(cstring_t, op1_res1, "ExecutionUnitRefList", "");
    amxc_var_add_key(uint32_t, op1_res1, "FaultCode", 0);
    amxc_var_add_key(cstring_t, op1_res1, "FaultString", "");
    amxc_var_add_key(bool, op1_res1, "Resolved", false);
    amxc_var_add_key(cstring_t, op1_res1, "UUID", "");
    amxc_var_add_key(cstring_t, op1_res1, "Version", "");

    amxc_var_t* op1_res2 = NULL;
    op1_res2 = amxc_var_add_key(amxc_htable_t, &results, "ManagementServer.SMM.DUStateChangeComplete.1.Operations.1.Results.2.", NULL);
    amxc_var_add_key(amxc_ts_t, op1_res2, "CompleteTime", NULL);
    amxc_var_add_key(amxc_ts_t, op1_res2, "StartTime", NULL);
    amxc_var_add_key(cstring_t, op1_res2, "CurrentState", "Failed");
    amxc_var_add_key(cstring_t, op1_res2, "DeploymentUnitRef", "");
    amxc_var_add_key(cstring_t, op1_res2, "ExecutionUnitRefList", "");
    amxc_var_add_key(uint32_t, op1_res2, "FaultCode", 0);
    amxc_var_add_key(cstring_t, op1_res2, "FaultString", "");
    amxc_var_add_key(bool, op1_res2, "Resolved", false);
    amxc_var_add_key(cstring_t, op1_res2, "UUID", "");
    amxc_var_add_key(cstring_t, op1_res2, "Version", "");

    amxc_var_t* op2_res1 = NULL;
    op2_res1 = amxc_var_add_key(amxc_htable_t, &results, "ManagementServer.SMM.DUStateChangeComplete.1.Operations.2.Results.1.", NULL);
    amxc_var_add_key(amxc_ts_t, op2_res1, "CompleteTime", NULL);
    amxc_var_add_key(amxc_ts_t, op2_res1, "StartTime", NULL);
    amxc_var_add_key(cstring_t, op2_res1, "CurrentState", "Uninstalled");
    amxc_var_add_key(cstring_t, op2_res1, "DeploymentUnitRef", "");
    amxc_var_add_key(cstring_t, op2_res1, "ExecutionUnitRefList", "");
    amxc_var_add_key(uint32_t, op2_res1, "FaultCode", 0);
    amxc_var_add_key(cstring_t, op2_res1, "FaultString", "");
    amxc_var_add_key(bool, op2_res1, "Resolved", false);
    amxc_var_add_key(cstring_t, op2_res1, "UUID", "");
    amxc_var_add_key(cstring_t, op2_res1, "Version", "");

    test_heartbeat_get_servervalue("0", DM_ENG_EntityType_SYSTEM, DM_ENG_PERIODICINFORMENABLE, 0);
    test_heartbeat_get_servervalue("0", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATENABLE, 0);
    DM_ENG_InformMessageScheduler_init(NULL);
    retval = DM_ENG_InformMessageScheduler_SendDUStateChangeComplete(&results, "2403", "ManagementServer.SMM.DUStateChangeComplete.1.");
    retval = DM_ENG_InformMessageScheduler_SendDUStateChangeComplete(&results, "3103", "ManagementServer.SMM.DUStateChangeComplete.2.");
    DM_ENG_InformMessageScheduler_cleanup();
    amxc_var_clean(&results);
    assert_int_equal(retval, 0);
}

