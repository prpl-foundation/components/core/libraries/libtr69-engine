/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include "test_heartbeat.h"
#include <debug/sahtrace.h>

#include <dmengine/DM_ENG_InformMessageScheduler.h>
#include <dmcom/dm_com_digest.h>

int test_setup(UNUSED void** state) {

    sahTraceSetLevel(500);
    sahTraceAddZone(500, "DM_ENGINE");
    sahTraceOpen("TEST", TRACE_TYPE_STDOUT);
    return 0;
}

int test_teardown(UNUSED void** state) {
    sahTraceClose();
    return 0;
}

static void test_heartbeat_get_servervalue(const char* value, DM_ENG_EntityType type, DM_ENG_SystemParameter_t param, int retval) {
    char* servervalue = strdup(value);
    expect_value(__wrap_DM_ENG_GetManagementServerValue, entity, type);
    expect_value(__wrap_DM_ENG_GetManagementServerValue, parameter, param);
    will_return(__wrap_DM_ENG_GetManagementServerValue, servervalue);
    will_return(__wrap_DM_ENG_GetManagementServerValue, retval);
}

void test_heartbeat_unavailable(UNUSED void** state) {
    test_heartbeat_get_servervalue("", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATENABLE, -1);

    DM_ENG_InformMessageScheduler_Heartbeat_configure();
}

void test_heartbeat_disabled(UNUSED void** state) {
    test_heartbeat_get_servervalue("0", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATENABLE, 0);

    DM_ENG_InformMessageScheduler_Heartbeat_configure();
}

void test_heartbeat_enabled_invalid_interval(UNUSED void** state) {
    test_heartbeat_get_servervalue("1", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATENABLE, 0);
    test_heartbeat_get_servervalue("1", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATREPORTINGINTERVAL, 0);

    DM_ENG_InformMessageScheduler_Heartbeat_configure();
}

void test_heartbeat_enabled_valid_interval(UNUSED void** state) {
    test_heartbeat_get_servervalue("1", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATENABLE, 0);
    test_heartbeat_get_servervalue("100", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATREPORTINGINTERVAL, 0);
    test_heartbeat_get_servervalue("0001-01-01T00:00:00Z", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATINITIATIONTIME, 0);

    DM_ENG_InformMessageScheduler_Heartbeat_configure();
}


void test_heartbeat_inform_timer(UNUSED void** state) {
    //init periodic inform
    test_heartbeat_get_servervalue("0", DM_ENG_EntityType_SYSTEM, DM_ENG_PERIODICINFORMENABLE, 0);

    //heartbeat configure
    test_heartbeat_get_servervalue("1", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATENABLE, 0);
    test_heartbeat_get_servervalue("100", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATREPORTINGINTERVAL, 0);
    test_heartbeat_get_servervalue("0001-01-01T00:00:00Z", DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATINITIATIONTIME, 0);

    //reaches hearbeat_inform_timer_cb
    //Heartbeat not triggered, does not contain HB event
    //DM_ENG_InformMessageScheduler_sendMessage callback called
    test_heartbeat_get_servervalue("1.1.1.1", DM_ENG_EntityType_SYSTEM, DM_ENG_CONNECTIONREQUESTHOST, 0);
    test_heartbeat_get_servervalue("1", DM_ENG_EntityType_SYSTEM, DM_ENG_ENABLECWMP, 0);

    //new deviceid struct
    test_heartbeat_get_servervalue("WNC", DM_ENG_EntityType_SYSTEM, DM_ENG_MANUFACTURER, 0);
    test_heartbeat_get_servervalue("000B6B", DM_ENG_EntityType_SYSTEM, DM_ENG_MANUFACTUREROUI, 0);
    test_heartbeat_get_servervalue("CR1000A", DM_ENG_EntityType_SYSTEM, DM_ENG_PRODUCTCLASS, 0);
    test_heartbeat_get_servervalue("ABV31002350", DM_ENG_EntityType_SYSTEM, DM_ENG_SERIALNUMBER, 0);


    test_heartbeat_get_servervalue("0", DM_ENG_EntityType_SYSTEM, DM_ENG_INHIBIT_VALUE_CHANGE_UPON_BOOT, 0);

    // forced params
    //second send message
    test_heartbeat_get_servervalue("1.1.1.1", DM_ENG_EntityType_SYSTEM, DM_ENG_CONNECTIONREQUESTHOST, 0);
    test_heartbeat_get_servervalue("1", DM_ENG_EntityType_SYSTEM, DM_ENG_ENABLECWMP, 0);

    //new deviceid struct
    test_heartbeat_get_servervalue("WNC", DM_ENG_EntityType_SYSTEM, DM_ENG_MANUFACTURER, 0);
    test_heartbeat_get_servervalue("000B6B", DM_ENG_EntityType_SYSTEM, DM_ENG_MANUFACTUREROUI, 0);
    test_heartbeat_get_servervalue("CR1000A", DM_ENG_EntityType_SYSTEM, DM_ENG_PRODUCTCLASS, 0);
    test_heartbeat_get_servervalue("ABV31002350", DM_ENG_EntityType_SYSTEM, DM_ENG_SERIALNUMBER, 0);

    DM_ENG_InformMessageScheduler_init(NULL);
    DM_ENG_InformMessageScheduler_cleanup();
}