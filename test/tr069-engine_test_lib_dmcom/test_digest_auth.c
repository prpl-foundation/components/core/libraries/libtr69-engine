#include "test_digest_auth.h"

#include <debug/sahtrace.h>

char* __wrap_DM_COM_GenerateRequestDigestMsg(char* randomNonceStr, char* randomOpaqueStr) {
    char* requestDigestMsg = (char*) malloc(SIZE_HTTP_MSG); // No need to require a very large buffer.
    memset((void*) requestDigestMsg, 0x00, SIZE_HTTP_MSG);

    memset((void*) &randomNonceStr[0], '\0', sizeof(randomNonceStr));
    memset((void*) &randomOpaqueStr[0], '\0', sizeof(randomOpaqueStr));

    strcpy(randomNonceStr, "tp2oOgsxpp3tg0fIDqgJD7nO1XFZ1Rsi5k");
    strcpy(randomOpaqueStr, "NnauonreTmNC2tfbKJhz6jI2ZzHlC94d");
    snprintf(requestDigestMsg, SIZE_HTTP_MSG, requestDigestAuthStr, randomNonceStr, randomOpaqueStr);
    return requestDigestMsg;
}

bool __wrap__generateRandomString(char* strToGenerate, int strSizeToGenerate) {
    memset((void*) strToGenerate, 0x00, strSizeToGenerate);
    strcpy(strToGenerate, "test");
    return true;
}

void test_checkDigestAuthMessageContent() {
    const char* str = "Authorization: Digest username=\"softathome\",realm=\"sahrealm\",nonce=\"GH7os4Ye0tbyBp9xFPHH05dm7gPRXNTrkN\",uri=\"/test\",qop=\"auth\",nc=\"00000001\",cnonce=\"daa6ceaf1b72adc3\",response=\"d43c198cb013ec7c2777aef9f8363831\",opaque=\"6SnkzxxT5niOBPu0KbgX96HwRJc1J45C\"";
    assert_true(DM_COM_CheckDigestAuthMessageContent(str));
}

void test_generateRequestDigestMsg() {
    char randomNonceStr[35], randomOpaqueStr[35];
    char* requestDigestMsg = DM_COM_GenerateRequestDigestMsg(randomNonceStr, randomOpaqueStr);
    assert_non_null(requestDigestMsg);
    assert_non_null(randomNonceStr);
    assert_non_null(randomOpaqueStr);
    DM_ENG_FREE(requestDigestMsg);
}

void test_performClientDigestAuthentication() {
    const char* username = "test";
    const char* password = "test";
    const char* realm = "sahrealm";
    const char* uri = "/test";
    const char* cnonce = "12345";
    unsigned int nc = 0x1;
    // with the test values above the response should be like this
    const char* response = "a40d2c6ee313f97fdb39d9767e61b263";

    char randomNonceStr[35], randomOpaqueStr[35];
    char* requestDigestMsg = DM_COM_GenerateRequestDigestMsg(randomNonceStr, randomOpaqueStr);

    char* str = malloc(sizeof(char) * MAX_RESPONSE_SIZE);
    sprintf(str, replyDigestAuthStr1, username, realm, uri, randomNonceStr, nc, cnonce, response, randomOpaqueStr);
    assert_true(DM_COM_PerformClientDigestAuthentication(str,
                                                         randomNonceStr,
                                                         randomOpaqueStr,
                                                         username,
                                                         password));
    DM_ENG_FREE(requestDigestMsg);
    DM_ENG_FREE(str);
}

void test_performClientDigestAuthentication_qop_quoted() {
    const char* username = "test";
    const char* password = "test";
    const char* realm = "sahrealm";
    const char* uri = "/test";
    const char* cnonce = "12345";
    unsigned int nc = 0x1;
    // with the test values above the response should be like this
    const char* response = "a40d2c6ee313f97fdb39d9767e61b263";

    char randomNonceStr[35], randomOpaqueStr[35];
    char* requestDigestMsg = DM_COM_GenerateRequestDigestMsg(randomNonceStr, randomOpaqueStr);

    char* str = malloc(sizeof(char) * MAX_RESPONSE_SIZE);
    // set qop as quoted string.
    char* reply_msg = "Digest username=\"%s\", realm=\"%s\", uri=\"%s\", nonce=\"%s\", nc=%08x, cnonce=\"%s\", qop=\"auth\", algorithm=MD5, response=\"%s\", opaque=\"%s\"";
    sprintf(str, reply_msg, username, realm, uri, randomNonceStr, nc, cnonce, response, randomOpaqueStr);
    assert_true(DM_COM_PerformClientDigestAuthentication(str,
                                                         randomNonceStr,
                                                         randomOpaqueStr,
                                                         username,
                                                         password));
    DM_ENG_FREE(requestDigestMsg);
    DM_ENG_FREE(str);
}

void test_performClientDigestAuthentication_nc_quoted() {
    const char* username = "test";
    const char* password = "test";
    const char* realm = "sahrealm";
    const char* uri = "/test";
    const char* cnonce = "12345";
    unsigned int nc = 0x1;
    // with the test values above the response should be like this
    const char* response = "a40d2c6ee313f97fdb39d9767e61b263";

    char randomNonceStr[35], randomOpaqueStr[35];
    char* requestDigestMsg = DM_COM_GenerateRequestDigestMsg(randomNonceStr, randomOpaqueStr);

    char* str = malloc(sizeof(char) * MAX_RESPONSE_SIZE);
    // set nc as quoted string.
    char* reply_msg = "Digest username=\"%s\", realm=\"%s\", uri=\"%s\", nonce=\"%s\", nc=\"%08x\", cnonce=\"%s\", qop=\"auth\", algorithm=MD5, response=\"%s\", opaque=\"%s\"";
    sprintf(str, reply_msg, username, realm, uri, randomNonceStr, nc, cnonce, response, randomOpaqueStr);
    assert_true(DM_COM_PerformClientDigestAuthentication(str,
                                                         randomNonceStr,
                                                         randomOpaqueStr,
                                                         username,
                                                         password));
    DM_ENG_FREE(requestDigestMsg);
    DM_ENG_FREE(str);
}

// test invalid random strings : nonce or opaque
void test_performClientDigestAuthentication_invalid_random_strings() {
    const char* username = "test";
    const char* password = "test";
    const char* realm = "sahrealm";
    const char* uri = "/test";
    const char* cnonce = "12345";
    unsigned int nc = 0x1;
    // with the test values above the response should be like this
    const char* response = "a40d2c6ee313f97fdb39d9767e61b263";

    char randomNonceStr[35], randomOpaqueStr[35];
    char* requestDigestMsg = DM_COM_GenerateRequestDigestMsg(randomNonceStr, randomOpaqueStr);

    char* str = malloc(sizeof(char) * MAX_RESPONSE_SIZE);
    // set nc as quoted string.
    char* reply_msg = "Digest username=\"%s\", realm=\"%s\", uri=\"%s\", nonce=\"%s\", nc=\"%08x\", cnonce=\"%s\", qop=\"auth\", algorithm=MD5, response=\"%s\", opaque=\"%s\"";
    sprintf(str, reply_msg, username, realm, uri, "test", nc, cnonce, response, "test");
    // performClientDigestAuthentication should failed
    // because Nonce and opaue strings are diferent from the generated ones
    assert_false(DM_COM_PerformClientDigestAuthentication(str,
                                                          randomNonceStr,
                                                          randomOpaqueStr,
                                                          username,
                                                          password));
    DM_ENG_FREE(requestDigestMsg);
    DM_ENG_FREE(str);
}

static char* get_response_from_str(const char* str) {
    char* firstTokenStr = NULL;
    char* secondTokenStr = NULL;
    int i = 0;

    char* ret_res = calloc(1, sizeof(char) * MAXTOKENSIZE);
    char response[MAXTOKENSIZE];
    memset((void*) response, 0x00, MAXTOKENSIZE);
    char* tmpStr = (char*) strstr(str, responseToken);
    // Find first '"'
    firstTokenStr = (char*) strchr(tmpStr, '"');
    firstTokenStr++;
    // Find the second '"'
    secondTokenStr = strchr(firstTokenStr, '"');

    while(firstTokenStr != secondTokenStr) {
        response[i] = *firstTokenStr;
        firstTokenStr++;
        i++;
    }

    strncpy(ret_res, &response[0], MAXTOKENSIZE);
    return ret_res;
}

void test_generateDigestResponse_MD5() {
    const char* username = "test";
    const char* password = "test";
    const char* method = "POST";
    const char* path = "/test";
    const char* nonce = "test";
    const char* opaque = "test";

    char* cnonce = malloc(MAXTOKENSIZE);
    _generateRandomString(cnonce, MAXTOKENSIZE);
    char* auth = malloc(sizeof(char) * MAX_RESPONSE_SIZE);
    sprintf(auth, requestDigestAuthStr, nonce, opaque);
    char* response_headers = DM_COM_GenerateDigestResponse_MD5(auth,
                                                               cnonce,
                                                               path,
                                                               method,
                                                               username,
                                                               password);

    assert_non_null(response_headers);
    // with the test values above the expected response should be like this
    const char* expected_response = "8540f55d36ddbeb53efd56b0f71fce8d";
    char* calculated_response = get_response_from_str(response_headers);
    assert_string_equal(calculated_response, expected_response);

    DM_ENG_FREE(auth);
    DM_ENG_FREE(cnonce);
    DM_ENG_FREE(response_headers);
    DM_ENG_FREE(calculated_response);
}

void test_generateDigestResponse_MD5_without_opaque() {
    const char* username = "test";
    const char* password = "test";
    const char* method = "POST";
    const char* path = "/test";

    const char* auth = "Digest realm=\"sahrealm\", qop=\"auth\", nonce=\"test\"";
    char* cnonce = malloc(MAXTOKENSIZE);
    _generateRandomString(cnonce, MAXTOKENSIZE);

    char* response_headers = DM_COM_GenerateDigestResponse_MD5(auth,
                                                               cnonce,
                                                               path,
                                                               method,
                                                               username,
                                                               password);
    assert_non_null(response_headers);
    // with the test values above the expected response should be like this
    const char* expected_response = "8540f55d36ddbeb53efd56b0f71fce8d";
    char* calculated_response = get_response_from_str(response_headers);
    assert_string_equal(calculated_response, expected_response);

    DM_ENG_FREE(cnonce);
    DM_ENG_FREE(response_headers);
    DM_ENG_FREE(calculated_response);
}

void test_generateDigestResponse_MD5_qop_list() {
    const char* username = "test";
    const char* password = "test";
    const char* method = "POST";
    const char* path = "/test";

    const char* auth = "Digest realm=\"sahrealm\", nonce=\"test\", qop=\"auth-int, auth\"";
    char* cnonce = malloc(MAXTOKENSIZE);
    _generateRandomString(cnonce, MAXTOKENSIZE);

    char* response_headers = DM_COM_GenerateDigestResponse_MD5(auth,
                                                               cnonce,
                                                               path,
                                                               method,
                                                               username,
                                                               password);
    assert_non_null(response_headers);
    // with the test values above the expected response should be like this
    const char* expected_response = "8540f55d36ddbeb53efd56b0f71fce8d";
    char* calculated_response = get_response_from_str(response_headers);
    assert_string_equal(calculated_response, expected_response);

    DM_ENG_FREE(cnonce);
    DM_ENG_FREE(response_headers);
    DM_ENG_FREE(calculated_response);
}

void test_generateDigestResponse_MD5_without_qop() {
    const char* username = "test";
    const char* password = "test";
    const char* method = "POST";
    const char* path = "/test";

    const char* auth = "Digest realm=\"sahrealm\", nonce=\"test\"";
    char* cnonce = malloc(MAXTOKENSIZE);
    _generateRandomString(cnonce, MAXTOKENSIZE);

    char* response_headers = DM_COM_GenerateDigestResponse_MD5(auth,
                                                               cnonce,
                                                               path,
                                                               method,
                                                               username,
                                                               password);
    assert_non_null(response_headers);
    // with the test values above the expected response should be like this
    // response = MD5(HA1:nonce:HA2) if qop is not specified
    const char* expected_response = "2bf578f1127e64013c7ccc5fd4766cf7";
    char* calculated_response = get_response_from_str(response_headers);
    assert_string_equal(calculated_response, expected_response);

    DM_ENG_FREE(cnonce);
    DM_ENG_FREE(response_headers);
    DM_ENG_FREE(calculated_response);
}