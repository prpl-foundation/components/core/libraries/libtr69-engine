#ifndef __TEST_DIGEST_AUTH_H__
#define __TEST_DIGEST_AUTH_H__

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <cmocka.h>

#include <dmcom/dm_com_digest.h>

char* __wrap_DM_COM_GenerateRequestDigestMsg(char* randomNonceStr, char* randomOpaqueStr);
bool __wrap__generateRandomString(char* strToGenerate, int strSizeToGenerate);
void test_checkDigestAuthMessageContent();
void test_performClientDigestAuthentication();
void test_generateRequestDigestMsg();
void test_performClientDigestAuthentication_qop_quoted();
void test_performClientDigestAuthentication_nc_quoted();
void test_performClientDigestAuthentication_invalid_random_strings();
void test_generateDigestResponse_MD5();
void test_generateDigestResponse_MD5_without_opaque();
void test_generateDigestResponse_MD5_qop_list();
void test_generateDigestResponse_MD5_without_qop();

#endif // __TEST_DIGEST_AUTH_