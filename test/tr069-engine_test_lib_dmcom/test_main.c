#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_digest_auth.h"

#include <debug/sahtrace.h>

static int test_teardown(UNUSED void** state) {
    sahTraceClose();
    return 0;
}

int main(void) {
    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "DM_COM");
    sahTraceOpen("TEST", TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO("DM_COM", "INIT TEST DM_COM");

    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_checkDigestAuthMessageContent),
        cmocka_unit_test(test_generateRequestDigestMsg),
        cmocka_unit_test(test_performClientDigestAuthentication),
        cmocka_unit_test(test_performClientDigestAuthentication_qop_quoted),
        cmocka_unit_test(test_performClientDigestAuthentication_nc_quoted),
        cmocka_unit_test(test_performClientDigestAuthentication_invalid_random_strings),
        cmocka_unit_test(test_generateDigestResponse_MD5),
        cmocka_unit_test(test_generateDigestResponse_MD5_without_opaque),
        cmocka_unit_test(test_generateDigestResponse_MD5_qop_list),
        cmocka_unit_test(test_generateDigestResponse_MD5_without_qop)
    };
    return cmocka_run_group_tests(tests, NULL, test_teardown);
}