/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_macros.h>
#include <amxut/amxut_sahtrace.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_util.h>

#include <dmengine/DM_ENG_Mapping.h>

#include "test_mapping.h"

static amxc_var_t* mapping_data = NULL;

int __wrap_DM_ENG_GetMapping(amxc_var_t* data);

int __wrap_DM_ENG_GetMapping(amxc_var_t* data) {
    amxc_var_copy(data, mapping_data);
    return 0;
}

int __wrap_DM_ENG_GetSMMMapping(char** data);

int __wrap_DM_ENG_GetSMMMapping(char** data) {
    *data = strdup("Device.X_TEST-COM_SoftwareModules");
    return 0;
}

int test_setup(UNUSED void** state) {
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "DM_ENGINE");
    sahTraceAddZone(500, "DM_COM");
    sahTraceOpen("TEST", TRACE_TYPE_STDOUT);
    mapping_data = amxut_util_read_json_from_file("./test_data/mapping.json");
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxc_var_delete(&mapping_data);
    sahTraceClose();
    return 0;
}

void test_get_dst(UNUSED void** state) {
    const char* src = NULL;
    const char* dst = NULL;

    assert_int_equal(DM_ENG_Mapping_init(), 0);

    dst = DM_ENG_Mapping_get_dst(src);
    assert_null(dst);

    src = "Device.Time.Status";
    dst = DM_ENG_Mapping_get_dst(src);
    assert_string_equal(dst, "Device.Time.Status");

    src = "Device.Hosts.Host.*.X_TEST-COM.*";
    dst = DM_ENG_Mapping_get_dst(src);
    assert_string_equal(dst, "Device.Hosts.Host.*.X_TEST-COM.");

    src = "Device.Hosts.Host.*.X_TEST-COM_*";
    dst = DM_ENG_Mapping_get_dst(src);
    assert_string_equal(dst, "Device.Hosts.Host.*.X_TEST-COM_DeviceName");

    DM_ENG_Mapping_clean();
}

void test_smm_translation(UNUSED void** state) {
    char* str = strdup("This is Device.X_TEST-COM_SoftwareModules.. Another Device.X_TEST-COM_SoftwareModules. is here. And one more Device.X_TEST-COM_SoftwareModules..");
    size_t str_len = strlen((const char*) (str));

    assert_int_equal(DM_ENG_Mapping_init(), 0);

    DM_ENG_Mapping_smm_translate(&str, &str_len, false);
    assert_string_equal(str, "This is Device.SoftwareModules.. Another Device.SoftwareModules. is here. And one more Device.SoftwareModules..");
    assert_int_equal(str_len, 111);
    DM_ENG_Mapping_smm_translate(&str, &str_len, true);
    assert_string_equal(str, "This is Device.X_TEST-COM_SoftwareModules.. Another Device.X_TEST-COM_SoftwareModules. is here. And one more Device.X_TEST-COM_SoftwareModules..");
    assert_int_equal(str_len, 144);
    free(str);
    DM_ENG_Mapping_clean();
}