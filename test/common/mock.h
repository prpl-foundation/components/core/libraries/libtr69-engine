#ifndef __COMMON_MOCK_H__
#define __COMMON_MOCK_H__

#include <../../include/dmengine/DM_ENG_EntityType.h>
#include <../../include/dmengine/DM_ENG_RPCInterface.h>

#define UNUSED __attribute__((unused))

int __wrap_DM_SendHttpMessage(const char* msgToSendStr);
int __wrap_client_startSession();
int __wrap_DM_CloseHttpSession(int closeMode);
int __wrap_DM_ENG_GetManagementServerValue(DM_ENG_EntityType entity, DM_ENG_SystemParameter_t parameter, char** pResult);
DM_ENG_NotificationStatus __wrap_DM_ENG_NotificationInterface_timerStart(const char* name, int waitTime, int intervalTime, timerHandler handler);
void __wrap_DM_ENG_SessionOpened(DM_ENG_EntityType entity);
DM_ENG_ParameterValueStruct* __wrap_DM_ENG_ParameterAttributesCacheGetForcedParams(bool updateCache, bool* parameterChanged);
DM_ENG_NotificationStatus __wrap_DM_ENG_NotificationInterface_inform(DM_ENG_DeviceIdStruct* id, DM_ENG_EventStruct* events[], DM_ENG_ParameterValueStruct* parameterList[], time_t currentTime, unsigned int retryCount);
DM_ENG_NotificationStatus __real_DM_ENG_NotificationInterface_inform(DM_ENG_DeviceIdStruct* id, DM_ENG_EventStruct* events[], DM_ENG_ParameterValueStruct* parameterList[], time_t currentTime, unsigned int retryCount);
int __wrap_DM_ENG_GetInformParameterValues(DM_ENG_EventStruct* eventList, DM_ENG_ParameterValueStruct** pvsList);

#endif // __COMMON_MOCK_H__
