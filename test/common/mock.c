#include <stdarg.h>
#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>
#include "mock.h"
#include <amxut/amxut_timer.h>
#include <debug/sahtrace.h>

char* g_randomCpeUrl = "/test";

static DM_ENG_ParameterValueStruct* build_param_struct(void) {
    DM_ENG_ParameterValueStruct* HWversion = calloc(1, sizeof(DM_ENG_ParameterValueStruct));
    DM_ENG_ParameterValueStruct* PCode = calloc(1, sizeof(DM_ENG_ParameterValueStruct));
    DM_ENG_ParameterValueStruct* SWversion = calloc(1, sizeof(DM_ENG_ParameterValueStruct));
    DM_ENG_ParameterValueStruct* IPAdd = calloc(1, sizeof(DM_ENG_ParameterValueStruct));
    DM_ENG_ParameterValueStruct* ABAdd = calloc(1, sizeof(DM_ENG_ParameterValueStruct));
    DM_ENG_ParameterValueStruct* CRURL = calloc(1, sizeof(DM_ENG_ParameterValueStruct));
    DM_ENG_ParameterValueStruct* PKey = calloc(1, sizeof(DM_ENG_ParameterValueStruct));
    DM_ENG_ParameterValueStruct* RDMversion = calloc(1, sizeof(DM_ENG_ParameterValueStruct));
    HWversion->parameterName = strdup("Device.DeviceInfo.HardwareVersion");
    HWversion->timestamp = 0;
    HWversion->type = DM_ENG_ParameterType_STRING;
    HWversion->value = strdup("1.0");
    HWversion->next = PCode;
    PCode->parameterName = strdup("Device.DeviceInfo.ProvisioningCode");
    PCode->timestamp = 0;
    PCode->type = DM_ENG_ParameterType_STRING;
    PCode->value = strdup("");
    PCode->next = SWversion;
    SWversion->parameterName = strdup("Device.DeviceInfo.SoftwareVersion");
    SWversion->timestamp = 0;
    SWversion->type = DM_ENG_ParameterType_STRING;
    SWversion->value = strdup("4.0.9.2");
    SWversion->next = IPAdd;
    IPAdd->parameterName = strdup("Device.IP.Interface.2.IPv4Address.1.IPAddress");
    IPAdd->timestamp = 0;
    IPAdd->type = DM_ENG_ParameterType_STRING;
    IPAdd->value = strdup("192.168.24.102");
    IPAdd->next = ABAdd;
    ABAdd->parameterName = strdup("Device.ManagementServer.AliasBasedAddressing");
    ABAdd->timestamp = 0;
    ABAdd->type = DM_ENG_ParameterType_BOOLEAN;
    ABAdd->value = strdup("0");
    ABAdd->next = CRURL;
    CRURL->parameterName = strdup("Device.ManagementServer.ConnectionRequestURL");
    CRURL->timestamp = 0;
    CRURL->type = DM_ENG_ParameterType_STRING;
    CRURL->value = strdup("http://ybt-fibre.internet-box.ch:4567/GykbafltJWRhfEND");
    CRURL->next = PKey;
    PKey->parameterName = strdup("Device.ManagementServer.ParameterKey");
    PKey->timestamp = 0;
    PKey->type = DM_ENG_ParameterType_STRING;
    PKey->value = strdup("3955546611");
    PKey->next = RDMversion;
    RDMversion->parameterName = strdup("Device.RootDataModelVersion");
    RDMversion->timestamp = 0;
    RDMversion->type = DM_ENG_ParameterType_STRING;
    RDMversion->value = strdup("2.14");
    RDMversion->next = NULL;

    return HWversion;
}

int __wrap_DM_SendHttpMessage(const char* msgToSendStr) {
    (void) msgToSendStr;
    return 0;
}
int __wrap_client_startSession() {
    return 0;
}
int __wrap_DM_CloseHttpSession(int closeMode) {
    (void) closeMode;
    return 0;
}
int __wrap_DM_ENG_GetManagementServerValue(DM_ENG_EntityType entity, DM_ENG_SystemParameter_t parameter, char** pResult) {
    check_expected(entity);
    check_expected(parameter);

    *pResult = mock_ptr_type(char*);

    return mock_type(int);
}

DM_ENG_NotificationStatus __wrap_DM_ENG_NotificationInterface_timerStart(const char* name, int waitTime, int intervalTime, timerHandler handler) {
    char* timer_name = strdup(name);

    amxut_timer_go_to_future_ms(waitTime);
    handler(timer_name);
    amxut_timer_go_to_future_ms(intervalTime);
    handler(timer_name);

    free(timer_name);

    return DM_ENG_SESSION_OPENING;
}


void __wrap_DM_ENG_SessionOpened(DM_ENG_EntityType entity) {
    (void) entity;
    return;
}

DM_ENG_ParameterValueStruct* __wrap_DM_ENG_ParameterAttributesCacheGetForcedParams(bool updateCache, bool* parameterChanged) {
    (void) updateCache;
    (void) parameterChanged;
    return build_param_struct();
}

DM_ENG_NotificationStatus __wrap_DM_ENG_NotificationInterface_inform(DM_ENG_DeviceIdStruct* id, DM_ENG_EventStruct* events[], DM_ENG_ParameterValueStruct* parameterList[], time_t currentTime, unsigned int retryCount) {
    //test id
    assert_true(strcmp(id->manufacturer, "WNC") == 0);
    assert_true(strcmp(id->OUI, "000B6B") == 0);
    assert_true(strcmp(id->productClass, "CR1000A") == 0);
    assert_true(strcmp(id->serialNumber, "ABV31002350") == 0);

    //test events
    assert_true(strcmp(events[0]->eventCode, "14 HEARTBEAT") == 0);
    assert_true(events[1] == NULL);

    //test parameterList
    assert_true(strcmp(parameterList[0]->parameterName, "Device.DeviceInfo.HardwareVersion") == 0);
    assert_true(strcmp(parameterList[0]->value, "1.0") == 0);
    assert_true(strcmp(parameterList[0]->next->parameterName, "Device.DeviceInfo.ProvisioningCode") == 0);
    assert_true(strcmp(parameterList[0]->next->value, "") == 0);
    assert_true(strcmp(parameterList[0]->next->next->parameterName, "Device.DeviceInfo.SoftwareVersion") == 0);
    assert_true(strcmp(parameterList[0]->next->next->value, "4.0.9.2") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->parameterName, "Device.IP.Interface.2.IPv4Address.1.IPAddress") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->value, "192.168.24.102") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->next->parameterName, "Device.ManagementServer.AliasBasedAddressing") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->next->value, "0") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->next->next->parameterName, "Device.ManagementServer.ConnectionRequestURL") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->next->next->value, "http://ybt-fibre.internet-box.ch:4567/GykbafltJWRhfEND") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->next->next->next->parameterName, "Device.ManagementServer.ParameterKey") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->next->next->next->value, "3955546611") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->next->next->next->next->parameterName, "Device.RootDataModelVersion") == 0);
    assert_true(strcmp(parameterList[0]->next->next->next->next->next->next->next->value, "2.14") == 0);

    return __real_DM_ENG_NotificationInterface_inform(id, events, parameterList, currentTime, retryCount);
}

int __wrap_DM_ENG_GetInformParameterValues(DM_ENG_EventStruct* eventList, DM_ENG_ParameterValueStruct** pvsList) {
    (void) eventList;
    (void) pvsList;
    return 0;
}
