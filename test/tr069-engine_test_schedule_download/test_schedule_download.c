/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <debug/sahtrace.h>

#include <dmengine/DM_ENG_RPC.h>
#include <dmxmlparser/ixml.h>
#include <dmcom/dm_com.h>

#include "test_schedule_download.h"

int test_setup(UNUSED void** state) {

    sahTraceSetLevel(500);
    sahTraceAddZone(500, "DM_ENGINE");
    sahTraceAddZone(500, "DM_COM");
    sahTraceOpen("TEST", TRACE_TYPE_STDOUT);
    return 0;
}

int test_teardown(UNUSED void** state) {
    sahTraceClose();
    return 0;
}

void test_schedule_download_rpc_00(UNUSED void** state) {

    int faultCode = 0;

    const char* commandKey = "";
    const char* fileType = FILETYPE_FIRMWARE_UPGRADE_IMAGE;
    const char* url = "https://ybt24.ddns.net/firmware/fakeimage.swu";
    const char* username = "";
    const char* password = "";
    uint32_t fileSize = 0;
    const char* targetFileName = "";
    TimeWindowStruct timeWindow1 = {.windowStart = 3600, .windowEnd = 4200, .windowMode = TIMEWINDOWSTRUCT_WINDOWMODE_IMMEDIATELY, .userMessage = "", .maxRetries = 0};
    TimeWindowStruct timeWindow2 = {.windowStart = 4200, .windowEnd = 5000, .windowMode = TIMEWINDOWSTRUCT_WINDOWMODE_IMMEDIATELY, .userMessage = "", .maxRetries = 0};
    TimeWindowStruct* timeWindowList[TIMEWINDOWLIST_MAX_LENGTH] = {&timeWindow1, &timeWindow2};

    faultCode = DM_ENG_RPC_ScheduleDownload(commandKey, fileType, url, username, password,
                                            fileSize, targetFileName, timeWindowList);

    assert_int_equal(faultCode, 0);
}

IXML_Document* test_soap_document_init(const char* xmlFile);
IXML_Document* test_soap_document_init(const char* xmlFile) {
    return ixmlLoadDocument(xmlFile);
}

void test_soap_document_clean(IXML_Document* doc);
void test_soap_document_clean(IXML_Document* doc) {
    ixmlDocument_free(doc);
}

GenericXmlNodePtr test_soap_msg_body(IXML_Document* doc);
GenericXmlNodePtr test_soap_msg_header(IXML_Document* doc);
const char* test_soap_msg_header_id(IXML_Document* doc);
GenericXmlNodePtr test_soap_msg_body_scheduleDownload(IXML_Document* doc);

GenericXmlNodePtr test_soap_msg_body(IXML_Document* doc) {
    return xmlGetFirstNodeWithTagName((GenericXmlDocumentPtr) doc, "soapenv:Body");
}

GenericXmlNodePtr test_soap_msg_header(IXML_Document* doc) {
    return xmlGetFirstNodeWithTagName((GenericXmlDocumentPtr) doc, "soapenv:Header");
}

const char* test_soap_msg_header_id(IXML_Document* doc) {
    const char* id = NULL;
    GenericXmlNodePtr headerNode = test_soap_msg_header(doc);
    xmlGetNodeParameters(xmlGetFirstNodeWithTagName(xmlNodeToDocument(headerNode), "cwmp:ID"), NULL, &id);
    return id;
}

GenericXmlNodePtr test_soap_msg_body_scheduleDownload(IXML_Document* doc) {
    GenericXmlNodePtr bodyNode = test_soap_msg_body(doc);
    return xmlGetFirstNodeWithTagName(xmlNodeToDocument(bodyNode), "cwmp:ScheduleDownload");
}

void test_schedule_download_rpc_01(UNUSED void** state) {
    const char* xmlFile = "files/valid_ScheduleDownload_00.xml";
    int retval = 0;

    IXML_Document* doc = test_soap_document_init(xmlFile);
    GenericXmlNodePtr scheduleDownloadNode = test_soap_msg_body_scheduleDownload(doc);
    retval = DM_SUB_ScheduleDownload(scheduleDownloadNode);
    test_soap_document_clean(doc);

    assert_int_equal(retval, 0);
}

void test_schedule_download_rpc_02(UNUSED void** state) {
    const char* xmlFile = "files/valid_ScheduleDownload_02.xml";
    int retval = 0;

    IXML_Document* doc = test_soap_document_init(xmlFile);
    GenericXmlNodePtr scheduleDownloadNode = test_soap_msg_body_scheduleDownload(doc);
    retval = DM_SUB_ScheduleDownload(scheduleDownloadNode);
    test_soap_document_clean(doc);

    assert_int_equal(retval, 0);
}
