#ifndef __TEST_LIB_DMXMLPARSER_H__
#define __TEST_LIB_DMXMLPARSER_H__

#include "dmxmlparser/ixml.h"

void test_lib_dmxmlparser_success();
void test_lib_dmxmlparser_failed();
void test_lib_dmxmlparser_syntax_error();

#endif // __TEST_LIB_DMXMLPARSER_H__