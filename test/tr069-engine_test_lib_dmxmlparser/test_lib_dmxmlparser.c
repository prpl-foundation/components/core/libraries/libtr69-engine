#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <ctype.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "test_lib_dmxmlparser.h"

void test_lib_dmxmlparser_success() {
    int rc;
    IXML_Document* doc = NULL;
    DOMString s;

    // test parse document OK (success).
    const char* xmlFile = "testdata/ixml_success.xml";

    rc = ixmlLoadDocumentEx(xmlFile, &doc);
    assert_int_equal(rc, IXML_SUCCESS);

    // test print document
    s = ixmlPrintDocument(doc);
    bool isEmpty = s[0] == '\0';
    assert_non_null(s);
    assert_false(isEmpty);

    ixmlFreeDOMString(s);
    ixmlDocument_free(doc);
}

void test_lib_dmxmlparser_failed() {
    int rc;
    IXML_Document* doc = NULL;

    // parse failed.
    const char* xmlFile = "testdata/ixml_failed.xml";
    rc = ixmlLoadDocumentEx(xmlFile, &doc);
    assert_int_equal(rc, IXML_FAILED);

    ixmlDocument_free(doc);
}

void test_lib_dmxmlparser_syntax_error() {
    int rc;
    IXML_Document* doc = NULL;

    // parse document with syntax error.
    const char* xmlFile = "testdata/ixml_syntax_error.xml";
    rc = ixmlLoadDocumentEx(xmlFile, &doc);
    assert_int_equal(rc, IXML_SYNTAX_ERR);

    ixmlDocument_free(doc);
}