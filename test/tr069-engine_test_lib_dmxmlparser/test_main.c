#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_lib_dmxmlparser.h"

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_lib_dmxmlparser_success),
        cmocka_unit_test(test_lib_dmxmlparser_syntax_error),
        cmocka_unit_test(test_lib_dmxmlparser_failed)
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}