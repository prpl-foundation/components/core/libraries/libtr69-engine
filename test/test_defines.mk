MACHINE = $(shell $(CC) -dumpmachine)

COMSRCDIR = $(realpath ../../dmcom/src)
COMMONSRCDIR = $(realpath ../../dmcommon/src)
ENGINESRCDIR = $(realpath ../../dmengine/src)
XMLSRCDIR = $(realpath ../../dmxmlparser/src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIRS = -I../../dmengine/include -I../../dmcommon/include -I../../dmcom/include -I../../dmxmlparser/include \
		  $(if $(STAGINGDIR), -I$(STAGINGDIR)/include) \
		  $(if $(STAGINGDIR), -I$(STAGINGDIR)/usr/include)
MOCK_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIRS)/*.h)
SOURCES = $(wildcard $(COMSRCDIR)/*.c)
SOURCES += $(wildcard $(COMMONSRCDIR)/*.c)
SOURCES += $(wildcard $(ENGINESRCDIR)/*.c)
SOURCES += $(wildcard $(XMLSRCDIR)/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

COMCFLAGS += -Wall -Wextra -Wno-strict-aliasing \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -fPIC -g3 -I$(INCDIRS) -D_GNU_SOURCE \
		  -Wredundant-decls -DSAHTRACES_ENABLED \
		  -DSAHTRACES_LEVEL_DEFAULT=700 \
		  $(shell pkg-config --cflags libxml-2.0) \
		  $(shell pkg-config --cflags cmocka) -pthread

ENGINECFLAGS += -Wall -Wextra -Wno-strict-aliasing \
		  -fPIC -g3 -I$(INCDIRS) -D_GNU_SOURCE \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wredundant-decls -DSAHTRACES_ENABLED \
		  -DSAHTRACES_LEVEL_DEFAULT=700 \
		  $(shell pkg-config --cflags libxml-2.0) \
		  $(shell pkg-config --cflags cmocka) -pthread

XMLCFLAGS = -DSAHTRACES_ENABLED -Wall -Wextra -Werror \
		  -Wredundant-decls -fPIC -g3 -I$(INCDIRS) \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread

ifeq ($(CC_NAME),g++)
	COMCFLAGS += -std=c++2a
	ENGINECFLAGS += -std=c++2a
else
	COMCFLAGS += -std=c11
	ENGINECFLAGS += -std=c11
endif


CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIRS)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
		  $(shell pkg-config --cflags libxml-2.0) \
		  $(shell pkg-config --cflags cmocka) -pthread


LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -DUNIT_TEST \
		   -lamxc -lamxp -lamxd -lamxo -lamxb -lamxut -lamxj \
		   -ldl -lpthread -lsahtrace -lxml2

WRAP_FUNC=-Wl,--wrap=
MOCK_WRAP = DM_SendHttpMessage \
			client_startSession \
			DM_CloseHttpSession \
			DM_ENG_GetManagementServerValue \
			DM_ENG_NotificationInterface_timerStart \
			DM_ENG_SessionOpened \
			DM_ENG_ParameterAttributesCacheGetForcedParams \
			DM_ENG_NotificationInterface_inform \
			DM_ENG_GetInformParameterValues

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
