/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_SubscriptionStruct.h
 *
 * Created     : 15/03/2024
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_SubscriptionStruct.h
 *
 * @brief Data on subscriptions.
 *
 */

#ifndef _DM_ENG_SUBSCRIPTION_STRUCT_H_
#define _DM_ENG_SUBSCRIPTION_STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <dmengine/DM_ENG_Common.h>

/**
 * Data structure representing a scheduled transfer request
 */
typedef struct DM_ENG_SubscriptionStruct {
    /** path of subscription */
    char* path;
    /** type of notification: Forced, Active, Passive */
    char* type;
    struct DM_ENG_SubscriptionStruct* next;

} __attribute ((packed)) DM_ENG_SubscriptionStruct;

void DM_ENG_deleteSubscriptionStruct(DM_ENG_SubscriptionStruct* ss);
DM_ENG_SubscriptionStruct* DM_ENG_newSubscriptionStruct(const char* path, const char* type);
void DM_ENG_addSubscriptionStruct(DM_ENG_SubscriptionStruct** pAqts, DM_ENG_SubscriptionStruct* newAqts);

#ifdef __cplusplus
}
#endif

#endif
