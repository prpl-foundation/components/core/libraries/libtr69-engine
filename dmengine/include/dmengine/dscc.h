#ifndef _DSCC_H_
#define _DSCC_H_

#include <amxc/amxc.h>

typedef struct _dscc_t {
    amxc_llist_it_t it;
    char* path;
    char* commandKey;
    amxc_llist_t results;
} dscc_t;

typedef struct _dscc_result_t {
    char* uuid;
    char* deploymentUnitRef;
    char* version;
    char* currentState;
    bool resolved;
    char* executionUnitRefList;
    time_t startTime;
    time_t completeTime;
    uint32_t faultCode;
    char* faultString;
    amxc_llist_it_t it;
} dscc_result_t;


dscc_t* dscc_new(amxc_var_t* results, const char* commandKey, const char* path);
void dscc_delete(dscc_t** entry);
void dscc_print(amxc_llist_t* data);
#endif /* _DSCC_H_ */