/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_RPCInterface.h
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_RPCInterface.h
 *
 * @brief The RPC Interface.
 *
 * This header file defines the RPC Interface which provides similar functions to the TR-069 RPC methods, plus
 * somme functions for the notification by Inform messages and for the session management.
 *
 */
#ifndef _DM_ENG_RPC_INTERFACE_H_
#define _DM_ENG_RPC_INTERFACE_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <dmengine/DM_ENG_NotificationInterface.h>
#include <dmengine/DM_ENG_EntityType.h>
#include <dmengine/DM_ENG_ParameterInfoStruct.h>
#include <dmengine/DM_ENG_ParameterAttributesStruct.h>
#include <dmengine/DM_ENG_ParameterValueStruct.h>
#include <dmengine/DM_ENG_DeviceIdStruct.h>
#include <dmengine/DM_ENG_EventStruct.h>
#include <dmengine/DM_ENG_SetParameterValuesFault.h>
#include <dmengine/DM_ENG_AllQueuedTransferStruct.h>
#include <dmengine/DM_ENG_SubscriptionStruct.h>
#include <dmengine/DM_ENG_ScheduleInformStruct.h>
#include <dmengine/DM_ENG_NotificationMode.h>

#include <dmengine/DM_ENG_TransferResultStruct.h>
#include <dmengine/DM_ENG_Error.h>
#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>


#define IPPING_DIAGNOSTIC_REQUEST     (1 << 0)
#define TRACEROUTE_DIAGNOSTIC_REQUEST (1 << 1)
#define DOWNLOAD_DIAGNOSTIC_REQUEST   (1 << 2)
#define UPLOAD_DIAGNOSTIC_REQUEST     (1 << 3)

typedef enum {
    // tr98 managementserver parameters
    DM_ENG_USERNAME,               //
    DM_ENG_PASSWORD,               //
    DM_ENG_URL,                    //
    DM_ENG_ACSIPAFFINITY,          //
    DM_ENG_ACSIPTTL,               //
    DM_ENG_ACSADDRFAMILY,
    DM_ENG_PERIODICINFORMINTERVAL, //
    DM_ENG_PERIODICINFORMTIME,     //
    DM_ENG_PERIODICINFORMENABLE,   //
    DM_ENG_PERIODICINFORMCOUNTER,  //
    DM_ENG_INFORMFAILURECOUNT,     //
    DM_ENG_PARAMETERKEY,           //
    DM_ENG_ENABLECWMP,
    DM_ENG_DEFAULTACTIVENOTIFICATIONTHROTTLE,
    DM_ENG_CONNECTIONREQUESTUSERNAME, //
    DM_ENG_CONNECTIONREQUESTURL,
    DM_ENG_CONNECTIONREQUESTPASSWORD, //
    DM_ENG_MANAGEABLEDEVICENOTIFICATIONLIMIT,
    DM_ENG_INSTANCEWILDCARDSSUPPORTED,
    DM_ENG_REFUSEBASICAUTHENTICATION,

    // other system parameters
    DM_ENG_HEARTBEATENABLE,
    DM_ENG_HEARTBEATINITIATIONTIME,
    DM_ENG_HEARTBEATREPORTINGINTERVAL,
    DM_ENG_CONNECTIONREQUESTHOST, //
    DM_ENG_CONNECTIONREQUESTPORT, //
    DM_ENG_CONNECTIONREQUESTPATH, //
    DM_ENG_CONNECTIONREQUESTPATHTYPE,
    DM_ENG_MAXCONNECTIONREQUEST,
    DM_ENG_FREQCONNECTIONREQUEST,
    DM_ENG_SESSIONTIMEOUT,    //
    DM_ENG_MAXSTARTUPDELAY,   //
    DM_ENG_REBOOTCOMMANDKEY,
    DM_ENG_FACTORYRESETOCCURRED,
    DM_ENG_BOOTSTRAPSENT,
    DM_ENG_SESSIONSTATUS,
    DM_ENG_REBOOTBYACS,
    DM_ENG_LASTSESSION,
    DM_ENG_SSLACCEPTSELFSIGNED,
    DM_ENG_SSLVERIFYHOSTNAME,
    DM_ENG_SSLACCEPTEXPIRED,
    DM_ENG_SSLVERIFYPARTIALCHAIN,
    DM_ENG_VERIFYSUPPORTEDACSMETHODS,
    DM_ENG_INTERFACE,
    DM_ENG_SESSIONSSINCEBOOT,
    DM_ENG_MAXDOWNLOADDELAY,
    DM_ENG_MAXUPLOADDELAY,
    DM_ENG_BOOTPERSISTENTSCHEDULEINFORM,
    DM_ENG_ALLOWMULTIPLESCHEDULEINFORM,
    DM_ENG_ALWAYSEMPTYPOSTATENDOFSESSION,
    DM_ENG_ACSIPLIST,
    DM_ENG_ACSIP,
    DM_ENG_ALLOWCONNECTIONREQUESTFROMUNKNOWNHOST,
    DM_ENG_ALLOWCONNECTIONREQUESTFROMADDRESS,
    DM_ENG_GETPARAMETERVALUEREQUESTS,
    DM_ENG_SMM_MAPPING,
    DM_ENG_MAXDOWNLOADS,
    DM_ENG_UPGRADESAVAILABLE,
    DM_ENG_UPGRADEBOOTDELAY,
    DM_ENG_ACSEVENTS,
    DM_ENG_DELIVEREDEVENTS,
    DM_ENG_BLOCKEDEVENTS,
    DM_ENG_MAXDOWNLOADSERRORCODE,
    DM_ENG_VERIFYPARAMETERTYPE,
    DM_ENG_ACCEPTUNSIGNEDINTASBOOLEAN,
    DM_ENG_DISABLEBOOTINFORM,
    DM_ENG_ALIASBASEDADDRESSING,
    DM_ENG_INSTANCEMODE,
    DM_ENG_AUTOCREATEINSTANCES,
    DM_ENG_LOCALIPADDRESS,
    DM_ENG_INHIBIT_VALUE_CHANGE_UPON_BOOT,
    DM_ENG_IPV4IPV6WANMODE,
    //tr98 device info parameters
    DM_ENG_MANUFACTURER,
    DM_ENG_MANUFACTUREROUI,
    DM_ENG_SERIALNUMBER,
    DM_ENG_PRODUCTCLASS,
    DM_ENG_NTPSTATUS,

    //internal cwmp settings
    DM_ENG_DATAMODEL,
    DM_ENG_DATAMODELURL,
    DM_ENG_PERSISTENTRPCPATH,
    DM_ENG_TIMEPLUGINPATH
} DM_ENG_SystemParameter_t;

typedef enum {
    DM_ENG_UPDATEACSIP
} DM_ENG_SystemFunction_t;

int DM_ENG_GetRPCMethods(DM_ENG_EntityType entity, OUT const char** pMethods[]);
int DM_ENG_SetParameterValues(DM_ENG_EntityType entity, DM_ENG_ParameterValueStruct* parameterList[], const char* parameterKey, OUT DM_ENG_ParameterStatus* pStatus, OUT DM_ENG_SetParameterValuesFault** pFaults[]);
int DM_ENG_GetParameterValues(DM_ENG_EntityType entity, char* parameterNames[], OUT DM_ENG_ParameterValueStruct** pResult[]);
int DM_ENG_GetParameterValuesAll(xmlNodePtr node_body);
int DM_ENG_GetParameterAttributesAll(xmlNodePtr node_body);
int DM_ENG_GetParameterNames(DM_ENG_EntityType entity, const char* path, bool nextLevel, OUT DM_ENG_ParameterInfoStruct** pResult[]);
int DM_ENG_AddObject(DM_ENG_EntityType entity, const char* objectName, const char* parameterKey, OUT unsigned int* pInstanceNumber, OUT DM_ENG_ParameterStatus* pStatus);
int DM_ENG_DeleteObject(DM_ENG_EntityType entity, const char* objectName, const char* parameterKey, OUT DM_ENG_ParameterStatus* pStatus);
int DM_ENG_SetParameterAttributes(DM_ENG_EntityType entity, DM_ENG_ParameterAttributesStruct* pasList[]);
int DM_ENG_GetParameterAttributes(DM_ENG_EntityType entity, char* names[], OUT DM_ENG_ParameterAttributesStruct** pResult[]);
int DM_ENG_ScheduleInform(DM_ENG_EntityType entity, unsigned int delay, const char* commandKey);
int DM_ENG_RequestConnection(DM_ENG_EntityType entity);
int DM_ENG_Download(DM_ENG_EntityType entity, char* fileType, char* url, char* username, char* password, unsigned int fileSize, char* targetFileName,
                    unsigned int delay, char* successURL, char* failureURL, char* commandKey, OUT DM_ENG_TransferResultStruct** pResult);
int DM_ENG_Upload(DM_ENG_EntityType entity, char* fileType, char* url, char* username, char* password, unsigned int delay, char* commandKey, OUT DM_ENG_TransferResultStruct** pResult);
int DM_ENG_Reboot(DM_ENG_EntityType entity, char* commandKey);
int DM_ENG_FactoryReset(DM_ENG_EntityType entity);
int DM_ENG_GetAllQueuedTransfers(DM_ENG_EntityType entity, OUT DM_ENG_AllQueuedTransferStruct** pResult[]);

int DM_ENG_DataModelConnect(const char* rpcPath, const char* aclfile, void** systemCtx, void** acsCtx, const char* vendorPrefix, const char* backend, const char* uri);

int DM_ENG_ActivateNotification(DM_ENG_EntityType entity,
                                DM_ENG_F_INFORM inform,
                                DM_ENG_F_TRANSFER_COMPLETE transferComplete,
                                DM_ENG_F_REQUEST_DOWNLOAD requestDownload,
                                DM_ENG_F_GETRPCMETHODS getRPCMethods,
                                DM_ENG_F_TIMER_START timerStart,
                                DM_ENG_F_TIMER_STOP timerStop,
                                DM_ENG_F_TIMER_TIME_REMAINING timerTimeRemaining,
                                DM_ENG_F_EVENT engineEvent,
                                DM_ENG_F_DU_STATE_CHANGE_COMPLETE duStateChangeComplete);

void DM_ENG_DeactivateNotification(DM_ENG_EntityType entity);
void DM_ENG_SessionOpened(DM_ENG_EntityType entity);
bool DM_ENG_IsReadyToClose(DM_ENG_EntityType entity);
void DM_ENG_SessionClosed(DM_ENG_EntityType entity, bool success);

int DM_ENG_GetManagementServerValue(DM_ENG_EntityType entity, DM_ENG_SystemParameter_t parameter, char** pResult);
int DM_ENG_SetManagementServerValue(DM_ENG_EntityType entity, DM_ENG_SystemParameter_t parameter, const char* pValue);
int DM_ENG_ExecuteManagementServerFunction(DM_ENG_EntityType entity, DM_ENG_SystemFunction_t function);
int DM_ENG_GetForcedParameterValues(DM_ENG_ParameterValueStruct** pvsList);
int DM_ENG_GetInformParameterValues(DM_ENG_EventStruct* list, DM_ENG_ParameterValueStruct** pvsList);
bool DM_ENG_TransferCompleteResponseArrived(DM_ENG_EntityType entity);
bool DM_ENG_DownloadRequestResponseArrived(DM_ENG_EntityType entity);
bool DM_ENG_InformResponseArrived(DM_ENG_EntityType entity);
int DM_ENG_TransferDoneAcknowledged(DM_ENG_EntityType entity, char* uniqueid);
void DM_ENG_TR098SetExternalIPAddressPath(char* externalIPAddressPath);
void DM_ENG_TR181SetExternalIPAddressPath(char* externalIPAddressPath);
void DM_ENG_ScheduleInformConfirmedDelivery(int uniqueID);
const char* DM_ENG_CurrentIGDExternalIPAddressPath();
bool DM_ENG_isDiagnosticRequestPending(unsigned int diagnosticRequest);
void DM_ENG_setPendingDiagnosticRequest(unsigned int diagnosticRequest);
void DM_ENG_clearPendingDiagnosticRequest(unsigned int diagnosticRequest);
bool DM_ENG_DUStateChangeCompleteResponseArrived(DM_ENG_EntityType entity);
int DM_ENG_DUStateChangeCompleteAcknowledged(DM_ENG_EntityType entity, const char* path);
int DM_ENG_GetMapping(amxc_var_t* data);
int DM_ENG_GetSMMMapping(char** data);
int DM_ENG_UpdateQosInfo(bool enable, const char* dstIP, int32_t dstPort, int32_t dstIPVersion);

#ifdef __cplusplus
}
#endif

#endif
