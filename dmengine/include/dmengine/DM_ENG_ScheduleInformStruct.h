/*---------------------------------------------------------------------------
 *---------------------------------------------------------------------------
 * File        : DM_ENG_ScheduleInformStruct.h
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_ScheduleInformStruct.h
 *
 * @brief Definition of the schedule inform struct
 *
 **/

#ifndef _DM_ENG_SCHEDULEINFORM_STRUCT_H_
#define _DM_ENG_SCHEDULEINFORM_STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <time.h>

/**
 * schedule inform structure
 */
typedef struct _DM_ENG_ScheduleInformStruct {
    /** The UTC time whe the inform should be send out */
    time_t time;
    /** The corresponding command key */
    char* commandKey;
    int uniqueID;
    struct _DM_ENG_ScheduleInformStruct* next;

} __attribute ((packed)) DM_ENG_ScheduleInformStruct;

DM_ENG_ScheduleInformStruct* DM_ENG_newScheduleInformStruct(time_t time, const char* ck, int uniqueID);
void DM_ENG_deleteScheduleInformStruct(DM_ENG_ScheduleInformStruct* is);
void DM_ENG_deleteAllScheduleInformStruct(DM_ENG_ScheduleInformStruct** pIs);
void DM_ENG_addScheduleInformStruct(DM_ENG_ScheduleInformStruct** pIs, DM_ENG_ScheduleInformStruct* is);
void DM_ENG_deleteScheduleInformStructUsingUniqueID(DM_ENG_ScheduleInformStruct** pIs, int uniqueID);
DM_ENG_ScheduleInformStruct* DM_ENG_takeScheduleInformStructUsingCommandKey(DM_ENG_ScheduleInformStruct** pIs, const char* commandKey);

#ifdef __cplusplus
}
#endif

#endif
