
#include <dmengine/DM_ENG_NotificationMode.h>
#include <dmengine/DM_ENG_ParameterAttributesStruct.h>
#include <dmengine/DM_ENG_ParameterValueStruct.h>
#include <dmcommon/CMN_Type_Def.h>
#include <amxc/amxc_htable.h>

typedef struct _cache_dict_entry_t {
    amxc_htable_it_t hit;
    DM_ENG_ParameterAttributesStruct* pas;
} cache_dict_entry_t;

void DM_ENG_CacheDict_Build(amxc_htable_t* dict);
void DM_ENG_CacheDict_Destroy(amxc_htable_t* dict);
int DM_ENG_CacheDict_GetInfo(amxc_htable_t* dict, const char* name, DM_ENG_NotificationMode* notificationMode, char** accessList[]);

/*
 * Add a new parameter to the parameter attribute list
 * If the ellement already exists, overwrite the existing ellement
 */
int DM_ENG_AddParameterAttributesCacheEllement(const char* name, DM_ENG_NotificationMode notificationMode, char* accessList[]);


/*
 * Go over the cache:
 * - if ellement found return the custom notificationmode and accesslist,
 * - if ellement not found, return the default notificationmode and accesslist
 */
int DM_ENG_GetParameterAttributesCacheEllement(const char* name, DM_ENG_NotificationMode* notificationMode, char** accessList[]);

/*
 * Go over the cache:
 * - if ellement found return the custom notificationmode, path and accesslist,
 * - if ellement not found, return the default notificationmode and accesslist
 */
char* DM_ENG_GetParameterAttributesCacheEllementPath(int id);

/*
 * Go over the cache and remove the specified element, event if it's a forced parameter
 * - if ellement found return 0
 * - if ellement not found -1
 */
int DM_ENG_RemoveAttributesCacheEllement(const char* name, bool allowRemoveForcedParameter);

/*
 * Go over the cache, and check if the ellement still exists in the datamodel
 */
int DM_ENG_VerifyParameterAttributesCache();

/*
 * Sync te parameter attribute cache parameter values
 */
int DM_ENG_SyncParameterAttributesCache();


/*
 * Initialize the parameter Cache
 */
int DM_ENG_InitializeParameterAttributesCache(DM_ENG_ParameterAttributesStruct** acacheArray);

/*
 * Cleanup the parameter Cache
 */
int DM_ENG_CleanupParameterAttributesCache();

/*
 * Display the parameter Cache
 */
void DM_ENG_DisplayParameterAttributesCache();

/*
 * Add a cached value
 */
void DM_ENG_AddCachedValueToParameterAttributesCache(const char* paramName, const char* paramValue);


/*
 * Check if the attributes cache contains a cached parameter value that is equal to the one
 * we're getting from a notification. In case we find the parameter, we clear the value of the
 * attributescache.
 *
 * @return true if the value was in the cache (e.g. this value was set by the ACS)
 *         false if the value was not in the cache (e.g. this value was not set by the ACS)
 */
bool DM_ENG_ValueWasCachedInParameterAttributesCache(const char* paramName, const char* paramValue);

/*
 *  Get the changed passive parameters
 */
DM_ENG_ParameterValueStruct* DM_ENG_ParameterAttributesCacheGetChangedPassiveParams();

/*
 *  Get the forced parameters
 */
DM_ENG_ParameterValueStruct* DM_ENG_ParameterAttributesCacheGetForcedParams(bool updateCache, bool* parameterChanged);

/*
 *  Get the changed Active parameters
 */
DM_ENG_ParameterValueStruct* DM_ENG_ParameterAttributesCacheGetChangedActiveParams();








