/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_Device.h
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_Device.h
 *
 * @brief Low API from the DM Engine to the Device Adapter
 *
 **/

#ifndef _DDM_ENG_DEVICE_H_
#define _DDM_ENG_DEVICE_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <dmengine/DM_ENG_Common.h>
#include <dmengine/DM_ENG_ParameterValueStruct.h>
#include <dmengine/DM_ENG_ParameterInfoStruct.h>
#include <dmengine/DM_ENG_ParameterAttributesStruct.h>
#include <dmengine/DM_ENG_SetParameterValuesFault.h>
#include <dmengine/DM_ENG_RPCInterface.h>
#include <dmengine/DM_ENG_Type.h>
#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

bool DM_ENG_connectionRequestAllowed();
bool DM_ENG_getRpcMethodSupportedOnDevice();
bool DM_ENG_setParameterValuesSupportedOnDevice();
bool DM_ENG_getParameterValuesSupportedOnDevice();
bool DM_ENG_setParameterAttributesSupportedOnDevice();
bool DM_ENG_getParameterAttributesSupportedOnDevice();
bool DM_ENG_getParameterNamesSupportedOnDevice();
bool DM_ENG_addObjectSupportedOnDevice();
bool DM_ENG_deleteObjectSupportedOnDevice();
bool DM_ENG_rebootSupportedOnDevice();
bool DM_ENG_downloadSupportedOnDevice();
bool DM_ENG_scheduleDownloadSupportedOnDevice();
bool DM_ENG_changeDUStateSupportedOnDevice();
bool DM_ENG_factoryResetSupportedOnDevice();
bool DM_ENG_scheduleInformSupportedOnDevice();
bool DM_ENG_uploadSupportedOnDevice();
bool DM_ENG_getQueuedTransfersSupportedOnDevice();
bool DM_ENG_setVouchersSupportedOnDevice();
bool DM_ENG_getOptionsSupportedOnDevice();
bool DM_ENG_getAllQueuedTransfersSupportedOnDevice();

const char* DM_ENG_getDatamodelPrefix();


typedef struct {
    /**
     * Performs the necessary initializations of the device adapter if any, when starting the DM Agent.
     *
     * @param systemCrx Returns the amxb bus context system subscriptions
     * @param acsCtx Returns the amxb bus context used for the ACS subscriptions
     *
     * @return false if error
     */
    bool (* DM_ENG_Device_Init)(void** systemCrx, void** acsCtx, const char* rpcPath, const char* aclfile, const char* vendorPrefix, const char* backend, const char* uri);
    /**
     * Performs the necessary operations when stopping the DM Agent.
     */
    bool (* DM_ENG_Device_Release)();

    /**
     * Notify the Device Adapter of the beginning of a session with the ACS. It allows to perform the necessary initializations.
     */
    void (* DM_ENG_Device_OpenSession)();

    /**
     * Notify the Device Adapter of the end of the session with the ACS.
     */
    void (* DM_ENG_Device_CloseSession)();

    /**
     * This function may be used by the ACS to obtain the value of one or more CPE parameters.
     *
     * @param parameterNames Array of strings, each representing the name of the requested parameter.
     * @param Pointer to the array of the ParameterValueStruct.
     *
     * @return 0 if OK, else an error code (9001, ..) as specified in TR-069
     */
    int (* DM_ENG_Device_GetParameterValues)(char**, OUT DM_ENG_ParameterValueStruct**);

    /**
     * This function is used to obtain the value of one or more CPE inform parameters, based on the incoming events.
     *
     * @param eventList Pointer to the array of the EventStruct.
     * @param pvsList Pointer to the array of the ParameterValueStruct.
     *
     * @return 0 if OK, else an error code (9001, ..) as specified in TR-069
     */
    int (* DM_ENG_Device_GetInformParameterValues)(DM_ENG_EventStruct*, OUT DM_ENG_ParameterValueStruct**);

    /**
     * Sets the values of the system parameters.
     *
     * @param parameterList Array of name-value pairs as specified. For each name-value pair, the CPE is instructed
     * to set the Parameter specified by the name to the corresponding value.
     *
     * @param parameterKey The value to set the ParameterKey parameter. This MAY be used by the server to identify
     * Parameter updates, or left empty.
     *
     * @param pStatus Pointer to get the status defined as follows :
     * - DM_ENG_ParameterStatus_APPLIED (=0) : Parameter changes have been validated and applied.
     * - DM_ENG_ParameterStatus_READY_TO_APPLY (=1) : Parameter changes have been validated and committed, but not yet applied
     * (for example, if a reboot is required before the new values are applied).
     * - DM_ENG_ParameterStatus_UNDEFINED : Undefined status (for example, parameterList is empty).
     *
     *  Note : The result is DM_ENG_ParameterStatus_APPLIED if all the parameter changes are APPLIED.
     * The result is DM_ENG_ParameterStatus_READY_TO_APPLY if one or more parameter changes is READY_TO_APPLY.
     *
     * @param pFaults Pointer to get the faults struct if any, as specified in TR-069
     *
     * @return 0 if OK, else an error code (9001, ..) as specified in TR-069
     */
    int (* DM_ENG_Device_SetParameterValues)(DM_ENG_ParameterValueStruct**, const char*, OUT DM_ENG_ParameterStatus*, OUT DM_ENG_SetParameterValuesFault**);

    /**
     * Gets all the names of parameters of a sub-tree defined by the given object.
     *
     * @param path Parameter path
     * @param nextLevel If true, the response must only contains the parameters in the next level. If false, the response must also
     * contains the parameter below.
     * @param pResult Pointer to the array of the ParameterInfoStruct.
     *
     * @return 0 if OK, else an error code (9001, ..) as specified in TR-069
     */
    int (* DM_ENG_Device_GetParameterNames)(const char*, bool, OUT DM_ENG_ParameterInfoStruct**);

    /**
     * Creates a new instance for the object and returns the instance number.
     *
     * @param objectName Name of an object prototype
     * @param parameterKey Key parameter value
     * @param pInstanceNumber Instance number that was created
     * @param pStatus Status of the new parameter
     *
     * @return 0 if OK, else an error code (9001, ..) as specified in TR-069
     */
    int (* DM_ENG_Device_AddObject)(const char*, const char*, OUT unsigned int*, OUT DM_ENG_ParameterStatus*);

    /**
     * Deletes an instance of an object.
     *
     * @param objectName Name of an object prototype
     * @param parameterKey Parameter key of the object
     * @param pStatus The status of the resulting object
     *
     * @return 0 if OK, else an error code (9001, ..) as specified in TR-069
     */
    int (* DM_ENG_Device_DeleteObject)(const char*, const char*, DM_ENG_ParameterStatus*);

    /**
     * Commands the device to reboot.
     *
     */
    void (* DM_ENG_Device_Reboot)();

    /**
     * Commands the device to do a factory reset.
     *
     */
    void (* DM_ENG_Device_FactoryReset)();

    /**
     * Commands the device to download a file.
     *
     * @param commandkey The command key
     * @param fileType A string as defined in TR-069
     * @param url URL specifying the source file location
     * @param username Username to be used by the CPE to authenticate with the file server
     * @param password Password to be used by the CPE to authenticate with the file server
     * @param fileSize Size of the file to be downloaded in bytes
     * @param targetFileName Name of the file to be used on the target file system
     * @param delayseconds Delay in seconds before starting the download
     * @param successURL URL the CPE may redirect the user's browser to if the download completes successfully
     * @param failureURL URL the CPE may redirect the user's browser to if the download does not complete successfully
     *
     * @return 0 if the download is carried out synchronously, 1 if asynchronously, else an error code (9001, ..) as specified in TR-069
     */
    int (* DM_ENG_Device_Download)(char*, char*, char*, char*, char*, unsigned int, char*, unsigned int, char*, char*);

    /**
     * Commands the device to upload a file.
     *
     * @param commandkey The command key
     * @param fileType A string as defined in TR-069
     * @param url URL specifying the destination file location
     * @param username Username to be used by the CPE to authenticate with the file server
     * @param password Password to be used by the CPE to authenticate with the file server
     * @param delayseconds Delay in seconds before starting the download
     *
     * @return 0 if the upload id carried out synchronously, 1 if asynchronously, else an error code (9001, ..) as specified in TR-069
     */
    int (* DM_ENG_Device_Upload)(char*, char*, char*, char*, char*, unsigned int);

    /**
     * Save the engine configuration: reboot command key, attribute cache.
     *
     * @param acacheArray The array containing all acache entries that need to be saved
     * @param is Linked list containing all scheduleinform entries that were undelivered
     *
     * @ return 0 if save was succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_SaveConfig)(DM_ENG_ParameterAttributesStruct* acacheArray[], DM_ENG_ScheduleInformStruct* is);

    /**
     * Load the saved engine parameters: reboot command key,  attribute cache.
     *
     * @param acacheArray The array containing all acache entries that need to be loaded
     * @param is Linked list containing all scheduleinform entries that were undelivered
     *
     * @ return 0 if save was succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_LoadConfig)(DM_ENG_ScheduleInformStruct** is);
    /**
     * Subscribe for parameter updates.
     *
     * @param subscriptionPath The path of the parameter we want to subscribe to
     *
     * @ return 0 if succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_AddNotification)(const char* subscriptionPath, int* subscriptionID, DM_ENG_NotificationMode mode);

    /**
     * Unsubscribe for parameter updates.
     *
     * @param subscriptionPath The path of the parameter we want to unsubscribe
     *
     * @ return 0 if succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_RemoveNotification)(const char* subscriptionPath, int subscriptionID);

    /**
     * Set the accessrights for the specified path.
     *
     * @param accessrightsPath The path of the parameter we want to unsubscribe
     *
     * @ return 0 if succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_SetAccessRights)(const char* accessrightsPath, char* al[]);

    /**
     * Set a configuration value
     *
     * @param parameter The parameter of interest
     * @param pValue The new value of this parameter
     *
     * @ return 0 if succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_SetConfigValue)(DM_ENG_SystemParameter_t parameter, const char* pValue);

    /**
     * Get a configuration value
     *
     * @param parameter The parameter of interest
     * @param pValue The value of this parameter
     *
     * @ return 0 if succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_GetConfigValue)(DM_ENG_SystemParameter_t parameter, char** pResult);

    /**

     * Execute a configuration function
     *
     * @param function The function of interest
     *
     * @ return 0 if succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_ExecuteConfigFunction)(DM_ENG_SystemFunction_t function);

    /**
     * Get a list of queued transfers
     *
     * @param pResult The queued transfer result structure
     *
     * @return 0 if succesfull, else an error code (9001, ..) as specified in TR-069
     */
    int (* DM_ENG_Device_GetAllQueuedTransfers)(DM_ENG_AllQueuedTransferStruct** pResult[]);

    /**
     * Called when a transfer complete message has been acknowledged by the acs
     *
     * @param uniqueID The unique id identifying this download
     *
     * @return 0 if succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_TransferDoneAcknowledged)(char* uniqueID);

    /**
     * Get a list of subscriptions
     *
     * @param pResult The subscriptions result structure
     *
     * @return 0 if succesfull, -1 if an error occurred
     */
    int (* DM_ENG_Device_GetSubscriptions)(DM_ENG_SubscriptionStruct** pResult);

    /**
     * @brief used to download a specified file from the designated location and apply it within either one or two specified time windows
     *
     * @param CommandKey The string the CPE uses to refer to a particular download
     * @param FileType An integer followed by a space followed by the file type description.
     * @param URL URL specifying the source file location
     * @param Username Username to be used by the CPE to authenticate with the file server.
     * @param Password Password to be used by the CPE to authenticate with the file server.
     * @param FileSize The size of the file to be downloaded in bytes.
     * @param TargetFileName The name of the file to be used on the target file system
     * @param TimeWindowList defines the time window(s) during which the CPE MUST perform and apply the download.
     *
     * @return 0 on success or an fault code (9000,9001, ..) as specified in TR-069 on failure
     */

    int (* DM_ENG_Device_ScheduleDownload)(const char* CommandKey,
                                           const char* FileType,
                                           const char* URL,
                                           const char* Username,
                                           const char* Password,
                                           uint32_t FileSize,
                                           const char* TargetFileName,
                                           TimeWindowStruct* TimeWindowList[TIMEWINDOWLIST_MAX_LENGTH]);


    //TODO: add description
    int (* DM_ENG_Device_ChangeDUState)(OperationStruct* operations, const char* commandKey);
    int (* DM_ENG_Device_DUStateChangeCompleteAcknowledged)(const char* path);
    int (* DM_ENG_Device_GetParameterValuesAll)(xmlNodePtr node_body);
    int (* DM_ENG_Device_GetMapping)(amxc_var_t* data);
    int (* DM_ENG_Device_GetParameterAttributesAll)(xmlNodePtr node_body);
    int (* DM_ENG_Device_Set)(const char* object, amxc_var_t* values, amxc_var_t* ret);

} device_adapter_t;

bool DM_ENG_Device_Load(const char* device_adapter_library);
void DM_ENG_Device_Unload();
void DM_ENG_setDatamodelPrefix(const char* prefix);

#ifdef __cplusplus
}
#endif

#endif
