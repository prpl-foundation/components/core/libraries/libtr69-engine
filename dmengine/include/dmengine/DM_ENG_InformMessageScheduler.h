/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_InformMessageScheduler.h
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_InformMessageScheduler.h
 *
 * @brief
 *
 **/


#ifndef _DM_ENG_INFORM_MESSAGE_SCHEDULER_H_
#define _DM_ENG_INFORM_MESSAGE_SCHEDULER_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <dmengine/DM_ENG_Common.h>
#include <dmengine/DM_ENG_Global.h>
#include <dmengine/DM_ENG_ParameterValueStruct.h>
#include <dmengine/DM_ENG_NotificationMode.h>
#include <dmengine/DM_ENG_TransferCompleteStruct.h>
#include <dmengine/DM_ENG_EventStruct.h>
#include <dmengine/DM_ENG_DownloadRequest.h>
#include <dmengine/DM_ENG_Type.h>
#include <amxc/amxc.h>

void DM_ENG_InformMessageScheduler_init(DM_ENG_EventStruct* eventList);
void DM_ENG_InformMessageScheduler_cleanup();
void DM_ENG_InformMessageScheduler_bootstrapInform();
int DM_ENG_InformMessageScheduler_scheduleInform(unsigned int delay, const char* commandKey, int uniqueID);
void DM_ENG_InformMessageScheduler_requestConnection();
void DM_ENG_InformMessageScheduler_sessionClosed(bool success);
void DM_ENG_InformMessageScheduler_parameterValueChanged(DM_ENG_ParameterValueStruct* param, DM_ENG_NotificationMode mode);
void DM_ENG_InformMessageScheduler_diagnosticsComplete();
void DM_ENG_InformMessageScheduler_transferComplete(DM_ENG_TransferCompleteStruct* tcs);
void DM_ENG_InformMessageScheduler_requestDownload(DM_ENG_DownloadRequest* dr);
void DM_ENG_InformMessageScheduler_vendorSpecificEvent(bool active, const char* eventCode);
void DM_ENG_InformMessageScheduler_blockedEventsChanged();
bool DM_ENG_InformMessageScheduler_isReadyToClose();
void DM_ENG_InformMessageScheduler_initializePeriodicInform();
bool DM_ENG_InformMessageScheduler_transferCompleteResponseArrived();
bool DM_ENG_InformMessageScheduler_informResponseArrived();
void DM_ENG_InformMessageScheduler_sendMessage(char* name);
bool DM_ENG_InformMessageScheduler_RequestDownloadResponseArrived();
void DM_ENG_InformMessageScheduler_removeForcedParametersFromSession();
bool DM_ENG_InformMessageScheduler_getRPCMethodsResponseArrived(char* methods[]);
void DM_ENG_InformMessageScheduler_disableSendInform();
void DM_ENG_InformMessageScheduler_time_synchronized_notification();
bool DM_ENG_InformMessageScheduler_containsEvent(DM_ENG_EventStruct* list, const char* eventCode, const char* commandKey);
void DM_ENG_InformMessageScheduler_Heartbeat_configure();
int DM_ENG_InformMessageScheduler_SendTransferComplete(bool autonomous, const char* announceURL, const char* transferURL,
                                                       const char* fileType, unsigned int fileSize, const char* targetFileName,
                                                       bool isDownload, bool isScheduleDownload,
                                                       const char* commandKey, uint32_t faultCode, const char* faultString,
                                                       time_t startTime, time_t completeTime, const char* path);
int DM_ENG_InformMessageScheduler_SendEvents(const char* events, bool immediately, const char* source);
int DM_ENG_InformMessageScheduler_SendDUStateChangeComplete(amxc_var_t* results, const char* commandKey, const char* path);
bool DM_ENG_InformMessageScheduler_DUStateChangeCompleteResponseArrived();

#ifdef __cplusplus
}
#endif

#endif