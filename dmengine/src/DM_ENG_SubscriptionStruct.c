/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_SubscriptionStruct.c
 *
 * Created     : 15/03/2024
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_SubscriptionStruct.c
 *
 * @brief Data on subscriptions.
 *
 **/

#include <dmengine/DM_ENG_SubscriptionStruct.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>


void DM_ENG_deleteSubscriptionStruct(DM_ENG_SubscriptionStruct* ss) {
    if(ss != NULL) {
        free(ss->path);
        free(ss->type);
        free(ss);
    }
}

DM_ENG_SubscriptionStruct* DM_ENG_newSubscriptionStruct(const char* path, const char* type) {
    DM_ENG_SubscriptionStruct* sub = (DM_ENG_SubscriptionStruct*) malloc(sizeof(DM_ENG_SubscriptionStruct));
    if(sub != NULL) {
        sub->path = (path == NULL ? NULL : strdup(path));
        sub->type = (type == NULL ? NULL : strdup(type));
        sub->next = NULL;
    }
    return sub;
}

/**
 * Appends a new DM_ENG_SubscriptionStruct object to the list
 *
 * @param pAqts Pointer to the first pointer of the list.
 * @param newAqts A pointer to a DM_ENG_SubscriptionStruct object
 *
 */
void DM_ENG_addSubscriptionStruct(DM_ENG_SubscriptionStruct** pAqts, DM_ENG_SubscriptionStruct* newAqts) {
    DM_ENG_SubscriptionStruct* aqts = *pAqts;
    if(aqts == NULL) {
        *pAqts = newAqts;
    } else {
        while(aqts->next != NULL) {
            aqts = aqts->next;
        }
        aqts->next = newAqts;
    }
}
