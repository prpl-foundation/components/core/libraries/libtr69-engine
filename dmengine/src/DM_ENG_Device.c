/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_Device.c
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_Device.c
 *
 * @brief link to bus adapter
 *
 **/
#include <dmengine/DM_ENG_Common.h>
#include <dmcom/dm_com.h>           /* DM_COM definition					*/
#include <dmengine/DM_ENG_Device.h>
#include <dmcommon/DM_GlobalDefs.h>

#include <debug/sahtrace.h>
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>


static void* dl;
device_adapter_t* device_adapter;

static const char* datamodel_prefix = NULL;

const char* DM_ENG_getDatamodelPrefix() {
    char* val = NULL;
    if(!datamodel_prefix) {
        if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_DATAMODEL, &val) != 0) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Could not get prefix, defaulting to IGD");
            datamodel_prefix = "Device.";
        } else {
            if(( strcmp(val, "TR098") == 0)) {
                datamodel_prefix = "InternetGatewayDevice.";
            } else {
                datamodel_prefix = "Device.";
            }
            free(val);
        }
    }
    return datamodel_prefix;
}
void DM_ENG_setDatamodelPrefix(const char* prefix) {
    datamodel_prefix = prefix;
}

bool DM_ENG_Device_Load(const char* device_adapter_library) {
    device_adapter = (device_adapter_t*) calloc(1, sizeof(device_adapter_t));
    if(device_adapter == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot allocate memory");
        goto error;
    }

    dl = dlopen(device_adapter_library, RTLD_NOW);
    if(dl == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to open shared lib %s(%s)", device_adapter_library, dlerror());
        goto error;
    }
    dlerror();

    device_adapter->DM_ENG_Device_Init = (bool (*)(void**, void**, const char*, const char*, const char*, const char*, const char*))dlsym(dl, "DM_ENG_Device_Init");
    if(device_adapter->DM_ENG_Device_Init == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }

    dlerror();
    device_adapter->DM_ENG_Device_Release = (bool (*)())dlsym(dl, "DM_ENG_Device_Release");
    if(device_adapter->DM_ENG_Device_Release == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_OpenSession = (void (*)())dlsym(dl, "DM_ENG_Device_OpenSession");
    if(device_adapter->DM_ENG_Device_OpenSession == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_CloseSession = (void (*)())dlsym(dl, "DM_ENG_Device_CloseSession");
    if(device_adapter->DM_ENG_Device_CloseSession == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_GetParameterValues = (int (*)(char**, OUT DM_ENG_ParameterValueStruct**))dlsym(dl, "DM_ENG_Device_GetParameterValues");
    if(device_adapter->DM_ENG_Device_GetParameterValues == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_GetInformParameterValues = (int (*)(DM_ENG_EventStruct*, OUT DM_ENG_ParameterValueStruct**))dlsym(dl, "DM_ENG_Device_GetInformParameterValues");
    if(device_adapter->DM_ENG_Device_GetInformParameterValues == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_SetParameterValues = (int (*)(DM_ENG_ParameterValueStruct**, const char*, OUT DM_ENG_ParameterStatus*, OUT DM_ENG_SetParameterValuesFault**))dlsym(dl, "DM_ENG_Device_SetParameterValues");
    if(device_adapter->DM_ENG_Device_SetParameterValues == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_GetParameterNames = (int (*)(const char*, bool, OUT DM_ENG_ParameterInfoStruct**))dlsym(dl, "DM_ENG_Device_GetParameterNames");
    if(device_adapter->DM_ENG_Device_GetParameterNames == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_AddObject = (int (*)(const char*, const char*, OUT unsigned int*, OUT DM_ENG_ParameterStatus*))dlsym(dl, "DM_ENG_Device_AddObject");
    if(device_adapter->DM_ENG_Device_AddObject == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_DeleteObject = (int (*)(const char*, const char*, OUT DM_ENG_ParameterStatus*))dlsym(dl, "DM_ENG_Device_DeleteObject");
    if(device_adapter->DM_ENG_Device_DeleteObject == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_Reboot = (void (*)())dlsym(dl, "DM_ENG_Device_Reboot");
    if(device_adapter->DM_ENG_Device_Reboot == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_FactoryReset = (void (*)())dlsym(dl, "DM_ENG_Device_FactoryReset");
    if(device_adapter->DM_ENG_Device_FactoryReset == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_Download = (int (*)(char*, char*, char*, char*, char*, unsigned int, char*, unsigned int, char*, char*))dlsym(dl, "DM_ENG_Device_Download");
    if(device_adapter->DM_ENG_Device_Download == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_Upload = (int (*)(char*, char*, char*, char*, char*, unsigned int))dlsym(dl, "DM_ENG_Device_Upload");
    if(device_adapter->DM_ENG_Device_Upload == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_LoadConfig = (int (*)(DM_ENG_ScheduleInformStruct**))dlsym(dl, "DM_ENG_Device_LoadConfig");
    if(device_adapter->DM_ENG_Device_LoadConfig == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_AddNotification = (int (*)(const char*, int*, DM_ENG_NotificationMode))dlsym(dl, "DM_ENG_Device_AddNotification");
    if(device_adapter->DM_ENG_Device_AddNotification == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_RemoveNotification = (int (*)(const char*, int))dlsym(dl, "DM_ENG_Device_RemoveNotification");
    if(device_adapter->DM_ENG_Device_RemoveNotification == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_SetAccessRights = (int (*)(const char*, char**))dlsym(dl, "DM_ENG_Device_SetAccessRights");
    if(device_adapter->DM_ENG_Device_SetAccessRights == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_SetConfigValue = (int (*)(DM_ENG_SystemParameter_t, const char*))dlsym(dl, "DM_ENG_Device_SetConfigValue");
    if(device_adapter->DM_ENG_Device_SetConfigValue == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_GetConfigValue = (int (*)(DM_ENG_SystemParameter_t, char**))dlsym(dl, "DM_ENG_Device_GetConfigValue");
    if(device_adapter->DM_ENG_Device_GetConfigValue == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_ExecuteConfigFunction = (int (*)(DM_ENG_SystemFunction_t))dlsym(dl, "DM_ENG_Device_ExecuteConfigFunction");
    if(device_adapter->DM_ENG_Device_ExecuteConfigFunction == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        goto error;
    }
    dlerror();
    device_adapter->DM_ENG_Device_GetAllQueuedTransfers = (int (*)(DM_ENG_AllQueuedTransferStruct***))dlsym(dl, "DM_ENG_Device_GetAllQueuedTransfers");
    if(device_adapter->DM_ENG_Device_GetAllQueuedTransfers == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_GetSubscriptions = (int (*)(DM_ENG_SubscriptionStruct**))dlsym(dl, "DM_ENG_Device_GetSubscriptions");
    if(device_adapter->DM_ENG_Device_GetSubscriptions == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_ScheduleDownload = (int (*)(const char*, const char*, const char*, const char*, const char*, uint32_t, const char*, TimeWindowStruct**))dlsym(dl, "DM_ENG_Device_ScheduleDownload");
    if(device_adapter->DM_ENG_Device_ScheduleDownload == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_ChangeDUState = (int (*)(OperationStruct*, const char*))dlsym(dl, "DM_ENG_Device_ChangeDUState");
    if(device_adapter->DM_ENG_Device_ChangeDUState == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();
    device_adapter->DM_ENG_Device_TransferDoneAcknowledged = (int (*)(char*))dlsym(dl, "DM_ENG_Device_TransferDoneAcknowledged");
    if(device_adapter->DM_ENG_Device_TransferDoneAcknowledged == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();

    device_adapter->DM_ENG_Device_DUStateChangeCompleteAcknowledged = (int (*)(const char*))dlsym(dl, "DM_ENG_Device_DUStateChangeCompleteAcknowledged");
    if(device_adapter->DM_ENG_Device_DUStateChangeCompleteAcknowledged == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();

    device_adapter->DM_ENG_Device_GetParameterValuesAll = (int (*)(xmlNodePtr))dlsym(dl, "DM_ENG_Device_GetParameterValuesAll");
    if(device_adapter->DM_ENG_Device_GetParameterValuesAll == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();

    device_adapter->DM_ENG_Device_GetMapping = (int (*)(amxc_var_t*))dlsym(dl, "DM_ENG_Device_GetMapping");
    if(device_adapter->DM_ENG_Device_GetMapping == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();

    device_adapter->DM_ENG_Device_GetParameterAttributesAll = (int (*)(xmlNodePtr))dlsym(dl, "DM_ENG_Device_GetParameterAttributesAll");
    if(device_adapter->DM_ENG_Device_GetParameterAttributesAll == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();

    device_adapter->DM_ENG_Device_Set = (int (*)(const char*, amxc_var_t*, amxc_var_t*))dlsym(dl, "DM_ENG_Device_Set");
    if(device_adapter->DM_ENG_Device_Set == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "unable to fetch symbol: %s", dlerror());
        // not REQUIRED
    }
    dlerror();

    return true;
error:
    SAH_TRACEZ_ERROR("DM_ENGINE", "Error loading the plugin");
    free(device_adapter);
    if(dl) {
        dlclose(dl);
    }
    return false;
}

void DM_ENG_Device_Unload() {

    if(device_adapter->DM_ENG_Device_Release() == false) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not release device");
    }
    free(device_adapter);
    dlclose(dl);
    return;
}

bool DM_ENG_connectionRequestAllowed() {
    return _CONNECTION_REQUEST_SUPPORTED;
}

bool DM_ENG_getRpcMethodSupportedOnDevice() {
    return _GETRPCMETHODS_RPC_SUPPORTED;
}

bool DM_ENG_setParameterValuesSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_SetParameterValues != NULL);
}

bool DM_ENG_getParameterValuesSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_GetParameterValues != NULL);
}

bool DM_ENG_setParameterAttributesSupportedOnDevice() {
    return true;
}

bool DM_ENG_getParameterAttributesSupportedOnDevice() {
    return true;
}

bool DM_ENG_getParameterNamesSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_GetParameterNames != NULL);
}

bool DM_ENG_addObjectSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_AddObject != NULL);
}

bool DM_ENG_deleteObjectSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_DeleteObject != NULL);
}

bool DM_ENG_rebootSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_Reboot != NULL);
}

bool DM_ENG_downloadSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_Download != NULL);
}

bool DM_ENG_scheduleDownloadSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_ScheduleDownload != NULL);
}

bool DM_ENG_changeDUStateSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_ChangeDUState != NULL);
}

bool DM_ENG_factoryResetSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_FactoryReset != NULL);
}

bool DM_ENG_uploadSupportedOnDevice() {
    return (device_adapter->DM_ENG_Device_Upload != NULL);
}

bool DM_ENG_scheduleInformSupportedOnDevice() {
    return _SCHEDULEINFORM_RPC_SUPPORTED;
}

bool DM_ENG_getQueuedTransfersSupportedOnDevice() {
    return _GETQUEUEDTRANSFERS_RPC_SUPPORTED;
}

bool DM_ENG_getAllQueuedTransfersSupportedOnDevice() {
    return _GETALLQUEUEDTRANSFERS_RPC_SUPPORTED;
}

bool DM_ENG_setVouchersSupportedOnDevice() {
    return _SETVOUCHERS_RPC_SUPPORTED;
}

bool DM_ENG_getOptionsSupportedOnDevice() {
    return _GETOPTIONS_RPC_SUPPORTED;
}


