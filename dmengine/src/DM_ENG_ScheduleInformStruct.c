/*---------------------------------------------------------------------------
 *---------------------------------------------------------------------------
 * File        : DM_ENG_ScheduleInformStruct.c
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_ScheduleInformStruct.c
 *
 * @brief Definition of the event which must be delivered to the ACS
 *
 **/

#include <dmengine/DM_ENG_ScheduleInformStruct.h>
#include <dmengine/DM_ENG_Common.h>
#include <string.h>
#include <debug/sahtrace.h>


/**
 * Creates a new event struct
 *
 * @param time The time in UTC time when to send the event
 * @param ck A command key
 * @param uniqueID AA unique ID, if 0 the unique id is automatically generated
 *
 * @return A pointer to the struct newly created
 */
DM_ENG_ScheduleInformStruct* DM_ENG_newScheduleInformStruct(time_t time, const char* ck, int uniqueID) {
    static int uID = 1;
    DM_ENG_ScheduleInformStruct* res = (DM_ENG_ScheduleInformStruct*) malloc(sizeof(DM_ENG_ScheduleInformStruct));
    res->time = time;
    res->commandKey = (ck == NULL ? NULL : strdup(ck));
    res->next = NULL;
    if(uniqueID != 0) {
        res->uniqueID = uniqueID;
    } else {
        res->uniqueID = uID;
    }
    uID++;
    return res;
}

/**
 * Deletes the struct
 *
 * @param event An event struct
 */
void DM_ENG_deleteScheduleInformStruct(DM_ENG_ScheduleInformStruct* is) {
    free(is->commandKey);
    free(is);
}

/**
 * Deletes all the struct in the list
 *
 * @param pEvent A pointer to the firt struct element in the list
 */
void DM_ENG_deleteAllScheduleInformStruct(DM_ENG_ScheduleInformStruct** pIs) {
    DM_ENG_ScheduleInformStruct* is = *pIs;
    while(is != NULL) {
        DM_ENG_ScheduleInformStruct* isv = is;
        is = is->next;
        DM_ENG_deleteScheduleInformStruct(isv);
    }
    *pIs = NULL;
}

/**
 * Adds an element to the list of EventStruct
 *
 * @param pIs Pointer to the first element
 * @param is EventStruct to add
 */
void DM_ENG_addScheduleInformStruct(DM_ENG_ScheduleInformStruct** pIs, DM_ENG_ScheduleInformStruct* is) {
    DM_ENG_ScheduleInformStruct* ptr = *pIs;

    SAH_TRACEZ_INFO("DM_ENGINE", "Adding Schedule inform to the event list");

    if(ptr == NULL) {
        //no ellement in array,
        SAH_TRACEZ_INFO("DM_ENGINE", "No ellement in array");
        *pIs = is;
    } else {
        if(is->time < ptr->time) {
            // we should replace the first element in the array
            SAH_TRACEZ_INFO("DM_ENGINE", "Replace firs element in array");
            is->next = *pIs;
            *pIs = is;
        } else {
            while(ptr->next != NULL) {
                if(is->time < ptr->next->time) {
                    //inserting the element
                    is->next = ptr->next;
                    ptr->next = is;
                    return;
                }
                ptr = ptr->next;
            }
            // appending at the end
            ptr->next = is;
            SAH_TRACEZ_INFO("DM_ENGINE", "Appending at the end");
        }
    }
}

void DM_ENG_deleteScheduleInformStructUsingUniqueID(DM_ENG_ScheduleInformStruct** pIs, int uniqueID) {
    DM_ENG_ScheduleInformStruct* is = *pIs;
    DM_ENG_ScheduleInformStruct* prev = NULL;
    if(is == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Trying to remove schedule inform struct with uniqueID %d that does not exist", uniqueID);
        return;
    } else {
        while(is != NULL) {
            if(is->uniqueID == uniqueID) {
                //remove it
                if(prev) {
                    prev->next = is->next;
                    DM_ENG_deleteScheduleInformStruct(is);
                } else {
                    // it's the first element
                    DM_ENG_ScheduleInformStruct* isv = *pIs;
                    *pIs = (*pIs)->next;
                    DM_ENG_deleteScheduleInformStruct(isv);
                }
                return;
            }
            prev = is;
            is = is->next;
        }
        SAH_TRACEZ_ERROR("DM_ENGINE", "Trying to remove schedule inform struct with uniqueID %d that does not exist", uniqueID);
    }
}

DM_ENG_ScheduleInformStruct* DM_ENG_takeScheduleInformStructUsingCommandKey(DM_ENG_ScheduleInformStruct** pIs, const char* commandKey) {
    DM_ENG_ScheduleInformStruct* is = *pIs;
    DM_ENG_ScheduleInformStruct* prev = NULL;
    if(( is == NULL) || ( commandKey == NULL)) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Trying to take a schedule inform struct with commandKey %s that does not exist", commandKey);
        return NULL;
    } else {
        //   SAH_TRACEZ_INFO("DM_ENGINE","Looping trough list is=%p",is);
        while(is != NULL) {
            if(is->commandKey && ( strcmp(is->commandKey, commandKey) == 0)) {
                //remove it
                if(prev) {
                    prev->next = is->next;
                    is->next = NULL;
                    return is;
                } else {
                    // it's the first element
                    DM_ENG_ScheduleInformStruct* isv = *pIs;
                    *pIs = (*pIs)->next;
                    isv->next = NULL;
                    return isv;
                }
            }
            prev = is;
            is = is->next;
        }
        SAH_TRACEZ_ERROR("DM_ENGINE", "Trying to take a schedule inform struct with commandKey %s that does not exist", commandKey);
    }
    return NULL;
}
