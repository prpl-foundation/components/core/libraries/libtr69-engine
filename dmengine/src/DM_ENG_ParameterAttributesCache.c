
#include <dmengine/DM_ENG_ParameterAttributesCache.h>
#include <dmengine/DM_ENG_ParameterAttributesStruct.h>
#include <dmengine/DM_ENG_Common.h>
#include <dmengine/DM_ENG_Device.h>
#include <string.h>

#include <debug/sahtrace.h>
/*
 * TODO:
 * The cache is currently implemented as a sorted linked list. This is not optimal for performance,
 * and should be changed to a more efficient (hash?) in the future
 */

static DM_ENG_ParameterAttributesStruct* acache = NULL;
extern device_adapter_t* device_adapter;

static void DM_ENG_locateParameterAttributesCacheEllement(const char* name, DM_ENG_ParameterAttributesStruct** cur, DM_ENG_ParameterAttributesStruct** prev) {
    *cur = acache;
    *prev = NULL;

    while(*cur) {
        if((*cur)->parameterName && name && ( strcmp((*cur)->parameterName, name) >= 0)) {
            break;
        }
        *prev = *cur;
        *cur = (*cur)->next;
    }

}

int DM_ENG_RemoveAttributesCacheEllement(const char* name, bool allowRemoveForcedParameter) {
    SAH_TRACEZ_INFO("DM_ENGINE", "Removing parameter attribute %s (allowRemoveForcedParameter=%d)", name, allowRemoveForcedParameter);

    if(( acache == NULL) || ( name == NULL)) {
        return -1;
    }
    DM_ENG_ParameterAttributesStruct* cur = NULL;
    DM_ENG_ParameterAttributesStruct* prev = NULL;

    DM_ENG_locateParameterAttributesCacheEllement(name, &cur, &prev);

    if(( cur != NULL) && ( strcmp(cur->parameterName, name) == 0)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Parameter found, removing it");
        if(( cur->notification == DM_ENG_NotificationMode_FORCED) && ( allowRemoveForcedParameter == false)) {
            return -1;
        }
        //remove from list
        if(cur == acache) {
            acache = cur->next;
        } else {
            prev->next = cur->next;
        }
        cur->next = NULL;

        //remove the subscription
        if(( cur->notificationID != 0) && device_adapter->DM_ENG_Device_RemoveNotification) {
            device_adapter->DM_ENG_Device_RemoveNotification(cur->parameterName, cur->notificationID);
        }

        //remove the item
        DM_ENG_deleteParameterAttributesStruct(cur);
    } else {
        SAH_TRACEZ_INFO("DM_ENGINE", "Parameter not found");
        return -1;
    }

    return 0;
}

int DM_ENG_AddParameterAttributesCacheEllement(const char* name, DM_ENG_NotificationMode notificationMode, char* accessList[]) {
    int notificationID = 0;
    char* value = NULL;

    SAH_TRACEZ_INFO("DM_ENGINE", "Adding parameter attribute %s, mode %d", name, notificationMode);

    if(name == NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Parameter name = NULL, not adding the value to the attributes cache");
        return -1;
    }

    //for passive notifications we need to load the cachedValue
    //for active notification we need to load the cachedvalue
    if(( notificationMode == DM_ENG_NotificationMode_PASSIVE) || ( notificationMode == DM_ENG_NotificationMode_ACTIVE)) {
        DM_ENG_ParameterValueStruct** pResult = NULL;
        char* paramArray[2];
        paramArray[0] = name;
        paramArray[1] = NULL;
        // get the new value of the parameter
        if(DM_ENG_GetParameterValues(DM_ENG_EntityType_SYSTEM, (char**) paramArray, &pResult) == 0) {
            if(pResult) {
                value = (pResult[0]->value) ? strdup(pResult[0]->value) : NULL;
                DM_ENG_deleteAllParameterValueStruct(pResult);
                free(pResult);
            }
        } else {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Could not get parameter value for parameter %s, not adding it to the cache", name);
            return -1;
        }
    }

    if(acache == NULL) {
        acache = DM_ENG_newParameterAttributesStruct(name, notificationMode, accessList);
        acache->notificationID = 0;
        if(device_adapter->DM_ENG_Device_AddNotification) {
            device_adapter->DM_ENG_Device_AddNotification(name, &notificationID, notificationMode);
        }
        if(( notificationMode == DM_ENG_NotificationMode_ACTIVE) || ( notificationMode == DM_ENG_NotificationMode_FORCED)) {
            acache->notificationID = notificationID;
        }
        if(accessList != NULL) {
            if(device_adapter->DM_ENG_Device_SetAccessRights) {
                device_adapter->DM_ENG_Device_SetAccessRights(name, accessList);
            }
        }
        acache->cachedValue = value;
        return 0;
    }
    DM_ENG_ParameterAttributesStruct* cur = NULL;
    DM_ENG_ParameterAttributesStruct* prev = NULL;

    DM_ENG_locateParameterAttributesCacheEllement(name, &cur, &prev);

    if(( cur != NULL) && ( strcmp(cur->parameterName, name) == 0)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Parameter found, updating it");
        // we found the element, update it

        if(cur->notification != DM_ENG_NotificationMode_UNDEFINED) {  //if undefined, we leave the current notification mode as is
            if(cur->notification != DM_ENG_NotificationMode_FORCED) { // we only modify non-forced notifications
                // subscribe only to active notifications
                if(device_adapter->DM_ENG_Device_AddNotification) {
                    device_adapter->DM_ENG_Device_AddNotification(name, &notificationID, notificationMode);
                }
                if(( cur->notification == DM_ENG_NotificationMode_ACTIVE) && ( notificationMode != DM_ENG_NotificationMode_ACTIVE)) {
                    cur->notificationID = 0;
                }
                if(( cur->notification != DM_ENG_NotificationMode_ACTIVE) && ( notificationMode == DM_ENG_NotificationMode_ACTIVE)) {
                    cur->notificationID = notificationID;
                }
                cur->notification = notificationMode;
            }
        }
        if(accessList != NULL) {
            if(cur->accessList != NULL) {
                DM_ENG_deleteTabString(cur->accessList);
            }
            cur->accessList = DM_ENG_copyTabString(accessList);
        }
        if(( notificationMode == DM_ENG_NotificationMode_ACTIVE) && ( value != NULL) && ( *value == 0)) {
            free(cur->cachedValue);
            cur->cachedValue = strdup("NULL");
            free(value);
        } else {
            free(cur->cachedValue);
            cur->cachedValue = value;
        }
    } else {
        SAH_TRACEZ_INFO("DM_ENGINE", "Parameter not found, adding it to list");
        // location where to insert the element is reached
        DM_ENG_ParameterAttributesStruct* pas = DM_ENG_newParameterAttributesStruct(name, notificationMode, accessList);
        if(prev == NULL) {
            acache = pas;
        } else {
            prev->next = pas;
        }
        pas->next = cur;

        if(device_adapter->DM_ENG_Device_AddNotification) {
            device_adapter->DM_ENG_Device_AddNotification(name, &notificationID, notificationMode);
        }
        if(( notificationMode == DM_ENG_NotificationMode_ACTIVE) || ( notificationMode == DM_ENG_NotificationMode_FORCED)) {
            pas->notificationID = notificationID;
        }
        if(accessList != NULL) {
            if(device_adapter->DM_ENG_Device_SetAccessRights) {
                device_adapter->DM_ENG_Device_SetAccessRights(name, accessList);
            }
        }
        if(( notificationMode == DM_ENG_NotificationMode_ACTIVE) && ( value != NULL) && ( *value == 0)) {
            pas->cachedValue = strdup("NULL");
            free(value);
        } else {
            pas->cachedValue = value;
        }
    }

    return 0;
}

int DM_ENG_GetParameterAttributesCacheEllement(const char* name, DM_ENG_NotificationMode* notificationMode, char** accessList[]) {
    DM_ENG_ParameterAttributesStruct* cur = NULL;
    DM_ENG_ParameterAttributesStruct* prev = NULL;

    if(name == NULL) {
        *notificationMode = DM_ENG_NotificationMode_OFF;
        *accessList = NULL;
        return 0;
    }

    DM_ENG_locateParameterAttributesCacheEllement(name, &cur, &prev);
    if(( cur != NULL) && ( strcmp(cur->parameterName, name) == 0)) {
        // element found
        *notificationMode = cur->notification;
        *accessList = cur->accessList;
    } else {
        // element not found, return defaults
        *notificationMode = DM_ENG_NotificationMode_OFF;
        *accessList = NULL;
    }
    return 0;
}

char* DM_ENG_GetParameterAttributesCacheEllementPath(int id) {
    DM_ENG_ParameterAttributesStruct* cur = acache;

    while(cur) {
        if(cur->notificationID == id) {
            return strdup(cur->parameterName);
        }
        cur = cur->next;
    }

    return NULL;
}

int DM_ENG_VerifyParameterAttributesCache() {
    if(device_adapter->DM_ENG_Device_GetParameterNames) {
        DM_ENG_ParameterAttributesStruct* cache = acache;
        DM_ENG_ParameterAttributesStruct* prev = NULL;
        DM_ENG_ParameterInfoStruct* infoList = NULL;
        while(cache) {
            if(device_adapter->DM_ENG_Device_GetParameterNames(cache->parameterName, false, &infoList) != 0) {
                // there is something wrong with this parameter, remove it from list
                if(prev == NULL) {
                    acache = cache->next;
                    DM_ENG_deleteParameterAttributesStruct(cache);
                    cache = acache;
                } else {
                    prev->next = cache->next;
                    DM_ENG_deleteParameterAttributesStruct(cache);
                    cache = prev->next;
                }

            } else {
                prev = cache;
                cache = cache->next;
            }
            DM_ENG_deleteAllParameterInfoStruct(&infoList);
        }
    }

    return 0;
}

int DM_ENG_SyncParameterAttributesCache() {
    DM_ENG_ParameterAttributesStruct* cache = acache;
    while(cache) {
        DM_ENG_ParameterValueStruct** pResult = NULL;
        char* paramArray[2];
        paramArray[0] = cache->parameterName;
        paramArray[1] = NULL;
        // get the new value of the parameter
        if((DM_ENG_GetParameterValues(DM_ENG_EntityType_SYSTEM, (char**) paramArray, &pResult) == 0) && (pResult != NULL)) {
            free(cache->cachedValue);
            cache->cachedValue = strdup(pResult[0]->value);
            DM_ENG_deleteAllParameterValueStruct(pResult);
            free(pResult);
        } else {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Could not get parameter value for parameter %s, not adding it to the cache", cache->parameterName);
        }
        cache = cache->next;
    }

    return 0;
}


/*
 * Add a cached value
 */
void DM_ENG_AddCachedValueToParameterAttributesCache(const char* paramName, const char* paramValue) {
    DM_ENG_ParameterAttributesStruct* cache = acache;
    while(cache) {
        if(( cache->parameterName != NULL) && ( paramName != NULL) && ( strcmp(cache->parameterName, paramName) == 0)) {
            // we found the parameter
            SAH_TRACEZ_INFO("DM_ENGINE", "** Updating cached value for parameter %s (%s -> %s)", paramName, cache->cachedValue, paramValue);
            free(cache->cachedValue);
            if(paramValue) {
                cache->cachedValue = strdup(paramValue);
            } else {
                cache->cachedValue = NULL;
            }
            return;
        }
        cache = cache->next;
    }
    // if we get here, there are no attributes for this parameter
}


/*
 * Check if the attributes cache contains a cached parameter value that is equal to the one
 * we're getting from a notification. In case we find the parameter, we clear the value of the
 * attributescache.
 *
 * @return true if the value was in the cache (e.g. this value was set by the ACS)
 *         false if the value was not in the cache (e.g. this value was not set by the ACS)
 */
bool DM_ENG_ValueWasCachedInParameterAttributesCache(const char* paramName, const char* paramValue) {
    DM_ENG_ParameterAttributesStruct* cache = acache;
    while(cache) {
        if(( cache->parameterName != NULL) && ( paramName != NULL) && ( strcmp(cache->parameterName, paramName) == 0)) {
            // we found the parameter
            SAH_TRACEZ_INFO("DM_ENGINE", "Parameter %s found in cache, comparing cached value %s with %s", paramName, cache->cachedValue, paramValue);
            if(( cache->cachedValue != NULL) && ( paramValue != NULL) && ( strcmp(cache->cachedValue, paramValue) == 0)) {
                return true;
            } else if(( cache->cachedValue != NULL) && ( strcmp(cache->cachedValue, "NULL") == 0) && ( paramValue != NULL) && ( *paramValue == 0)) {
                return true; //cache->cacheValue=="NULL" && paramValue==""
            } else if(( cache->notification == DM_ENG_NotificationMode_ACTIVE) && ( paramValue != NULL) && ( cache->cachedValue != NULL) && ( *paramValue == 0)) {
                free(cache->cachedValue);
                cache->cachedValue = strdup("NULL");
                return false;
            } else if(( cache->notification == DM_ENG_NotificationMode_ACTIVE) && ( paramValue != NULL) && ( cache->cachedValue != NULL) && ( *paramValue != 0)) {
                free(cache->cachedValue);
                cache->cachedValue = strdup(paramValue);
                return false;
            } else {
                free(cache->cachedValue);
                cache->cachedValue = NULL;
                return false;
            }
        }
        cache = cache->next;
    }
    // if we get here, there are no attributes for this parameter
    return false;
}



DM_ENG_ParameterValueStruct* DM_ENG_ParameterAttributesCacheGetChangedPassiveParams() {
    DM_ENG_ParameterValueStruct* pvs = NULL;
    DM_ENG_ParameterAttributesStruct* cache = acache;
    while(cache) {
        // only test the passive notifications
        if(cache->notification == DM_ENG_NotificationMode_PASSIVE) {
            SAH_TRACEZ_INFO("DM_ENGINE", "Passive notification %s found", cache->parameterName);
            DM_ENG_ParameterValueStruct** pResult = NULL;
            char* paramArray[2];
            paramArray[0] = cache->parameterName;
            paramArray[1] = NULL;
            // get the new value of the parameter
            if(DM_ENG_GetParameterValues(DM_ENG_EntityType_SYSTEM, (char**) paramArray, &pResult) == 0) {
                if(pResult) {
                    SAH_TRACEZ_INFO("DM_ENGINE", "old value = %s, new value = %s", cache->cachedValue, pResult[0]->value);
                    // check if the parameter has changed
                    if(pResult[0]->value && cache->cachedValue && ( strcmp(pResult[0]->value, cache->cachedValue) == 0)) {
                        //  if unchanged: do nothing
                        SAH_TRACEZ_INFO("DM_ENGINE", "No change, do nothing");
                        DM_ENG_deleteAllParameterValueStruct(pResult);
                    } else {
                        //  if changed: add to pvs, update cached values
                        SAH_TRACEZ_INFO("DM_ENGINE", "Parameter changed, add it to the result list and update the cache");
                        DM_ENG_addParameterValueStruct(&pvs, pResult[0]);
                        free(cache->cachedValue);
                        cache->cachedValue = strdup(pResult[0]->value);
                    }
                    free(pResult);
                }
            } else {
                SAH_TRACEZ_ERROR("DM_ENGINE", "Could not get parameter value for parameter %s, removing it from the cache", cache->parameterName);
                //todo: remove parameter from cache
            }
        }
        cache = cache->next;
    }
    return pvs;
}

DM_ENG_ParameterValueStruct* DM_ENG_ParameterAttributesCacheGetChangedActiveParams() {
    DM_ENG_ParameterValueStruct* pvs = NULL;
    DM_ENG_ParameterAttributesStruct* cache = acache;
    while(cache) {
        // only test the active notifications
        if(cache->notification == DM_ENG_NotificationMode_ACTIVE) {
            SAH_TRACEZ_INFO("DM_ENGINE", " notification %s found", cache->parameterName);
            DM_ENG_ParameterValueStruct** pResult = NULL;
            char* paramArray[2];
            paramArray[0] = cache->parameterName;
            paramArray[1] = NULL;
            // get the new value of the parameter
            if((DM_ENG_GetParameterValues(DM_ENG_EntityType_SYSTEM, (char**) paramArray, &pResult) == 0) && (pResult != NULL) && pResult[0]->value) {
                SAH_TRACEZ_INFO("DM_ENGINE", " old value = %s, new value = %s", cache->cachedValue, pResult[0]->value);
                // check if the parameter has changed
                if(cache->cachedValue && (strcmp(pResult[0]->value, cache->cachedValue) == 0)) {
                    //  if unchanged: do nothing
                    SAH_TRACEZ_INFO("DM_ENGINE", " No change, do nothing");
                    DM_ENG_deleteAllParameterValueStruct(pResult);
                } else if((*pResult[0]->value == 0) && cache->cachedValue && (strcmp(cache->cachedValue, "NULL") == 0)) {
                    //  NULL parameter unchanged: do nothing
                    SAH_TRACEZ_WARNING("DM_ENGINE", " NULL parameter unchanged, do nothing");
                    DM_ENG_deleteAllParameterValueStruct(pResult);
                } else {//  if changed: add to pvs if old value is not null, update cached values
                    if((cache->cachedValue == NULL) && pResult[0] && (strlen(pResult[0]->value) == 0)) {
                        SAH_TRACEZ_INFO("DM_ENGINE", " Parameter changed the value is empty , update the cache not add it to the result list");
                        cache->cachedValue = strdup("NULL");
                    } else if((cache->cachedValue == NULL) && pResult[0] && (strlen(pResult[0]->value) != 0)) {
                        cache->cachedValue = strdup(pResult[0]->value);
                        SAH_TRACEZ_INFO("DM_ENGINE", " Parameter changed the value is not empty , update the cache not add it to the result list %s ", cache->cachedValue);
                    } else {
                        SAH_TRACEZ_INFO("DM_ENGINE", " Parameter  changed update cache, add it to the result list and update the cache %s ", cache->cachedValue);
                        DM_ENG_addParameterValueStruct(&pvs, pResult[0]);
                        free(cache->cachedValue);
                        cache->cachedValue = strdup(pResult[0]->value);
                    }
                }

                free(pResult);
                pResult = NULL;
            } else {
                SAH_TRACEZ_ERROR("DM_ENGINE", " Could not get parameter value for parameter %s, removing it from the cache", cache->parameterName);
            }
        }
        cache = cache->next;
    }
    return pvs;
}


DM_ENG_ParameterValueStruct* DM_ENG_ParameterAttributesCacheGetForcedParams(bool updateCache, bool* parameterChanged) {
    DM_ENG_ParameterValueStruct* pvs = NULL;
    DM_ENG_ParameterAttributesStruct* cache = acache;
    if(parameterChanged) {
        *parameterChanged = false;
    }
    while(cache) {
        // only test the forced notifications
        if(cache->notification == DM_ENG_NotificationMode_FORCED) {
            SAH_TRACEZ_INFO("DM_ENGINE", "Forced notification %s found", cache->parameterName);
            DM_ENG_ParameterValueStruct** pResult = NULL;
            char* paramArray[2];
            paramArray[0] = cache->parameterName;
            paramArray[1] = NULL;

            // get the value of the parameter
            if((DM_ENG_GetParameterValues(DM_ENG_EntityType_SYSTEM, (char**) paramArray, &pResult) == 0) && (pResult != NULL)) {
                // add to pvs
                SAH_TRACEZ_INFO("DM_ENGINE", "add it to the result forced list");
                DM_ENG_addParameterValueStruct(&pvs, pResult[0]);
                if(updateCache) {
                    if(!(pResult[0]->value && cache->cachedValue && ( strcmp(pResult[0]->value, cache->cachedValue) == 0))) {
                        //parameter changed : indicate this and update cache
                        if(parameterChanged) {
                            SAH_TRACEZ_INFO("DM_ENGINE", " ** Adding event value changed for %s (%s)", pResult[0]->parameterName, pResult[0]->value);
                            *parameterChanged = true;
                        }
                        free(cache->cachedValue);
                        cache->cachedValue = (pResult[0]->value) ? strdup(pResult[0]->value) : NULL;
                    }
                }

                free(pResult);
                pResult = NULL;
            } else {
                SAH_TRACEZ_ERROR("DM_ENGINE", "Could not get parameter value for parameter %s, removing it from the cache", cache->parameterName);
                //todo: remove parameter from cache
            }
        }
        cache = cache->next;
    }
    return pvs;
}




int DM_ENG_InitializeParameterAttributesCache(DM_ENG_ParameterAttributesStruct** acacheArray) {
    int size = DM_ENG_tablen((void**) acacheArray);
    int i = 0;
    for(i = 0; i < size; i++) {
        DM_ENG_AddParameterAttributesCacheEllement(acacheArray[i]->parameterName, acacheArray[i]->notification, acacheArray[i]->accessList);
        DM_ENG_AddCachedValueToParameterAttributesCache(acacheArray[i]->parameterName, acacheArray[i]->cachedValue);
    }
    return 0;
}

int DM_ENG_CleanupParameterAttributesCache() {
    DM_ENG_ParameterAttributesStruct* cache = acache;
    DM_ENG_ParameterAttributesStruct* prev;
    while(cache) {
        prev = cache;
        cache = cache->next;
        DM_ENG_deleteParameterAttributesStruct(prev);
    }
    acache = NULL;

    return 0;
}


/*
 * Display the parameter Cache
 */
void DM_ENG_DisplayParameterAttributesCache() {
    DM_ENG_ParameterAttributesStruct* cache = acache;
    SAH_TRACEZ_INFO("DM_ENGINE", "----------------");
    SAH_TRACEZ_INFO("DM_ENGINE", "attribute cache:");
    SAH_TRACEZ_INFO("DM_ENGINE", "----------------");
    SAH_TRACEZ_INFO("DM_ENGINE", "path - notification - accesslist - subscriptionID");
    while(cache) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s - %s(%d) - %s - %d", cache->parameterName, DM_ENG_NotificationModeString(cache->notification),
                        cache->notification, cache->accessList ? cache->accessList[0] : "NULL", cache->notificationID);
        cache = cache->next;
    }

}

const char* DM_ENG_NotificationModeString(DM_ENG_NotificationMode mode) {
    switch(mode) {
    case DM_ENG_NotificationMode_OFF:       return "Off";
    case DM_ENG_NotificationMode_PASSIVE:   return "Passive";
    case DM_ENG_NotificationMode_ACTIVE:    return "Active";
    case DM_ENG_NotificationMode_FORCED:    return "Forced";
    case DM_ENG_NotificationMode_UNDEFINED: return "Undefined";
    default: break;
    }
    return "Error";
}

DM_ENG_NotificationMode DM_ENG_StringNotificationMode(const char* mode) {

    if(strcmp(mode, "Off") == 0) {
        return DM_ENG_NotificationMode_OFF;
    } else if(strcmp(mode, "Passive") == 0) {
        return DM_ENG_NotificationMode_PASSIVE;
    } else if(strcmp(mode, "Active") == 0) {
        return DM_ENG_NotificationMode_ACTIVE;
    } else {
        return DM_ENG_NotificationMode_UNDEFINED;
    }
}

void DM_ENG_CacheDict_Build(amxc_htable_t* dict) {
    DM_ENG_ParameterAttributesStruct* pas = acache;
    amxc_htable_init(dict, 0);
    while(pas) {
        if((pas->parameterName != NULL) && (strcmp(pas->parameterName, "") != 0)) {
            cache_dict_entry_t* entry = (cache_dict_entry_t*) calloc(1, sizeof(cache_dict_entry_t));
            entry->pas = pas;
            amxc_htable_insert(dict, pas->parameterName, &entry->hit);
        }
        pas = pas->next;
    }
}

static void cache_dict_entry_free(UNUSED const char* key, amxc_htable_it_t* it) {
    cache_dict_entry_t* entry = amxc_htable_it_get_data(it, cache_dict_entry_t, hit);
    free(entry);
}

void DM_ENG_CacheDict_Destroy(amxc_htable_t* dict) {
    amxc_htable_clean(dict, cache_dict_entry_free);
}

int DM_ENG_CacheDict_GetInfo(amxc_htable_t* dict, const char* name, DM_ENG_NotificationMode* notificationMode, char** accessList[]) {
    int retval = -1;
    amxc_htable_it_t* it = NULL;
    cache_dict_entry_t* entry = NULL;
    *notificationMode = DM_ENG_NotificationMode_OFF;
    *accessList = NULL;

    if((dict == NULL) || (name == NULL) || ((strcmp(name, "") == 0))) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Invalid arg(s)");
        goto stop;
    }
    if((amxc_htable_is_empty(dict) == true)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Cache dict is empty");
        goto stop;
    }
    it = amxc_htable_get(dict, name);
    if(it == NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Iterator not found [%s]", name);
        goto stop;
    }
    entry = amxc_htable_it_get_data(it, cache_dict_entry_t, hit);
    if(entry == NULL) {
        // unexpected error.
        SAH_TRACEZ_ERROR("DM_ENGINE", "Entry not valid [%s]", name);
        goto stop;
    }
    if(entry->pas == NULL) {
        // unexpected error.
        SAH_TRACEZ_ERROR("DM_ENGINE", "Invalid PAS within the entry [%s]", name);
        goto stop;
    }
    *notificationMode = entry->pas->notification;
    *accessList = entry->pas->accessList;
    retval = 0;
stop:
    return retval;
}
