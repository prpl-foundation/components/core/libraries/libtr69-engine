/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_RPCInterface.c
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_RPCInterface.c
 *
 * @brief The RPC Interface.
 *
 * This header file defines the RPC Interface which provides similar functions to the TR-069 RPC methods, plus
 * somme functions for the notification by Inform messages and for the session management.
 *
 */


#include <dmengine/DM_ENG_RPCInterface.h>
#include <dmengine/DM_ENG_NotificationInterface.h>
#include <dmengine/DM_ENG_InformMessageScheduler.h>
#include <dmengine/DM_ENG_Device.h>
#include <dmengine/DM_ENG_ParameterAttributesCache.h>
#include <dmengine/DM_ENG_Mapping.h>
#include <dmcommon/DM_GlobalDefs.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <debug/sahtrace.h>

extern device_adapter_t* device_adapter;

static DM_ENG_ScheduleInformStruct* undeliveredScheduleInforms = NULL;
static bool performReboot = false;
static bool performResetToFactoryDefaults = false;
static const char** forcedParameters = NULL;
static unsigned int pendingDiagnosticRequests = 0;

static const char* forcedTR181Parameters[10] = {
    "Device.RootDataModelVersion",
    "Device.DeviceInfo.HardwareVersion",
    "Device.DeviceInfo.SoftwareVersion",
    "Device.ManagementServer.ParameterKey",
    "Device.ManagementServer.ConnectionRequestURL",
    "Device.ManagementServer.AliasBasedAddressing",
    "Device.DeviceInfo.ProvisioningCode",
    "Device.ManagementServer.ParameterKey",
    NULL, // e.g. "Device.IP.Interface.{i}.IPvXAddress.{j}.IPAddress", filled in dynamically
    NULL
};

static const char* forcedTR106Parameters[7] = {
    "Device.DeviceSummary",
    "Device.DeviceInfo.HardwareVersion",
    "Device.DeviceInfo.SoftwareVersion",
    "Device.ManagementServer.ConnectionRequestURL",
    "Device.ManagementServer.ParameterKey",
    "Device.LAN.IPAddress",
    NULL
};

static const char* forcedTR098Parameters[9] = {
    "InternetGatewayDevice.DeviceSummary",
    "InternetGatewayDevice.DeviceInfo.SpecVersion",
    "InternetGatewayDevice.DeviceInfo.HardwareVersion",
    "InternetGatewayDevice.DeviceInfo.SoftwareVersion",
    "InternetGatewayDevice.DeviceInfo.ProvisioningCode",
    "InternetGatewayDevice.ManagementServer.ConnectionRequestURL",
    "InternetGatewayDevice.ManagementServer.ParameterKey",
    NULL, // e.g. "InternetGatewayDevice.WANDevice.{i}.WANConnectionDevice.{j}.WAN{***}Connection.{k}.ExternalIPAddress", filled in dynamically
    NULL
};


/**
 * Set the IGD external IP address path
 *
 * e.g. "InternetGatewayDevice.WANDevice.{i}.WANConnectionDevice.{j}.WAN{***}Connection.{k}.ExternalIPAddress"
 *
 * @param externalIPAddressPath The new external ip address path
 *
 */
void DM_ENG_TR098SetExternalIPAddressPath(char* externalIPAddressPath) {
    if(forcedTR098Parameters[7] != NULL) {
        //remove old path from cache
        DM_ENG_RemoveAttributesCacheEllement(forcedTR098Parameters[7], true);
        free((char*) forcedTR098Parameters[7]);
    }
    forcedTR098Parameters[7] = externalIPAddressPath;
    DM_ENG_AddParameterAttributesCacheEllement(forcedTR098Parameters[7], DM_ENG_NotificationMode_FORCED, NULL);
}

/**
 * Set the Device external IP address path
 *
 * e.g. "Device.IP.Interface.{i}.IPvXAddress.{j}.IPAddress"
 *
 * @param externalIPAddressPath The new external ip address path
 *
 */
void DM_ENG_TR181SetExternalIPAddressPath(char* externalIPAddressPath) {
    if(forcedTR181Parameters[8] != NULL) {
        //remove old path from cache
        DM_ENG_RemoveAttributesCacheEllement(forcedTR181Parameters[8], true);
        free((char*) forcedTR181Parameters[8]);
    }
    forcedTR181Parameters[8] = externalIPAddressPath;
    DM_ENG_AddParameterAttributesCacheEllement(forcedTR181Parameters[8], DM_ENG_NotificationMode_FORCED, NULL);
}

/**
 * This function retrieves the forced parameter values needed for an inform message
 *
 * @param Pointer to the array of the ParameterValueStruct.
 *
 * @return 0 if OK, else an error code (9001, ..)
 */
int DM_ENG_GetForcedParameterValues(DM_ENG_ParameterValueStruct** pvsList) {
    int error = 0;

    SAH_TRACEZ_IN("DM_DA");
    error = device_adapter->DM_ENG_Device_GetParameterValues((char**) forcedParameters, pvsList);

    SAH_TRACEZ_OUT("DM_DA");
    return error;
}

/**
 * This function retrieves the inform parameter values, based on the parameters set
 * in the inform datamodel.
 *
 * @param eventList pointer to the array of the EventStruct.
 * @param pvsList pointer to the array of the ParameterValueStruct.
 *
 * @return 0 if OK, else an error code (9001, ..)
 */
int DM_ENG_GetInformParameterValues(DM_ENG_EventStruct* eventList, DM_ENG_ParameterValueStruct** pvsList) {
    int error = 0;

    SAH_TRACEZ_IN("DM_DA");
    error = device_adapter->DM_ENG_Device_GetInformParameterValues(eventList, pvsList);

    SAH_TRACEZ_OUT("DM_DA");
    return error;
}

/**
 * Activates the notification.
 *
 * @param rpcPath rpc path
 * @param systemCtx system ctx
 * @param acsCtx acs ctx
 *
 * @return 0 if succesfull, -1 if an error occurred
 */
int DM_ENG_DataModelConnect(const char* rpcPath,
                            const char* aclfile,
                            void** systemCtx,
                            void** acsCtx,
                            const char* vendorPrefix,
                            const char* backend,
                            const char* uri) {
    if(device_adapter->DM_ENG_Device_Init(systemCtx, acsCtx, rpcPath, aclfile, vendorPrefix, backend, uri) == false) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not Connect to DATA-MODEL");
        return -1;
    }
    return 0;
}


/**
 * Activates the notification.
 *
 * @param entity Entity number
 * @param inform Inform callback
 * @param transferComplete callback
 *
 * @return 0 if succesfull, -1 if an error occurred
 */
int DM_ENG_ActivateNotification(DM_ENG_EntityType entity,
                                DM_ENG_F_INFORM inform,
                                DM_ENG_F_TRANSFER_COMPLETE transferComplete,
                                DM_ENG_F_REQUEST_DOWNLOAD requestDownload,
                                DM_ENG_F_GETRPCMETHODS getRPCMethods,
                                DM_ENG_F_TIMER_START timerStart,
                                DM_ENG_F_TIMER_STOP timerStop,
                                DM_ENG_F_TIMER_TIME_REMAINING timerTimeRemaining,
                                DM_ENG_F_EVENT engineEvent,
                                DM_ENG_F_DU_STATE_CHANGE_COMPLETE duStateChangeComplete) {
    unsigned int i = 0;
    DM_ENG_ParameterAttributesStruct** acacheArray = NULL;
    char* rebootByACS = NULL;
    char* cmdkey = NULL;
    char* factoryResetOccurred = NULL;
    char* val = NULL;
    DM_ENG_EventStruct* eventList = NULL;
    DM_ENG_EventStruct* es = NULL;
    DM_ENG_SubscriptionStruct* pResult = NULL;
    DM_ENG_NotificationMode mode;

    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_REBOOTBYACS, &rebootByACS) != 0) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Cannot fetch the REBOOTBYACS param");
    }
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_REBOOTCOMMANDKEY, &cmdkey) != 0) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Cannot fetch the reboot command key");
    }
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_FACTORYRESETOCCURRED, &factoryResetOccurred) != 0) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Cannot fetch the FACTORYRESETOCCURRED param");
    }
    /* activate the notification interface */
    DM_ENG_NotificationInterface_activate(entity, inform, transferComplete, requestDownload, getRPCMethods, timerStart, timerStop, timerTimeRemaining, engineEvent, duStateChangeComplete);

    /* Create the subscriptions for the forced parameters */
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_DATAMODEL, &val) != 0) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Cannot fetch the DM_ENG_DATAMODEL param default to TR098");
        forcedParameters = forcedTR181Parameters;
    } else {
        if(strcmp(val, "TR106") == 0) {
            forcedParameters = forcedTR106Parameters;
        } else if(strcmp(val, "TR098") == 0) {
            forcedParameters = forcedTR098Parameters;
        } else {
            forcedParameters = forcedTR181Parameters;
        }
    }
    for(i = 0; forcedParameters[i] != NULL; i++) {
        DM_ENG_AddParameterAttributesCacheEllement(forcedParameters[i], DM_ENG_NotificationMode_FORCED, NULL);
    }

    if(device_adapter->DM_ENG_Device_LoadConfig) {
        if(device_adapter->DM_ENG_Device_LoadConfig(&undeliveredScheduleInforms) == -1) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Failed to load the configuration");
        }
    }

    DM_ENG_Mapping_init();
    // Load the saved dm parameters
    if(device_adapter->DM_ENG_Device_GetSubscriptions) {
        device_adapter->DM_ENG_Device_GetSubscriptions(&pResult);
    }
    if(pResult != NULL) {
        DM_ENG_SubscriptionStruct* ss = pResult;
        DM_ENG_SubscriptionStruct* next_ss = NULL;
        while(ss != NULL) {
            next_ss = ss->next;
            ss->next = NULL;
            mode = DM_ENG_StringNotificationMode(ss->type);
            if(mode != DM_ENG_NotificationMode_FORCED) {
                DM_ENG_AddParameterAttributesCacheEllement(ss->path, mode, NULL);
            }
            DM_ENG_deleteSubscriptionStruct(ss);
            ss = next_ss;
        }
    }

    DM_ENG_InitializeParameterAttributesCache(acacheArray);

    DM_ENG_DisplayParameterAttributesCache();

    if(( factoryResetOccurred != NULL) && ( strcmp(factoryResetOccurred, "1") == 0)) {
        /* 3.7.1.5 The specific conditions that MUST result in the BOOTSTRAP EventCode are:
           - First time connection of the CPE to the ACS from the factory
           - First time connection after a factory reset */
        es = DM_ENG_newEventStruct(DM_ENG_EVENT_BOOTSTRAP, NULL);
        DM_ENG_addEventStruct(&eventList, es);
    }

    es = DM_ENG_newEventStruct(DM_ENG_EVENT_BOOT, NULL);
    DM_ENG_addEventStruct(&eventList, es);

    if(( rebootByACS != NULL) && ( strcmp(rebootByACS, "1") == 0)) {
        es = DM_ENG_newEventStruct(DM_ENG_EVENT_M_REBOOT, cmdkey);
        DM_ENG_addEventStruct(&eventList, es);
        DM_ENG_acsEventListAddEvent(DM_ENG_EVENT_M_REBOOT, cmdkey);
    }

    DM_ENG_acsEventListUpdate();
    SAH_TRACEZ_INFO("DM_ENGINE", "is=%p", undeliveredScheduleInforms);
    if(undeliveredScheduleInforms != NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "There were undelivered schedule informs, re-schedule them");
        time_t now;
        time(&now);

        DM_ENG_ScheduleInformStruct* isp = undeliveredScheduleInforms;
        while(isp) {
            unsigned int delay = 0;
            if(now < isp->time) {
                delay = isp->time - now;
            }

            DM_ENG_InformMessageScheduler_scheduleInform(delay, isp->commandKey, isp->uniqueID);
            isp = isp->next;
        }
    }

    DM_ENG_InformMessageScheduler_init(eventList);


    DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);

    if(acacheArray != NULL) {
        DM_ENG_deleteTabParameterAttributesStruct(acacheArray);
    }
    free(rebootByACS);
    free(cmdkey);
    free(factoryResetOccurred);
    free(val);
    return 0;
}


/**
 * Deactivate the notification
 * @param entity Entity number
 */
void DM_ENG_DeactivateNotification(DM_ENG_EntityType entity) {
    DM_ENG_Mapping_clean();
    DM_ENG_CleanupParameterAttributesCache();
    free((char*) forcedTR098Parameters[7]);
    free((char*) forcedTR181Parameters[8]);
    forcedTR098Parameters[7] = NULL;
    forcedTR181Parameters[8] = NULL;
    DM_ENG_NotificationInterface_deactivate(entity);
    DM_ENG_deleteAllScheduleInformStruct(&undeliveredScheduleInforms);
}

/**
 * Indicates that the session is opened.
 *
 * @param entity Entity number
 */
void DM_ENG_SessionOpened(DM_ENG_EntityType entity) {
    (void) entity;

    if(device_adapter->DM_ENG_Device_OpenSession) {
        device_adapter->DM_ENG_Device_OpenSession();
    }
    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_SESSIONSTATUS, "Busy") != 0) {
        SAH_TRACE_ERROR("Could not set Session Status in the datamodel");
    }
}

/**
 * Asks if the DM Engine is ready to close the session (No more transfer complete to send)
 *
 * @param entity Entity number
 *
 * @return True if the DM Engine is ready, false otherwise
 */
bool DM_ENG_IsReadyToClose(DM_ENG_EntityType entity) {
    bool res = true;
    if((entity == DM_ENG_EntityType_ACS)) {
        res = DM_ENG_InformMessageScheduler_isReadyToClose();
    }
    return res;
}

/**
 * Notify the engine that a transfer complete response has arrived
 *
 * @param entity Entity number
 *
 * @return True if notifying the engine was OK, false otherwise
 */
bool DM_ENG_TransferCompleteResponseArrived(DM_ENG_EntityType entity) {
    bool res = true;
    if((entity == DM_ENG_EntityType_ACS)) {
        res = DM_ENG_InformMessageScheduler_transferCompleteResponseArrived();
    }
    return res;
}

/**
 * Notify the engine that a downloadrequest complete response has arrived
 *
 * @param entity Entity number
 *
 * @return True if notifying the engine was OK, false otherwise
 */
bool DM_ENG_DownloadRequestResponseArrived(DM_ENG_EntityType entity) {
    bool res = true;
    if((entity == DM_ENG_EntityType_ACS)) {
        res = DM_ENG_InformMessageScheduler_RequestDownloadResponseArrived();
    }
    return res;
}

bool DM_ENG_DUStateChangeCompleteResponseArrived(DM_ENG_EntityType entity) {
    bool res = true;
    if((entity == DM_ENG_EntityType_ACS)) {
        res = DM_ENG_InformMessageScheduler_DUStateChangeCompleteResponseArrived();
    }
    return res;
}

/**
 * Notify the engine that an inform response has arrived
 *
 * @param entity Entity number
 *
 * @return True if notifying the engine was OK, false otherwise
 */
bool DM_ENG_InformResponseArrived(DM_ENG_EntityType entity) {
    bool res = true;
    if((entity == DM_ENG_EntityType_ACS)) {
        res = DM_ENG_InformMessageScheduler_informResponseArrived();
    }
    return res;
}




/**
 * Notify the DM Engine that the session is closed.
 *
 * @param entity Entity number
 * @param success True if the session succeeded, false otherwise
 */
void DM_ENG_SessionClosed(DM_ENG_EntityType entity, bool success) {
    (void) entity;
    if(device_adapter->DM_ENG_Device_CloseSession) {
        device_adapter->DM_ENG_Device_CloseSession();
    }

    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_SESSIONSTATUS, "Idle") != 0) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Could not set Session Status in the datamodel");
    }
    DM_ENG_InformMessageScheduler_sessionClosed(success);
    if(performReboot == true) {
        performReboot = false;
        device_adapter->DM_ENG_Device_Reboot();
    }
    if(performResetToFactoryDefaults == true) {
        performResetToFactoryDefaults = false;
        device_adapter->DM_ENG_Device_FactoryReset();
    }

}


/**
 * Returns Array of strings containing the names of each of the RPC methods the CPE supports.
 *
 * @param entity Entity number
 * @param pMethods Pointer to null terminated array to get the result
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_GetRPCMethods(DM_ENG_EntityType entity, const char** pMethods[]) {
    (void) entity;
    SAH_TRACEZ_IN("DM_ENGINE");
    static const char* _methods[MAX_SUPPORTED_RPC_METHODS + 1];
    int i = 0;
    if(true == DM_ENG_getRpcMethodSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_GETRPCMETHODS);
        _methods[i] = SUPPORTED_RPC_GETRPCMETHODS;
        i++;
    }
    if(true == DM_ENG_setParameterValuesSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_SETPARAMETERVALUES);
        _methods[i] = SUPPORTED_RPC_SETPARAMETERVALUES;
        i++;
    }
    if(true == DM_ENG_getParameterValuesSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_GETPARAMETERVALUES);
        _methods[i] = SUPPORTED_RPC_GETPARAMETERVALUES;
        i++;
    }
    if(true == DM_ENG_getParameterNamesSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_GETPARAMETERNAMES);
        _methods[i] = SUPPORTED_RPC_GETPARAMETERNAMES;
        i++;
    }
    if(true == DM_ENG_setParameterAttributesSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_SETPARAMETERATTRIBUTES);
        _methods[i] = SUPPORTED_RPC_SETPARAMETERATTRIBUTES;
        i++;
    }
    if(true == DM_ENG_getParameterAttributesSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_GETPARAMETERATTRIBUTES);
        _methods[i] = SUPPORTED_RPC_GETPARAMETERATTRIBUTES;
        i++;
    }
    if(true == DM_ENG_addObjectSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_ADDOBJECT);
        _methods[i] = SUPPORTED_RPC_ADDOBJECT;
        i++;
    }
    if(true == DM_ENG_deleteObjectSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_DELETEOBJECT);
        _methods[i] = SUPPORTED_RPC_DELETEOBJECT;
        i++;
    }
    if(true == DM_ENG_rebootSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_REBOOT);
        _methods[i] = SUPPORTED_RPC_REBOOT;
        i++;
    }
    if(true == DM_ENG_downloadSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_DOWNLOAD);
        _methods[i] = SUPPORTED_RPC_DOWNLOAD;
        i++;
    }
    if(true == DM_ENG_scheduleDownloadSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_SCHEDULEDOWNLOAD);
        _methods[i] = SUPPORTED_RPC_SCHEDULEDOWNLOAD;
        i++;
    }
    if(true == DM_ENG_changeDUStateSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_CHANGEDUSTATE);
        _methods[i] = SUPPORTED_RPC_CHANGEDUSTATE;
        i++;
    }
    if(true == DM_ENG_uploadSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_UPLOAD);
        _methods[i] = SUPPORTED_RPC_UPLOAD;
        i++;
    }
    if(true == DM_ENG_factoryResetSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_FACTORYRESET);
        _methods[i] = SUPPORTED_RPC_FACTORYRESET;
        i++;
    }
    if(true == DM_ENG_getQueuedTransfersSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_GETQUEUEDTRANSFERS);
        _methods[i] = SUPPORTED_RPC_GETQUEUEDTRANSFERS;
        i++;
    }
    if(true == DM_ENG_getAllQueuedTransfersSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_GETALLQUEUEDTRANSFERS);
        _methods[i] = SUPPORTED_RPC_GETALLQUEUEDTRANSFERS;
        i++;
    }
    if(true == DM_ENG_scheduleInformSupportedOnDevice()) {
        SAH_TRACEZ_INFO("DM_ENGINE", "%s supported", SUPPORTED_RPC_SCHEDULEINFORM);
        _methods[i] = SUPPORTED_RPC_SCHEDULEINFORM;
        i++;
    }
    _methods[i] = NULL;

    *pMethods = _methods;
    SAH_TRACEZ_OUT("DM_ENGINE");
    return 0;
}

/**
 * This function may be used by the ACS to modify the value of one or more CPE Parameters.
 *
 * @param entity Entity number
 *
 * @param parameterList Array of name-value pairs as specified. For each name-value pair, the CPE is instructed
 * to set the Parameter specified by the name to the corresponding value.
 *
 * @param parameterKey The value to set the ParameterKey parameter. This MAY be used by the server to identify
 * Parameter updates, or left empty.
 *
 * @param pStatus Pointer to get the status defined as follows :
 * - DM_ENG_ParameterStatus_APPLIED (=0) : Parameter changes have been validated and applied.
 * - DM_ENG_ParameterStatus_READY_TO_APPLY (=1) : Parameter changes have been validated and committed, but not yet applied
 * (for example, if a reboot is required before the new values are applied).
 * - DM_ENG_ParameterStatus_UNDEFINED : Undefined status (for example, parameterList is empty).
 *
 *  Note : The result is DM_ENG_ParameterStatus_APPLIED if all the parameter changes are APPLIED.
 * The result is DM_ENG_ParameterStatus_READY_TO_APPLY if one or more parameter changes is READY_TO_APPLY.
 *
 * @param pFaults Pointer to get the faults struct if any, as specified in TR-069
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_SetParameterValues(DM_ENG_EntityType entity, DM_ENG_ParameterValueStruct* parameterList[], const char* parameterKey, DM_ENG_ParameterStatus* pStatus, DM_ENG_SetParameterValuesFault** pFaults[]) {
    (void) entity;
    int error = 0;
    DM_ENG_SetParameterValuesFault* faultsList = NULL;
    SAH_TRACEZ_IN("DM_ENGINE");

    if(device_adapter->DM_ENG_Device_SetParameterValues == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }

    *pStatus = DM_ENG_ParameterStatus_UNDEFINED;

    error = device_adapter->DM_ENG_Device_SetParameterValues(parameterList, parameterKey, pStatus, &faultsList);

    if(error == 0) {
        /* if we change parameterkey value, we need to update it in Attributes cache first*/
        char* parameterkey_path = NULL;
        if(asprintf(&parameterkey_path, "%s%s", DM_ENG_getDatamodelPrefix(), "ManagementServer.ParameterKey") == -1) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Failed to create parameter path");
        } else {
            DM_ENG_AddCachedValueToParameterAttributesCache(parameterkey_path, parameterKey);
            free(parameterkey_path);
        }

        error = DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PARAMETERKEY, parameterKey);
        DM_ENG_deleteAllSetParameterValuesFault(&faultsList);
        *pFaults = NULL;
    } else {
        *pFaults = DM_ENG_toSetParameterValuesFaultArray(faultsList);
    }

    SAH_TRACEZ_OUT("DM_ENGINE");
    return error;

}
#if 0
/*
 * Sert à filter les valeurs remontées à l'ACS. Seul cas aujourd'hui : ConnectionRequestPassword remonté comme chaîne vide.
 * ajouter Password, STUNPassword
 */
static char* _filteredValue(DM_ENG_EntityType entity, char* paramName, char* value) {
    return ((entity == DM_ENG_EntityType_ACS)
            && ((strcmp(paramName, DM_TR106_ACS_PASSWORD) == 0)
                || (strcmp(paramName, DM_TR106_CONNECTIONREQUESTPASSWORD) == 0)
                || (strcmp(paramName, DM_TR106_STUN_PASSWORD) == 0))
            ? (char*) DM_ENG_EMPTY
            : value);
}
#endif

static void _update_ParameterValuesRequests(void) {
    int bytes = 0;
    char* nrequestsstring = NULL;
    unsigned int nrequests = 0;
    char* tempBuffer = NULL;

    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_GETPARAMETERVALUEREQUESTS, &nrequestsstring) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not get parameter PARAMETERVALUEREQUESTS");
        goto exit;
    }
    if(!nrequestsstring || !*nrequestsstring) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "PARAMETERVALUEREQUESTS parameter is empty");
        goto exit;
    }
    nrequests = atoi(nrequestsstring);
    nrequests++;
    bytes = asprintf(&tempBuffer, "%d", nrequests);
    if(bytes == -1) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "asprintf error [%s]", strerror(errno));
        goto exit;
    }
    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_GETPARAMETERVALUEREQUESTS, tempBuffer) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not set DM_ENG_GETPARAMETERVALUEREQUESTS parameter in the datamodel");
        goto exit;
    }

exit:
    free(tempBuffer);
    free(nrequestsstring);
}

/**
 * This function may be used by the ACS to obtain the value of one or more CPE parameters.
 *
 * @param entity Entity number
 * @param parameterNames Array of strings, each representing the name of the requested parameter.
 * @param Pointer to the array of the ParameterValueStruct.
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_GetParameterValues(DM_ENG_EntityType entity, char* parameterNames[], DM_ENG_ParameterValueStruct** pResult[]) {
    int error = DM_ENG_METHOD_NOT_SUPPORTED;
    DM_ENG_ParameterValueStruct* pvsList = NULL;
    *pResult = NULL;

    if(device_adapter->DM_ENG_Device_GetParameterValues == NULL) {
        goto stop;
    }
    error = device_adapter->DM_ENG_Device_GetParameterValues(parameterNames, &pvsList);
    if(error != 0) {
        goto stop;
    }
    *pResult = DM_ENG_toParameterValueStructArray(pvsList);
stop:
    if(entity == DM_ENG_EntityType_ACS) {
        _update_ParameterValuesRequests();
    }
    return error;
}

int DM_ENG_GetMapping(amxc_var_t* data) {
    int retval = -1;
    if(device_adapter->DM_ENG_Device_GetMapping) {
        retval = device_adapter->DM_ENG_Device_GetMapping(data);
    }
    return retval;
}

int DM_ENG_UpdateQosInfo(bool enable, const char* dstIP, int32_t dstPort, int32_t dstIPVersion) {
    int retval = -1;
    amxc_var_t set;
    amxc_var_init(&set);
    amxc_var_t ret;
    amxc_var_init(&ret);

    if(device_adapter->DM_ENG_Device_Set == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "DM_ENG_Device_Set not supported");
        goto stop;
    }

    if(enable) {
        if((dstIP == NULL) || (*dstIP == 0)) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Invalid destination IP");
            goto stop;
        }
        if(dstPort <= 0) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Invalid destination port");
            goto stop;
        }
        if((dstIPVersion != 4) && (dstIPVersion != 6)) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Invalid destination IP version");
            goto stop;
        }
    }

    amxc_var_set_type(&set, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &set, "Enable", enable);
    amxc_var_add_key(cstring_t, &set, "DestIP", enable ? dstIP:"");
    amxc_var_add_key(int32_t, &set, "DestPort", enable ? dstPort:-1);
    amxc_var_add_key(int32_t, &set, "IPVersion", enable ? dstIPVersion:-1);
    retval = device_adapter->DM_ENG_Device_Set("QoS.Classification.cwmp-client.", &set, &ret);
    if(retval != 0) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Failed to update QoS Info [%d]", retval);
    }
stop:
    amxc_var_clean(&set);
    amxc_var_clean(&ret);
    return retval;
}

int DM_ENG_GetSMMMapping(char** data) {
    int retval = 0;
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_SMM_MAPPING, data) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Failed to get SMM mapping");
        retval = -1;
    }
    return retval;
}

int DM_ENG_GetParameterValuesAll(xmlNodePtr node_body) {
    int error = DM_ENG_METHOD_NOT_SUPPORTED;
    if(device_adapter->DM_ENG_Device_GetParameterValuesAll == NULL) {
        goto stop;
    }
    error = device_adapter->DM_ENG_Device_GetParameterValuesAll(node_body);

    _update_ParameterValuesRequests();
stop:
    return error;
}

int DM_ENG_GetParameterAttributesAll(xmlNodePtr node_body) {
    int error = DM_ENG_METHOD_NOT_SUPPORTED;
    if(device_adapter->DM_ENG_Device_GetParameterAttributesAll == NULL) {
        goto stop;
    }
    error = device_adapter->DM_ENG_Device_GetParameterAttributesAll(node_body);
stop:
    return error;
}

/**
 * This function may be used by the ACS to obtain the value of one or more CPE parameters.
 *
 * @param entity Entity number
 * @param path Parameter path
 * @param nextLevel If true, the response must only contains the parameters in the next level. If false, the response must also
 * contains the parameter below.
 * @param pResult Pointer to the array of the ParameterInfoStruct.
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_GetParameterNames(DM_ENG_EntityType entity, const char* path, bool nextLevel, DM_ENG_ParameterInfoStruct** pResult[]) {
    (void) entity;
    int error = DM_ENG_METHOD_NOT_SUPPORTED;
    DM_ENG_ParameterInfoStruct* infoList = NULL;

    if(device_adapter->DM_ENG_Device_GetParameterNames == NULL) {
        goto stop;
    }

    error = device_adapter->DM_ENG_Device_GetParameterNames(path, nextLevel, &infoList);

    if(error == 0) {
        *pResult = DM_ENG_toParameterInfoStructArray(infoList);
    } else {
        *pResult = NULL;
    }

stop:
    return error;
}

/**
 * This function may be used by the ACS to create a new instance of a multi-instance object.
 *
 * @param entity Entity number
 * @param objectName The path name of the collection objects
 * @param parameterKey The value to set the ParameterKey parameter
 * @param pInstanceNumber Pointer to an integer to obtain the new instance number
 * @param pStatus Pointer to integer to obtain the status as specified.
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_AddObject(DM_ENG_EntityType entity, const char* objectName, const char* parameterKey, unsigned int* pInstanceNumber, DM_ENG_ParameterStatus* pStatus) {
    (void) entity;
    int error = 0;
    if(device_adapter->DM_ENG_Device_AddObject == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }

    error = device_adapter->DM_ENG_Device_AddObject(objectName, parameterKey, pInstanceNumber, pStatus);

    if(error == 0) {
        /* if we change parameterkey value, we need to update it in Attributes cache first*/
        char* parameterkey_path = NULL;
        if(asprintf(&parameterkey_path, "%s%s", DM_ENG_getDatamodelPrefix(), "ManagementServer.ParameterKey") == -1) {
            SAH_TRACEZ_WARNING("DM_ENGINE", "Failed to create parameter path");
        } else {
            DM_ENG_AddCachedValueToParameterAttributesCache(parameterkey_path, parameterKey);
            free(parameterkey_path);
        }

        error = DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PARAMETERKEY, parameterKey);
    }

    return error;
}

/**
 * This function may be used by the ACS to remove a particular instance of an object.
 *
 * @param entity Entity number
 * @param objectName The path name of the object instance to be removed
 * @param parameterKey The value to set the ParameterKey parameter
 * @param pStatus Pointer to integer to obtain the status as specified.
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_DeleteObject(DM_ENG_EntityType entity, const char* objectName, const char* parameterKey, DM_ENG_ParameterStatus* pStatus) {
    (void) entity;
    int error = 0;

    if(device_adapter->DM_ENG_Device_DeleteObject == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }

    error = device_adapter->DM_ENG_Device_DeleteObject(objectName, parameterKey, pStatus);

    if(error == 0) {
        /* if we change parameterkey value, we need to update it in Attributes cache first*/
        char* parameterkey_path = NULL;
        if(asprintf(&parameterkey_path, "%s%s", DM_ENG_getDatamodelPrefix(), "ManagementServer.ParameterKey") == -1) {
            SAH_TRACEZ_WARNING("DM_ENGINE", "Failed to create parameter path");
        } else {
            DM_ENG_AddCachedValueToParameterAttributesCache(parameterkey_path, parameterKey);
            free(parameterkey_path);
        }

        error = DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PARAMETERKEY, parameterKey);
    }

    return error;
}

/**
 * This function may be used by the ACS to modify attributes associated with on or more parameters.
 *
 * @param entity Entity number
 * @param pasList List of changes to be made to the attributes for a set of parameters
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_SetParameterAttributes(DM_ENG_EntityType entity, DM_ENG_ParameterAttributesStruct* pasList[]) {
    (void) entity;
    int i = 0;
    int error = 0;
    SAH_TRACEZ_IN("DM_ENGINE");

    DM_ENG_DisplayParameterAttributesCache();

    int len = DM_ENG_tablen((void**) pasList);
    // todo: operation should be atomic
    for(i = 0; i < len; i++) { //for each ellement in list
        // do a getparameternames to get a list of available parameters in the datamodel
        DM_ENG_ParameterInfoStruct* infoList = NULL;
        DM_ENG_ParameterInfoStruct* ilp = NULL;

        if(pasList[i]->parameterName[strlen(pasList[i]->parameterName) - 1] == '.') {
            // we got an object path, apply the attributes to all underlying parameters
            error = device_adapter->DM_ENG_Device_GetParameterNames(pasList[i]->parameterName, false, &infoList);

            if(error == 0) {
                ilp = infoList;
                while(ilp) {
                    // go over each parameter in the list
                    if(ilp->parameterName[strlen(ilp->parameterName) - 1] != '.') {
                        // only set the attributes for parameters, not for objects
                        DM_ENG_AddParameterAttributesCacheEllement(ilp->parameterName, pasList[i]->notification, pasList[i]->accessList);
                    }
                    ilp = ilp->next;
                }
            }
        } else {
            // we got a complete parameter path, apply the attribute to this parameter only
            error = device_adapter->DM_ENG_Device_GetParameterNames(pasList[i]->parameterName, false, &infoList);

            if(error == 0) {
                DM_ENG_AddParameterAttributesCacheEllement(pasList[i]->parameterName, pasList[i]->notification, pasList[i]->accessList);
            } else {
                SAH_TRACEZ_ERROR("DM_ENGINE", "Parameter %s does not exist, cannot set attribute", pasList[i]->parameterName);
            }
        }
        DM_ENG_deleteAllParameterInfoStruct(&infoList);
        infoList = NULL;
    }
    //DM_ENG_CleanupParameterAttributesCache();
    DM_ENG_DisplayParameterAttributesCache();
    SAH_TRACEZ_OUT("DM_ENGINE");
    return error;

}

/**
 * This function may be used by the ACS to read the attributes associated with on or more parameters.
 *
 * @param entity Entity number
 * @param names Array of string, each representing the name of a requested parameter
 * @param pResult Pointer to array of ParameterAttributesStruct to obtain the result
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_GetParameterAttributes(DM_ENG_EntityType entity, char* names[], DM_ENG_ParameterAttributesStruct** pResult[]) {
    (void) entity;
    int ilpLength = 0;
    int i = 0;
    int len = 0;
    int error = DM_ENG_METHOD_NOT_SUPPORTED;
    DM_ENG_ParameterAttributesStruct* pasList = NULL;

    SAH_TRACEZ_IN("DM_ENGINE");

    DM_ENG_DisplayParameterAttributesCache();
    *pResult = NULL;

    if(device_adapter->DM_ENG_Device_GetParameterNames == NULL) {
        goto stop;
    }

    len = DM_ENG_tablen((void**) names);
    SAH_TRACEZ_INFO("DM_ENGINE", "Fetching parameter attributes for %d names", len);

    for(i = 0; i < len; i++) { //for each ellement in list
        SAH_TRACEZ_INFO("DM_ENGINE", "Fetching parameter attributes for %s", names[i]);
        // do a getparameternames to get a list of available parameters in the datamodel
        DM_ENG_ParameterInfoStruct* infoList = NULL;
        DM_ENG_ParameterInfoStruct* ilp = NULL;
        error = device_adapter->DM_ENG_Device_GetParameterNames(names[i], false, &infoList);

        if(error != 0) {
            SAH_TRACEZ_INFO("DM_ENGINE", "Cannot find parameter %s", names[i]);
            DM_ENG_deleteAllParameterAttributesStruct(&pasList);
            break; // we should return the error code
        }

        ilp = infoList;
        while(ilp) {
            ilpLength = 0;
            SAH_TRACEZ_INFO("DM_ENGINE", "Path %s found", ilp->parameterName);
            if(ilp->parameterName != NULL) {
                ilpLength = strlen(ilp->parameterName);
            }
            if(( ilp->parameterName != NULL) && ( ilp->parameterName[ilpLength - 1] != '.')) { //do not list objects in the result
                SAH_TRACEZ_INFO("DM_ENGINE", "It's a parameter, add it to the list");
                // add each ellement to the list
                DM_ENG_NotificationMode nm;
                char** al;
                DM_ENG_GetParameterAttributesCacheEllement(ilp->parameterName, &nm, &al);
                if(nm == DM_ENG_NotificationMode_FORCED) {
                    nm = DM_ENG_NotificationMode_ACTIVE;
                }
                DM_ENG_ParameterAttributesStruct* pas = DM_ENG_newParameterAttributesStruct(ilp->parameterName, nm, al);
                DM_ENG_addParameterAttributesStruct(&pasList, pas);
            }
            ilp = ilp->next;
        }
        DM_ENG_deleteAllParameterInfoStruct(&infoList);
        infoList = NULL;
    }

    if(error == 0) {
        *pResult = DM_ENG_toParameterAttributesStructArray(pasList);
    }

stop:
    SAH_TRACEZ_OUT("DM_ENGINE");
    return error;
}

/**
 * This function is called when the delivery of a schedule inform request was confirmed.
 * It removes the scheduled inform message from the undeliveredScheduleInforms structure
 *
 * @param uniqueID unique id for the schedule inform request
 *
 */
void DM_ENG_ScheduleInformConfirmedDelivery(int uniqueID) {
    DM_ENG_deleteScheduleInformStructUsingUniqueID(&undeliveredScheduleInforms, uniqueID);
}

/**
 *
 * return The external ip address path
 *
 */
const char* DM_ENG_CurrentIGDExternalIPAddressPath() {
    return forcedTR098Parameters[7];
}

/**
 * This function may be used by the ACS to request the CPE to schedule an Inform method call.
 *
 * @param entity Entity number
 * @param delay Delay in seconds
 * @param commandKey Command key string associated with this command
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_ScheduleInform(DM_ENG_EntityType entity, unsigned int delay, const char* commandKey) {
    SAH_TRACEZ_IN("DM_ENGINE");
    (void) entity;

    if(delay == 0) {
        return DM_ENG_INVALID_ARGUMENTS;
    }

    time_t now;
    time(&now);

    DM_ENG_ScheduleInformStruct* is = DM_ENG_newScheduleInformStruct(now + delay, commandKey, 0);

    DM_ENG_acsEventListAddEvent(DM_ENG_EVENT_M_SCHEDULE_INFORM, commandKey);
    DM_ENG_acsEventListUpdate();

    int res = DM_ENG_InformMessageScheduler_scheduleInform(delay, commandKey, is->uniqueID);

    SAH_TRACEZ_OUT("DM_ENGINE");
    return res;
}

/**
 * This function may be used to request the CPE to call the Inform method with the CONNECTION REQUEST event.
 *
 * @param entity Entity number
 *
 * @return 0
 */
int DM_ENG_RequestConnection(DM_ENG_EntityType entity) {
    (void) entity;

    DM_ENG_InformMessageScheduler_requestConnection();

    return 0;
}




/**
 * This function may be used by the ACS to cause the CPE to download the specified file from the designated location.
 *
 * @param entity Entity number
 * @param fileType A string as defined in TR-069
 * @param url URL specifying the source file location
 * @param username Username to be used by the CPE to authenticate with the file server
 * @param password Password to be used by the CPE to authenticate with the file server
 * @param fileSize Size of the file to be downloaded in bytes
 * @param targetFileName Name of the file to be used on the target file system
 * @param delay Delay in seconds to perform the download opération
 * @param successURL URL the CPE may redirect the user's browser to if the download completes successfully
 * @param failureURL URL the CPE may redirect the user's browser to if the download does not complete successfully
 * @param commandKey String used to refer to a particular download
 * @param pResult Pointer to a DM_ENG_TransferResultStruct to obtain the result of the command
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_Download(DM_ENG_EntityType entity, char* fileType, char* url, char* username, char* password, unsigned int fileSize, char* targetFileName,
                    unsigned int delay, char* successURL, char* failureURL, char* commandKey, DM_ENG_TransferResultStruct** pResult) {
    (void) entity;
    int error = 0;
    *pResult = NULL;

    if(device_adapter->DM_ENG_Device_Download == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }

    error = device_adapter->DM_ENG_Device_Download(commandKey, fileType, url, username, password, fileSize, targetFileName, delay, successURL, failureURL);

    DM_ENG_acsEventListAddEvent(DM_ENG_EVENT_M_DOWNLOAD, commandKey);
    DM_ENG_acsEventListUpdate();

    // we only use seperate transfercomplete commands, therefore we reply with unkown time
    *pResult = DM_ENG_newTransferResultStruct(DM_ENG_ParameterStatus_READY_TO_APPLY, 0, 0);

    return error;
}

/**
 * This function may be used by the ACS to cause the CPE to upload the specified file to the designated location.
 *
 * @param entity Entity number
 * @param fileType A string as defined in TR-069
 * @param url URL specifying the destination file location
 * @param username Username to be used by the CPE to authenticate with the file server
 * @param password Password to be used by the CPE to authenticate with the file server
 * @param delay Delay in seconds to perform the upload opération
 * @param commandKey String used to refer to a particular upload
 * @param pResult Pointer to a DM_ENG_TransferResultStruct to obtain the result of the command
 *
 * @return 0 if OK, else a fault code (9001, ...) as specified in TR-069
 */
int DM_ENG_Upload(DM_ENG_EntityType entity, char* fileType, char* url, char* username, char* password, unsigned int delay, char* commandKey, DM_ENG_TransferResultStruct** pResult) {
    (void) entity;
    int error = 0;
    *pResult = NULL;

    if(device_adapter->DM_ENG_Device_Upload == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }

    error = device_adapter->DM_ENG_Device_Upload(commandKey, fileType, url, username, password, delay);

    // we only use seperate transfercomplete commands, therefore we reply with unkown time
    *pResult = DM_ENG_newTransferResultStruct(DM_ENG_ParameterStatus_READY_TO_APPLY, 0, 0);

    DM_ENG_acsEventListAddEvent(DM_ENG_EVENT_M_UPLOAD, commandKey);
    DM_ENG_acsEventListUpdate();

    return error;
}

/**
 * This function may be used by the ACS to cause the CPE to reboot.
 *
 * @param entity Entity number
 * @param commandKey The string to return in the CommandKey element of the InformStruct
 *
 * @return 0
 */
int DM_ENG_Reboot(DM_ENG_EntityType entity, char* commandKey) {
    (void) entity;

    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_REBOOTCOMMANDKEY, commandKey) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not set reboot command key in the datamodel");
    }

    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_REBOOTBYACS, "1") != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not set RebootByACS in the datamodel");
    }


    if(device_adapter->DM_ENG_Device_Reboot == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }

    performReboot = true; // reboot will occur after the current session has ended

    DM_ENG_NotificationInterface_timerStop("periodic-inform-timer");
    DM_ENG_InformMessageScheduler_disableSendInform();

    DM_ENG_acsEventListAddEvent(DM_ENG_EVENT_M_REBOOT, commandKey);
    DM_ENG_acsEventListUpdate();

    return 0;
}

/**
 * This function may be used by the ACS to reset the CPE to its factory default state.
 *
 * @param entity Entity number
 *
 * @return 0
 */
int DM_ENG_FactoryReset(DM_ENG_EntityType entity) {
    (void) entity;
    if(device_adapter->DM_ENG_Device_FactoryReset == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }

    performResetToFactoryDefaults = true;

    return 0;
}

/**
 * This function may be used by the ACS to determine the status of previously requested downloads or uploads.
 *
 * @param entity Entity number
 * @param pResult Pointer to an array of DM_ENG_AllQueuedTransferStruct to obtain the result of the command
 *
 * @return 0 if OK, else a fault code (9000, ...) as specified in TR-069
 */
int DM_ENG_GetAllQueuedTransfers(DM_ENG_EntityType entity, DM_ENG_AllQueuedTransferStruct** pResult[]) {
    (void) entity;
    if(device_adapter->DM_ENG_Device_GetAllQueuedTransfers == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }

    return device_adapter->DM_ENG_Device_GetAllQueuedTransfers(pResult);
}


/**
 * This function may be used by the engine to indicate a transfercomplete message was succesfully send to the ACS.
 *
 * @param entity Entity number
 * @param pResult Pointer to an array of DM_ENG_AllQueuedTransferStruct to obtain the result of the command
 *
 * @return 0 if OK, else a fault code (9000, ...) as specified in TR-069
 */
int DM_ENG_TransferDoneAcknowledged(DM_ENG_EntityType entity, char* uniqueid) {
    (void) entity;
    if(device_adapter->DM_ENG_Device_TransferDoneAcknowledged == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }

    return device_adapter->DM_ENG_Device_TransferDoneAcknowledged(uniqueid);
}


/**
 * This function may be used by the ACS get the management server values
 *
 * @param entity Entity number
 * @param parameter Parameter enum you are interested in
 * @param pResult Parameter value
 *
 * @return 0 if OK,  else a fault code (9000, ...) as specified in TR-069
 */
int DM_ENG_GetManagementServerValue(DM_ENG_EntityType entity, DM_ENG_SystemParameter_t parameter, char** pResult) {
    if(entity != DM_ENG_EntityType_SYSTEM) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Only system calls are authorized to fetch management server values this way");
        return 9000;
    }
    if(device_adapter->DM_ENG_Device_GetConfigValue == NULL) {
        return 9000;
    }
    return device_adapter->DM_ENG_Device_GetConfigValue(parameter, pResult);
}


/**
 * This function may be used by the ACS get the management server values
 *
 * @param entity Entity number
 * @param parameter Parameter enum you are interested in
 * @param pResult Parameter value
 *
 * @return 0 if OK,  else a fault code (9000, ...) as specified in TR-069
 */
int DM_ENG_SetManagementServerValue(DM_ENG_EntityType entity, DM_ENG_SystemParameter_t parameter, const char* pValue) {

    if(entity != DM_ENG_EntityType_SYSTEM) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Only system calls are authorized to set management server values this way");
        return 9000;
    }

    if(device_adapter->DM_ENG_Device_SetConfigValue == NULL) {
        return 9000;
    }

    return device_adapter->DM_ENG_Device_SetConfigValue(parameter, pValue);
}

/**
 * This function may be used by the ACS to execute a management server function
 *
 * @param entity Entity number
 * @param function function enum you are interested in
 *
 * @return 0 if OK,  else a fault code (9000, ...) as specified in TR-069
 */
int DM_ENG_ExecuteManagementServerFunction(DM_ENG_EntityType entity, DM_ENG_SystemFunction_t function) {

    if(entity != DM_ENG_EntityType_SYSTEM) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Only system calls are authorized to execute management server functions this way");
        return 9000;
    }

    if(device_adapter->DM_ENG_Device_ExecuteConfigFunction == NULL) {
        return 9000;
    }

    return device_adapter->DM_ENG_Device_ExecuteConfigFunction(function);
}


bool DM_ENG_isDiagnosticRequestPending(unsigned int diagnosticRequest) {

    return pendingDiagnosticRequests & diagnosticRequest;
}

void DM_ENG_setPendingDiagnosticRequest(unsigned int diagnosticRequest) {

    pendingDiagnosticRequests |= diagnosticRequest;
}

void DM_ENG_clearPendingDiagnosticRequest(unsigned int diagnosticRequest) {

    pendingDiagnosticRequests &= ~diagnosticRequest;
}

unsigned int DM_ENG_getAllPendingDiagnosticRequests() {

    return pendingDiagnosticRequests;
}

void DM_ENG_setAllPendingDiagnosticRequests(unsigned int pendingDiagnosticRequestsState) {

    pendingDiagnosticRequests = pendingDiagnosticRequestsState;
}

int DM_ENG_DUStateChangeCompleteAcknowledged(DM_ENG_EntityType entity, const char* path) {
    (void) entity;
    if(device_adapter->DM_ENG_Device_DUStateChangeCompleteAcknowledged == NULL) {
        return DM_ENG_METHOD_NOT_SUPPORTED;
    }
    return device_adapter->DM_ENG_Device_DUStateChangeCompleteAcknowledged(path);
}
