/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_EventStruct.c
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_EventStruct.c
 *
 * @brief Definition of the event which must be delivered to the ACS
 *
 **/
#include <stdio.h>
#include <dmengine/DM_ENG_EventStruct.h>
#include <dmengine/DM_ENG_Common.h>
#include <dmengine/DM_ENG_RPCInterface.h>
#include <string.h>
#include <debug/sahtrace.h>

#define ME "DM_ENGINE"

const char* DM_ENG_EVENT_BOOTSTRAP = "0 BOOTSTRAP";
const char* DM_ENG_EVENT_BOOT = "1 BOOT";
const char* DM_ENG_EVENT_PERIODIC = "2 PERIODIC";
const char* DM_ENG_EVENT_SCHEDULED = "3 SCHEDULED";
const char* DM_ENG_EVENT_VALUE_CHANGE = "4 VALUE CHANGE";
const char* DM_ENG_EVENT_KICKED = "5 KICKED";
const char* DM_ENG_EVENT_CONNECTION_REQUEST = "6 CONNECTION REQUEST";
const char* DM_ENG_EVENT_TRANSFER_COMPLETE = "7 TRANSFER COMPLETE";
const char* DM_ENG_EVENT_DIAGNOSTICS_COMPLETE = "8 DIAGNOSTICS COMPLETE";
const char* DM_ENG_EVENT_REQUEST_DOWNLOAD = "9 REQUEST DOWNLOAD";
const char* DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE = "10 AUTONOMOUS TRANSFER COMPLETE";
const char* DM_ENG_EVENT_HEARTBEAT = "14 HEARTBEAT";
const char* DM_ENG_EVENT_M_REBOOT = "M Reboot";
const char* DM_ENG_EVENT_M_SCHEDULE_INFORM = "M ScheduleInform";
const char* DM_ENG_EVENT_M_DOWNLOAD = "M Download";
const char* DM_ENG_EVENT_M_SCHEDULEDOWNLOAD = "M ScheduleDownload";
const char* DM_ENG_EVENT_M_CHANGEDUSTATE = "M ChangeDUState";
const char* DM_ENG_EVENT_DU_STATE_CHANGE_COMPLETE = "11 DU STATE CHANGE COMPLETE";
const char* DM_ENG_EVENT_M_UPLOAD = "M Upload";


static char* acsEventList = NULL;
static char* deliveredEventList = NULL;


static const char* _internEventCode(const char* ec) {
    return ((strcmp(ec, DM_ENG_EVENT_BOOTSTRAP) == 0) ? DM_ENG_EVENT_BOOTSTRAP
            : (strcmp(ec, DM_ENG_EVENT_BOOT) == 0) ? DM_ENG_EVENT_BOOT
            : (strcmp(ec, DM_ENG_EVENT_PERIODIC) == 0) ? DM_ENG_EVENT_PERIODIC
            : (strcmp(ec, DM_ENG_EVENT_SCHEDULED) == 0) ? DM_ENG_EVENT_SCHEDULED
            : (strcmp(ec, DM_ENG_EVENT_VALUE_CHANGE) == 0) ? DM_ENG_EVENT_VALUE_CHANGE
            : (strcmp(ec, DM_ENG_EVENT_KICKED) == 0) ? DM_ENG_EVENT_KICKED
            : (strcmp(ec, DM_ENG_EVENT_CONNECTION_REQUEST) == 0) ? DM_ENG_EVENT_CONNECTION_REQUEST
            : (strcmp(ec, DM_ENG_EVENT_TRANSFER_COMPLETE) == 0) ? DM_ENG_EVENT_TRANSFER_COMPLETE
            : (strcmp(ec, DM_ENG_EVENT_DIAGNOSTICS_COMPLETE) == 0) ? DM_ENG_EVENT_DIAGNOSTICS_COMPLETE
            : (strcmp(ec, DM_ENG_EVENT_REQUEST_DOWNLOAD) == 0) ? DM_ENG_EVENT_REQUEST_DOWNLOAD
            : (strcmp(ec, DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE) == 0) ? DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE
            : (strcmp(ec, DM_ENG_EVENT_HEARTBEAT) == 0) ? DM_ENG_EVENT_HEARTBEAT
            : (strcmp(ec, DM_ENG_EVENT_M_REBOOT) == 0) ? DM_ENG_EVENT_M_REBOOT
            : (strcmp(ec, DM_ENG_EVENT_M_SCHEDULE_INFORM) == 0) ? DM_ENG_EVENT_M_SCHEDULE_INFORM
            : (strcmp(ec, DM_ENG_EVENT_M_DOWNLOAD) == 0) ? DM_ENG_EVENT_M_DOWNLOAD
            : (strcmp(ec, DM_ENG_EVENT_M_SCHEDULEDOWNLOAD) == 0) ? DM_ENG_EVENT_M_SCHEDULEDOWNLOAD
            : (strcmp(ec, DM_ENG_EVENT_M_CHANGEDUSTATE) == 0) ? DM_ENG_EVENT_M_CHANGEDUSTATE
            : (strcmp(ec, DM_ENG_EVENT_DU_STATE_CHANGE_COMPLETE) == 0) ? DM_ENG_EVENT_DU_STATE_CHANGE_COMPLETE
            : (strcmp(ec, DM_ENG_EVENT_M_UPLOAD) == 0) ? DM_ENG_EVENT_M_UPLOAD : strdup(ec));
}

static bool _isInternEventCode(const char* ec) {
    return ((ec == DM_ENG_EVENT_BOOTSTRAP)
            || (ec == DM_ENG_EVENT_BOOT)
            || (ec == DM_ENG_EVENT_PERIODIC)
            || (ec == DM_ENG_EVENT_SCHEDULED)
            || (ec == DM_ENG_EVENT_VALUE_CHANGE)
            || (ec == DM_ENG_EVENT_KICKED)
            || (ec == DM_ENG_EVENT_CONNECTION_REQUEST)
            || (ec == DM_ENG_EVENT_TRANSFER_COMPLETE)
            || (ec == DM_ENG_EVENT_DIAGNOSTICS_COMPLETE)
            || (ec == DM_ENG_EVENT_REQUEST_DOWNLOAD)
            || (ec == DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE)
            || (ec == DM_ENG_EVENT_HEARTBEAT)
            || (ec == DM_ENG_EVENT_M_REBOOT)
            || (ec == DM_ENG_EVENT_M_SCHEDULE_INFORM)
            || (ec == DM_ENG_EVENT_M_DOWNLOAD)
            || (ec == DM_ENG_EVENT_M_SCHEDULEDOWNLOAD)
            || (ec == DM_ENG_EVENT_M_CHANGEDUSTATE)
            || (ec == DM_ENG_EVENT_DU_STATE_CHANGE_COMPLETE)
            || (ec == DM_ENG_EVENT_M_UPLOAD));
}

/**
 * Creates a new event struct
 *
 * @param ec An event code
 * @param ck A command key
 *
 * @return A pointer to the struct newly created
 */
DM_ENG_EventStruct* DM_ENG_newEventStruct(const char* ec, char* ck) {
    DM_ENG_EventStruct* res = (DM_ENG_EventStruct*) malloc(sizeof(DM_ENG_EventStruct));
    res->eventCode = _internEventCode(ec);
    res->commandKey = (ck == NULL ? NULL : strdup(ck));
    res->next = NULL;
    return res;
}

/**
 * Deletes the struct
 *
 * @param event An event struct
 */
void DM_ENG_deleteEventStruct(DM_ENG_EventStruct* event) {
    if(!_isInternEventCode(event->eventCode)) {
        free((char*) event->eventCode);
    }
    free(event->commandKey);
    free(event);
}

/**
 * Deletes all the struct in the list
 *
 * @param pEvent A pointer to the firt struct element in the list
 */
void DM_ENG_deleteAllEventStruct(DM_ENG_EventStruct** pEvent) {
    DM_ENG_EventStruct* event = *pEvent;
    while(event != NULL) {
        DM_ENG_EventStruct* ev = event;
        event = event->next;
        DM_ENG_deleteEventStruct(ev);
    }
    *pEvent = NULL;
}

/**
 * Allocates memory for an array of EventStruct
 *
 * @param size Size of the array
 *
 * @return A pointer to the array newly created
 */
DM_ENG_EventStruct** DM_ENG_newTabEventStruct(int size) {
    return (DM_ENG_EventStruct**) calloc(size + 1, sizeof(DM_ENG_EventStruct*));
}

/**
 * Deletes an array of EventStruct
 *
 * @param tEvent The array to delete
 */
void DM_ENG_deleteTabEventStruct(DM_ENG_EventStruct* tEvent[]) {
    int i = 0;
    while(tEvent[i] != NULL) {
        DM_ENG_deleteEventStruct(tEvent[i++]);
    }
    free(tEvent);
}

/**
 * Adds an element to the list of EventStruct
 *
 * @param pEvent Pointer to the first element
 * @param newEvt EventStruct to add
 */
void DM_ENG_addEventStruct(DM_ENG_EventStruct** pEvent, DM_ENG_EventStruct* newEvt) {
    DM_ENG_EventStruct* event = *pEvent;

    if(newEvt == NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "No event to add");
        return;
    }

    SAH_TRACEZ_INFO("DM_ENGINE", "Adding event %s to the event list", newEvt->eventCode);

    if(event == NULL) {
        *pEvent = newEvt;
    } else {
        while(event->next != NULL) {
            event = event->next;
        }
        //  SAH_TRACEZ_INFO("DM_ENGINE","Attaching event %s to the end of the list",newEvt->eventCode);
        event->next = newEvt;
    }
}

void DM_ENG_printEventStruct(DM_ENG_EventStruct* events) {
    DM_ENG_EventStruct* event = events;
    if(events == NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "No events in list");
        return;
    }
    do {
        SAH_TRACEZ_INFO("DM_ENGINE", "Event %s", event->eventCode);
        event = event->next;
    } while(event != NULL);

}


int DM_ENG_countEventStruct(DM_ENG_EventStruct* events) {
    DM_ENG_EventStruct* event = events;
    int i = 0;

    if(events == NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "No events in list");
        return i;
    }
    do {
        event = event->next;
        i++;
    } while(event != NULL);
    return i;
}


DM_ENG_EventStruct** DM_ENG_createTabEventStruct(DM_ENG_EventStruct* events) {
    int sizeEl = DM_ENG_countEventStruct(events);
    int i;
    DM_ENG_EventStruct** evt = DM_ENG_newTabEventStruct(sizeEl);
    evt[0] = events;
    for(i = 1; i < sizeEl; i++) {
        evt[i] = evt[i - 1]->next;
    }

    return evt;
}


/* This function removes all transfereventstructs from the given linked list*/
void DM_ENG_removeTransferEventStruct(DM_ENG_EventStruct** pEvent) {
    DM_ENG_EventStruct* event = *pEvent;
    DM_ENG_EventStruct* prev = NULL;

    while(event != NULL) {
        if((strcmp(event->eventCode, DM_ENG_EVENT_TRANSFER_COMPLETE) == 0) ||
           (strcmp(event->eventCode, DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE) == 0) ||
           (strcmp(event->eventCode, DM_ENG_EVENT_M_DOWNLOAD) == 0) ||
           (strcmp(event->eventCode, DM_ENG_EVENT_M_UPLOAD) == 0)) {
            //remove event from list
            if(prev == NULL) {
                *pEvent = event->next;
                DM_ENG_deleteEventStruct(event);
                event = *pEvent;
            } else {
                prev->next = event->next;
                DM_ENG_deleteEventStruct(event);
                event = prev->next;
            }
        } else {
            prev = event;
            event = event->next;
        }
    }
}

DM_ENG_EventStruct* DM_ENG_takeEventStruct(DM_ENG_EventStruct** pEvent, const char* eventCode, const char* commandKey) {
    DM_ENG_EventStruct* event = *pEvent;
    DM_ENG_EventStruct* prev = NULL;

    if(eventCode == NULL) {
        return NULL;
    }

    while(event != NULL) {
        if((strcmp(event->eventCode, eventCode) == 0) && (( commandKey == NULL) || (( commandKey != NULL) && ( strcmp(event->commandKey, commandKey) == 0)))) {
            //remove event from list
            if(prev == NULL) {
                *pEvent = event->next;
            } else {
                prev->next = event->next;
            }
            event->next = NULL;
            return event;
        } else {
            prev = event;
            event = event->next;
        }
    }
    return NULL;
}



static bool DM_ENG_eventListAddEvent(char** list, const char* event, const char* cmdkey) {
    char* buf, * tmp;

    if(!event || !list) {
        return false;
    }

    // construct the event
    if(cmdkey) {
        if(asprintf(&buf, "%s(%s)", event, cmdkey) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not allocate memory");
        }
    } else {
        if(asprintf(&buf, "%s", event) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not allocate memory");
        }
    }
    // make sure the event is not yet in there
    if(*list) {
        if(strstr(*list, buf) != NULL) {
            //event already in there
            //  SAH_TRACEZ_INFO("DM_ENGINE","event already there");
            free(buf);
            return false;
        }
    } else {
        //  SAH_TRACEZ_INFO("DM_ENGINE","adding to list as first item");
        *list = buf;
        return true;
    }
    // SAH_TRACEZ_INFO("DM_ENGINE","adding to list as next item");
    tmp = *list;
    if(asprintf(list, "%s;%s", tmp, buf) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not allocate memory");
    }
    free(tmp);
    free(buf);


    return true;
}

bool DM_ENG_acsEventListAddEvent(const char* event, const char* cmdkey) {
    SAH_TRACEZ_INFO("DM_ENGINE", "Adding to acs event list %s (%s) [%s]", event ? event : "NULL", cmdkey ? cmdkey : "NULL", acsEventList ? acsEventList : "NULL");
    return DM_ENG_eventListAddEvent(&acsEventList, event, cmdkey);
}

bool DM_ENG_deliveredEventListAddEvent(const char* event, const char* cmdkey) {
    SAH_TRACEZ_INFO("DM_ENGINE", "Adding to acs event list %s (%s) [%s]", event ? event : "NULL", cmdkey ? cmdkey : "NULL", deliveredEventList ? deliveredEventList : "NULL");
    return DM_ENG_eventListAddEvent(&deliveredEventList, event, cmdkey);
}

static bool DM_ENG_eventListRemoveEvent(char** list, const char* event, const char* cmdkey) {
    char* buf, * tmp;
    size_t lenEventList;
    size_t lenEvent;

    if(!event || !list || !*list) {
        return false;
    }

    // construct the event
    if(cmdkey) {
        if(asprintf(&buf, "%s(%s)", event, cmdkey) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not allocate memory");
        }
    } else {
        if(asprintf(&buf, "%s", event) < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not allocate memory");
        }
    }
    tmp = strstr(*list, buf);
    if(tmp == NULL) {
        //event not in there
        // SAH_TRACEZ_INFO("DM_ENGINE","event not in there");
        free(buf);
        return false;
    }

    //  SAH_TRACEZ_INFO("DM_ENGINE","event in there");
    lenEventList = strlen(*list);
    lenEvent = strlen(buf);

    if((lenEventList == lenEvent) || (lenEventList == lenEvent + 1)) {
        //SAH_TRACEZ_INFO("DM_ENGINE","last event in list, cleanup list");
        free(*list);
        *list = NULL;
    } else {
        if(tmp + lenEvent == *list + lenEventList) {
            // last event, just put \0
            // SAH_TRACEZ_INFO("DM_ENGINE","last event, put 0");
            *tmp = '\0';
        } else {
            // +1 due to ";" 1+ due to '\0'
            // SAH_TRACEZ_INFO("DM_ENGINE","last event, memmove");
            memmove(tmp, tmp + lenEvent + 1, 1 + (*list + lenEventList) - (tmp + lenEvent + 1));
        }
    }

    free(buf);
    return true;
}

bool DM_ENG_acsEventListRemoveEvent(const char* event, const char* cmdkey) {
    SAH_TRACEZ_INFO("DM_ENGINE", "Removing from acs event list %s (%s) [%s]", event ? event : "NULL", cmdkey ? cmdkey : "NULL", acsEventList ? acsEventList : "NULL");
    return DM_ENG_eventListRemoveEvent(&acsEventList, event, cmdkey);
}

bool DM_ENG_deliveredEventListRemoveEvent(const char* event, const char* cmdkey) {
    SAH_TRACEZ_INFO("DM_ENGINE", "Removing from acs event list %s (%s) [%s]", event ? event : "NULL", cmdkey ? cmdkey : "NULL", deliveredEventList ? deliveredEventList : "NULL");
    return DM_ENG_eventListRemoveEvent(&deliveredEventList, event, cmdkey);
}

void DM_ENG_acsEventListCleanup() {
    SAH_TRACEZ_INFO("DM_ENGINE", "Cleaning up acs event list");
    free(acsEventList);
    acsEventList = NULL;
}

void DM_ENG_deliveredEventListCleanup() {
    SAH_TRACEZ_INFO("DM_ENGINE", "Cleaning up delivered event list");
    free(deliveredEventList);
    deliveredEventList = NULL;
}


static bool DM_ENG_eventListUpdate(DM_ENG_SystemParameter_t param, char* list) {
    // update event list
    if(list) {
        if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, param, list) != 0) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Could not update event list in the datamodel");
            return false;
        }
        return true;
    }
    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, param, list) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not update event list in the datamodel");
        return false;
    }
    return true;
}

bool DM_ENG_acsEventListUpdate() {
    return DM_ENG_eventListUpdate(DM_ENG_ACSEVENTS, acsEventList);
}

bool DM_ENG_deliveredEventListUpdate() {
    return DM_ENG_eventListUpdate(DM_ENG_DELIVEREDEVENTS, deliveredEventList);
}
