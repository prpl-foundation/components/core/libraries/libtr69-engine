/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_DeviceIdStruct.c
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_DeviceIdStruct.c
 *
 * @brief Structure that uniquely identifies the CPE. Used in Inform Messages.
 *
 **/

#include <dmengine/DM_ENG_DeviceIdStruct.h>
#include <dmengine/DM_ENG_RPCInterface.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

DM_ENG_DeviceIdStruct* DM_ENG_newDeviceIdStruct() {
    DM_ENG_DeviceIdStruct* res = (DM_ENG_DeviceIdStruct*) malloc(sizeof(DM_ENG_DeviceIdStruct));
    char* tmp = NULL;

    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_MANUFACTURER, &tmp) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the device manufacturer");
    }
    res->manufacturer = tmp;

    tmp = NULL;
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_MANUFACTUREROUI, &tmp) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the device OUI");
    }
    res->OUI = tmp;

    tmp = NULL;
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PRODUCTCLASS, &tmp) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the device productClass");
    }
    res->productClass = tmp;

    tmp = NULL;
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_SERIALNUMBER, &tmp) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the device serialNumber");
    }
    res->serialNumber = tmp;

    return res;
}

void DM_ENG_deleteDeviceIdStruct(DM_ENG_DeviceIdStruct* id) {
    if(id == NULL) {
        return;
    }
    if(id->manufacturer != NULL) {
        free(id->manufacturer);
    }
    if(id->OUI != NULL) {
        free(id->OUI);
    }
    if(id->productClass != NULL) {
        free(id->productClass);
    }
    if(id->serialNumber != NULL) {
        free(id->serialNumber);
    }
    free(id);
}
