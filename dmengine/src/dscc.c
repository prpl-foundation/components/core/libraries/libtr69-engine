#include <dmengine/dscc.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <string.h>
#include <stdlib.h>

#define ME "DM_ENGINE"

static time_t amxc_tc_to_time_utc(const amxc_ts_t* tsp) {
    time_t retval = -1;
    struct tm tmp = {0};
    when_failed_trace(amxc_ts_to_tm_utc(tsp, &tmp), stop, ERROR, "Failed to convert timestamp");
    retval = mktime(&tmp);
stop:
    return retval;
}

static dscc_result_t* dscc_result_from_var(amxc_var_t* var) {
    bool error = true;
    const char* currentState = NULL;
    const char* deploymentUnitRef = NULL;
    const char* executionUnitRefList = NULL;
    const char* uuid = NULL;
    const char* version = NULL;
    uint32_t faultCode = 0;
    const char* faultString = NULL;
    bool resolved = false;
    amxc_ts_t* startTime = NULL;
    amxc_ts_t* completeTime = NULL;
    dscc_result_t* result = NULL;

    currentState = GET_CHAR(var, "CurrentState");
    when_null_trace(currentState, stop, ERROR, "Invalid CurrentState");
    deploymentUnitRef = GET_CHAR(var, "DeploymentUnitRef");
    when_null_trace(deploymentUnitRef, stop, ERROR, "Invalid DeploymentUnitRef");
    executionUnitRefList = GET_CHAR(var, "ExecutionUnitRefList");
    when_null_trace(executionUnitRefList, stop, ERROR, "Invalid ExecutionUnitRefList");
    uuid = GET_CHAR(var, "UUID");
    when_null_trace(uuid, stop, ERROR, "Invalid UUID");
    version = GET_CHAR(var, "Version");
    when_null_trace(version, stop, ERROR, "Invalid Version");
    faultString = GET_CHAR(var, "FaultString");
    when_null_trace(faultString, stop, ERROR, "Invalid FaultString");
    faultCode = GET_UINT32(var, "FaultCode");
    resolved = GET_BOOL(var, "Resolved");
    startTime = amxc_var_dyncast(amxc_ts_t, GET_ARG(var, "StartTime"));
    when_null_trace(startTime, stop, ERROR, "Invalid StartTime");
    completeTime = amxc_var_dyncast(amxc_ts_t, GET_ARG(var, "CompleteTime"));
    when_null_trace(completeTime, stop, ERROR, "Invalid CompleteTime");

    result = (dscc_result_t*) calloc(1, sizeof(dscc_result_t));

    result->currentState = strdup(currentState);
    result->deploymentUnitRef = strdup(deploymentUnitRef);
    result->executionUnitRefList = strdup(executionUnitRefList);
    result->uuid = strdup(uuid);
    result->version = strdup(version);
    result->faultString = strdup(faultString);
    result->faultCode = faultCode;
    result->resolved = resolved;
    result->startTime = amxc_tc_to_time_utc(startTime);
    result->completeTime = amxc_tc_to_time_utc(completeTime);

    error = false;
stop:
    if(error) {
        free(result);
        result = NULL;
    }
    free(startTime);
    free(completeTime);
    return result;
}

dscc_t* dscc_new(amxc_var_t* results, const char* commandKey, const char* path) {
    dscc_t* entry = (dscc_t*) calloc(1, sizeof(dscc_t));
    entry->commandKey = strdup(commandKey);
    entry->path = strdup(path);
    amxc_llist_init(&(entry->results));
    amxc_var_for_each(var, results) {
        amxc_var_dump(var, STDOUT_FILENO);
        dscc_result_t* result = dscc_result_from_var(var);
        if(result != NULL) {
            amxc_llist_append(&entry->results, &result->it);
        }
    }
    return entry;
}

static void dscc_result_delete_func(amxc_llist_it_t* it) {
    dscc_result_t* result = amxc_container_of(it, dscc_result_t, it);
    amxc_llist_it_take(it);
    free(result->currentState);
    free(result->deploymentUnitRef);
    free(result->executionUnitRefList);
    free(result->uuid);
    free(result->version);
    free(result->faultString);
    free(result);
}

void dscc_delete(dscc_t** entry) {
    when_null_trace(entry, stop, ERROR, "Invalid entry");
    free((*entry)->commandKey);
    free((*entry)->path);
    amxc_llist_clean(&(*entry)->results, dscc_result_delete_func);
    free(*entry);
    *entry = NULL;
stop:
    return;
}

void dscc_print(amxc_llist_t* data) {
    dscc_t* entry = NULL;
    dscc_result_t* result = NULL;
    amxc_llist_for_each(lit1, data) {
        entry = amxc_container_of(lit1, dscc_t, it);
        SAH_TRACEZ_INFO(ME, "Path [%s] - CommandKey [%s]", entry->path, entry->commandKey);
        amxc_llist_for_each(lit2, &entry->results) {
            result = amxc_container_of(lit2, dscc_result_t, it);
            SAH_TRACEZ_INFO(ME, "CurrentState [%s]", result->currentState);
            //TODO: add the other fields
        }
    }
}
