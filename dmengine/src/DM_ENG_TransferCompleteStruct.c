/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *---------------------------------------------------------------------------
 * File        : DM_ENG_TransferCompleteStruct.c
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_TransferCompleteStruct.c
 *
 * @brief TransferComplete data.
 *
 * A structure representing the completion (either successful or unsuccessful) of a file transfer initiated
 * by an earlier method call.
 */

#include <dmengine/DM_ENG_TransferCompleteStruct.h>
#include <dmengine/DM_ENG_Common.h>
#include <debug/sahtrace.h>

#include <string.h>
#include <stdio.h>



DM_ENG_TransferCompleteStruct* DM_ENG_newTransferCompleteStruct(bool autonomousTransfer, const char* announceURL, const char* transferURL, const char* fileType,
                                                                unsigned int fileSize, const char* targetFileName, bool isDownload, bool isScheduleDownload, const char* commandKey,
                                                                unsigned int faultCode, const char* faultString, time_t startTime, time_t completeTime,
                                                                const char* uniqueID) {
    DM_ENG_TransferCompleteStruct* res = (DM_ENG_TransferCompleteStruct*) calloc(1, sizeof(DM_ENG_TransferCompleteStruct));
    res->autonomousTransfer = autonomousTransfer;
    res->announceURL = (announceURL == NULL ? NULL : strdup(announceURL));
    res->transferURL = (transferURL == NULL ? NULL : strdup(transferURL));
    res->fileType = (fileType == NULL ? NULL    : strdup(fileType));
    res->fileSize = fileSize;
    res->targetFileName = (targetFileName == NULL ? NULL : strdup(targetFileName));
    res->isDownload = isDownload;
    res->commandKey = (commandKey == NULL ? NULL : strdup(commandKey));
    res->faultCode = faultCode;
    /*A.3.3.2 FaultString:
       A human-readable text description of the fault. This field SHOULD be empty if the
       FaultCode equals 0 (zero). */
    if(faultCode == 0) {
        res->faultString = strdup("");
    } else {
        res->faultString = (faultString == NULL ? strdup("") : strdup(faultString));
    }
    res->startTime = startTime;
    res->completeTime = completeTime;
    res->next = NULL;
    res->uniqueID = (uniqueID == NULL ? NULL : strdup(uniqueID));
    res->isScheduleDownload = isScheduleDownload;
    return res;
}

void DM_ENG_deleteTransferCompleteStruct(DM_ENG_TransferCompleteStruct* tcs) {
    free(tcs->commandKey);
    free(tcs->announceURL);
    free(tcs->transferURL);
    free(tcs->fileType);
    free(tcs->targetFileName);
    free(tcs->faultString);
    free(tcs->uniqueID);
    free(tcs);
}

void DM_ENG_addTransferCompleteStruct(DM_ENG_TransferCompleteStruct** pTcs, DM_ENG_TransferCompleteStruct* newTcs) {
    DM_ENG_TransferCompleteStruct* tcs = *pTcs;
    if(tcs == NULL) {
        *pTcs = newTcs;
    } else {
        while(tcs->next != NULL) {
            if(tcs->uniqueID && newTcs->uniqueID && ( strcmp(tcs->uniqueID, newTcs->uniqueID) == 0)) {
                break;
            }
            tcs = tcs->next;
        }
        if(( tcs->next != NULL) || (( *pTcs == tcs) && tcs->uniqueID && newTcs->uniqueID && ( strcmp(tcs->uniqueID, newTcs->uniqueID) == 0))) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Object %s already exists", newTcs->uniqueID);
        } else {
            tcs->next = newTcs;
        }
    }
}

void DM_ENG_deleteAllTransferCompleteStruct(DM_ENG_TransferCompleteStruct** pTcs) {
    DM_ENG_TransferCompleteStruct* tcs = *pTcs;
    while(tcs != NULL) {
        DM_ENG_TransferCompleteStruct* tc = tcs;
        tcs = tcs->next;
        DM_ENG_deleteTransferCompleteStruct(tc);
    }
    *pTcs = NULL;
}

DM_ENG_TransferCompleteStruct* DM_ENG_takeTransferCompleteStruct(DM_ENG_TransferCompleteStruct** pTcs, bool autonomous, bool isDownload, const char* commandKey) {
    DM_ENG_TransferCompleteStruct* tcs = *pTcs;
    DM_ENG_TransferCompleteStruct* prev = NULL;

    while(tcs != NULL) {
        if(( tcs->autonomousTransfer == autonomous) && ( tcs->isDownload == isDownload) && (( commandKey == NULL) || (( commandKey != NULL) && ( strcmp(tcs->commandKey, commandKey) == 0)))) {
            //remove tcs from list
            if(prev == NULL) {
                *pTcs = tcs->next;
            } else {
                prev->next = tcs->next;
            }
            tcs->next = NULL;
            return tcs;
        } else {
            prev = tcs;
            tcs = tcs->next;
        }
    }
    return NULL;
}
