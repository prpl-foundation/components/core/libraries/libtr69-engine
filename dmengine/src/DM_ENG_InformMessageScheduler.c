/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_InformMessageScheduler.c
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_InformMessageScheduler.c
 *
 * @brief
 *
 **/

#include <dmcommon/DM_GlobalDefs.h>
#include <dmengine/DM_ENG_InformMessageScheduler.h>
#include <dmengine/DM_ENG_EventStruct.h>
#include <dmengine/DM_ENG_Error.h>
#include <dmengine/DM_ENG_NotificationInterface.h>
#include <dmengine/DM_ENG_DownloadRequest.h>
#include <dmengine/DM_ENG_ScheduleInformStruct.h>
#include <dmengine/DM_ENG_ParameterAttributesCache.h>
#include <dmengine/DM_ENG_Device.h>
#include <dmengine/dscc.h>

#include <sys/timeb.h>
#include <limits.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <stdlib.h>
#include <errno.h>
#include <amxc/amxc.h>

#define HEARTBEAT_INFORM_TIMER "heartbeat-inform-timer"
#define HEARTBEAT_REPORTINGINTERVAL_MIN_VALUE 30
#define DEFAULT_PERIODICINFORMTIME_INTERVAL 300


static int retryCount = 0;


typedef struct _DM_ENG_InformMessageScheduler_data_t {
    DM_ENG_EventStruct* events;
    DM_ENG_ParameterValueStruct* parameters;
    DM_ENG_TransferCompleteStruct* transferCompleteMessages;
    DM_ENG_DownloadRequest* downloadRequests;
    DM_ENG_ScheduleInformStruct* scheduleInforms;
    amxc_llist_t dscc; // dscc stands for DUStateChangeComplete
} DM_ENG_InformMessageScheduler_data_t;


static DM_ENG_InformMessageScheduler_data_t* sessionData = NULL;
static DM_ENG_InformMessageScheduler_data_t* queuedData = NULL;
static bool sessionOngoing = false;
static bool disableSendInform = false;
static bool triggerInformAfterSessionEnd = false;
static bool triggerActiveNotificationInformAfterSessionEnd = false;
static bool triggerTr111ActiveNotificationInformAfterSessionEnd = false;
static bool nextSessionContainsTr111ActiveNotification = false;
static bool tr111triggeredSession = false;
static bool retryTriggeredSession = false;
static time_t firstSessionTime = 0;
static time_t lastSessionTime = 0;
static time_t lastTR111SessionTime = 0;
static char* acsSupportedRPCs[6];
static bool heartbeatTriggered = false;

typedef struct _lastSession_t {
    bool synchronized;
    time_t time;
    struct timespec clock_start;
    bool ignore_time_synchronization_notif; // used to control if time synchronization notification should be ignored or not
} lastSession_t;

static lastSession_t lastSession = {.synchronized = false, .time = 0, .clock_start = {.tv_sec = 0, .tv_nsec = 0}, .ignore_time_synchronization_notif = true};

static void update_lastsession_time(const time_t* lastsession) {
    char tmp[256] = "";
    struct tm* t = (struct tm*) calloc(1, sizeof(struct tm));
    gmtime_r(lastsession, t);
    strftime(tmp, sizeof(tmp), "%Y-%m-%dT%H:%M:%SZ", t);
    free(t);
    SAH_TRACEZ_INFO("DM_ENGINE", "Updating LastSession to %s (%ds)", tmp, *lastsession);
    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_LASTSESSION, tmp) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not set DM_ENG_LASTSESSION parameter in the datamodel");
    }
}

static void update_failure_counter(bool reset) {

    int failureCount = 0;
    if(reset == false) {
        char* InformFailureCount = NULL;
        if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_INFORMFAILURECOUNT, &InformFailureCount) != 0) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the periodic counter parameter");
        }
        if(InformFailureCount) {
            failureCount = atoi(InformFailureCount);
        }
        failureCount++;
    }
    /* set back to datamodel */
    char* tempBuffer = NULL;
    int bytes = asprintf(&tempBuffer, "%d", failureCount);
    if(bytes == -1) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "asprintf error [%s]", strerror(errno));
    }
    DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_INFORMFAILURECOUNT, tempBuffer);
    free(tempBuffer);
    tempBuffer = NULL;

}

static void update_periodic_inform_counter(bool reset) {
    /* get the periodic inform counter */
    int periodicCounter = 0;

    if(reset == false) {
        char* periodicInformCounter = NULL;
        if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PERIODICINFORMCOUNTER, &periodicInformCounter) != 0) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the periodic counter parameter");
        }
        if(periodicInformCounter) {
            periodicCounter = atoi(periodicInformCounter);
        }
        free(periodicInformCounter);
        periodicCounter++;
    }

    /* set back to datamodel */
    char* tempBuffer = NULL;
    int bytes = asprintf(&tempBuffer, "%d", periodicCounter);
    if(bytes == -1) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "asprintf error [%s]", strerror(errno));
    }
    DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PERIODICINFORMCOUNTER, tempBuffer);
    free(tempBuffer);
    tempBuffer = NULL;
}

void DM_ENG_InformMessageScheduler_time_synchronized_notification() {
    SAH_TRACEZ_INFO("DM_ENGINE", "Notification received: Time synchronized");
    // Ignore notification if time have been synchronized once.
    if(lastSession.synchronized == true) {
        return;
    }

    lastSession.synchronized = true;

    // Synchronization notification could be received at any moment
    // (before that any session has started for example)
    if(lastSession.ignore_time_synchronization_notif == true) {
        return;
    }

    // Adjust time if needed
    if((lastSession.time == 0) && (lastSession.clock_start.tv_sec != 0)) {
        time_t elapsed_time = 0;
        struct timespec clock_stop;

        clock_gettime(CLOCK_MONOTONIC_RAW, &clock_stop);
        elapsed_time = clock_stop.tv_sec - lastSession.clock_start.tv_sec;

        time_t current_time = 0;
        lastSession.time = time(&current_time) - elapsed_time;

        SAH_TRACEZ_INFO("DM_ENGINE", "LastSession Time %ds (adjusted) = %ds (current) - %ds (elapsed) ", lastSession.time, current_time, elapsed_time);
    }

    // update LastSession only if there is no session
    if((sessionOngoing == false) && (lastSession.time != 0)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "No session in progress and Time has been adjusted");
        update_lastsession_time(&(lastSession.time));
    }
}

/*
 * event logic:
 *  - all incoming events are first added to the queuedData
 *  - when an inform message is send, all available queuedData is transferred to sessionData
 *    --> if the inform succeeds:
 *         * sessionData is cleaned up
 *         * queuedData is checked to see if a new inform message has to be send
 *    --> if the inform fails:
 *         * sessionData remains
 *         * on retry, new items in queuedData are added to sessionData
 */


/*
 * Add an event to the event queue
 */
static void DM_ENG_InformMessageScheduler_addQueuedEvent(const char* event, char* commandKey) {
    DM_ENG_EventStruct* es = DM_ENG_newEventStruct(event, commandKey);
    DM_ENG_addEventStruct(&queuedData->events, es);
}


/*
 * Check if an event is already
 */
bool DM_ENG_InformMessageScheduler_containsEvent(DM_ENG_EventStruct* list, const char* eventCode, const char* commandKey) {
    DM_ENG_EventStruct* ptr = list;
    //SAH_TRACEZ_INFO("DM_ENGINE","Checking for event %s (%s) %p",eventCode?eventCode:"NULL",commandKey?commandKey:"NULL",list);
    if(list == NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "empty list");
        return false;
    }
    while(ptr) {
        // SAH_TRACEZ_INFO("DM_ENGINE","Comparing to %s (%s)",ptr->eventCode?ptr->eventCode:"NULL",ptr->commandKey?ptr->commandKey:"NULL");
        if((eventCode && ptr->eventCode && ( strcmp(ptr->eventCode, eventCode) == 0)) && (!commandKey || (commandKey && ptr->commandKey && ( strcmp(ptr->commandKey, commandKey) == 0)))) {
            // SAH_TRACEZ_INFO("DM_ENGINE","found");
            return true;
        }
        ptr = ptr->next;
    }
    // SAH_TRACEZ_INFO("DM_ENGINE","not found");
    return false;
}

/*
 * add an event to the sessionData struct
 */
static void DM_ENG_InformMessageScheduler_addSessionEvent(DM_ENG_EventStruct* es) {
    if(strncmp(es->eventCode, "M ", 2) == 0) {
        /* 3.7.1.5 If an event with “Multiple” cumulative behavior occurs, the new EventCode MUST be included in the
           list of events, independent of any undelivered events of the same type, and this MUST NOT affect any
           such undelivered events. */
        DM_ENG_addEventStruct(&sessionData->events, es);
    } else {
        /* 3.7.1.5 If an event with “Single” cumulative behavior occurs, the list of events in the next Inform MUST
           contain only one instance of this EventCode, regardless of whether there are any undelivered events of
           the same type. */

        if(!DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, es->eventCode, es->commandKey)) {
            // if this is the first time we contact the acs or we rebooted (and inhibitValueChangeUponBoot is on), do not add the VALUE_CHANGED event
            bool inhibitValueChangeUponBootNow = false;
            char* val = NULL;
            if(( DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_INHIBIT_VALUE_CHANGE_UPON_BOOT, &val) == 0) && val && ( atol(val) == 1)) {
                inhibitValueChangeUponBootNow = DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_BOOT, NULL);
            }
            SAH_TRACEZ_INFO("DM_ENGINE", "val %s inhibitValueChangeUponBootNow %d", val, inhibitValueChangeUponBootNow);
            free(val);
            if((DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_BOOTSTRAP, NULL) || inhibitValueChangeUponBootNow) && ( es->eventCode == DM_ENG_EVENT_VALUE_CHANGE)) {
                SAH_TRACEZ_INFO("DM_ENGINE", "DM_ENG_EVENT_VALUE_CHANGE deleted");
                DM_ENG_deleteEventStruct(es);
            } else {
                DM_ENG_addEventStruct(&sessionData->events, es);
            }
        } else {
            DM_ENG_deleteEventStruct(es);
        }
    }
}

static bool DM_ENG_InformMessageScheduler_RemoveEvent(DM_ENG_EventStruct** es, const char* event, const char* commandKey) {
    DM_ENG_EventStruct* ptr = *es;
    DM_ENG_EventStruct* prev = NULL;

    SAH_TRACEZ_INFO("DM_ENGINE", "Removing event %s (%s)", event ? event : "NULL", commandKey ? commandKey : "NULL");
    while(ptr) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Comparing to %s (%s)", ptr->eventCode ? ptr->eventCode : "NULL", ptr->commandKey ? ptr->commandKey : "NULL");
        if((( commandKey == NULL) || (commandKey && ptr->commandKey && ( strcmp(commandKey, ptr->commandKey) == 0))) && ptr->eventCode && event && ( strcmp(ptr->eventCode, event) == 0)) {
            SAH_TRACEZ_INFO("DM_ENGINE", "Event found: remove");
            if(prev == NULL) {
                // event is at the top of the list
                *es = ptr->next;
                DM_ENG_deleteEventStruct(ptr);
            } else {
                //event is in the list
                prev->next = ptr->next;
                DM_ENG_deleteEventStruct(ptr);
            }
            return true;
        }
        prev = ptr;
        ptr = ptr->next;
    }
    return false;
}



static bool DM_ENG_InformMessageScheduler_containsParameter(DM_ENG_ParameterValueStruct* pendingParameters, DM_ENG_ParameterValueStruct* param) {
    DM_ENG_ParameterValueStruct* pvs = pendingParameters;
    while(pvs != NULL) {
        if(( pvs->parameterName != NULL) && ( param->parameterName != NULL) && ( strcmp(pvs->parameterName, param->parameterName) == 0)) {
            /* A 3.3.1 If a parameter has changed more than once since the last successful
               Inform notification, the parameter MUST be listed only once, with only
               the most recent value given. In this case, the parameter MUST be
               included in the ParameterList even if its value has changed back to the
               value it had at the time of the last successful Inform. */

            // parameter found, update the value
            // SAH_TRACEZ_ERROR("DM_ENGINE","Parameter found, updating the value");
            free(pvs->value);
            pvs->value = (param->value == NULL ? NULL : strdup(param->value));

            return true;
        }
        pvs = pvs->next;
    }
    return false;
}

static void DM_ENG_InformMessageScheduler_removeParameter(DM_ENG_ParameterValueStruct** pendingParameters, char* paramName) {
    DM_ENG_ParameterValueStruct* pvs = *pendingParameters;
    DM_ENG_ParameterValueStruct* prev = NULL;
    while(pvs != NULL) {
        if(( pvs->parameterName != NULL) && ( paramName != NULL) && ( strcmp(pvs->parameterName, paramName) == 0)) {
            /* remove the ellement from the list */
            if(prev == NULL) {
                *pendingParameters = pvs->next;
                pvs->next = NULL;
                DM_ENG_deleteParameterValueStruct(pvs);
            } else {
                prev->next = pvs->next;
                pvs->next = NULL;
                DM_ENG_deleteParameterValueStruct(pvs);
            }
            return;
        }
        prev = pvs;
        pvs = pvs->next;
    }
}

bool DM_ENG_InformMessageScheduler_addParameter(DM_ENG_ParameterValueStruct** pendingParameters, DM_ENG_ParameterValueStruct* param) {
    if(DM_ENG_InformMessageScheduler_containsParameter(*pendingParameters, param) == false) {
        // add the parameter to the list
        //SAH_TRACEZ_ERROR("DM_ENGINE","Parameter not in list, adding it");
        DM_ENG_addParameterValueStruct(pendingParameters, param);
        return true;
    } else {
        // SAH_TRACEZ_ERROR("DM_ENGINE","Parameter was already there, deleting struct");
        DM_ENG_deleteParameterValueStruct(param);
        return false;
    }
}

static int DM_ENG_InformMessageScheduler_calcRetryTime() {
    /* 3.2.1.1:
       A CPE MUST retry a failed session after waiting for an interval of time specified in Table 3 or when a new
       event occurs, whichever comes first. The CPE MUST choose the wait interval by randomly selecting a
       number of seconds from a range given by the post-reboot session retry count. When retrying a failed
       session after an intervening reboot, the CPE MUST reset the wait intervals it chooses from as though it
       were making its first session retry attempt. In other words, if a session is retried when a new event other
       than BOOT occurs, it does not reset the wait interval, although the continued occurrence of new events
       might cause sessions to be initiated more frequently than shown in the table. Regardless of the reason a
       previous session failed or the condition prompting session retry, the CPE MUST communicate to the ACS
       the session retry count.
       Beginning with the tenth post-reboot session retry attempt, the CPE MUST choose from a range between
       2560 and 5120 seconds. The CPE MUST continue to retry a failed session until it is successfully
       terminated. Once a session terminates successfully, the CPE MUST reset the session retry count to zero and
       no longer apply session retry policy to determine when to initiate the next session.

       Table 3 – Session Retry Wait Intervals
       Post reboot session     Wait interval range (min-max seconds)
       retry count
     #1                      5-10
     #2                      10-20
     #3                      20-40
     #4                      40-80
     #5                      80-160
     #6                      160-320
     #7                      320-640
     #8                      640-1280
     #9                      1280-2560
     #10 and subsequent      2560-5120 */


    int minimum = 5;
    int maximum = 10;
    int i, iterations;
    if(retryCount > 10) {
        iterations = 10;
    } else {
        iterations = retryCount;
    }

    for(i = 1; i < iterations; i++) {
        minimum = minimum * 2;
        maximum = maximum * 2;
    }
    srand((unsigned int) time(NULL));
    int random_number = (rand() % (maximum - minimum));

    random_number -= 10;
    // on some hardwares (slow), it can happen that the tcp syn
    // is send just above the maximum, to avoid this 10 seconds are substracted
    // of the random number, if the random number is smaller then 0, it is reset to 0
    if(random_number < 0) {
        random_number = 0;
    }

    return minimum + random_number;
}

/* TR98 ManagementServer.PeriodicInformTime:
   An absolute time reference in UTC to determine  when the CPE will initiate the periodic Inform
   method calls. Each Inform call MUST occur at this reference time plus or minus an integer multiple of
   the PeriodicInformInterval. PeriodicInformTime is used only to set the “phase”
   of the periodic Informs. The actual value of PeriodicInformTime can be arbitrarily far into the
   past or future. For example, if PeriodicInformInterval is 86400 (a day) and if PeriodicInformTime is set to UTC
   midnight on some day (in the past, present, or future) then periodic Informs will occur every day at
   UTC midnight. These MUST begin on the very next midnight, even if PeriodicInformTime refers to
   a day in the future. The Unknown Time value defined in section 2.2 indicates that no particular time reference is
   specified. That is, the CPE MAY locally choose the time reference, and needs only to adhere to the
   specified PeriodicInformInterval. If absolute time is not available to the CPE, its periodic Inform behavior MUST
   be the same as if the PeriodicInformTime parameter was set to the Unknown Time value.
 */
static void DM_ENG_InformMessageScheduler_getPeriodicInformTimings(int* initialOffset, int* periodicInterval) {
    char* periodicInformInterval = NULL;
    char* periodicInformTime = NULL;

    // initialize to defaults
    *periodicInterval = DEFAULT_PERIODICINFORMTIME_INTERVAL;
    *initialOffset = DEFAULT_PERIODICINFORMTIME_INTERVAL;

    // get interval from backend
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PERIODICINFORMINTERVAL, &periodicInformInterval) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the periodic time interval, using default");
    } else {
        if(periodicInformInterval) {
            *periodicInterval = atoi(periodicInformInterval);
        }
        free(periodicInformInterval);
    }
    if(*periodicInterval == 0) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Interval was 0, this should not happen");
        *periodicInterval = DEFAULT_PERIODICINFORMTIME_INTERVAL;
    }

    // calculate initial offset
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PERIODICINFORMTIME, &periodicInformTime) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the periodic time, using default");
    } else {
        /*
           REF_TIME_OFFSET_TO_TIME_0 = REF_TIME % INTERVAL
           OFFSET_FROM_LAST_INFORM   = (CUR_TIME - REF_TIME_OFFSET_TO_TIME_0) % INTERVAL
           initialOffset = INTERVAL - OFFSET_FROM_LAST_INFORM
         */
        if(( periodicInformTime == NULL) || (( periodicInformTime != NULL) && ( strcmp(periodicInformTime, "0001-01-01T00:00:00Z") == 0))) {
            /*
               No periodic inform time set, use the first session time as a reference
             */
            *initialOffset = (*periodicInterval) - ((time(NULL) - (firstSessionTime % (*periodicInterval))) % (*periodicInterval));
            SAH_TRACEZ_INFO("DM_ENGINE", "referenceTime=firstinform - currentTime=%ld - Interval=%d - InitialOffset=%d", time(NULL), *periodicInterval, *initialOffset);
        } else {
            struct tm tm_struct;
            time_t ref_time;
            if(strptime(periodicInformTime, "%Y-%m-%dT%H:%M:%SZ", &tm_struct) == NULL) {
                SAH_TRACEZ_WARNING("DM_ENGINE", "Cannot extract the time from the string, using the current time as reference");
                *initialOffset = *periodicInterval;
            } else {
                ref_time = timegm(&tm_struct);
                *initialOffset = (*periodicInterval) - ((time(NULL) - (ref_time % (*periodicInterval))) % (*periodicInterval));
                SAH_TRACEZ_INFO("DM_ENGINE", "referenceTime=%s - currentTime=%ld - Interval=%d - InitialOffset=%d", periodicInformTime, time(NULL), *periodicInterval, *initialOffset);
            }
        }
        free(periodicInformTime);
    }
}

/**
 * Function used to remove a forced notification, this is needed in case of igd.wandevice...
 */
void DM_ENG_InformMessageScheduler_removeForcedParametersFromSession() {
    // add/update the forced parameters
    DM_ENG_ParameterValueStruct* firstParam = NULL;
    DM_ENG_ParameterValueStruct* param = NULL;

    firstParam = DM_ENG_ParameterAttributesCacheGetForcedParams(false, NULL);
    if(!firstParam) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Failed to get the forced parameter values");
    }
    param = firstParam;
    while(param) {
        DM_ENG_InformMessageScheduler_removeParameter(&sessionData->parameters, param->parameterName);
        param = param->next;
    }
    DM_ENG_deleteAllParameterValueStruct(&firstParam);
}


static bool DM_ENG_InformMessageScheduler_blockEvent(const char* blockedEvent, const char* commandKey) {
    SAH_TRACEZ_INFO("DM_ENGINE", "Trying to block event %s %s", blockedEvent ? blockedEvent : "NULL", commandKey ? commandKey : "NULL");
    if(!blockedEvent) {
        return false;
    }
    if((strcmp(blockedEvent, DM_ENG_EVENT_PERIODIC) == 0) ||      // blocking periodics should be done using ManagementServer.PeriodicInformEnable
       ( strcmp(blockedEvent, DM_ENG_EVENT_VALUE_CHANGE) == 0)) { // blocking value changes is currently not implemented
        SAH_TRACEZ_INFO("DM_ENGINE", "Do not block this kind of event");
        return true;
    }
    SAH_TRACEZ_INFO("DM_ENGINE", "Trying to block event %s %s", blockedEvent ? blockedEvent : "NULL", commandKey ? commandKey : "NULL");
    if(!DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, blockedEvent, commandKey)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Event not found in session, continue");
        return true;
    }

    if((strcmp(blockedEvent, DM_ENG_EVENT_DIAGNOSTICS_COMPLETE) == 0) ||
       ( strcmp(blockedEvent, DM_ENG_EVENT_CONNECTION_REQUEST) == 0) ||
       ( strcmp(blockedEvent, DM_ENG_EVENT_BOOTSTRAP) == 0) ||
       ( strcmp(blockedEvent, DM_ENG_EVENT_BOOT) == 0) ||
       ( strcmp(blockedEvent, DM_ENG_EVENT_M_REBOOT) == 0)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Move event from session event to queue event");
        DM_ENG_EventStruct* es = DM_ENG_takeEventStruct(&sessionData->events, blockedEvent, NULL);
        if(es) {
            es->next = queuedData->events;
            queuedData->events = es;
        }
    } else if(strcmp(blockedEvent, DM_ENG_EVENT_REQUEST_DOWNLOAD) == 0) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Removing all request download events and moving sessionData->downloadRequests to queuedData");
        DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_REQUEST_DOWNLOAD, NULL);
        if(sessionData->downloadRequests) {
            sessionData->downloadRequests->next = queuedData->downloadRequests;
            queuedData->downloadRequests = sessionData->downloadRequests;
            sessionData->downloadRequests = NULL;
        }
    } else if((strcmp(blockedEvent, DM_ENG_EVENT_SCHEDULED) == 0) ||
              (( strcmp(blockedEvent, DM_ENG_EVENT_M_SCHEDULE_INFORM) == 0) && ( commandKey == NULL))) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Removing all schedule inform events and moving sessionData->scheduleInforms to queuedData");
        DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_SCHEDULED, NULL);
        while(DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_M_SCHEDULE_INFORM, NULL)) {
        }
        if(sessionData->scheduleInforms) {
            queuedData->scheduleInforms = sessionData->scheduleInforms;
            sessionData->scheduleInforms = NULL;
        }
    } else if(strcmp(blockedEvent, DM_ENG_EVENT_M_SCHEDULE_INFORM) == 0) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Removing specific schedule inform event and moving sessionData->scheduleInforms to queuedData");
        DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_M_SCHEDULE_INFORM, commandKey);
        if(DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_M_SCHEDULE_INFORM, NULL)) {
            SAH_TRACEZ_INFO("DM_ENGINE", "There still is an inform event, take this one and move it to the queue");
            DM_ENG_ScheduleInformStruct* si = DM_ENG_takeScheduleInformStructUsingCommandKey(&sessionData->scheduleInforms, commandKey);
            if(si) {
                si->next = queuedData->scheduleInforms;
                queuedData->scheduleInforms = si;
            }
        } else {
            SAH_TRACEZ_INFO("DM_ENGINE", "No more event, just move all and remove DM_ENG_EVENT_SCHEDULED event");
            DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_SCHEDULED, NULL);
            if(sessionData->scheduleInforms) {
                sessionData->scheduleInforms->next = queuedData->scheduleInforms;
                queuedData->scheduleInforms = sessionData->scheduleInforms;
                sessionData->scheduleInforms = NULL;
            }
        }
    } else if(strcmp(blockedEvent, DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE) == 0) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Removing all autonomous transfer complete event and moving autonomous sessionData->transferCompleteMessages to queuedData");
        DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE, NULL);
        DM_ENG_TransferCompleteStruct* atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, true, false, NULL);
        while(atcs) {
            atcs->next = queuedData->transferCompleteMessages;
            queuedData->transferCompleteMessages = atcs;
            atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, true, false, NULL);
        }
        atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, true, true, NULL);
        while(atcs) {
            atcs->next = queuedData->transferCompleteMessages;
            queuedData->transferCompleteMessages = atcs;
            atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, true, true, NULL);
        }
    } else if(strcmp(blockedEvent, DM_ENG_EVENT_TRANSFER_COMPLETE) == 0) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Removing all transfer complete event and moving sessionData->transferCompleteMessages to queuedData");
        DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_TRANSFER_COMPLETE, NULL);
        while(DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_M_DOWNLOAD, NULL)) {
        }
        while(DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_M_UPLOAD, NULL)) {
        }
        DM_ENG_TransferCompleteStruct* atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, false, NULL);
        while(atcs) {
            atcs->next = queuedData->transferCompleteMessages;
            queuedData->transferCompleteMessages = atcs;
            atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, false, NULL);
        }
        atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, true, NULL);
        while(atcs) {
            atcs->next = queuedData->transferCompleteMessages;
            queuedData->transferCompleteMessages = atcs;
            atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, true, NULL);
        }
    } else if((strcmp(blockedEvent, DM_ENG_EVENT_M_DOWNLOAD) == 0) && (commandKey == NULL)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Removing all download transfer complete event and moving download sessionData->transferCompleteMessages to queuedData");
        while(DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_M_DOWNLOAD, NULL)) {
        }
        DM_ENG_TransferCompleteStruct* atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, true, NULL);
        while(atcs) {
            atcs->next = queuedData->transferCompleteMessages;
            queuedData->transferCompleteMessages = atcs;
            atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, true, NULL);
        }
        if(!DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE, NULL) &&
           !DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_M_DOWNLOAD, NULL) &&
           !DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_M_UPLOAD, NULL)) {
            DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_TRANSFER_COMPLETE, NULL);
        }
    } else if(strcmp(blockedEvent, DM_ENG_EVENT_M_DOWNLOAD) == 0) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Removing specific download transfer complete event and moving specific download sessionData->transferCompleteMessages to queuedData");
        DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_M_DOWNLOAD, commandKey);
        DM_ENG_TransferCompleteStruct* atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, true, commandKey);
        atcs->next = queuedData->transferCompleteMessages;
        queuedData->transferCompleteMessages = atcs;
        if(!DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE, NULL) &&
           !DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_M_DOWNLOAD, NULL) &&
           !DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_M_UPLOAD, NULL)) {
            DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_TRANSFER_COMPLETE, NULL);
        }
    } else if((strcmp(blockedEvent, DM_ENG_EVENT_M_UPLOAD) == 0) && (commandKey == NULL)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Removing all upload transfer complete event and moving upload sessionData->transferCompleteMessages to queuedData");
        while(DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_M_UPLOAD, NULL)) {
        }
        DM_ENG_TransferCompleteStruct* atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, false, NULL);
        while(atcs) {
            atcs->next = queuedData->transferCompleteMessages;
            queuedData->transferCompleteMessages = atcs;
            atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, false, NULL);
        }
        if(!DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE, NULL) &&
           !DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_M_DOWNLOAD, NULL) &&
           !DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_M_UPLOAD, NULL)) {
            DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_TRANSFER_COMPLETE, NULL);
        }
    } else if(strcmp(blockedEvent, DM_ENG_EVENT_M_UPLOAD) == 0) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Removing specific  upload transfer complete event and moving specific upload sessionData->transferCompleteMessages to queuedData");
        DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_M_UPLOAD, commandKey);
        DM_ENG_TransferCompleteStruct* atcs = DM_ENG_takeTransferCompleteStruct(&sessionData->transferCompleteMessages, false, false, commandKey);
        atcs->next = queuedData->transferCompleteMessages;
        queuedData->transferCompleteMessages = atcs;
        if(!DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE, NULL) &&
           !DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_M_DOWNLOAD, NULL) &&
           !DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_M_UPLOAD, NULL)) {
            DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_TRANSFER_COMPLETE, NULL);
        }
    }
    return true;
}



static bool DM_ENG_InformMessageScheduler_blockEvents() {
    char* blockedEventList = NULL;
    char* blockedEvent;
    char* nextBlockedEvent;
    char* cmdKeyStart;
    char* cmdKeyEnd;

    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_BLOCKEDEVENTS, &blockedEventList) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not get blocked events");
        return false;
    }
    if(!blockedEventList) {
        return false;
    }
    if(strlen(blockedEventList) == 0) {
        free(blockedEventList);
        return true;
    }
    SAH_TRACEZ_INFO("DM_ENGINE", "There is something in the blocked list: %s", blockedEventList);


    //loop over the ; seperated list
    blockedEvent = blockedEventList;
    while(blockedEvent) {
        nextBlockedEvent = strchr(blockedEvent, ';');
        if(nextBlockedEvent) {
            *nextBlockedEvent = '\0';
            nextBlockedEvent++;
        }

        // check for (
        cmdKeyStart = strchr(blockedEvent, '(');
        cmdKeyEnd = strchr(blockedEvent, ')');
        if(cmdKeyStart) {
            *cmdKeyStart = '\0';
            cmdKeyStart++;
            *cmdKeyEnd = '\0';
        }

        SAH_TRACEZ_INFO("DM_ENGINE", "Block event %s (%s)", blockedEvent ? blockedEvent : "NULL", cmdKeyStart ? cmdKeyStart : "NULL");

        DM_ENG_InformMessageScheduler_blockEvent(blockedEvent, cmdKeyStart);

        blockedEvent = nextBlockedEvent;
    }

    free(blockedEventList);
    return true;
}

static bool DM_ENG_InformMessageScheduler_createSessionData() {

    bool heartbeatSession = false;

    if(sessionData->events != NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Inform Session (Retried)");
        if(heartbeatTriggered == true) {
            if(DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_HEARTBEAT, NULL) == false) {
                /**
                 * If a HEARTBEAT Event is outstanding or triggered while an Non-HEARTBEAT Inform
                 * is being retried, the CPE MUST wait and allow the Non-HEARTBEAT Inform to
                 * establish an Inform Session. Once the Inform Session for the Non-HEARTBEAT Inform
                 * is completed, a new Inform Session for the HEARTBEAT Event MUST be initiated.
                 */
                SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat: Retrying a Non-HEARTBEAT Inform, HEARTBEAT Inform will be initiated later");
                triggerInformAfterSessionEnd = true;
            } else {
                /**
                 * If a HEARTBEAT Event is triggered while an Inform Session for a previous
                 * HEARTBEAT Event is being retried, the CPE MUST discard the new HEARTBEAT Event.
                 */
                SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat: HEARTBEAT event discarded, another HEARTBEAT Inform session is being retried");
                heartbeatTriggered = false;
            }
        }
    } else {
        SAH_TRACEZ_INFO("DM_ENGINE", "Inform Session (New)");
        if(heartbeatTriggered == true) {
            if((queuedData != NULL) && (queuedData->events != NULL)) {
                /**
                 * If a HEARTBEAT Event is triggered at the same time another Event can trigger an
                 * Inform Session, the Inform Session for the HEARTBEAT Event MUST take precedence
                 * over the Events.
                 */
                SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat: HEARTBEAT event has precedence");
                triggerInformAfterSessionEnd = true;
            }
            DM_ENG_InformMessageScheduler_addSessionEvent(DM_ENG_newEventStruct(DM_ENG_EVENT_HEARTBEAT, NULL));
            heartbeatTriggered = false;
        }
    }

    if(DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_HEARTBEAT, NULL) == true) {
        heartbeatSession = true;
    }

    /*
     * A CPE MUST NOT issue another Event in the same Inform session when a
     * HEARTBEAT Event is issued.
     */
    // add the queued events
    if((queuedData->events != NULL) && (heartbeatSession == false)) {
        DM_ENG_EventStruct* event = queuedData->events;
        DM_ENG_EventStruct* nextEvent = NULL;
        while(event) {
            nextEvent = event->next;
            event->next = NULL;
            DM_ENG_InformMessageScheduler_addSessionEvent(event);
            event = nextEvent;
        }
        queuedData->events = NULL;
    }

    // add/update the forced parameters
    DM_ENG_ParameterValueStruct* param = NULL;
    DM_ENG_ParameterValueStruct* nextParam = NULL;
    bool parameterChanged = false;
    param = DM_ENG_ParameterAttributesCacheGetForcedParams(true, &parameterChanged);
    if(parameterChanged && (heartbeatSession == false)) {
        DM_ENG_EventStruct* es = NULL;
        es = DM_ENG_newEventStruct(DM_ENG_EVENT_VALUE_CHANGE, NULL);
        DM_ENG_InformMessageScheduler_addSessionEvent(es);
    }
    if(!param) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Failed to get the forced parameter values");
    }
    while(param) {
        nextParam = param->next;
        param->next = NULL;
        DM_ENG_InformMessageScheduler_addParameter(&sessionData->parameters, param);
        param = nextParam;
    }

    DM_ENG_ParameterValueStruct* informparam = NULL;
    DM_ENG_GetInformParameterValues(sessionData->events, &informparam);
    while(informparam) {
        nextParam = informparam->next;
        informparam->next = NULL;
        DM_ENG_InformMessageScheduler_addParameter(&sessionData->parameters, informparam);
        informparam = nextParam;
    }

    /*
     *  the HEARTBEAT Event contains only the Forced Inform parameters and no other Events are emitted in the same Inform session
     */
    if(heartbeatSession == true) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat: Ready to send HEARTBEAT Inform");
        goto done;
    }

    // now add the active/passive parameters when needed, in case of a bootstrap event do not add them
    if(!DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_BOOTSTRAP, NULL)) {
        //DM_ENG_EVENT_BOOTSTRAP not there, add all parameters
        if(queuedData->parameters != NULL) {
            // now add the remaining queued parameters
            param = queuedData->parameters;
            nextParam = NULL;
            while(param) {
                nextParam = param->next;
                param->next = NULL;
                DM_ENG_InformMessageScheduler_addParameter(&sessionData->parameters, param);
                param = nextParam;
            }
            queuedData->parameters = NULL;
        }

        // add the changed passive notification parameters
        //  we do not use subscriptions for passive parameters as some parameters (e.g. time, packetcount, bytestransferred)
        //  will not send out events to avoid flooding the system
        param = DM_ENG_ParameterAttributesCacheGetChangedPassiveParams();
        if(param) {
            DM_ENG_EventStruct* es = NULL;
            es = DM_ENG_newEventStruct(DM_ENG_EVENT_VALUE_CHANGE, NULL);
            DM_ENG_InformMessageScheduler_addSessionEvent(es);
            SAH_TRACEZ_INFO("DM_ENGINE", "Passive notification parameter changed, adding value changed event to the inform message");
        }
        while(param) {
            SAH_TRACEZ_INFO("DM_ENGINE", "Passive notification parameter changed, adding the parameter %s to the inform message", param->parameterName);
            nextParam = param->next;
            param->next = NULL;
            DM_ENG_InformMessageScheduler_addParameter(&sessionData->parameters, param);
            param = nextParam;
        }

        // add the changed active  notification parameters in case of a BOOT event
        if(DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_BOOT, NULL)) {
            param = DM_ENG_ParameterAttributesCacheGetChangedActiveParams();
            if(param) {
                DM_ENG_EventStruct* es = NULL;
                es = DM_ENG_newEventStruct(DM_ENG_EVENT_VALUE_CHANGE, NULL);
                DM_ENG_InformMessageScheduler_addSessionEvent(es);
                SAH_TRACEZ_INFO("DM_ENGINE", "Active notification parameter changed, adding value changed event to the inform message");
            }
            while(param) {
                SAH_TRACEZ_INFO("DM_ENGINE", "Active notification parameter changed, adding the parameter %s to the inform message", param->parameterName);
                nextParam = param->next;
                param->next = NULL;
                DM_ENG_InformMessageScheduler_addParameter(&sessionData->parameters, param);
                param = nextParam;
            }

        }

    } else {
        //DM_ENG_EVENT_BOOTSTRAP is there, do not add the value changed parameters, update the cache
        DM_ENG_deleteAllParameterValueStruct(&queuedData->parameters);
        queuedData->parameters = NULL;
        DM_ENG_SyncParameterAttributesCache();
    }

    // add the tranferComplete Events
    if(queuedData->transferCompleteMessages != NULL) {
        DM_ENG_TransferCompleteStruct* transfer = queuedData->transferCompleteMessages;
        while(transfer) {
            DM_ENG_EventStruct* es = NULL;
            if(transfer->autonomousTransfer == true) {
                es = DM_ENG_newEventStruct(DM_ENG_EVENT_AUTONOMOUS_TRANSFER_COMPLETE, NULL);
                DM_ENG_InformMessageScheduler_addSessionEvent(es);
            } else {
                es = DM_ENG_newEventStruct(DM_ENG_EVENT_TRANSFER_COMPLETE, NULL);
                DM_ENG_InformMessageScheduler_addSessionEvent(es);
                if(transfer->isScheduleDownload == true) {
                    es = DM_ENG_newEventStruct(DM_ENG_EVENT_M_SCHEDULEDOWNLOAD, transfer->commandKey);
                    DM_ENG_InformMessageScheduler_addSessionEvent(es);
                } else if(transfer->isDownload == true) {
                    es = DM_ENG_newEventStruct(DM_ENG_EVENT_M_DOWNLOAD, transfer->commandKey);
                    DM_ENG_InformMessageScheduler_addSessionEvent(es);
                } else {
                    es = DM_ENG_newEventStruct(DM_ENG_EVENT_M_UPLOAD, transfer->commandKey);
                    DM_ENG_InformMessageScheduler_addSessionEvent(es);
                }
            }
            transfer = transfer->next;
        }
    }

    // add the transferCompleteMessages from the queue
    if(queuedData->transferCompleteMessages != NULL) {
        DM_ENG_TransferCompleteStruct* transfer = queuedData->transferCompleteMessages;
        DM_ENG_TransferCompleteStruct* nextTransfer = NULL;
        while(transfer) {
            nextTransfer = transfer->next;
            transfer->next = NULL;
            DM_ENG_addTransferCompleteStruct(&sessionData->transferCompleteMessages, transfer);

            transfer = nextTransfer;
        }
        queuedData->transferCompleteMessages = NULL;
    }

    if(amxc_llist_is_empty(&queuedData->dscc) != true) {
        dscc_t* entry = NULL;
        DM_ENG_EventStruct* es = NULL;
        es = DM_ENG_newEventStruct(DM_ENG_EVENT_DU_STATE_CHANGE_COMPLETE, NULL);
        DM_ENG_InformMessageScheduler_addSessionEvent(es);
        amxc_llist_for_each(lit, &queuedData->dscc) {
            entry = amxc_container_of(lit, dscc_t, it);
            es = DM_ENG_newEventStruct(DM_ENG_EVENT_M_CHANGEDUSTATE, entry->commandKey);
            DM_ENG_InformMessageScheduler_addSessionEvent(es);
        }
        dscc_print(&queuedData->dscc);
        SAH_TRACEZ_INFO("DM_ENGINE", "Moving queued dscc list to session dscc list");
        amxc_llist_move(&sessionData->dscc, &queuedData->dscc);
    }

    // add the downloadRequests from the queue
    if(queuedData->downloadRequests != NULL) {
        DM_ENG_DownloadRequest* downloadR = queuedData->downloadRequests;
        DM_ENG_DownloadRequest* nextDownloadR = NULL;
        while(downloadR) {
            nextDownloadR = downloadR->next;
            downloadR->next = NULL;
            DM_ENG_addDownloadRequest(&sessionData->downloadRequests, downloadR);

            downloadR = nextDownloadR;
        }
        queuedData->downloadRequests = NULL;
    }

    // add the downloadRequest Events
    if(sessionData->downloadRequests != NULL) {
        DM_ENG_EventStruct* es = NULL;
        es = DM_ENG_newEventStruct(DM_ENG_EVENT_REQUEST_DOWNLOAD, NULL);
        DM_ENG_InformMessageScheduler_addSessionEvent(es);
    }

    // check if multiple schedule informs per session are allowed
    char* val = NULL;
    // SAH_TRACE_ERROR("*** checking if schedule inform per sessions are allowed...");
    bool allowMultipleScheduleInform = true;
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_ALLOWMULTIPLESCHEDULEINFORM, &val) == 0) {
        if(atol(val) == 0) {
            allowMultipleScheduleInform = false;
        }
        free(val);
    }

    // add the scheduleInforms Events
    if(queuedData->scheduleInforms != NULL) {
        if(( allowMultipleScheduleInform == false) && ( sessionData->scheduleInforms != NULL)) {
            SAH_TRACEZ_INFO("DM_ENGINE", "*** There is already a schedule inform in the sessiondata, do not add more");
        } else {
            DM_ENG_EventStruct* es = NULL;
            DM_ENG_ScheduleInformStruct* ptr = queuedData->scheduleInforms;
            es = DM_ENG_newEventStruct(DM_ENG_EVENT_SCHEDULED, NULL);
            DM_ENG_InformMessageScheduler_addSessionEvent(es);
            while(ptr) {
                es = DM_ENG_newEventStruct(DM_ENG_EVENT_M_SCHEDULE_INFORM, ptr->commandKey);
                DM_ENG_InformMessageScheduler_addSessionEvent(es);
                if(allowMultipleScheduleInform == false) {
                    break;
                }
                ptr = ptr->next;
            }
        }
    }

    // add the scheduleInforms from the queue
    if(queuedData->scheduleInforms != NULL) {
        if(( allowMultipleScheduleInform == false) && ( sessionData->scheduleInforms != NULL)) {
            SAH_TRACEZ_INFO("DM_ENGINE", "*** There is already a schedule inform in the sessiondata, do not add more");
            triggerInformAfterSessionEnd = true;
        } else {
            DM_ENG_ScheduleInformStruct* ptr = queuedData->scheduleInforms;
            DM_ENG_ScheduleInformStruct* nextPtr = NULL;
            while(ptr) {
                nextPtr = ptr->next;
                ptr->next = NULL;
                DM_ENG_addScheduleInformStruct(&sessionData->scheduleInforms, ptr);

                if(allowMultipleScheduleInform == false) {
                    if(nextPtr != NULL) {
                        triggerInformAfterSessionEnd = true;
                    }
                    break;
                }
                ptr = nextPtr;
            }
            queuedData->scheduleInforms = nextPtr;
        }
    }

    DM_ENG_InformMessageScheduler_blockEvents();

done:
    SAH_TRACEZ_INFO("DM_ENGINE", "========== QUEUED EVENT STRUCT ===========");
    DM_ENG_printEventStruct(queuedData->events);
    SAH_TRACEZ_INFO("DM_ENGINE", "========== SESSION EVENT STRUCT ===========");
    DM_ENG_printEventStruct(sessionData->events);
    SAH_TRACEZ_INFO("DM_ENGINE", "========== QUEUED PARAMETER STRUCT ===========");
    DM_ENG_printParameterValueStruct(queuedData->parameters);
    SAH_TRACEZ_INFO("DM_ENGINE", "========== SESSION PARAMETER STRUCT ===========");
    DM_ENG_printParameterValueStruct(sessionData->parameters);

    /* return false in case there are no events that have to be send*/
    if(sessionData->events == NULL) {
        return false;
    }

    return true;
}

void DM_ENG_InformMessageScheduler_disableSendInform() {
    disableSendInform = true;
}

void DM_ENG_InformMessageScheduler_retryMessage(char* name) {
    retryTriggeredSession = true;
    DM_ENG_InformMessageScheduler_sendMessage(name);
}

void DM_ENG_InformMessageScheduler_sendMessage(char* name) {
    (void) name;
    DM_ENG_DeviceIdStruct* id = NULL;
    DM_ENG_EventStruct** evt = NULL;
    DM_ENG_ParameterValueStruct** parameterList = NULL;
    char* cpe_enabled = NULL;
    char* connreqhost = NULL;
    DM_ENG_NotificationStatus sendMessageStatus;

    if(disableSendInform == true) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Send inform message is disabled, returning");
        goto error;
    }

    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_CONNECTIONREQUESTHOST, &connreqhost) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the connection request host ");
    }
    SAH_TRACEZ_INFO("DM_ENGINE", "connreqhost=%s", connreqhost);
    if(!connreqhost || !(*connreqhost) || (connreqhost && ( strncmp(connreqhost, "0.0.0.0", strlen("0.0.0.0")) == 0))) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Trying to send an inform message while connreqhost is empty");
        goto error;
    }

    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_ENABLECWMP, &cpe_enabled) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the ACS enableflag ");
    }
    SAH_TRACEZ_INFO("DM_ENGINE", "cpe-enabled=%s", cpe_enabled);
    if(cpe_enabled && ( strcmp(cpe_enabled, "0") == 0)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Trying to send an inform message while cwmp is disabled, returning");
        goto error;
    }

    if(sessionOngoing == true) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot send a new inform message while a session is going on");
        triggerInformAfterSessionEnd = true;
        goto error;
    }

    DM_ENG_SessionOpened(DM_ENG_EntityType_ACS);

    if(nextSessionContainsTr111ActiveNotification == true) {
        tr111triggeredSession = true;
        nextSessionContainsTr111ActiveNotification = false;
    }

    // construct the device id struct
    id = DM_ENG_newDeviceIdStruct();
    if(id == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not create the device struct");
        goto error;
    }

    // construct the sessionData
    if(DM_ENG_InformMessageScheduler_createSessionData() == false) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Trying to send inform message without events, aborting");
        goto error;
    }

    // Construct the event struct
    evt = DM_ENG_createTabEventStruct(sessionData->events);
    if(evt == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not create the pending events array");
        goto error;
    }

    // Construct the parameter list
    parameterList = DM_ENG_toParameterValueStructArray(sessionData->parameters);
    if(( sessionData->parameters != NULL) && ( parameterList == NULL)) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not create the parameter event array");
        goto error;
    }

    // Send the message
    sendMessageStatus = DM_ENG_NotificationInterface_inform(id, evt, parameterList, time(NULL), retryCount);

    if(sendMessageStatus == DM_ENG_SESSION_OPENING) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Sending inform message");
        sessionOngoing = true;

        // once synchronized, there is no need to check the ntp status again.
        if(lastSession.synchronized == false) {
            char* ntpStatus = NULL;
            if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_NTPSTATUS, &ntpStatus) == 0) {
                if(ntpStatus && (strcmp(ntpStatus, "Synchronized") == 0)) {
                    lastSession.synchronized = true;
                }
            } else {
                SAH_TRACEZ_ERROR("DM_ENGINE", "Could not get DM_ENG_NTPSTATUS");
            }
            free(ntpStatus);
        }

        if(lastSession.synchronized == false) {
            clock_gettime(CLOCK_MONOTONIC_RAW, &(lastSession.clock_start));
            lastSession.time = 0;
            SAH_TRACEZ_INFO("DM_ENGINE", "LastSession hasn't been mesured - clock started at: %ds", lastSession.clock_start.tv_sec);
        } else {
            lastSession.clock_start.tv_sec = 0;
            lastSession.clock_start.tv_nsec = 0;
            lastSession.time = time(&lastSessionTime);
            SAH_TRACEZ_INFO("DM_ENGINE", "LastSession has been measured Time %ds", lastSession.time);
        }

        if(tr111triggeredSession == true) {
            time(&lastTR111SessionTime);
        }
    }
    // remove the device id struct
    DM_ENG_deleteDeviceIdStruct(id);

    // retain the sessiondata untill you receive confirmation it was send correctly
    free(evt);
    free(parameterList);
    free(cpe_enabled);
    free(connreqhost);
    return;

error:
    // remove the device id struct
    DM_ENG_deleteDeviceIdStruct(id);

    //make sure the sessionstate is Idle
    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_SESSIONSTATUS, "Idle") != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not set Session Status in the datamodel");
    }

    // retain the sessiondata untill you receive confirmation it was send correctly
    free(evt);
    free(parameterList);
    free(cpe_enabled);
    free(connreqhost);
    return;
}


bool DM_ENG_InformMessageScheduler_informResponseArrived() {
    // 3.7.1.5: Table-7 - ACS Response for succesfull delivery:
    // inform response has arrived, remove all sessionData->events and sessionData->parameters as they are delivered succesfully
    // note: TransferComplete and other events will also be removed at this point (they are re-generated each time an inform message is send)
    DM_ENG_deliveredEventListCleanup();
    DM_ENG_EventStruct* event = sessionData->events;
    while(event) {
        if(strcmp(event->eventCode, DM_ENG_EVENT_M_REBOOT) == 0) {
            DM_ENG_acsEventListRemoveEvent(DM_ENG_EVENT_M_REBOOT, event->commandKey);
        }

        if(strcmp(event->eventCode, DM_ENG_EVENT_PERIODIC) == 0) {
            update_periodic_inform_counter(false);
        }
        DM_ENG_deliveredEventListAddEvent(event->eventCode, event->commandKey);
        event = event->next;
    }
    DM_ENG_deleteAllEventStruct(&sessionData->events);
    sessionData->events = NULL;
    DM_ENG_deliveredEventListUpdate();

    if(firstSessionTime == 0) {
        time(&firstSessionTime);
        DM_ENG_InformMessageScheduler_initializePeriodicInform();
    }

    DM_ENG_deleteAllParameterValueStruct(&sessionData->parameters);
    sessionData->parameters = NULL;
    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_FACTORYRESETOCCURRED, "0") != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not set DM_ENG_FACTORYRESETOCCURRED parameter in the datamodel");
    }
    if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_REBOOTBYACS, "0") != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Could not set DM_ENG_REBOOTBYACS parameter in the datamodel");
    }

    update_failure_counter(true);
    DM_ENG_ScheduleInformStruct* ptr = sessionData->scheduleInforms;
    while(ptr) {
        DM_ENG_ScheduleInformConfirmedDelivery(ptr->uniqueID);
        DM_ENG_acsEventListRemoveEvent(DM_ENG_EVENT_M_SCHEDULE_INFORM, ptr->commandKey);
        ptr = ptr->next;
    }
    DM_ENG_deleteAllScheduleInformStruct(&sessionData->scheduleInforms);
    sessionData->scheduleInforms = NULL;
    DM_ENG_acsEventListUpdate();


    return true;
}

bool DM_ENG_InformMessageScheduler_transferCompleteResponseArrived() {
    // 3.7.1.5: Table-7 - ACS Response for succesfull delivery:
    // if this function is called, a tranfer complete response has come in
    // this means that the first item in sessionData->transferCompleteMessages was send succesfully
    SAH_TRACEZ_INFO("DM_ENGINE", "Receiving Transfer Complete Message ");
    if(sessionData->transferCompleteMessages != NULL) {
        DM_ENG_TransferCompleteStruct* current = sessionData->transferCompleteMessages;
        sessionData->transferCompleteMessages = sessionData->transferCompleteMessages->next;
        DM_ENG_TransferDoneAcknowledged(DM_ENG_EntityType_SYSTEM, current->uniqueID);
        if(current->isScheduleDownload == true) {
            DM_ENG_acsEventListRemoveEvent(DM_ENG_EVENT_M_SCHEDULEDOWNLOAD, current->commandKey);
        } else if(current->isDownload) {
            DM_ENG_acsEventListRemoveEvent(DM_ENG_EVENT_M_DOWNLOAD, current->commandKey);
        } else {
            DM_ENG_acsEventListRemoveEvent(DM_ENG_EVENT_M_UPLOAD, current->commandKey);
        }
        DM_ENG_acsEventListUpdate();
        DM_ENG_deleteTransferCompleteStruct(current);
    } else {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Receiving a tranfser complete response, but no more tranfers in session queue???");
    }
    return true;
}

bool DM_ENG_InformMessageScheduler_RequestDownloadResponseArrived() {
    // 3.7.1.5: Table-7 - ACS Response for succesfull delivery:
    // if this function is called, a request download complete response has come in
    // this means that the first item in sessionData->downloadRequests was send succesfully

    if(sessionData->downloadRequests != NULL) {
        DM_ENG_DownloadRequest* current = sessionData->downloadRequests;
        sessionData->downloadRequests = sessionData->downloadRequests->next;
        DM_ENG_deleteDownloadRequest(current);
    } else {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Receiving a downloadrequest complete response, but no more download requests in session queue???");
    }
    return true;
}

bool DM_ENG_InformMessageScheduler_DUStateChangeCompleteResponseArrived() {
    SAH_TRACEZ_INFO("DM_ENGINE", "Receiving DUStateChangeCompleteResponse Message");
    if(amxc_llist_is_empty(&sessionData->dscc) != true) {
        amxc_llist_it_t* lit = amxc_llist_take_first(&sessionData->dscc);
        dscc_t* entry = amxc_container_of(lit, dscc_t, it);
        DM_ENG_DUStateChangeCompleteAcknowledged(DM_ENG_EntityType_SYSTEM, entry->path);
        DM_ENG_acsEventListRemoveEvent(DM_ENG_EVENT_M_CHANGEDUSTATE, entry->commandKey);
        DM_ENG_acsEventListUpdate();
        dscc_delete(&entry);
    } else {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Session queue is empty!!!");
    }
    return true;
}

/**
 * Calculate the inform delay for a parameter change at the current time
 */
static unsigned int DM_ENG_InformMessageScheduler_parameterValueChangedDelay(bool tr111Parameter) {
    char* active_notification_throttle;
    int active_notification_throttle_value = 0;
    char* manageable_device_notification_limit;
    int manageable_device_notification_limit_value = 0;
    time_t now;
    time(&now);

    /* get the default active notification throttle value */
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_DEFAULTACTIVENOTIFICATIONTHROTTLE, &active_notification_throttle) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the active_notification_throttle");
    }
    if(active_notification_throttle) {
        active_notification_throttle_value = atoi(active_notification_throttle);
        free(active_notification_throttle);
    }

    if(tr111Parameter == false) {
        if((active_notification_throttle_value == 0) ||                       // no throttle
           ((now - lastSessionTime) >= active_notification_throttle_value)) { // lastsession out of throttle period
            return 0;
        } else {
            return lastSessionTime + active_notification_throttle_value - now;
        }
    } else {
        /* get the default active notification manageable device notification limit value */
        if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_MANAGEABLEDEVICENOTIFICATIONLIMIT, &manageable_device_notification_limit) != 0) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the manageable device notification limit parameter ");
        }
        if(manageable_device_notification_limit) {
            manageable_device_notification_limit_value = atoi(manageable_device_notification_limit);
            free(manageable_device_notification_limit);
        }

        if(( active_notification_throttle_value == 0) && ( manageable_device_notification_limit_value == 0)) {
            return 0;
        } else if(( active_notification_throttle_value != 0) && ( manageable_device_notification_limit_value == 0)) { //only a default throttle
            if((now - lastSessionTime) >= active_notification_throttle_value) {
                return 0;
            } else {
                return lastSessionTime + active_notification_throttle_value - now;
            }
        } else if(( active_notification_throttle_value == 0) && ( manageable_device_notification_limit_value != 0)) { //only a tr111 throttle
            if((now - lastTR111SessionTime) >= manageable_device_notification_limit_value) {
                return 0;
            } else {
                return lastTR111SessionTime + manageable_device_notification_limit_value - now;
            }
        } else { // both throttles are active
            if(((now - lastSessionTime) >= active_notification_throttle_value) && ((now - lastTR111SessionTime) >= manageable_device_notification_limit_value)) {
                return 0;
            } else if(((now - lastSessionTime) < active_notification_throttle_value) && ((now - lastTR111SessionTime) >= manageable_device_notification_limit_value)) {
                return lastSessionTime + active_notification_throttle_value - now;
            } else if(((now - lastSessionTime) >= active_notification_throttle_value) && ((now - lastTR111SessionTime) < manageable_device_notification_limit_value)) {
                return lastTR111SessionTime + manageable_device_notification_limit_value - now;
            } else { // both times are in the throttle period
                if(lastSessionTime + active_notification_throttle_value > lastTR111SessionTime + manageable_device_notification_limit_value) {
                    return lastSessionTime + active_notification_throttle_value - now;
                } else {
                    return lastTR111SessionTime + manageable_device_notification_limit_value - now;
                }
            }
        }
    }
}


/* 3.1.7.5
   For most events, delivery is confirmed when the CPE receives a successful InformResponse. Four standard
   event types (KICKED, TRANSFER COMPLETE, AUTONOMOUS TRANSFER COMPLETE,
   REQUEST DOWNLOAD) indicate that one or more methods (Kicked [section A.4.2.1], TransferComplete
   [section A.3.3.2], AutonomousTransferComplete [section A.3.3.3], RequestDownload [section A.4.2.2]
   respectively) will be called later in the session, and it is the successful response to these methods that
   indicates event delivery. The CPE MAY also send vendor-specific events (using the syntax specified in
   Table 7), in which case successful delivery, retry, and discard policy is subject to vendor definition.
 */

void DM_ENG_InformMessageScheduler_sessionClosed(bool success) {
    SAH_TRACEZ_INFO("DM_ENGINE", "========== +SESSION EVENT STRUCT ===========");
    DM_ENG_printEventStruct(sessionData->events);
    char* nsessions = NULL;
    int ns = 0;

    DM_ENG_NotificationInterface_timerStop("Session-timer");

    //remove the results of the get acs rpc's - we might connect to a different server in a next session
    int i = 0;
    while(acsSupportedRPCs[i] != NULL) {
        free(acsSupportedRPCs[i]);
        acsSupportedRPCs[i] = NULL;
        i++;
    }

    sessionOngoing = false;

    if(success == true) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Inform message was send succesfully, cleanup the data");
        DM_ENG_NotificationInterface_timerStop("Retry-timer");
        retryCount = 0;
        retryTriggeredSession = false;

        if(tr111triggeredSession == true) {
            tr111triggeredSession = false;
        }

        if(lastSession.synchronized == false) {
            SAH_TRACEZ_INFO("DM_ENGINE", "LastSession will be updated once synchronized");
            lastSession.ignore_time_synchronization_notif = false;
        } else {
            lastSession.ignore_time_synchronization_notif = true;
            update_lastsession_time(&(lastSession.time));
        }

        if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_SESSIONSSINCEBOOT, &nsessions) == 0) {
            if(nsessions) {
                ns = atoi(nsessions);
            }
        }
        free(nsessions);
        ns += 1;
        char tempBuffer[256] = "";
        snprintf(tempBuffer, 256, "%d", ns);
        if(DM_ENG_SetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_SESSIONSSINCEBOOT, tempBuffer) != 0) {
            SAH_TRACEZ_ERROR("DM_ENGINE", "Could not set DM_ENG_SESSIONSSINCEBOOT parameter in the datamodel");
        }

        /* if an event came in during a session that required sending a new inform, start a new session */
        if(triggerInformAfterSessionEnd == true) {
            SAH_TRACEZ_INFO("DM_ENGINE", "An event occurred during the previous session, reschedule an inform");
            triggerInformAfterSessionEnd = false;
            triggerActiveNotificationInformAfterSessionEnd = false; //all informs, except active parameter informs may be send immediately
            triggerTr111ActiveNotificationInformAfterSessionEnd = false;
            DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
            return;
        }
        if(( triggerActiveNotificationInformAfterSessionEnd == true) || ( triggerTr111ActiveNotificationInformAfterSessionEnd == true)) {
            unsigned int delay = 0;
            if(( triggerActiveNotificationInformAfterSessionEnd == true) && ( triggerTr111ActiveNotificationInformAfterSessionEnd == false)) {
                delay = DM_ENG_InformMessageScheduler_parameterValueChangedDelay(false);
            } else if(( triggerActiveNotificationInformAfterSessionEnd == false) && ( triggerTr111ActiveNotificationInformAfterSessionEnd == true)) {
                delay = DM_ENG_InformMessageScheduler_parameterValueChangedDelay(true);
            } else { // both normal and tr111 active notification was detected, the normal throttle always overrides the tr111 throttle
                delay = DM_ENG_InformMessageScheduler_parameterValueChangedDelay(false);
            }

            triggerActiveNotificationInformAfterSessionEnd = false;
            triggerTr111ActiveNotificationInformAfterSessionEnd = false;
            DM_ENG_NotificationInterface_timerStart("send_inform_message", delay, 0, DM_ENG_InformMessageScheduler_sendMessage);
        }

    } else {
        SAH_TRACEZ_INFO("DM_ENGINE", "Inform message was not send succesfully");
        SAH_TRACEZ_INFO("DM_ENGINE", "========== ++SESSION EVENT STRUCT ===========");
        DM_ENG_printEventStruct(sessionData->events);
        update_failure_counter(false);
        // remove connection requests, as they should not be included in a new inform message
        DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_CONNECTION_REQUEST, NULL);
        // remove request download events
        DM_ENG_InformMessageScheduler_RemoveEvent(&sessionData->events, DM_ENG_EVENT_REQUEST_DOWNLOAD, NULL);
        SAH_TRACEZ_INFO("DM_ENGINE", "========== +++SESSION EVENT STRUCT ===========");
        DM_ENG_printEventStruct(sessionData->events);

        // test if there are still events in the list
        if(sessionData->events == NULL) {
            SAH_TRACEZ_INFO("DM_ENGINE", "Previous inform was triggered by a connection request only, do not retry to send this message");
            DM_ENG_NotificationInterface_timerStop("Retry-timer");
            retryCount = 0;
            retryTriggeredSession = false;
        } else {
            if(( retryCount == 0) || ( retryTriggeredSession == true)) {
                // schedule retry
                retryCount++;
                int retryTime = DM_ENG_InformMessageScheduler_calcRetryTime();
                SAH_TRACEZ_INFO("DM_ENGINE", "New message will be send in %d seconds", retryTime);
                DM_ENG_NotificationInterface_timerStart("Retry-timer", retryTime, 0, DM_ENG_InformMessageScheduler_retryMessage);
                retryTriggeredSession = false;
            }
        }
    }
}

bool DM_ENG_InformMessageScheduler_getRPCMethodsResponseArrived(char* methods[]) {
    int i = 0;
    if(methods == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", " Not a valid argument");
        return false;
    }

    while(methods[i] != NULL) {
        free(acsSupportedRPCs[i]);
        acsSupportedRPCs[i] = strdup(methods[i]);
        i++;
    }
    free(acsSupportedRPCs[i]);
    acsSupportedRPCs[i] = NULL;
    return true;
}

/* called when a session is ready to be closed, to see if there are any CPE RPC's available */
bool DM_ENG_InformMessageScheduler_isReadyToClose() {
    int i = 0;
    bool checkSupportedACSRPCS = false;

    SAH_TRACEZ_IN("DM_ENGINE");


    char* val = NULL;
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_VERIFYSUPPORTEDACSMETHODS, &val) == 0) {
        if(atol(val)) {
            SAH_TRACEZ_INFO("DM_ENGINE", "We need to check the supported ACS methods");
            checkSupportedACSRPCS = true;
        }
        free(val);
    }

    /* check first which RPC's are supported */
    if(( checkSupportedACSRPCS == true) && ( acsSupportedRPCs[0] == NULL)) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Fetching ACS RPC Method list");
        DM_ENG_NotificationInterface_getRPCMethods();
        SAH_TRACEZ_OUT("DM_ENGINE");
        return false;
    }

    /* If there are transfer complete messages available, send the first one now */
    if(sessionData->transferCompleteMessages != NULL) {
        if(checkSupportedACSRPCS == true) {
            while(acsSupportedRPCs[i] != NULL) {
                if(( strcasecmp(acsSupportedRPCs[i], "TransferComplete") == 0) || ( strcasecmp(acsSupportedRPCs[i], "AutonomousTransferComplete") == 0)) {
                    DM_ENG_NotificationInterface_transferComplete(sessionData->transferCompleteMessages);
                    SAH_TRACEZ_OUT("DM_ENGINE");
                    return false;
                }
                i++;
            }
            SAH_TRACEZ_ERROR("DM_ENGINE", "ACS does not support a transfercomplete RPC?");
        } else {
            DM_ENG_NotificationInterface_transferComplete(sessionData->transferCompleteMessages);
            SAH_TRACEZ_OUT("DM_ENGINE");
            return false;
        }
    }

    if(amxc_llist_is_empty(&sessionData->dscc) != true) {
        DM_ENG_NotificationInterface_DUStateChangeComplete(amxc_container_of(amxc_llist_get_first(&sessionData->dscc), dscc_t, it));
        SAH_TRACEZ_OUT("DM_ENGINE");
        return false;
    }

    /* If there are request download messages available, send the first one now */
    if(sessionData->downloadRequests != NULL) {
        if(checkSupportedACSRPCS == true) {
            while(acsSupportedRPCs[i] != NULL) {
                if(strcasecmp(acsSupportedRPCs[i], "RequestDownload") == 0) {
                    DM_ENG_NotificationInterface_requestDownload(sessionData->downloadRequests->fileType, sessionData->downloadRequests->args);
                    SAH_TRACEZ_OUT("DM_ENGINE");
                    return false;
                }
                i++;
            }
            SAH_TRACEZ_ERROR("DM_ENGINE", "ACS does not support a request download RPC?");
        } else {
            DM_ENG_NotificationInterface_requestDownload(sessionData->downloadRequests->fileType, sessionData->downloadRequests->args);
            SAH_TRACEZ_OUT("DM_ENGINE");
            return false;
        }
    }

    SAH_TRACEZ_OUT("DM_ENGINE");
    return true;
}

static void DM_ENG_InformMessageScheduler_scheduleInformCallback(char* name) {
    char commandKey[33]; //commandkey = max 32 characters
    int uniqueID;
    memset(commandKey, 0, sizeof(commandKey));
    sscanf(name, "%*30s %*30s %*30s %12d %33[^\t]", &uniqueID, commandKey);

    DM_ENG_ScheduleInformStruct* ptr = DM_ENG_newScheduleInformStruct(0, commandKey, uniqueID);

    DM_ENG_addScheduleInformStruct(&queuedData->scheduleInforms, ptr);


    if(sessionOngoing == true) {
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }

}

/**
 * @param delay Delay in seconds from the time this method is called to the time the CPE
 * is requested to intiate a one-time Inform method call.
 * @param commandKey The string to return in the CommandKey element of the InformStruct
 * when the CPE calls the Inform method.
 * @param commandKey unique id to identify this scheduled inform message
 */
int DM_ENG_InformMessageScheduler_scheduleInform(unsigned int delay, const char* commandKey, int uniqueID) {
    SAH_TRACEZ_INFO("DM_ENGINE", "Adding schedule inform timer (delay=%d,ck=%s,id=%d)", delay, commandKey, uniqueID);

    char* siTimerName = NULL;

    if(asprintf(&siTimerName, "Schedule inform timer %d %s", uniqueID, commandKey) == -1) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Asprintf returned an error");
        return DM_ENG_RESOURCES_EXCEEDED;
    }

    DM_ENG_NotificationInterface_timerStart(siTimerName, delay, 0, DM_ENG_InformMessageScheduler_scheduleInformCallback);
    free(siTimerName);

    return 0;
}

/**
 * Callback when a parameter value changed. Check for the notification type:
 *  - if active or forced, trigger an inform message
 *  - if passive add the parameter to the list
 * Also add VALUE CHANGED event to the event list
 */
void DM_ENG_InformMessageScheduler_parameterValueChanged(DM_ENG_ParameterValueStruct* param, DM_ENG_NotificationMode mode) {

    /* tr98 amendment 2: Managementserver.DefaultActiveNotificationThrottle
     * This parameter is used to control throttling of active notifications sent by the CPE to the ACS. It defines
     * the minimum number of seconds that the CPE MUST wait since the end of the last session with the ACS before
     * establishing a new session for the purpose of delivering an active notification.
     * In other words, if CPE needs to establish a new session with the ACS for the sole purpose of delivering an active
     * notification, it MUST delay establishing such a session as needed to ensure that the minimum time since the last session
     * completion has been met. The time is counted since the last successfully completed session, regardless of whether or not it
     * was used for active notifications or other purposes. However, if connection to the ACS is established for purposes other
     * than just delivering active notifications, including for the purpose of retrying a failed session, such connection MUST
     * NOT be delayed based on this parameter value, and the pending active notifications MUST be communicated during that
     * connection. The time of the last session completion does not need to be tracked across reboots.
     */
    if(!param) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Invalid parameter value struct");
        return;
    }

    if(mode != DM_ENG_NotificationMode_FORCED) {// forced parameters + corresponding 4 value change are added to the list automatically
        DM_ENG_InformMessageScheduler_addQueuedEvent(DM_ENG_EVENT_VALUE_CHANGE, NULL);
        if(DM_ENG_InformMessageScheduler_addParameter(&queuedData->parameters, param) == false) {
            // parameter was already added
            return;
        }
    }

    SAH_TRACEZ_INFO("DM_ENGINE", "Parameter value changed with mode %d", mode);

    /* if its an active notification, trigger a send */
    if(( mode == DM_ENG_NotificationMode_ACTIVE) || ( mode == DM_ENG_NotificationMode_FORCED)) {
        bool tr111Parameter = false;
        if(param && param->parameterName && ( strstr(param->parameterName, "ManageableDeviceNumberOfEntries") != NULL)) {
            tr111Parameter = true;
            nextSessionContainsTr111ActiveNotification = true;
            SAH_TRACEZ_INFO("DM_ENGINE", "Notification for ManageableDeviceNumberOfEntries!!!");
        }

        if(sessionOngoing == true) {
            if(tr111Parameter) {
                triggerTr111ActiveNotificationInformAfterSessionEnd = true;
            } else {
                triggerActiveNotificationInformAfterSessionEnd = true;
            }
        } else {
            unsigned int delay = DM_ENG_InformMessageScheduler_parameterValueChangedDelay(tr111Parameter);
            unsigned int currentTimerValue = DM_ENG_NotificationInterface_timer_remainingTime("send_inform_message");
            SAH_TRACEZ_INFO("DM_ENGINE", "Starting delay timer with delay %d", delay);
            if(( currentTimerValue == 0) || ( delay < currentTimerValue)) {
                DM_ENG_NotificationInterface_timerStart("send_inform_message", delay, 0, DM_ENG_InformMessageScheduler_sendMessage);
            }
            //in the other case the timer is already active!
        }
    }
    if(mode == DM_ENG_NotificationMode_FORCED) {// clean up in case of a forced parameter
        DM_ENG_deleteParameterValueStruct(param);
    }
}

/**
 * Add BOOT event to event list, if enabled try to send the message
 */
void DM_ENG_InformMessageScheduler_bootstrapInform() {
    DM_ENG_InformMessageScheduler_addQueuedEvent(DM_ENG_EVENT_BOOTSTRAP, NULL);
    if(sessionOngoing == true) {
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }
    update_periodic_inform_counter(true);

}

/**
 * Add CONNECTION REQUEST event to event list, if enabled try to send the message
 */
void DM_ENG_InformMessageScheduler_requestConnection() {
    DM_ENG_InformMessageScheduler_addQueuedEvent(DM_ENG_EVENT_CONNECTION_REQUEST, NULL);
    if(sessionOngoing == true) {
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }
}

/**
 * Add DIAGNOSTICS COMPLETE event to event list, if enabled try to send the message
 */
void DM_ENG_InformMessageScheduler_diagnosticsComplete() {
    DM_ENG_InformMessageScheduler_addQueuedEvent(DM_ENG_EVENT_DIAGNOSTICS_COMPLETE, NULL);
    if(sessionOngoing == true) {
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }
}

/**
 * Add TRANSFER COMPLETE event to event list, if enabled try to send the message
 */
void DM_ENG_InformMessageScheduler_transferComplete(DM_ENG_TransferCompleteStruct* tcs) {
    DM_ENG_addTransferCompleteStruct(&queuedData->transferCompleteMessages, tcs);
    if(sessionOngoing == true) {
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }
}

/**
 * Add DOWNLOAD event to event list, if enabled try to send the message
 */
void DM_ENG_InformMessageScheduler_requestDownload(DM_ENG_DownloadRequest* dr) {
    DM_ENG_addDownloadRequest(&queuedData->downloadRequests, dr);
    if(sessionOngoing == true) {
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }
}

/**
 * Add vendor specific event to event list, if enabled try to send the message
 */
void DM_ENG_InformMessageScheduler_vendorSpecificEvent(bool active, const char* eventCode) {
    DM_ENG_InformMessageScheduler_addQueuedEvent(eventCode, NULL);
    if(active) {
        if(sessionOngoing == true) {
            triggerInformAfterSessionEnd = true;
        } else {
            DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
        }
    }
}

/**
 * Check if there were blocked events, in case there are blocked events trigger an inform
 */
void DM_ENG_InformMessageScheduler_blockedEventsChanged() {
    if(!queuedData->downloadRequests && !queuedData->scheduleInforms && !queuedData->transferCompleteMessages &&
       !queuedData->parameters && !queuedData->events) {
        return;
    }

    if(sessionOngoing == true) {
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }

}


/**
 * Add PERIODIC INFORM event to event list, if enabled try to send the message
 */
void DM_ENG_InformMessageScheduler_periodicInform(char* name) {
    (void) name;
    char* periodicInformEnable;
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PERIODICINFORMENABLE, &periodicInformEnable) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the periodic enable flag");
        return;
    }
    if(periodicInformEnable && ( strcmp(periodicInformEnable, "0") == 0)) {
        free(periodicInformEnable);
        SAH_TRACEZ_NOTICE("DM_ENGINE", "Periodic inform disabled, not sending any message");
        return;
    }
    free(periodicInformEnable);
    DM_ENG_InformMessageScheduler_addQueuedEvent(DM_ENG_EVENT_PERIODIC, NULL);
    if(sessionOngoing == true) {
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }
}

int DM_ENG_InformMessageScheduler_SendTransferComplete(bool autonomous, const char* announceURL, const char* transferURL,
                                                       const char* fileType, unsigned int fileSize, const char* targetFileName,
                                                       bool isDownload, bool isScheduleDownload,
                                                       const char* commandKey, uint32_t faultCode, const char* faultString,
                                                       time_t startTime, time_t completeTime, const char* path) {
    int retval = -1;
    DM_ENG_TransferCompleteStruct* tcs = NULL;

    tcs = DM_ENG_newTransferCompleteStruct(autonomous, announceURL, transferURL, fileType,
                                           fileSize, targetFileName, isDownload, isScheduleDownload, commandKey,
                                           faultCode, faultString, startTime, completeTime, path);

    if(tcs == NULL) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Failed to create a new TransferCompleteStruct");
        goto stop;
    }

    DM_ENG_InformMessageScheduler_transferComplete(tcs);
    retval = 0;

stop:
    return retval;
}


int DM_ENG_InformMessageScheduler_SendDUStateChangeComplete(amxc_var_t* results, const char* commandKey, const char* path) {
    int retval = -1;
    dscc_t* entry = NULL;
    entry = dscc_new(results, commandKey, path);
    SAH_TRACEZ_INFO("DM_ENGINE", "CommandKey [%s] - Path [%s]", commandKey, path);

    amxc_llist_append(&queuedData->dscc, &entry->it);
    /* TODO: fix test */
    #if 1
    if(sessionOngoing == true) {
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }
    #endif

    retval = 0;
    return retval;
}


void DM_ENG_InformMessageScheduler_initializePeriodicInform() {
    int initOffset;
    int interval;
    char* periodicInformEnable = NULL;
    bool enabled = true;

    // stop the timer if already running
    DM_ENG_NotificationInterface_timerStop("periodic-inform-timer");

    // get enable flag from backend
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_PERIODICINFORMENABLE, &periodicInformEnable) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the periodic time enable flag, using true");
    } else {
        SAH_TRACEZ_INFO("DM_ENGINE", "Enable = %s", periodicInformEnable);
        if(( periodicInformEnable != NULL) && ( strcmp(periodicInformEnable, "1") == 0)) {
            enabled = true;
        } else {
            enabled = false;
        }
        free(periodicInformEnable);
    }
    if(enabled) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Periodic inform is enabled, starting timer");
        DM_ENG_InformMessageScheduler_getPeriodicInformTimings(&initOffset, &interval);
        DM_ENG_NotificationInterface_timerStart("periodic-inform-timer", initOffset, interval, DM_ENG_InformMessageScheduler_periodicInform);
    } else {
        SAH_TRACEZ_INFO("DM_ENGINE", "Periodic inform is disabled");
    }

}

void heartbeat_inform_timer_cb(char* name) {
    SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat: %s Callback", name);
    if(sessionData == NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat inform timer cb called before sessionData is initialized");
        return;
    }
    if((heartbeatTriggered == true) || (DM_ENG_InformMessageScheduler_containsEvent(sessionData->events, DM_ENG_EVENT_HEARTBEAT, NULL) == true)) {
        /**
         * If a HEARTBEAT Event is triggered at the same time the another HEARTBEAT Event
         * Inform session is active, the CPE MUST discard the new HEARTBEAT Event.
         */
        SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat: New HEARTBEAT event discarded, another HEARTBEAT Inform session is active");
        return;
    }

    heartbeatTriggered = true;

    if(sessionOngoing == true) {
        /**
         * If a HEARTBEAT Event is triggered when an Inform Session is currently active, then the
         * CPE MUST wait until the current Inform Session has completed prior to issuing a new
         * Inform session for the HEARTBEAT Event
         */
        SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat: HEATBEAT Inform will be issued once the current Inform Session has completed");
        triggerInformAfterSessionEnd = true;
    } else {
        DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
    }

}

static void heartbeat_inform_timer_stop() {
    DM_ENG_NotificationInterface_timerStop(HEARTBEAT_INFORM_TIMER);
}

static int heartbeat_get_interval() {
    int retval = -1;
    char* result = NULL;
    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATREPORTINGINTERVAL, &result) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the value Heartbeate's ReportingInterval");
    } else if(result != NULL) {
        retval = atoi(result);
        SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat's ReportingInterval [%d]", retval);
    }
    free(result);
    return retval;
}

static int heartbeat_get_phase(int interval) {
    int retval = -1;
    char* result = NULL;

    time_t curr_time = time(NULL);
    time_t ref_time = curr_time;

    if(interval < HEARTBEAT_REPORTINGINTERVAL_MIN_VALUE) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Heartbeat: invalid interval value [%d]");
        return retval;
    }

    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATINITIATIONTIME, &result) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the value Heartbeate's InitiationTime");
    } else if(result != NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat's InitiationTime [%s]", result);
        if(strcmp(result, "0001-01-01T00:00:00Z") != 0) {
            struct tm tmp_tm;
            if(strptime(result, "%Y-%m-%dT%H:%M:%SZ", &tmp_tm) == NULL) {
                SAH_TRACEZ_WARNING("DM_ENGINE", "Failed to get reference time from [%s]", result);
            } else {
                ref_time = timegm(&tmp_tm);
            }
        }
    }
    free(result);

    SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat: interval [%ld], absolute time reference [%d], current time [%d]", interval, ref_time, curr_time);
    retval = (interval) - ((curr_time - (ref_time % (interval))) % (interval));

    return retval;
}

static void heartbeat_inform_timer_start() {
    int phase = 0;
    int interval = heartbeat_get_interval();

    if(interval < HEARTBEAT_REPORTINGINTERVAL_MIN_VALUE) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Heartbeat: Invalid interval value [%d], don't start %s", interval, HEARTBEAT_INFORM_TIMER);
        return;
    }

    phase = heartbeat_get_phase(interval);

    if(phase < 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Heartbeat: Invalid phase of the Heartbeat event Informs, don't start %s", HEARTBEAT_INFORM_TIMER);
        return;
    }

    SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat: Phase [%d], Interval [%d]", phase, interval);
    DM_ENG_NotificationInterface_timerStart(HEARTBEAT_INFORM_TIMER, phase, interval, heartbeat_inform_timer_cb);
}

void DM_ENG_InformMessageScheduler_Heartbeat_configure() {
    char* heartbeatEnable = NULL;

    heartbeat_inform_timer_stop();
    heartbeatTriggered = false;

    if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_HEARTBEATENABLE, &heartbeatEnable) != 0) {
        SAH_TRACEZ_ERROR("DM_ENGINE", "Cannot fetch the value Heartbeate's Enable");
    } else if(heartbeatEnable != NULL) {
        SAH_TRACEZ_INFO("DM_ENGINE", "Heartbeat's Enable [%s]", heartbeatEnable);
        if(strcmp(heartbeatEnable, "1") == 0) {
            heartbeat_inform_timer_start();
        }
    }
    free(heartbeatEnable);
}

static void DM_ENG_InformMessageScheduler_addQueuedEvents(const char* events) {
    char* token = NULL;
    char* str = NULL;
    if((events == NULL) || (*events == 0)) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Missing events");
        goto stop;
    }
    str = strdup(events);
    token = strtok(str, ",");
    while(token != NULL) {
        DM_ENG_EventStruct* es = DM_ENG_newEventStruct(token, NULL);
        DM_ENG_addEventStruct(&queuedData->events, es);
        token = strtok(NULL, ",");
    }
stop:
    free(str);
    return;
}

int DM_ENG_InformMessageScheduler_SendEvents(const char* events, bool immediately, const char* source) {
    int retval = -1;
    SAH_TRACEZ_IN("DM_ENGINE");
    if((events == NULL) || (*events == 0)) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Missing events");
        goto stop;
    }
    if((source == NULL) || (*source == 0)) {
        SAH_TRACEZ_WARNING("DM_ENGINE", "Missing source");
        goto stop;
    }
    SAH_TRACEZ_INFO("DM_ENGINE", "Send Inform: events [%s]", events);
    SAH_TRACEZ_INFO("DM_ENGINE", "Send Inform: source [%s], immediately [%s]", source, immediately ? "yes":"no");
    DM_ENG_InformMessageScheduler_addQueuedEvents(events);
    if(immediately) {
        if(sessionOngoing == true) {
            triggerInformAfterSessionEnd = true;
        } else {
            DM_ENG_NotificationInterface_timerStart("send_inform_message", 0, 0, DM_ENG_InformMessageScheduler_sendMessage);
        }
    }
    retval = 0;
stop:
    SAH_TRACEZ_OUT("DM_ENGINE");
    return retval;
}

void DM_ENG_InformMessageScheduler_init(DM_ENG_EventStruct* eventList) {
    retryCount = 0;
    acsSupportedRPCs[0] = NULL;

    /* Initialize the pending data structures */
    sessionData = (DM_ENG_InformMessageScheduler_data_t*) calloc(sizeof(DM_ENG_InformMessageScheduler_data_t), 1);
    queuedData = (DM_ENG_InformMessageScheduler_data_t*) calloc(sizeof(DM_ENG_InformMessageScheduler_data_t), 1);

    amxc_llist_init(&queuedData->dscc);
    amxc_llist_init(&sessionData->dscc);

    DM_ENG_InformMessageScheduler_initializePeriodicInform();

    DM_ENG_InformMessageScheduler_Heartbeat_configure();
    DM_ENG_InformMessageScheduler_RemoveEvent(&eventList, DM_ENG_EVENT_HEARTBEAT, NULL);

    DM_ENG_addEventStruct(&queuedData->events, eventList);
}


void dscc_delete_func(amxc_llist_it_t* it) {
    dscc_t* entry = amxc_container_of(it, dscc_t, it);
    amxc_llist_it_take(it);
    dscc_delete(&entry);
}

void DM_ENG_InformMessageScheduler_cleanup() {
    if(sessionData) {
        DM_ENG_deleteAllEventStruct(&sessionData->events);
        DM_ENG_deleteAllParameterValueStruct(&sessionData->parameters);
        DM_ENG_deleteAllTransferCompleteStruct(&sessionData->transferCompleteMessages);
        DM_ENG_deleteAllDownloadRequest(&sessionData->downloadRequests);
        DM_ENG_deleteAllScheduleInformStruct(&sessionData->scheduleInforms);
        dscc_print(&sessionData->dscc);
        amxc_llist_clean(&sessionData->dscc, dscc_delete_func);
        free(sessionData);
    }

    if(queuedData) {
        DM_ENG_deleteAllEventStruct(&queuedData->events);
        DM_ENG_deleteAllParameterValueStruct(&queuedData->parameters);
        DM_ENG_deleteAllTransferCompleteStruct(&queuedData->transferCompleteMessages);
        DM_ENG_deleteAllDownloadRequest(&queuedData->downloadRequests);
        DM_ENG_deleteAllScheduleInformStruct(&queuedData->scheduleInforms);
        dscc_print(&queuedData->dscc);
        amxc_llist_clean(&queuedData->dscc, dscc_delete_func);
        free(queuedData);
    }

    retryCount = 0;
    sessionOngoing = false;
    triggerInformAfterSessionEnd = false;
}
