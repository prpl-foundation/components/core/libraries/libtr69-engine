/*
 * ----------------------------------------------------------------------------
 *                                 Apache License
 *                           Version 2.0, January 2004
 *                        http://www.apache.org/licenses/
 *
 *   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
 *
 *   1. Definitions.
 *
 *      "License" shall mean the terms and conditions for use, reproduction,
 *      and distribution as defined by Sections 1 through 9 of this document.
 *
 *      "Licensor" shall mean the copyright owner or entity authorized by
 *      the copyright owner that is granting the License.
 *
 *      "Legal Entity" shall mean the union of the acting entity and all
 *      other entities that control, are controlled by, or are under common
 *      control with that entity. For the purposes of this definition,
 *      "control" means (i) the power, direct or indirect, to cause the
 *      direction or management of such entity, whether by contract or
 *      otherwise, or (ii) ownership of fifty percent (50%) or more of the
 *      outstanding shares, or (iii) beneficial ownership of such entity.
 *
 *      "You" (or "Your") shall mean an individual or Legal Entity
 *      exercising permissions granted by this License.
 *
 *      "Source" form shall mean the preferred form for making modifications,
 *      including but not limited to software source code, documentation
 *      source, and configuration files.
 *
 *      "Object" form shall mean any form resulting from mechanical
 *      transformation or translation of a Source form, including but
 *      not limited to compiled object code, generated documentation,
 *      and conversions to other media types.
 *
 *      "Work" shall mean the work of authorship, whether in Source or
 *      Object form, made available under the License, as indicated by a
 *      copyright notice that is included in or attached to the work
 *      (an example is provided in the Appendix below).
 *
 *      "Derivative Works" shall mean any work, whether in Source or Object
 *      form, that is based on (or derived from) the Work and for which the
 *      editorial revisions, annotations, elaborations, or other modifications
 *      represent, as a whole, an original work of authorship. For the purposes
 *      of this License, Derivative Works shall not include works that remain
 *      separable from, or merely link (or bind by name) to the interfaces of,
 *      the Work and Derivative Works thereof.
 *
 *      "Contribution" shall mean any work of authorship, including
 *      the original version of the Work and any modifications or additions
 *      to that Work or Derivative Works thereof, that is intentionally
 *      submitted to Licensor for inclusion in the Work by the copyright owner
 *      or by an individual or Legal Entity authorized to submit on behalf of
 *      the copyright owner. For the purposes of this definition, "submitted"
 *      means any form of electronic, verbal, or written communication sent
 *      to the Licensor or its representatives, including but not limited to
 *      communication on electronic mailing lists, source code control systems,
 *      and issue tracking systems that are managed by, or on behalf of, the
 *      Licensor for the purpose of discussing and improving the Work, but
 *      excluding communication that is conspicuously marked or otherwise
 *      designated in writing by the copyright owner as "Not a Contribution."
 *
 *      "Contributor" shall mean Licensor and any individual or Legal Entity
 *      on behalf of whom a Contribution has been received by Licensor and
 *      subsequently incorporated within the Work.
 *
 *   2. Grant of Copyright License. Subject to the terms and conditions of
 *      this License, each Contributor hereby grants to You a perpetual,
 *      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *      copyright license to reproduce, prepare Derivative Works of,
 *      publicly display, publicly perform, sublicense, and distribute the
 *      Work and such Derivative Works in Source or Object form.
 *
 *   3. Grant of Patent License. Subject to the terms and conditions of
 *      this License, each Contributor hereby grants to You a perpetual,
 *      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *      (except as stated in this section) patent license to make, have made,
 *      use, offer to sell, sell, import, and otherwise transfer the Work,
 *      where such license applies only to those patent claims licensable
 *      by such Contributor that are necessarily infringed by their
 *      Contribution(s) alone or by combination of their Contribution(s)
 *      with the Work to which such Contribution(s) was submitted. If You
 *      institute patent litigation against any entity (including a
 *      cross-claim or counterclaim in a lawsuit) alleging that the Work
 *      or a Contribution incorporated within the Work constitutes direct
 *      or contributory patent infringement, then any patent licenses
 *      granted to You under this License for that Work shall terminate
 *      as of the date such litigation is filed.
 *
 *   4. Redistribution. You may reproduce and distribute copies of the
 *      Work or Derivative Works thereof in any medium, with or without
 *      modifications, and in Source or Object form, provided that You
 *      meet the following conditions:
 *
 *      (a) You must give any other recipients of the Work or
 *          Derivative Works a copy of this License; and
 *
 *      (b) You must cause any modified files to carry prominent notices
 *          stating that You changed the files; and
 *
 *      (c) You must retain, in the Source form of any Derivative Works
 *          that You distribute, all copyright, patent, trademark, and
 *          attribution notices from the Source form of the Work,
 *          excluding those notices that do not pertain to any part of
 *          the Derivative Works; and
 *
 *      (d) If the Work includes a "NOTICE" text file as part of its
 *          distribution, then any Derivative Works that You distribute must
 *          include a readable copy of the attribution notices contained
 *          within such NOTICE file, excluding those notices that do not
 *          pertain to any part of the Derivative Works, in at least one
 *          of the following places: within a NOTICE text file distributed
 *          as part of the Derivative Works; within the Source form or
 *          documentation, if provided along with the Derivative Works; or,
 *          within a display generated by the Derivative Works, if and
 *          wherever such third-party notices normally appear. The contents
 *          of the NOTICE file are for informational purposes only and
 *          do not modify the License. You may add Your own attribution
 *          notices within Derivative Works that You distribute, alongside
 *          or as an addendum to the NOTICE text from the Work, provided
 *          that such additional attribution notices cannot be construed
 *          as modifying the License.
 *
 *      You may add Your own copyright statement to Your modifications and
 *      may provide additional or different license terms and conditions
 *      for use, reproduction, or distribution of Your modifications, or
 *      for any such Derivative Works as a whole, provided Your use,
 *      reproduction, and distribution of the Work otherwise complies with
 *      the conditions stated in this License.
 *
 *   5. Submission of Contributions. Unless You explicitly state otherwise,
 *      any Contribution intentionally submitted for inclusion in the Work
 *      by You to the Licensor shall be under the terms and conditions of
 *      this License, without any additional terms or conditions.
 *      Notwithstanding the above, nothing herein shall supersede or modify
 *      the terms of any separate license agreement you may have executed
 *      with Licensor regarding such Contributions.
 *
 *   6. Trademarks. This License does not grant permission to use the trade
 *      names, trademarks, service marks, or product names of the Licensor,
 *      except as required for reasonable and customary use in describing the
 *      origin of the Work and reproducing the content of the NOTICE file.
 *
 *   7. Disclaimer of Warranty. Unless required by applicable law or
 *      agreed to in writing, Licensor provides the Work (and each
 *      Contributor provides its Contributions) on an "AS IS" BASIS,
 *      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *      implied, including, without limitation, any warranties or conditions
 *      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
 *      PARTICULAR PURPOSE. You are solely responsible for determining the
 *      appropriateness of using or redistributing the Work and assume any
 *      risks associated with Your exercise of permissions under this License.
 *
 *   8. Limitation of Liability. In no event and under no legal theory,
 *      whether in tort (including negligence), contract, or otherwise,
 *      unless required by applicable law (such as deliberate and grossly
 *      negligent acts) or agreed to in writing, shall any Contributor be
 *      liable to You for damages, including any direct, indirect, special,
 *      incidental, or consequential damages of any character arising as a
 *      result of this License or out of the use or inability to use the
 *      Work (including but not limited to damages for loss of goodwill,
 *      work stoppage, computer failure or malfunction, or any and all
 *      other commercial damages or losses), even if such Contributor
 *      has been advised of the possibility of such damages.
 *
 *   9. Accepting Warranty or Additional Liability. While redistributing
 *      the Work or Derivative Works thereof, You may choose to offer,
 *      and charge a fee for, acceptance of support, warranty, indemnity,
 *      or other liability obligations and/or rights consistent with this
 *      License. However, in accepting such obligations, You may act only
 *      on Your own behalf and on Your sole responsibility, not on behalf
 *      of any other Contributor, and only if You agree to indemnify,
 *      defend, and hold each Contributor harmless for any liability
 *      incurred by, or claims asserted against, such Contributor by reason
 *      of your accepting any such warranty or additional liability.
 *
 *   END OF TERMS AND CONDITIONS
 *
 *   APPENDIX: How to apply the Apache License to your work.
 *
 *      To apply the Apache License to your work, attach the following
 *      boilerplate notice, with the fields enclosed by brackets "[]"
 *      replaced with your own identifying information. (Don't include
 *      the brackets!)  The text should be enclosed in the appropriate
 *      comment syntax for the file format. We also recommend that a
 *      file or class name and description of purpose be included on the
 *      same "printed page" as the copyright notice for easier
 *      identification within third-party archives.
 *
 *   Copyright [2024] [SoftAtHome]
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * ---------------------------------------------------------------------------
 */

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <dmengine/DM_ENG_Type.h>
#include <dmengine/DM_ENG_RPC.h>
#include <dmengine/DM_ENG_Fault.h>
#include <dmengine/DM_ENG_Device.h>

#define ME "DM_ENGINE"

extern device_adapter_t* device_adapter;

static int valid_String(const char* value, size_t len);

static int valid_TimeWindowStruct_WindowMode(const char* value) {
    int retval = -1;
    if((value != NULL) && (valid_String(value, 64) == 0) && ((strcmp(value, TIMEWINDOWSTRUCT_WINDOWMODE_IMMEDIATELY) == 0) || (strcmp(value, TIMEWINDOWSTRUCT_WINDOWMODE_AT_ANY_TIME) == 0))) {
        retval = 0;
    }

    if(retval != 0) {
        SAH_TRACEZ_INFO(ME, "Supported WindowMode [%s, %s]", TIMEWINDOWSTRUCT_WINDOWMODE_IMMEDIATELY, TIMEWINDOWSTRUCT_WINDOWMODE_AT_ANY_TIME);
    }
    return retval;
}

static int valid_FileType(const char* value) {
    int retval = -1;
    if((value != NULL) && (valid_String(value, 64) == 0) && (strcmp(value, FILETYPE_FIRMWARE_UPGRADE_IMAGE) == 0)) {
        retval = 0;
    }

    if(retval != 0) {
        SAH_TRACEZ_INFO(ME, "Supported FileType [%s]", FILETYPE_FIRMWARE_UPGRADE_IMAGE);
    }
    return retval;
}

static int valid_URL(const char* value, size_t len) {
    int retval = -1;
    if((value != NULL) && (strcmp(value, "") != 0) && (valid_String(value, len) == 0)) {
        retval = 0;
    }
    return retval;
}

static int valid_TimeWindowList(TimeWindowStruct* timeWindowList[TIMEWINDOWLIST_MAX_LENGTH]) {
    int retval = -1;

    TimeWindowStruct* timeWindow1 = timeWindowList[0];
    TimeWindowStruct* timeWindow2 = timeWindowList[1];
    when_null_trace(timeWindow1, stop, ERROR, "First TimeWindow is required");
    when_true_trace((timeWindow1->windowStart > timeWindow1->windowEnd), stop, ERROR,
                    "Invalid WindowStart and WindowEnd");
    when_failed_trace(valid_TimeWindowStruct_WindowMode(timeWindow1->windowMode), stop, ERROR, "Invalid WindowMode");

    if(timeWindow2 != NULL) {
        when_true_trace((timeWindow2->windowStart > timeWindow2->windowEnd), stop, ERROR,
                        "Invalid WindowStart and WindowEnd");
        when_failed_trace(valid_TimeWindowStruct_WindowMode(timeWindow2->windowMode), stop, ERROR, "Invalid WindowMode");
        when_true_trace((timeWindow1->windowEnd > timeWindow2->windowStart), stop, ERROR,
                        "Time windows overlap");
    }

    retval = 0;
stop:
    return retval;
}

static int valid_String(const char* value, size_t len) {
    int retval = DM_ENG_FAULTCODE_9003;
    if(value == NULL) {
        goto stop;
    }

    if((strlen(value) > len)) {
        SAH_TRACEZ_ERROR(ME, "Length of [%s] higher than [%d]", value, len);
        goto stop;
    }

    retval = 0;
stop:
    return retval;
}

static void print_TimeWindowList(TimeWindowStruct* timeWindowList[TIMEWINDOWLIST_MAX_LENGTH]) {
    for(size_t i = 0; i < TIMEWINDOWLIST_MAX_LENGTH; i++) {
        if(timeWindowList[i] != NULL) {
            SAH_TRACEZ_INFO(ME, "WindowStart [%d], WindowEnd [%d]", timeWindowList[i]->windowStart, timeWindowList[i]->windowEnd);
            SAH_TRACEZ_INFO(ME, "WindowMode [%s]", timeWindowList[i]->windowMode);
            SAH_TRACEZ_INFO(ME, "UserMessage [%s], MaxRetries [%d]", timeWindowList[i]->userMessage, timeWindowList[i]->maxRetries);
        }
    }
}

int DM_ENG_RPC_ScheduleDownload(const char* commandKey, const char* fileType, const char* url, const char* username, const char* password, uint32_t fileSize, const char* targetFileName, TimeWindowStruct* timeWindowList[TIMEWINDOWLIST_MAX_LENGTH]) {
    int retval = 0;
    retval = valid_String(commandKey, 32);
    when_failed_trace(retval, stop, ERROR, "Invalid CommandKey");
    retval = valid_FileType(fileType);
    when_failed_trace(retval, stop, ERROR, "Invalid FileType");
    retval = valid_URL(url, 256);
    when_failed_trace(retval, stop, ERROR, "Invalid URL");
    retval = valid_String(username, 256);
    when_failed_trace(retval, stop, ERROR, "Invalid Username");
    retval = valid_String(password, 256);
    when_failed_trace(retval, stop, ERROR, "Invalid Password");
    retval = valid_String(targetFileName, 256);
    when_failed_trace(retval, stop, ERROR, "Invalid TargetFileName");
    retval = valid_TimeWindowList(timeWindowList);
    when_failed_trace(retval, stop, ERROR, "Invalid TimeWindowList");

    SAH_TRACEZ_INFO(ME, "CommandKey [%s], FileType [%s]", commandKey, fileType);
    SAH_TRACEZ_INFO(ME, "URL [%s]", url);
    SAH_TRACEZ_INFO(ME, "Username [%s], Password [%s]", username, password);
    SAH_TRACEZ_INFO(ME, "FileSize [%d], TargetFileName [%s]", fileSize, targetFileName);
    print_TimeWindowList(timeWindowList);

    /* FIXME: Test broken due to this code */
    retval = device_adapter->DM_ENG_Device_ScheduleDownload(commandKey, fileType, url, username, password, fileSize, targetFileName, timeWindowList);

    DM_ENG_acsEventListAddEvent(DM_ENG_EVENT_M_SCHEDULEDOWNLOAD, commandKey);
    DM_ENG_acsEventListUpdate();

stop:
    return retval;
}

static int valid_operations(OperationStruct* operations) {
    int retval = DM_ENG_FAULTCODE_9003;
    when_null_trace(operations, stop, ERROR, "Invalid arg(s)");

    switch(operations->type) {
    case OPERATION_TYPE_INSTALL:
        retval = valid_URL(operations->data.install.url, 1024);
        when_failed_trace(retval, stop, ERROR, "Invalid URL");
        retval = valid_String(operations->data.install.uuid, 36);
        when_failed_trace(retval, stop, ERROR, "Invalid UUID");
        retval = valid_String(operations->data.install.username, 256);
        when_failed_trace(retval, stop, ERROR, "Invalid Username");
        retval = valid_String(operations->data.install.password, 256);
        when_failed_trace(retval, stop, ERROR, "Invalid Password");
        retval = valid_String(operations->data.install.executionEnvRef, 256);
        when_failed_trace(retval, stop, ERROR, "Invalid ExecutionEnvRef");
        break;

    case OPERATION_TYPE_UPDATE:
        retval = valid_String(operations->data.update.uuid, 36);
        when_failed_trace(retval, stop, ERROR, "Invalid UUID");
        retval = valid_String(operations->data.update.version, 32);
        when_failed_trace(retval, stop, ERROR, "Invalid Version");
        retval = valid_URL(operations->data.update.url, 1024);
        when_failed_trace(retval, stop, ERROR, "Invalid URL");
        retval = valid_String(operations->data.update.username, 256);
        when_failed_trace(retval, stop, ERROR, "Invalid Username");
        retval = valid_String(operations->data.update.password, 256);
        when_failed_trace(retval, stop, ERROR, "Invalid Password");
        break;

    case OPERATION_TYPE_UNINSTALL:
        retval = valid_String(operations->data.uninstall.uuid, 36);
        when_failed_trace(retval, stop, ERROR, "Invalid UUID");
        retval = valid_String(operations->data.update.version, 32);
        when_failed_trace(retval, stop, ERROR, "Invalid Version");
        retval = valid_String(operations->data.uninstall.executionEnvRef, 256);
        when_failed_trace(retval, stop, ERROR, "Invalid ExecutionEnvRef");
        break;

    default:
        retval = DM_ENG_FAULTCODE_9003;
        when_failed_trace(retval, stop, ERROR, "Invalid operations type");
        break;
    }

    retval = 0;
stop:
    return retval;
}

static void print_operations(OperationStruct* operations) {
    when_null_trace(operations, stop, ERROR, "Invalid arg(s)");

    switch(operations->type) {
    case OPERATION_TYPE_INSTALL:
        SAH_TRACEZ_INFO(ME, "Install operation:");
        SAH_TRACEZ_INFO(ME, "UUID [%s], ExecutionEnvRef [%s]", operations->data.install.uuid, operations->data.install.executionEnvRef);
        SAH_TRACEZ_INFO(ME, "URL [%s], Username [%s], Password [%s]", operations->data.install.url, operations->data.install.username, operations->data.install.password);
        break;

    case OPERATION_TYPE_UPDATE:
        SAH_TRACEZ_INFO(ME, "Update operation:");
        SAH_TRACEZ_INFO(ME, "UUID [%s], Version [%s]", operations->data.update.uuid, operations->data.update.version);
        SAH_TRACEZ_INFO(ME, "URL [%s], Username [%s], Password [%s]", operations->data.update.url, operations->data.update.username, operations->data.update.password);
        break;

    case OPERATION_TYPE_UNINSTALL:
        SAH_TRACEZ_INFO(ME, "Uninstall operation:");
        SAH_TRACEZ_INFO(ME, "UUID [%s], Version [%s], ExecutionEnvRef [%s]", operations->data.uninstall.uuid, operations->data.uninstall.version, operations->data.uninstall.executionEnvRef);
        break;

    default:
        SAH_TRACEZ_ERROR(ME, "Invalid operations type");
        break;
    }

stop:
    return;
}

int DM_ENG_RPC_ChangeDUState(OperationStruct* operations, const char* commandKey) {
    int retval = 0;
    retval = valid_operations(operations);
    when_failed_trace(retval, stop, ERROR, "Invalid Operations");
    retval = valid_String(commandKey, 32);
    when_failed_trace(retval, stop, ERROR, "Invalid CommandKey");

    print_operations(operations);
    SAH_TRACEZ_INFO(ME, "CommandKey [%s]", commandKey);

    /* FIXME: Test broken due to this code */
    #if 1
    retval = device_adapter->DM_ENG_Device_ChangeDUState(operations, commandKey);
    DM_ENG_acsEventListAddEvent(DM_ENG_EVENT_M_CHANGEDUSTATE, commandKey);
    DM_ENG_acsEventListUpdate();
    #endif
stop:
    return retval;
}

