/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <dmengine/DM_ENG_Mapping.h>
#include <dmengine/DM_ENG_RPCInterface.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>

#define ME "DM_ENGINE"

typedef struct _mapping_entry_t {
    char* src;
    char* dst;
    amxc_llist_it_t it;
} mapping_entry_t;

static amxc_llist_t mapping_list;

static char* smm_mapping = NULL;

mapping_entry_t* mapping_entry_add(const char* src, const char* dst) {
    mapping_entry_t* entry = NULL;
    when_str_empty(src, stop);
    when_str_empty(dst, stop);
    entry = (mapping_entry_t*) calloc(1, sizeof(mapping_entry_t));
    entry->src = strdup(src);
    entry->dst = strdup(dst);
stop:
    return entry;
}

void mapping_entry_delete(mapping_entry_t** entry) {
    when_null(entry, stop);
    when_null(*entry, stop);
    free((*entry)->src);
    free((*entry)->dst);
    free(*entry);
    *entry = NULL;
stop:
    return;
}

void mapping_list_print(amxc_llist_t* data) {
    mapping_entry_t* entry = NULL;
    amxc_llist_for_each(lit, data) {
        entry = amxc_container_of(lit, mapping_entry_t, it);
        SAH_TRACEZ_INFO(ME, "Mapping: Src [%s] - Dst [%s]", entry->src, entry->dst);
    }
}

void mapping_entry_delete_func(amxc_llist_it_t* it) {
    mapping_entry_t* entry = amxc_container_of(it, mapping_entry_t, it);
    amxc_llist_it_take(it);
    mapping_entry_delete(&entry);
}

static void build_mapping(amxc_var_t* data) {
    mapping_entry_t* entry = NULL;

    when_true(amxc_var_is_null(data), stop);

    amxc_var_for_each(var, data) {
        entry = mapping_entry_add(GET_CHAR(var, "Src"), GET_CHAR(var, "Dst"));
        if(entry != NULL) {
            amxc_llist_append(&mapping_list, &entry->it);
        }
    }

    mapping_list_print(&mapping_list);
stop:
    return;
}

int DM_ENG_Mapping_init(void) {
    int retval = -1;
    amxc_var_t data;
    amxc_llist_init(&mapping_list);
    amxc_var_init(&data);
    when_failed_trace(DM_ENG_GetMapping(&data), stop, ERROR, "Failed to get mapping info");
    build_mapping(GETI_ARG(&data, 0));
    when_failed_trace(DM_ENG_GetSMMMapping(&smm_mapping), stop, ERROR, "Failed to get smm mapping info");
    amxc_var_clean(&data);
    retval = 0;
stop:
    return retval;
}

void DM_ENG_Mapping_clean(void) {
    free(smm_mapping);
    amxc_llist_clean(&mapping_list, mapping_entry_delete_func);
}

const char* DM_ENG_Mapping_get_dst(const char* src) {
    mapping_entry_t* entry = NULL;
    const char* dst = src;

    when_str_empty(src, stop);
    when_true((amxc_llist_is_empty(&mapping_list) == true), stop);
    amxc_llist_for_each(lit, &mapping_list) {
        entry = amxc_container_of(lit, mapping_entry_t, it);
        if((entry != NULL) && (entry->src != NULL) && (strcmp(entry->src, src) == 0)) {
            dst = entry->dst;
        }
    }

stop:
    return dst;
}

void DM_ENG_Mapping_smm_translate(char** str, size_t* str_len, bool direction) {
    amxc_string_t tmp;
    amxc_string_init(&tmp, 0);

    when_str_empty(smm_mapping, stop);
    when_null(str, stop);
    when_true((str_len == 0), stop);
    when_str_empty((*str), stop);

    amxc_string_set(&tmp, (*str));
    if(direction == false) {
        amxc_string_replace(&tmp, smm_mapping, "Device.SoftwareModules", UINT32_MAX);
    } else {
        amxc_string_replace(&tmp, "Device.SoftwareModules", smm_mapping, UINT32_MAX);
    }
    free((*str));
    *str = amxc_string_dup(&tmp, 0, amxc_string_text_length(&tmp));
    *str_len = strlen((const char*) (*str));
stop:
    amxc_string_clean(&tmp);
    return;
}