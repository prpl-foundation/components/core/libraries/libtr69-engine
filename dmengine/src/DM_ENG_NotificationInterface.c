/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : DM_ENG_NotificationInterface.c
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_ENG_NotificationInterface.c
 *
 * @brief
 *
 **/


#include <dmengine/DM_ENG_NotificationInterface.h>
#include <string.h>

static DM_ENG_NotificationInterface* listeners[2] = { NULL, NULL };

void DM_ENG_NotificationInterface_activate(DM_ENG_EntityType entity,
                                           DM_ENG_F_INFORM inform,
                                           DM_ENG_F_TRANSFER_COMPLETE transferComplete,
                                           DM_ENG_F_REQUEST_DOWNLOAD requestDownload,
                                           DM_ENG_F_GETRPCMETHODS getRPCMethods,
                                           DM_ENG_F_TIMER_START timerStart,
                                           DM_ENG_F_TIMER_STOP timerStop,
                                           DM_ENG_F_TIMER_TIME_REMAINING timerTimeRemaining,
                                           DM_ENG_F_EVENT engineEvent,
                                           DM_ENG_F_DU_STATE_CHANGE_COMPLETE duStateChangeComplete) {
    DM_ENG_NotificationInterface_deactivate(entity);
    switch(entity) {
    case DM_ENG_EntityType_ACS:
    case DM_ENG_EntityType_SELFCARE_APP:
        listeners[entity] = (DM_ENG_NotificationInterface*) malloc(sizeof(DM_ENG_NotificationInterface));
        listeners[entity]->inform = inform;
        listeners[entity]->getRPCMethods = getRPCMethods;
        listeners[entity]->transferComplete = transferComplete;
        listeners[entity]->requestDownload = requestDownload;
        listeners[entity]->timerStart = timerStart;
        listeners[entity]->timerStop = timerStop;
        listeners[entity]->timerTimeRemaining = timerTimeRemaining;
        listeners[entity]->engineEvent = engineEvent;
        listeners[entity]->duStateChangeComplete = duStateChangeComplete;
        break;
    case DM_ENG_EntityType_SYSTEM:
    case DM_ENG_EntityType_SUBSCRIBER:
    case DM_ENG_EntityType_ANY:
        break;
    }
}

void DM_ENG_NotificationInterface_deactivateAll() {
    DM_ENG_NotificationInterface_deactivate(DM_ENG_EntityType_ACS);
    DM_ENG_NotificationInterface_deactivate(DM_ENG_EntityType_SELFCARE_APP);
}

void DM_ENG_NotificationInterface_deactivate(DM_ENG_EntityType entity) {
    if(listeners[entity] != NULL) {
        switch(entity) {
        case DM_ENG_EntityType_ACS:
        case DM_ENG_EntityType_SELFCARE_APP:

            free(listeners[entity]);
            listeners[entity] = NULL;
            break;
        case DM_ENG_EntityType_SYSTEM:
        case DM_ENG_EntityType_SUBSCRIBER:
        case DM_ENG_EntityType_ANY:
            break;
        }
    }
}


DM_ENG_NotificationStatus DM_ENG_NotificationInterface_inform(DM_ENG_DeviceIdStruct* id, DM_ENG_EventStruct* events[], DM_ENG_ParameterValueStruct* parameterList[], time_t currentTime, unsigned int retryCount) {
    DM_ENG_NotificationStatus status = DM_ENG_CANCELLED;
    int entity;
    for(entity = 0; entity < 2; entity++) {
        if((listeners[entity] != NULL) && (listeners[entity]->inform != NULL)) {
            DM_ENG_NotificationStatus res = DM_ENG_CANCELLED;
            res = (DM_ENG_NotificationStatus) (listeners[entity]->inform)(id, events, parameterList, currentTime, retryCount);

            if((DM_ENG_EntityType) entity == DM_ENG_EntityType_ACS) {
                status = res;
            }                                                     // seul le résultat de l'envoi vers l'ACS compte
        }
    }
    return status;
}

DM_ENG_NotificationStatus DM_ENG_NotificationInterface_transferComplete(DM_ENG_TransferCompleteStruct* tcs) {
    DM_ENG_NotificationStatus status = DM_ENG_CANCELLED;
    int entity;
    for(entity = 0; entity < 2; entity++) {
        if((listeners[entity] != NULL) && (listeners[entity]->transferComplete != NULL)) {
            DM_ENG_NotificationStatus res = (DM_ENG_NotificationStatus) (listeners[entity]->transferComplete)(tcs);
            if((DM_ENG_EntityType) entity == DM_ENG_EntityType_ACS) {
                status = res;
            }                                                     // seul le résultat de l'envoi vers l'ACS compte
        }
    }
    return status;
}

DM_ENG_NotificationStatus DM_ENG_NotificationInterface_requestDownload(const char* fileType, DM_ENG_ArgStruct* args[]) {
    DM_ENG_NotificationStatus status = DM_ENG_CANCELLED;
    int entity;
    for(entity = 0; entity < 2; entity++) {
        if((listeners[entity] != NULL) && (listeners[entity]->requestDownload != NULL)) {
            DM_ENG_NotificationStatus res = (DM_ENG_NotificationStatus) (listeners[entity]->requestDownload)(fileType, args);
            if(entity == DM_ENG_EntityType_ACS) {
                status = res;
            }                                  // seul le résultat de l'envoi vers l'ACS compte
        }
    }
    return status;
}

DM_ENG_NotificationStatus DM_ENG_NotificationInterface_getRPCMethods() {
    DM_ENG_NotificationStatus status = DM_ENG_CANCELLED;
    int entity;
    for(entity = 0; entity < 2; entity++) {
        if((listeners[entity] != NULL) && (listeners[entity]->getRPCMethods != NULL)) {
            DM_ENG_NotificationStatus res = (DM_ENG_NotificationStatus) (listeners[entity]->getRPCMethods)();
            if(entity == DM_ENG_EntityType_ACS) {
                status = res;
            }                                  // seul le résultat de l'envoi vers l'ACS compte
        }
    }
    return status;
}

DM_ENG_NotificationStatus DM_ENG_NotificationInterface_timerStart(const char* name, int waitTime, int intervalTime, timerHandler handler) {
    DM_ENG_NotificationStatus status = DM_ENG_CANCELLED;
    int entity;
    for(entity = 0; entity < 2; entity++) {
        if((listeners[entity] != NULL) && (listeners[entity]->timerStart != NULL)) {
            DM_ENG_NotificationStatus res = (DM_ENG_NotificationStatus) (listeners[entity]->timerStart)(name, waitTime, intervalTime, handler);
            if(entity == DM_ENG_EntityType_ACS) {
                status = res;
            }                                  // seul le résultat de l'envoi vers l'ACS compte
        }
    }
    return status;
}

DM_ENG_NotificationStatus DM_ENG_NotificationInterface_timerStop(const char* name) {
    DM_ENG_NotificationStatus status = DM_ENG_CANCELLED;
    int entity;
    for(entity = 0; entity < 2; entity++) {
        if((listeners[entity] != NULL) && (listeners[entity]->timerStop != NULL)) {
            DM_ENG_NotificationStatus res = (DM_ENG_NotificationStatus) (listeners[entity]->timerStop)(name);
            if(entity == DM_ENG_EntityType_ACS) {
                status = res;
            }                                  // seul le résultat de l'envoi vers l'ACS compte
        }
    }
    return status;
}

unsigned int DM_ENG_NotificationInterface_timer_remainingTime(const char* name) {
    unsigned int res = 0;
    unsigned int acsres = 0;
    int entity;
    for(entity = 0; entity < 2; entity++) {
        if((listeners[entity] != NULL) && (listeners[entity]->timerTimeRemaining != NULL)) {
            res = (DM_ENG_NotificationStatus) (listeners[entity]->timerTimeRemaining)(name);
            if(entity == DM_ENG_EntityType_ACS) {
                acsres = res;
            }                                  // seul le résultat de l'envoi vers l'ACS compte
        }
    }
    return acsres;
}


DM_ENG_NotificationStatus DM_ENG_NotificationInterface_engineEvent(const char* eventType) {
    DM_ENG_NotificationStatus status = DM_ENG_CANCELLED;
    int entity;
    for(entity = 0; entity < 2; entity++) {
        if((listeners[entity] != NULL) && (listeners[entity]->engineEvent != NULL)) {
            DM_ENG_NotificationStatus res = (DM_ENG_NotificationStatus) (listeners[entity]->engineEvent)(eventType);
            if(entity == DM_ENG_EntityType_ACS) {
                status = res;
            }                                  // seul le résultat de l'envoi vers l'ACS compte
        }
    }
    return status;
}

DM_ENG_NotificationStatus DM_ENG_NotificationInterface_DUStateChangeComplete(dscc_t* dscc) {
    DM_ENG_NotificationStatus status = DM_ENG_CANCELLED;
    int entity;
    for(entity = 0; entity < 2; entity++) {
        if((listeners[entity] != NULL) && (listeners[entity]->duStateChangeComplete != NULL)) {
            DM_ENG_NotificationStatus res = (DM_ENG_NotificationStatus) (listeners[entity]->duStateChangeComplete)(dscc);
            if((DM_ENG_EntityType) entity == DM_ENG_EntityType_ACS) {
                status = res;
            }
        }
    }
    return status;
}
