include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C dmcom all
	$(MAKE) -C dmcommon all
	$(MAKE) -C dmengine all
	$(MAKE) -C dmxmlparser all

clean:
	$(MAKE) -C dmcom clean
	$(MAKE) -C dmcommon clean
	$(MAKE) -C dmengine clean
	$(MAKE) -C dmxmlparser clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/dmcom
	$(INSTALL) -D -p -m 0644 dmcom/include/dmcom/*.h $(DEST)$(INCLUDEDIR)/dmcom/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/libdmcom/libdmcom.so.$(VERSION) $(DEST)$(LIBDIR)/libdmcom.so.$(VERSION)
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/dmcommon
	$(INSTALL) -D -p -m 0644 dmcommon/include/dmcommon/*.h $(DEST)$(INCLUDEDIR)/dmcommon/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/libdmcommon/libdmcommon.so.$(VERSION) $(DEST)$(LIBDIR)/libdmcommon.so.$(VERSION)
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/dmengine
	$(INSTALL) -D -p -m 0644 dmengine/include/dmengine/*.h $(DEST)$(INCLUDEDIR)/dmengine/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/libdmengine/libdmengine.so.$(VERSION) $(DEST)$(LIBDIR)/libdmengine.so.$(VERSION)
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/dmxmlparser
	$(INSTALL) -D -p -m 0644 dmxmlparser/include/dmxmlparser/*.h $(DEST)$(INCLUDEDIR)/dmxmlparser/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/libdmxml_ixml/libdmxml_ixml.so.$(VERSION) $(DEST)$(LIBDIR)/libdmxml_ixml.so.$(VERSION)
	ln -sfr $(DEST)$(LIBDIR)/libdmcom.so.$(VERSION) $(DEST)$(LIBDIR)/libdmcom.so.$(VMAJOR)
	ln -sfr $(DEST)$(LIBDIR)/libdmcom.so.$(VERSION) $(DEST)$(LIBDIR)/libdmcom.so
	ln -sfr $(DEST)$(LIBDIR)/libdmcommon.so.$(VERSION) $(DEST)$(LIBDIR)/libdmcommon.so.$(VMAJOR)
	ln -sfr $(DEST)$(LIBDIR)/libdmcommon.so.$(VERSION) $(DEST)$(LIBDIR)/libdmcommon.so
	ln -sfr $(DEST)$(LIBDIR)/libdmengine.so.$(VERSION) $(DEST)$(LIBDIR)/libdmengine.so.$(VMAJOR)
	ln -sfr $(DEST)$(LIBDIR)/libdmengine.so.$(VERSION) $(DEST)$(LIBDIR)/libdmengine.so
	ln -sfr $(DEST)$(LIBDIR)/libdmxml_ixml.so.$(VERSION) $(DEST)$(LIBDIR)/libdmxml_ixml.so.$(VMAJOR)
	ln -sfr $(DEST)$(LIBDIR)/libdmxml_ixml.so.$(VERSION) $(DEST)$(LIBDIR)/libdmxml_ixml.so

package: all
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/dmcom
	$(INSTALL) -D -p -m 0644 dmcom/include/dmcom/*.h $(PKGDIR)$(INCLUDEDIR)/dmcom/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/libdmcom/libdmcom.so.$(VERSION) $(PKGDIR)$(LIBDIR)/libdmcom.so.$(VERSION)
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/dmcommon
	$(INSTALL) -D -p -m 0644 dmcommon/include/dmcommon/*.h $(PKGDIR)$(INCLUDEDIR)/dmcommon/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/libdmcommon/libdmcommon.so.$(VERSION) $(PKGDIR)$(LIBDIR)/libdmcommon.so.$(VERSION)
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/dmengine
	$(INSTALL) -D -p -m 0644 dmengine/include/dmengine/*.h $(PKGDIR)$(INCLUDEDIR)/dmengine/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/libdmengine/libdmengine.so.$(VERSION) $(PKGDIR)$(LIBDIR)/libdmengine.so.$(VERSION)
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/dmxmlparser
	$(INSTALL) -D -p -m 0644 dmxmlparser/include/dmxmlparser/*.h $(PKGDIR)$(INCLUDEDIR)/dmxmlparser/
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/libdmxml_ixml/libdmxml_ixml.so.$(VERSION) $(PKGDIR)$(LIBDIR)/libdmxml_ixml.so.$(VERSION)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package test