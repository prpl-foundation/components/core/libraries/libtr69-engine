/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : dm_com_digest.c
 *
 * Created     : 2008/06/05
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file dm_com_digest.c
 *
 *
 * This file performs the ACS Digest Autkentication
 *
 */

#include <dmcom/dm_com_digest.h>
#include <dmengine/DM_ENG_Global.h>
#include <debug/sahtrace.h>
#include <string.h>

#define NONCESIZE  (34)
#define OPAQUESIZE (32)

extern char* g_randomCpeUrl;

/* generate request digest message, used to send an authentication request to ACS Server.*/
char*
DM_COM_GenerateRequestDigestMsg(char* randomNonceStr, char* randomOpaqueStr) {
    char* requestDigestMsg = (char*) malloc(SIZE_HTTP_MSG); // No need to require a very large buffer.
    memset((void*) requestDigestMsg, 0x00, SIZE_HTTP_MSG);

    memset((void*) &randomNonceStr[0], '\0', sizeof(randomNonceStr));
    memset((void*) &randomOpaqueStr[0], '\0', sizeof(randomOpaqueStr));

    // Generate a random nonce and opaque string
    if((!_generateRandomString(randomNonceStr, NONCESIZE)) ||
       (!_generateRandomString(randomOpaqueStr, OPAQUESIZE))) {
        SAH_TRACEZ_ERROR("DM_COM", "CAN NOT GENERATE RANDOM STRING");
        free(requestDigestMsg);
        return NULL;
    }

    snprintf(requestDigestMsg, SIZE_HTTP_MSG, requestDigestAuthStr, randomNonceStr, randomOpaqueStr);
    SAH_TRACEZ_INFO("DM_COM", "Request Digest Authentication Message:%s", requestDigestMsg);
    return requestDigestMsg;
}

#if 0
/**
 * @brief Private routine used to send an authentication
 *        request tp the ACS.
 *
 * @param The socket aon wich the message is sent
 *
 * @Return OK DM_OK or ERROR
 *
 */
DMRET
_sendAuthenticationRequest(int nSocksExp) {
    char requestDigestMsg[SIZE_HTTP_MSG]; // No need to require a very large buffer.
    DMRET nRet = DM_ERR;

    SAH_TRACEZ_INFO("DM_COM", "Send Authentication Digest Request to the ACS");

    memset((void*) requestDigestMsg, 0x00, SIZE_HTTP_MSG);

    memset((void*) &randomNonceStr[0], '\0', sizeof(randomNonceStr));
    memset((void*) &randomOpaqueStr[0], '\0', sizeof(randomOpaqueStr));

    // Generate a random nonce and opaque string
    if((!_generateRandomString(randomNonceStr, NONCESIZE)) ||
       (!_generateRandomString(randomOpaqueStr, OPAQUESIZE))) {
        SAH_TRACEZ_ERROR("DM_COM", "CAN NOT GENERATE RANDOM STRING");
        return nRet;
    }

    snprintf(requestDigestMsg, SIZE_HTTP_MSG, requestDigestAuthStr, randomNonceStr, randomOpaqueStr);
    SAH_TRACEZ_INFO("DM_COM", "Request Digest Authentication Message:\n%s", requestDigestMsg);

    // Send the message to the ACS and Wait for the response.
#ifndef WIN32
    if(-1 == send(nSocksExp, requestDigestMsg, strlen(requestDigestMsg), MSG_DONTWAIT)) {
        SAH_TRACEZ_ERROR("DM_COM", "Error sending Authentication Request message on socket!!!");
    } else {
        nRet = DM_OK;
    }
#else
    if(-1 == send(nSocksExp, requestDigestMsg, strlen(requestDigestMsg), 0)) {
        SAH_TRACEZ_ERROR("DM_COM", "Error sending Authentication Request message on socket!!!");
    } else {
        nRet = DM_OK;
    }
#endif

    return nRet;
}
#endif

/**
 * @brief Private routine used to check if all
 *        the digest authentication data are in the message
 *
 * @param A pointer on the message to analyse
 *
 * @Return TRUE if all the digest authentication data are in the message
 *         FALSE otherwise
 *
 */
bool
DM_COM_CheckDigestAuthMessageContent(const char* _str) {
    // Make sure "Authorization:", "Digest username=", "realm=", "nonce=",
    // "uri=", "response=", "opaque=", "qop=", "nc" and "cnonce" are in the message

    if((NULL == strstr(_str, AuthorizationToken)) ||
       (NULL == strstr(_str, DigestUsernameToken)) ||
       (NULL == strstr(_str, realmToken)) ||
       (NULL == strstr(_str, nonceToken)) ||
       (NULL == strstr(_str, uriToken)) ||
       (NULL == strstr(_str, responseToken)) ||
       (NULL == strstr(_str, opaqueToken)) ||
       (NULL == strstr(_str, qopToken)) ||
       (NULL == strstr(_str, ncToken)) ||
       (NULL == strstr(_str, cnonceToken))) {
        SAH_TRACEZ_INFO("DM_COM", "No Digest Authentication Data found (or partial)");
        return false;
    }

    SAH_TRACEZ_INFO("DM_COM", "Digest Authentication Data found");
    return true;
}

/**
 * @brief Private routine used to get the string betwen quote ("The stream to retrieve")
 *
 * @param IN: A pointer on the message to analyse. The string to retrieve is the next string between "".
 *        OUT: The requested string. The memory must be allocated by the calling routine.
 *
 * @Return TRUE if everything if fine
 *         FALSE otherwise.
 *
 */
static bool _getQuotedString(const char* _inTokenStr, char* _outQuotedStr) {
    char* firstTokenStr = NULL;
    char* secondTokenStr = NULL;
    char tmpQuotedStr[MAXTOKENSIZE];
    int i = 0;

    memset((void*) tmpQuotedStr, 0x00, MAXTOKENSIZE);

    if(!_inTokenStr) {
        return false;
    }

    // Get the string between the first '"' and the second '"'.
    // Find the first '"'
    firstTokenStr = (char*) strchr(_inTokenStr, '"');
    if(NULL == firstTokenStr) {
        // SAH_TRACEZ_INFO("DM_COM", "Can not find the second '"'\n");
        return false;
    }
    // Increment firstTokenStr to go to the next charactere
    firstTokenStr++;

    // Find the second '"'
    secondTokenStr = strchr(firstTokenStr, '"');
    if(NULL == secondTokenStr) {
        // SAH_TRACEZ_INFO("DM_COM", "Can not find the first '"'\n");
        return false;
    }

    while(firstTokenStr != secondTokenStr) {
        tmpQuotedStr[i] = *firstTokenStr;
        firstTokenStr++;
        i++;
        if(i >= MAXTOKENSIZE) {
            // Error, MAXTOKENSIZE reached
            return false;
        }
    }

    strncpy(_outQuotedStr, &tmpQuotedStr[0], MAXTOKENSIZE);

    return true;
}

/**
 * @brief Private routine used to get the string betwen = and , (=The stream to retrieve,)
 *
 * @param IN: A pointer on the message to analyse. The string to retrieve is the next string between =...,
 *        OUT: The requested string. The memory must be allocated by the calling routine.
 *
 * @Return TRUE if everything if fine
 *         FALSE otherwise.
 *
 */
static bool _getNonQuotedString(const char* _inTokenStr, char* _outNonQuotedStr) {
    char* firstTokenStr = NULL;
    char* secondTokenStr = NULL;
    char tmpQuotedStr[MAXTOKENSIZE];
    int i = 0;

    memset((void*) tmpQuotedStr, 0x00, MAXTOKENSIZE);

    if(!_inTokenStr) {
        return false;
    }

    // Get the string between '=' and ','.
    // Find the first '='
    firstTokenStr = (char*) strchr(_inTokenStr, '=');
    if(NULL == firstTokenStr) {
        // SAH_TRACEZ_INFO("DM_COM", "Can not find the second '='\n");
        return false;
    }
    // Increment firstTokenStr to go to the next charactere
    firstTokenStr++;

    // Find the second token ','
    secondTokenStr = strchr(firstTokenStr, ',');
    if(NULL == secondTokenStr) {
        // SAH_TRACEZ_INFO("DM_COM", "Can not find the first ','\n");
        return false;
    }

    while(firstTokenStr != secondTokenStr) {
        tmpQuotedStr[i] = *firstTokenStr;
        firstTokenStr++;
        i++;
        if(i >= MAXTOKENSIZE) {
            // Error, MAXTOKENSIZE reached
            return false;
        }
    }

    strncpy(_outNonQuotedStr, &tmpQuotedStr[0], MAXTOKENSIZE);

    return true;
}

/**
 * @brief Private routine used to get all the Digest Authentication Data
 *
 * @param
 *
 * @Return TRUE if everything if fine
 *         FALSE otherwise.
 *
 */
static bool _getDigestMessageToken(
    const char* _str,
    char* digestUsernameTokenStr,
    char* realmTokenStr,
    char* nonceTokenStr,
    char* uriTokenStr,
    char* responseTokenStr,
    char* opaqueTokenStr,
    char* qopTokenStr,
    char* ncTokenStr,
    char* cnonceTokenStr) {

    char* tmpStr = NULL;

    SAH_TRACEZ_INFO("DM_COM", "HTTP Message to analyse:\n%s", _str);

    // ---------- Retrieve the digestUsernameTokenStr ----------
    tmpStr = (char*) strstr(_str, DigestUsernameToken);
    if(!_getQuotedString(tmpStr, digestUsernameTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the DigestUsername");
        return false;
    }
    SAH_TRACEZ_INFO("DM_COM", "digestUsernameTokenStr=%s", digestUsernameTokenStr);

    // ---------- Retrieve the realmTokenStr ----------
    tmpStr = (char*) strstr(_str, realmToken);
    if(!_getQuotedString(tmpStr, realmTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the realmTokenStr");
        return false;
    }
    SAH_TRACEZ_INFO("DM_COM", "realmTokenStr=%s", realmTokenStr);

    // ---------- Retrieve the nonceTokenStr ----------
    tmpStr = (char*) strstr(_str, nonceToken);
    if(!_getQuotedString(tmpStr, nonceTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the nonceTokenStr");
        return false;
    }
    SAH_TRACEZ_INFO("DM_COM", "nonceTokenStr=%s", nonceTokenStr);

    // ---------- Retrieve the uriTokenStr ----------
    tmpStr = (char*) strstr(_str, uriToken);
    if(!_getQuotedString(tmpStr, uriTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the uriTokenStr");
        return false;
    }
    SAH_TRACEZ_INFO("DM_COM", "uriTokenStr=%s", uriTokenStr);

    // ---------- Retrieve the responseTokenStr ----------
    tmpStr = (char*) strstr(_str, responseToken);
    if(!_getQuotedString(tmpStr, responseTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the responseTokenStr");
        return false;
    }
    SAH_TRACEZ_INFO("DM_COM", "responseTokenStr=%s", responseTokenStr);

    // ---------- Retrieve the opaqueTokenStr ----------
    tmpStr = (char*) strstr(_str, opaqueToken);
    if(!_getQuotedString(tmpStr, opaqueTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the opaqueTokenStr");
        return false;
    }
    SAH_TRACEZ_INFO("DM_COM", "opaqueTokenStr=%s", opaqueTokenStr);

    // ---------- Retrieve the qopTokenStr ----------
    tmpStr = (char*) strstr(_str, qopToken);
    if(!_getNonQuotedString(tmpStr, qopTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the qopTokenStr");
        return false;
    }
    // qop value should not be quoted but some server (like SAH ACS) send it as quoted value (qop="auth",)
    // check the first char in qop value if '"' so qop is qouted
    if(qopTokenStr[0] == '"') {
        if(!_getQuotedString(tmpStr, qopTokenStr)) {
            SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the qopTokenStr");
            return false;
        }
    }
    SAH_TRACEZ_INFO("DM_COM", "qopTokenStr=%s", qopTokenStr);

    // ---------- Retrieve the ncTokenStr ----------
    tmpStr = (char*) strstr(_str, ncToken);
    if(!_getNonQuotedString(tmpStr, ncTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the ncTokenStr");
        return false;
    }
    // nc value should not be quoted but some server (like SAH ACS) send it as quoted value
    // check the first char in qop value if '"' so qop is qouted
    if(ncTokenStr[0] == '"') {
        if(!_getQuotedString(tmpStr, ncTokenStr)) {
            SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the ncTokenStr");
            return false;
        }
    }
    SAH_TRACEZ_INFO("DM_COM", "ncTokenStr=%s", ncTokenStr);

    // ---------- Retrieve the cnonceTokenStr ----------
    tmpStr = (char*) strstr(_str, cnonceToken);
    if(!_getQuotedString(tmpStr, cnonceTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Can not retrieve the cnonceTokenStr");
        return false;
    }
    SAH_TRACEZ_INFO("DM_COM", "cnonceTokenStr=%s", cnonceTokenStr);

    return true;
}

/**
 * @brief Private routine used to check if the Digest Authentication message can be accepted or rejected.
 *
 * @param
 *
 * @Return TRUE if everything if fine
 *         FALSE otherwise.
 *
 */
bool
DM_COM_PerformClientDigestAuthentication(const char* _str,
                                         char* randomNonceStr,
                                         char* randomOpaqueStr,
                                         const char* cpeUsername,
                                         const char* cpeConnectionRequestPassword) {

    bool nRet = false;
    unsigned char signature[SIGNATURE_SIZE];

    char digestUsernameTokenStr[MAXTOKENSIZE];
    char realmTokenStr[MAXTOKENSIZE];
    char nonceTokenStr[MAXTOKENSIZE];
    char uriTokenStr[MAXTOKENSIZE];
    char responseTokenStr[MAXTOKENSIZE];
    char opaqueTokenStr[MAXTOKENSIZE];
    char qopTokenStr[MAXTOKENSIZE];
    char ncTokenStr[MAXTOKENSIZE];
    char cnonceTokenStr[MAXTOKENSIZE];
    const char* httpMethodPtr = NULL;
    char pBufferTemp[SIZE_HTTP_MSG]; // No need to require a very large buffer.

    // Below variables used to compute the response
    MD5_CTX ha1Ctx;
    MD5_CTX ha2Ctx;
    MD5_CTX responseCtx;
    char ha1Str[HA1_MAXTOKENSIZE];
    char ha2Str[HA2_MAXTOKENSIZE];
    char totalStr[TOTAL_MAXTOKENSIZE];
    char ha1StrResult[MAXTOKENSIZE];
    char ha2StrResult[MAXTOKENSIZE];
    char totalStrResult[MAXTOKENSIZE];

    memset((void*) digestUsernameTokenStr, 0x00, MAXTOKENSIZE);
    memset((void*) realmTokenStr, 0x00, MAXTOKENSIZE);
    memset((void*) nonceTokenStr, 0x00, MAXTOKENSIZE);
    memset((void*) uriTokenStr, 0x00, MAXTOKENSIZE);
    memset((void*) responseTokenStr, 0x00, MAXTOKENSIZE);
    memset((void*) opaqueTokenStr, 0x00, MAXTOKENSIZE);
    memset((void*) qopTokenStr, 0x00, MAXTOKENSIZE);
    memset((void*) ncTokenStr, 0x00, MAXTOKENSIZE);
    memset((void*) cnonceTokenStr, 0x00, MAXTOKENSIZE);
    memset((void*) pBufferTemp, 0x00, SIZE_HTTP_MSG);

    memset((void*) ha1Str, 0x00, HA1_MAXTOKENSIZE);
    memset((void*) ha2Str, 0x00, HA2_MAXTOKENSIZE);
    memset((void*) totalStr, 0x00, TOTAL_MAXTOKENSIZE);

    // Get all the digest data from the message
    if(!_getDigestMessageToken(_str,
                               digestUsernameTokenStr,
                               realmTokenStr,
                               nonceTokenStr,
                               uriTokenStr,
                               responseTokenStr,
                               opaqueTokenStr,
                               qopTokenStr,
                               ncTokenStr,
                               cnonceTokenStr)) {
        SAH_TRACEZ_ERROR("DM_COM", "Can not retrieve all the HTTP Digest Tokens");
        return false;
    }

    // Make sure the nonce and opaque str are identical to
    // the strings sent in the request authentication demand message
    // Note: These strings are generated randomly
    if((0 != strcmp(randomNonceStr, nonceTokenStr)) ||
       (0 != strcmp(randomOpaqueStr, opaqueTokenStr))) {
        SAH_TRACEZ_ERROR("DM_COM", "Authentication request does not match the generated Authentication demand message");
        SAH_TRACEZ_ERROR("DM_COM", "Nonce and opaue strings are diferent from the generated ones.");
        return false;
    } else {
        SAH_TRACEZ_INFO("DM_COM", "HTTP Digest MD5 Auth - Nonce and Opaque are valid");
    }

    // Make sure the requested URL corrspond to the randomly choosen one.
    if(NULL == strstr(uriTokenStr, g_randomCpeUrl)) {
        SAH_TRACEZ_ERROR("DM_COM", "The requested and choosen URL are not equal");
        SAH_TRACEZ_ERROR("DM_COM", "Requested: %s, randomly Choosen by CPE: %s", uriTokenStr, g_randomCpeUrl);
        // printf("Must return FALSE but for test purpose and while DM_ENGINE does not use g_randomCpeUrl continue...");
        return false;
    } else {
        SAH_TRACEZ_INFO("DM_COM", "HTTP Digest MD5 Auth - CPE URL is Valid");
    }

    SAH_TRACEZ_INFO("DM_COM", "CPE username: %s, password: %s", cpeUsername, cpeConnectionRequestPassword);

    // Build ha1Str (username:realm:password)
    strncpy(ha1Str, cpeUsername, HA1_MAXTOKENSIZE);
    strncat(ha1Str, ":", HA1_MAXTOKENSIZE - strlen(ha1Str));
    strncat(ha1Str, realmTokenStr, HA1_MAXTOKENSIZE - strlen(ha1Str));
    strncat(ha1Str, ":", HA1_MAXTOKENSIZE - strlen(ha1Str));
    strncat(ha1Str, cpeConnectionRequestPassword, HA1_MAXTOKENSIZE - strlen(ha1Str));

    // Compute the response.
    SAH_TRACEZ_INFO("DM_COM", "DIGEST AUTH - ha1Str:%s", ha1Str);

    MD5_Init(&ha1Ctx);
    MD5_Update(&ha1Ctx, (unsigned char*) ha1Str, strlen(ha1Str));
    memset((void*) signature, 0x00, SIGNATURE_SIZE);
    MD5_Final(signature, &ha1Ctx);
    getStringResult(signature, ha1StrResult);
    SAH_TRACEZ_INFO("DM_COM", "DIGEST AUTH - MD5(ha1Str):%s", ha1StrResult);

    // Build ha2Str (HTTPCommand:uri)
    // Recopy the buffer into a temp one to use strtok
    strncpy(pBufferTemp, _str, SIZE_HTTP_MSG);
    // httpMethodPtr   = strtok( pBufferTemp, " " );
    httpMethodPtr = "GET";
    strncpy(ha2Str, httpMethodPtr, HA2_MAXTOKENSIZE);
    strncat(ha2Str, ":", HA2_MAXTOKENSIZE - strlen(ha2Str));
    strncat(ha2Str, uriTokenStr, HA2_MAXTOKENSIZE - strlen(ha2Str));

    SAH_TRACEZ_INFO("DM_COM", "DIGEST AUTH - ha2Str:%s", ha2Str);

    MD5_Init(&ha2Ctx);
    MD5_Update(&ha2Ctx, (unsigned char*) ha2Str, strlen(ha2Str));
    memset((void*) signature, 0x00, SIGNATURE_SIZE);
    MD5_Final(signature, &ha2Ctx);
    getStringResult(signature, ha2StrResult);
    SAH_TRACEZ_INFO("DM_COM", "DIGEST AUTH - MD5(ha2Str):%s", ha2StrResult);

    // Build totalStr (ha1:noncevalue:nc-value:cnonce-value:qop:ha2)
    strncpy(totalStr, ha1StrResult, TOTAL_MAXTOKENSIZE);
    strncat(totalStr, ":", TOTAL_MAXTOKENSIZE - strlen(totalStr));
    strncat(totalStr, nonceTokenStr, TOTAL_MAXTOKENSIZE - strlen(totalStr));
    strncat(totalStr, ":", TOTAL_MAXTOKENSIZE - strlen(totalStr));
    strncat(totalStr, ncTokenStr, TOTAL_MAXTOKENSIZE - strlen(totalStr));
    strncat(totalStr, ":", TOTAL_MAXTOKENSIZE - strlen(totalStr));
    strncat(totalStr, cnonceTokenStr, TOTAL_MAXTOKENSIZE - strlen(totalStr));
    strncat(totalStr, ":", TOTAL_MAXTOKENSIZE - strlen(totalStr));
    strncat(totalStr, qopTokenStr, TOTAL_MAXTOKENSIZE - strlen(totalStr));
    strncat(totalStr, ":", TOTAL_MAXTOKENSIZE - strlen(totalStr));
    strncat(totalStr, ha2StrResult, TOTAL_MAXTOKENSIZE - strlen(totalStr));

    SAH_TRACEZ_INFO("DM_COM", "DIGEST AUTH - totalStr:%s", totalStr);

    MD5_Init(&responseCtx);
    MD5_Update(&responseCtx, (unsigned char*) totalStr, strlen(totalStr));
    memset((void*) signature, 0x00, SIGNATURE_SIZE);
    MD5_Final(signature, &responseCtx);
    getStringResult(signature, totalStrResult);

    // Compare the computed result and the transmitted one.
    if(0 == strcmp(totalStrResult, responseTokenStr)) {
        SAH_TRACEZ_INFO("DM_COM", "Authentication OK");
        nRet = true;
    } else {
        SAH_TRACEZ_ERROR("DM_COM", "Digest Authentication failed");
        SAH_TRACEZ_INFO("DM_COM", "DIGEST AUTH - Computed Result:%s", totalStrResult);
        SAH_TRACEZ_INFO("DM_COM", "DIGEST AUTH - Transmitted Result:%s", responseTokenStr);
    }

    return nRet;
}

/*
   Example :https://datatracker.ietf.org/doc/html/rfc7616#section-3.9.1
   WWW-Authenticate: Digest
    realm="http-auth@example.org",
    qop="auth, auth-int",
    algorithm=MD5,
    nonce="7ypf/xlj9XXwfDPEoM4URrv/xwf94BcCAzFZH4GiTo0v",
    opaque="FQhe/qaU925kfnzjCev0ciny7QMkPqMAFRtzCUYo5tdS"

 */
static bool DM_COM_GetAuthParameters(const char* _str,
                                     char* realm,
                                     char* nonce,
                                     char* opaque,
                                     char* qop,
                                     bool* userhash) {

    char* sub = NULL;
    char algo[MAXTOKENSIZE];
    memset(algo, 0x00, MAXTOKENSIZE * sizeof(char));

    SAH_TRACEZ_INFO("DM_COM", "Auth HDR to analyse: %s", _str);

    sub = (char*) strstr(_str, realmToken);
    if(!_getQuotedString(sub, realm)) {
        SAH_TRACEZ_ERROR("DM_COM", "Can not retrieve the realmTokenStr");
        return false;
    }

    sub = (char*) strstr(_str, nonceToken);
    if(!_getQuotedString(sub, nonce)) {
        SAH_TRACEZ_ERROR("DM_COM", "Can not retrieve the nonceTokenStr");
        return false;
    }

    sub = (char*) strstr(_str, opaqueToken);
    _getQuotedString(sub, opaque);//optional no error if missing

    sub = (char*) strstr(_str, qopToken);
    _getQuotedString(sub, qop);//use default if missing auth

    sub = (char*) strstr(_str, "userhash");
    if(sub && strstr(sub, "true")) {
        //safe, this is the only bool parameter
        *userhash = true;
    }

    sub = (char*) strstr(_str, "algorithm");
    if(sub && !strstr(sub, "MD5")) {
        SAH_TRACEZ_ERROR("DM_COM", "Digest algorithm is not supported");
        return false;
    }
    return true;
}

/* check server auth headers */
static bool DM_COM_CheckServerAuthHdr(const char* _str) {
    //check for the minimum, all others are optional
    if((NULL == strstr(_str, "Digest")) ||
       (NULL == strstr(_str, nonceToken)) ||
       (NULL == strstr(_str, realmToken))) {
        SAH_TRACEZ_INFO("DM_COM", "No Digest Authentication Data found (or partial)");
        return false;
    }
    return true;
}

/* check server auth headers qop = auth */
static bool authSupported(char* _str) {
    bool ret = false;
    char* tok = NULL;
    char* dup = strdup(_str);

    if(!dup) {
        goto exit;
    }
    tok = strtok(dup, ",");

    while(tok) {
        char* start = tok;
        char* end = tok + strlen(tok);
        int len = 0;
        //skip any white spaces
        while(start && (start + 1) && (*start) == ' ') {
            start++;
        }
        while(end && (end - 1) && (*end) == ' ') {
            end--;
        }
        len = end - start;

        if((len == 4) && (strncasecmp(start, "auth", 4) == 0)) {
            //good
            ret = true;
            goto exit;
        }
        //move to next
        tok = strtok(NULL, ",");
    }

exit:
    DM_ENG_FREE(dup);
    return ret;
}

/*Genearte A http auth headers*/
char* DM_COM_GenerateDigestResponse_MD5(const char* auth,
                                        const char* cnonce,
                                        const char* path,
                                        const char* method,
                                        const char* username,
                                        const char* passwd) {
    bool userhash = false;
    MD5_CTX h1Ctx;
    MD5_CTX h2Ctx;
    MD5_CTX userhashCtx;
    MD5_CTX responseCtx;
    unsigned char signature[SIGNATURE_SIZE];
    int nc = 1;

    //Server challenge data
    char realm[MAXTOKENSIZE];
    char nonce[MAXTOKENSIZE];
    char opaque[MAXTOKENSIZE];
    char qop[MAXTOKENSIZE];

    //computed
    char h1_str[HA1_MAXTOKENSIZE];
    char h2_str[HA2_MAXTOKENSIZE];
    char h1_MD5[MAXTOKENSIZE];
    char h2_MD5[MAXTOKENSIZE];
    char user_str[2 * MAXTOKENSIZE];
    char user_MD5[MAXTOKENSIZE];
    char response_str[TOTAL_MAXTOKENSIZE];
    char response_MD5[MAXTOKENSIZE];
    char* response_headers = NULL;

    //TODO! check if username should be reencoded
    //PROBLEM  : https://datatracker.ietf.org/doc/html/rfc7616#section-3.4
    //SOLUTION : https://datatracker.ietf.org/doc/html/rfc5987
    memset((void*) realm, 0x00, MAXTOKENSIZE);
    memset((void*) nonce, 0x00, MAXTOKENSIZE);
    memset((void*) opaque, 0x00, MAXTOKENSIZE);
    memset((void*) qop, 0x00, MAXTOKENSIZE);

    memset((void*) h1_str, 0x00, HA1_MAXTOKENSIZE);
    memset((void*) h2_str, 0x00, HA2_MAXTOKENSIZE);
    memset((void*) h1_MD5, 0x00, MAXTOKENSIZE);
    memset((void*) h2_MD5, 0x00, MAXTOKENSIZE);
    memset((void*) response_str, 0x00, TOTAL_MAXTOKENSIZE);
    memset((void*) response_MD5, 0x00, MAXTOKENSIZE);
    memset((void*) user_str, 0x00, 2 * MAXTOKENSIZE);
    memset((void*) user_MD5, 0x00, MAXTOKENSIZE);
    memset((void*) signature, 0x00, SIGNATURE_SIZE);

    if(!DM_COM_CheckServerAuthHdr(auth)) {
        SAH_TRACEZ_ERROR("DM_COM", "required Authenticate headers are missing");
        return NULL;
    }

    // Get all the digest data from the message
    if(!DM_COM_GetAuthParameters(auth,
                                 realm,
                                 nonce,
                                 opaque,
                                 qop,
                                 &userhash)) {
        SAH_TRACEZ_ERROR("DM_COM", "Cannot retrieve all the HTTP Digest Tokens");
        return NULL;
    }

    if((strlen(qop) > 0) && !authSupported(qop)) {
        SAH_TRACEZ_ERROR("DM_COM", "Server demand a non supported qop");
        goto stop;
    }

    //Build H1 = MD5(username:realm:password) if MD5
    //H1 = MD5(MD5(username:realm:password):nonce:cnonce) id MD5-sess not supported?
    snprintf(h1_str, HA1_MAXTOKENSIZE, "%s:%s:%s", username, realm, passwd);
    MD5_Init(&h1Ctx);
    MD5_Update(&h1Ctx, (unsigned char*) h1_str, strlen(h1_str));
    MD5_Final(signature, &h1Ctx);
    getStringResult(signature, h1_MD5);

    //Build H2 = MD5(method:digestURI) if qop = auth or unspecified
    //H2 = MD5(method:digestURI:MD5(entityBody)) if qop = auth-int not supported?
    snprintf(h2_str, HA2_MAXTOKENSIZE, "%s:%s", method, path);
    MD5_Init(&h2Ctx);
    MD5_Update(&h2Ctx, (unsigned char*) h2_str, strlen(h2_str));
    MD5_Final(signature, &h2Ctx);
    getStringResult(signature, h2_MD5);

    //response = MD5(HA1:nonce:nonceCount:cnonce:qop:HA2) if qop
    //response = MD5(HA1:nonce:HA2) if qop is not specified
    if(strlen(qop) > 0) {
        snprintf(response_str, TOTAL_MAXTOKENSIZE, "%s:%s:%08x:%s:auth:%s", h1_MD5, nonce, nc, cnonce, h2_MD5);
    } else {
        snprintf(response_str, TOTAL_MAXTOKENSIZE, "%s:%s:%s", h1_MD5, nonce, h2_MD5);
    }
    MD5_Init(&responseCtx);
    MD5_Update(&responseCtx, (unsigned char*) response_str, strlen(response_str));
    MD5_Final(signature, &responseCtx);
    getStringResult(signature, response_MD5);

    //hash username?
    //username = MD5(username:realm)
    if(userhash) {
        snprintf(user_str, 2 * MAXTOKENSIZE, "%s:%s", username, realm);
        MD5_Init(&userhashCtx);
        MD5_Update(&userhashCtx, (unsigned char*) user_str, strlen(user_str));
        MD5_Final(signature, &userhashCtx);
        getStringResult(signature, user_MD5);
        username = user_MD5;
    }

    //Build Auth headers
    response_headers = calloc(1, sizeof(char) * MAX_RESPONSE_SIZE);
    if(!response_headers) {
        response_headers = NULL;
        goto stop;
    }

    if(strlen(opaque) > 0) {
        sprintf(response_headers, replyDigestAuthStr1, username, realm, path, nonce, nc, cnonce, response_MD5, opaque);
    } else {
        sprintf(response_headers, replyDigestAuthStr, username, realm, path, nonce, nc, cnonce, response_MD5);
    }
    SAH_TRACEZ_INFO("DM_COM", "Auth headers : %s", response_headers);

stop:
    return response_headers; //caller will free this
}
