/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : dm_com_rpc_acs.c
 *
 * Created     : 2008/06/05
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file dm_com_rpc_acs.c
 *
 * @brief Implementation of the Remote Procedure Call of the ACS server
 *
 */

#ifndef __DM_COM_RPC_ACS_H_
#define _DM_COM_RPC_ACS_H_

// System headers
#include <stdlib.h>             /* Standard system library          */
#include <stdio.h>              /* Standard I/O functions           */
#include <stdarg.h>             /* */
#include <string.h>             /* Usefull string functions            */
#include <ctype.h>              /* */
#include <unistd.h>             /* Standard unix functions, like getpid()    */
#include <signal.h>             /* Signal name macros, and the signal() prototype  */
#include <time.h>               /* Time managing               */
#include <sys/types.h>          /* Various type definitions, like pid_t       */

#ifndef WIN32
#include <getopt.h>             /* Managing parameters              */
#include <netinet/in.h>         /*                    */
#include <arpa/inet.h>          /*                    */
#else
#include <winsock.h>
#endif

#include <errno.h>               /* Usefull for system errors           */


// Enable the tracing support
#ifdef DEBUG
#define SHOW_TRACES 1
#define TRACE_PREFIX "[DM-COM] %s:%s(),%d: ", __FILE__, __FUNCTION__, __LINE__
#endif

#include <dmcommon/CMN_Type_Def.h>
#include <dmengine/DM_ENG_Global.h>
#include <debug/sahtrace.h>

// DM_COM's header
#include <dmcom/dm_com.h>              /* DM_COM module definition            */
#include <dmcom/dm_com_rpc_acs.h>      /* Definition of the ACS RPC methods         */

// DM_ENGINE's header
#include <dmengine/DM_ENG_RPCInterface.h>    /* DM_Engine module definition            */

extern dm_com_struct g_DmComData;

/**
 * @brief Request for the Remote Procedure Call available on the ACS server
 *
 * @return see the TR069's RPC return code
 *
 */
int DM_ACS_GetRPCMethodsCallback() {
    DM_SoapXml SoapMsg;
    char* httpMsgString = NULL;
    GenericXmlNodePtr pRefGetRPCMethodsTag = NULL;
    char pUniqueHeaderID[HEADER_ID_SIZE_STRING];
    int nRet = DM_ENG_CANCELLED;

    memset((void*) &SoapMsg, 0x00, sizeof(SoapMsg));
    memset((void*) pUniqueHeaderID, 0x00, HEADER_ID_SIZE_STRING);

    SAH_TRACEZ_INFO("DM_COM", "Send a 'GetRPCMethods()' message to the ACS (from the callback) ");
    // ---------------------------------------------------------------------------
    // Initialize the SOAP data structure then create the XML/SOAP response
    // ---------------------------------------------------------------------------
    DM_InitSoapMsgReceived(&SoapMsg);
    DM_AnalyseSoapMessage(&SoapMsg, NULL, TYPE_CPE, false);

    // ---------------------------------------------------------------------------
    // Update the SOAP HEADER
    // ---------------------------------------------------------------------------
    if(DM_GenerateUniqueHeaderID(pUniqueHeaderID) == DM_OK) {
        DM_AddSoapHeaderID(pUniqueHeaderID, &SoapMsg);
    }

    // ---------------------------------------------------------------------------
    // Update the SOAP BODY
    // ---------------------------------------------------------------------------
    if(SoapMsg.pBody != NULL) {
        // ---------------------------------------------------------------------
        // Add a GETRPCMETHODS Tag
        // ---------------------------------------------------------------------
        pRefGetRPCMethodsTag = xmlAddNode(SoapMsg.pParser,   // doc
                                          SoapMsg.pBody,     // parent node
                                          RPC_GETRPCMETHODS, // new node tag
                                          NULL,              // Attribute Name
                                          NULL,              // Attribute Value
                                          NULL);             // Node value
        (void) pRefGetRPCMethodsTag;
    }

    // ---------------------------------------------------------------------------
    // Remake the XML buffer then send it to DM_SendHttpMessage
    // ---------------------------------------------------------------------------
    if(SoapMsg.pRoot != NULL) {
        httpMsgString = DM_RemakeXMLBufferACS(SoapMsg.pRoot);
        if(httpMsgString != NULL) {
            // Send the message
            if(DM_SendHttpMessage(httpMsgString) == DM_OK) {
                SAH_TRACEZ_INFO("DM_COM", "GETRPCMETHODS - Sending http message : OK");
            } else {
                DM_RemoveHeaderIDFromTab(pUniqueHeaderID);
                SAH_TRACEZ_ERROR("DM_COM", "GETRPCMETHODS - Sending http message : NOK");
            }
        }
        free(httpMsgString);
    }

    nRet = DM_ENG_SESSION_OPENING;

    if(NULL != SoapMsg.pParser) {
        // Free xml document
        xmlDocumentFree(SoapMsg.pParser);
        SoapMsg.pParser = NULL;

    }

    return( nRet );
} /* DM_ACS_GetRPCMethodsCallback */

/**
 * @brief Send an informa(tion) message to the ACS server
 *
 * @param DeviceId      A structure that uniquely identifies the CPE.
 *
 * @param Event         An array of structures. Its indicate the event that causes the transaction session to
 *                              be established.
 *
 * @param CurrentTime      The current date and time known to the CPE. This MUST be represented in the local time
 *          zone of the CPE, and MUST include the local time offset from UTC (will appropriate
 *          adjustment for daylight saving time).
 *
 * @param RetryCounts      Number of prior times an attempt was made to retry this session. This MUST be zero if
 *          and only if the previous session if any, completed succeffully. Eg : it will be reset
 *          to zero only when a session completes successfully.
 *
 * @param ParameterList    Array of name-values pairs.
 *
 * @return see the TR069's RPC return code
 *
 */
int
DM_ACS_InformCallback(
    DM_ENG_DeviceIdStruct* id,
    DM_ENG_EventStruct* events[],
    DM_ENG_ParameterValueStruct* parameterList[],
    time_t currentTime,
    unsigned int retryCount) {
    int nRet = DM_ENG_CANCELLED;

    // Check parameters
    if((id != NULL) && (events != NULL) && (parameterList != NULL)) {
        // Optimisation pour le cas d�connect� : si pas d'adresse IP locale, pas la peine de tenter la connection � l'ACS
        bool ready = true;
        int i;
        for(i = 0; parameterList[i] != NULL; i++) {
            if(strcmp(parameterList[i]->parameterName, DM_ENG_LAN_IP_ADDRESS_NAME) == 0) {
                ready = (parameterList[i]->value != NULL);
                break;
            }
        }
        if(ready) {
            SAH_TRACEZ_INFO("DM_COM", "Send an 'Inform()' message to the ACS (from the callback) ");

            // Translation with the internal DM_ACS_Inform function
            nRet = DM_ACS_Inform(id,
                                 events,
                                 MAX_ENVELOPPE_TR069, // Set to 1 (cf TR069 protocol)
                                 currentTime,
                                 retryCount,
                                 parameterList);
        }
        if(nRet == DM_ENG_SESSION_OPENING) {
            SAH_TRACEZ_INFO("DM_COM", "Inform Message : OK");

            // ---------------------------------------------------------------------------
            // Enable the session
            // ---------------------------------------------------------------------------
            SAH_TRACEZ_INFO("DM_COM", "Return code to give to the DM_ENGINE = %d", nRet);

            SAH_TRACEZ_INFO("DM_COM", "Create the ACS Session");
            g_DmComData.bSession = true;

            // Set the Time of the ACS Session creation.
            time(&g_DmComData.bSessionOpeningTime);

        } else {
            SAH_TRACEZ_ERROR("DM_COM", "Inform Message : NOK (%d) ", nRet);
        }
    } else {
        SAH_TRACEZ_ERROR("DM_COM", ERROR_INVALID_PARAMETERS);
    }

    return( nRet );
} /* DM_ACS_InformCallback */

/**
 * @brief Beside to "DM_ACS_Inform_Callback", this function will add the MaxEnveloppes parameter
 * @brief that must be only known by the DM_COM module.
 *
 * @remarks The MaxEnveloppes parameter is set to 1 in the current TR069 version
 *
 * Example of an SOAP Inform message :
 *
 * <soap-env:Envelope
 * soap-env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
 * xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"
 * xmlns:soap-enc="http://schemas.xmlsoap.org/soap/encoding/"
 * xmlns:cwmp="urn:dslforum-org:cwmp-1-0"
 * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 *      xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 * <soap-env:Header>
 * <cwmp:ID
 * soap-env:mustUnderstand="1"
 * xsi:type="xsd:string"
 * xmlns:cwmp="urn:dslforum-org:cwmp-1-0">8402</cwmp:ID>
 * </soap-env:Header>
 * <soap-env:Body>
 * <cwmp:Inform>
 *   <DeviceId>
 *     <Manufacturer>FT-RD</Manufacturer>
 *     <OUI>00147D</OUI>
 *     <ProductClass>OpenSTB v0.1</ProductClass>
 *     <SerialNumber>STB0528JTG8</SerialNumber>
 *   </DeviceId>  // ---------------------------------------------------------------------------
   // Lock the mutex in order to synchronise the SOAP request sent to the ACS
   // with the SOAP response received by the CPE
   // ---------------------------------------------------------------------------
   SAH_TRACEZ_WARNING("DM_COM",  "Lock file <SOAP Inform Request>/<SOAP Inform Response>" );
   DM_LockDuringWork( &g_DmComData.mutexCallBackSynchro );
 *  <Event
 * soap-enc:arrayType="cwmp:EventStruct[02]">
 *    <EventStruct>
 *      <EventCode>1 BOOT</EventCode>
 *      <CommandKey></CommandKey>
 *    </EventStruct>
 *    <EventStruct>
 *      <EventCode>2 PERIODIC</EventCode>
 *      <CommandKey></CommandKey>
 *    </EventStruct>
 *  </Event>
 *  <MaxEnveloppes>1</MaxEnveloppes>
 *  <CurrentTime>2007-07-16T14:24:42</CurrentTime>
 *  <RetryCount>2</RetryCount>
 *  <ParameterList soap-enc:arrayType="cwmp:ParameterValueStruct[06]">
 *    <ParameterValueStruct>
 *      <Name>Device.DeviceSummary</Name>
 *      <Value xsi:type="xsd:string">STB:1.0[](Baseline:1),ServiceA:1.0[1](Baseline:1)</Value>
 *    </ParameterValueStruct>
 *    <ParameterValueStruct>
 *      <Name>Device.DeviceInfo.HardwareVersion</Name>
 *      <Value xsi:type="xsd:string">OpenSTB-Hardware-0.1</Value>
 *    </ParameterValueStruct>
 *    <ParameterValueStruct>
 *      <Name>Device.DeviceInfo.SoftwareVersion</Name>
 *      <Value xsi:type="xsd:string">OpenSTB-Software-0.1</Value>
 *    </ParameterValueStruct>
 *    <ParameterValueStruct>
 *      <Name>Device.ManagementServer.ParameterKey</Name>
 *      <Value xsi:type="xsd:string"></Value>
 *    </ParameterValueStruct>
 *    <ParameterValueStruct>
 *      <Name>Device.ManagementServer.ConnectionRequestURL</Name>
 *      <Value xsi:type="xsd:string"></Value>
 *    </ParameterValueStruct>
 *    <ParameterValueStruct>
 *      <Name>Device.LAN.IPAddress</Name>
 *      <Value xsi:type="xsd:string">10.194.60.186</Value>
 *    </ParameterValueStruct>
 *  </ParameterList>
 * </cwmp:Inform>
 * </soap-env:Body>
 * </soap-env:Envelope>
 *
 */
int
DM_ACS_Inform(DM_ENG_DeviceIdStruct* DeviceId,
              DM_ENG_EventStruct* Event[],
              unsignedInt MaxEnveloppes,
              dateTime CurrentTime,
              unsignedInt RetryCounts,
              DM_ENG_ParameterValueStruct* ParameterList[]) {
    int nRet = DM_ENG_CANCELLED;
    DM_SoapXml SoapMsg;

    memset((void*) &SoapMsg, 0x00, sizeof(SoapMsg));

    // Check parameters
    if((Event != NULL) && (DeviceId != NULL) && (ParameterList != NULL)) {
        int nI = 0;
        char pUniqueHeaderID[HEADER_ID_SIZE];
        GenericXmlNodePtr pRefInformTag = NULL;
        char pTmpBuffer[TMPBUFFER_SIZE];
        char pTmpValueBuffer[TMPBUFFER_SIZE];
        int nNbEventStruct = 0;
        int nNbParameterValueStruct = 0;
        const char* commandKeyStr = NULL;
        char* httpMsgString = NULL;
        char* sTime = NULL;

        SAH_TRACEZ_NOTICE("DM_COM", "Send an 'Inform()' message to the ACS");


        // ---------------------------------------------------------------------------
        // Initialize the SOAP data structure then create the XML/SOAP response
        // ---------------------------------------------------------------------------
        DM_InitSoapMsgReceived(&SoapMsg);
        DM_AnalyseSoapMessage(&SoapMsg, NULL, TYPE_CPE, true);
        memset((void*) pUniqueHeaderID, 0x00, HEADER_ID_SIZE);

        // ---------------------------------------------------------------------------
        // Update the SOAP HEADER & BODY
        // ---------------------------------------------------------------------------
        // --> Add an INFORM tag inside the body XML node
        // ---------------------------------------------------------------------------
        if(DM_GenerateUniqueHeaderID(pUniqueHeaderID) == DM_OK) {
            //SAH_TRACEZ_INFO("DM_COM",   "-> HID = '%s' ", pUniqueHeaderID );
            DM_AddSoapHeaderID(pUniqueHeaderID, &SoapMsg);
        }

        if(SoapMsg.pBody != NULL) {
            pRefInformTag = xmlAddNode(SoapMsg.pParser, // XML Doc
                                       SoapMsg.pBody,   // XML Parent Node
                                       INFORM,          // Node Name
                                       NULL,            // Attribute Name
                                       NULL,            // Attribute Value
                                       NULL);           // Node Value


            if(pRefInformTag == NULL) {
                SAH_TRACEZ_ERROR("DM_COM", "Problem to add the <INFORM> XML tag!!");
            }
        }


        // ---------------------------------------------------------------------
        // Add a DeviceId tag and some sub-tags
        // ---------------------------------------------------------------------
        GenericXmlNodePtr pRefDeviceIdTag = xmlAddNode(SoapMsg.pParser, // XML Doc
                                                       pRefInformTag,   // XML Parent Node
                                                       INFORM_DEVICEID, // Node Name
                                                       NULL,            // Attribute Name
                                                       NULL,            // Attribute Value
                                                       NULL);           // Node Value

        // - Manufacturer :
        const char* manufacturerStr = NO_CONTENT;
        if(DeviceId->manufacturer != NULL) {
            // DM_AddElementWithValue( pRefDeviceIdTag, INFORM_MANUFACTURER, (char*)DeviceId->manufacturer );
            manufacturerStr = DeviceId->manufacturer;
        } else {
            // DM_AddElementWithValue( pRefDeviceIdTag, INFORM_MANUFACTURER, NO_CONTENT );
            SAH_TRACEZ_ERROR("DM_COM", "Missing manufacturer name!!");
        }

        xmlAddNode(SoapMsg.pParser,     // doc
                   pRefDeviceIdTag,     // parent node
                   INFORM_MANUFACTURER, // Node Name
                   NULL,                // Attribut Name
                   NULL,                // Attribut Value
                   manufacturerStr);    // Node Value

        // - Organisational Unique Identifier :
        const char* ouiStr = NO_CONTENT;
        if(DeviceId->OUI != NULL) {

            ouiStr = DeviceId->OUI;
        } else {

            SAH_TRACEZ_ERROR("DM_COM", "Missing OUI (Organizational Unique Identifier)!!");
        }

        xmlAddNode(SoapMsg.pParser, // doc
                   pRefDeviceIdTag, // parent node
                   INFORM_OUI,      // Node Name
                   NULL,            // Attribut Name
                   NULL,            // Attribut Value
                   ouiStr);         // Node Value


        // - Product Class :
        const char* productClassStr = NO_CONTENT;
        if(DeviceId->productClass != NULL) {
            productClassStr = DeviceId->productClass;
        } else {
            SAH_TRACEZ_ERROR("DM_COM", "Missing product class!!");
        }

        xmlAddNode(SoapMsg.pParser,     // doc
                   pRefDeviceIdTag,     // parent node
                   INFORM_PRODUCTCLASS, // Node Name
                   NULL,                // Attribut Name
                   NULL,                // Attribut Value
                   productClassStr);    // Node Value

        // - Serial Number :
        const char* serialNumberStr = NO_CONTENT;
        if(DeviceId->serialNumber != NULL) {
            serialNumberStr = DeviceId->serialNumber;
        } else {
            SAH_TRACEZ_ERROR("DM_COM", "Missing serial number!!");
        }

        xmlAddNode(SoapMsg.pParser,     // doc
                   pRefDeviceIdTag,     // parent node
                   INFORM_SERIALNUMBER, // Node Name
                   NULL,                // Attribut Name
                   NULL,                // Attribut Value
                   serialNumberStr);    // Node Value

        // ---------------------------------------------------------------------
        // Add an event tag and some sub-tags
        // ---------------------------------------------------------------------

        // ---------------------------------------------------------------------
        // Count how many Eventstruct there is in the list
        // ---------------------------------------------------------------------
        nNbEventStruct = DM_ENG_tablen((void**) Event);
        if(nNbEventStruct < 10) {
            snprintf(pTmpBuffer, TMPBUFFER_SIZE, INFORM_EVENT_ATTR_VAL1, nNbEventStruct);
        } else {
            snprintf(pTmpBuffer, TMPBUFFER_SIZE, INFORM_EVENT_ATTR_VAL2, nNbEventStruct);
        }

        GenericXmlNodePtr pRefEventListTag = xmlAddNode(SoapMsg.pParser,          // XML Doc
                                                        pRefInformTag,            // XML Parent Node
                                                        INFORM_EVENT,             // Node Name
                                                        DM_COM_ArrayTypeAttrName, // Attribute Name
                                                        pTmpBuffer,               // Attribute Value
                                                        NULL);                    // Node Value



        // Node * pRefEventStruct = NULL;
        GenericXmlNodePtr pRefEventStruct = NULL;
        for(nI = 0; nI < nNbEventStruct; nI++) {

            pRefEventStruct = xmlAddNode(SoapMsg.pParser,    // XML Doc
                                         pRefEventListTag,   // XML Parent Node
                                         INFORM_EVENTSTRUCT, // Node Name
                                         NULL,               // Attribute Name
                                         NULL,               // Attribute Value
                                         NULL);              // Node Value

            xmlAddNode(SoapMsg.pParser,                      // XML Doc
                       pRefEventStruct,                      // XML Parent Node
                       INFORM_EVENT_CODE,                    // Node Name
                       NULL,                                 // Attribute Name
                       NULL,                                 // Attribute Value
                       Event[nI]->eventCode);                // Node Value

            // This data may be empty for some events
            commandKeyStr = NO_CONTENT;
            if(Event[nI]->commandKey != NULL) {
                commandKeyStr = Event[nI]->commandKey;
            }

            xmlAddNode(SoapMsg.pParser,   // XML Doc
                       pRefEventStruct,   // XML Parent Node
                       INFORM_COMMANDKEY, // Node Name
                       NULL,              // Attribute Name
                       NULL,              // Attribute Value
                       commandKeyStr);    // Node Value
        }

        // ---------------------------------------------------------------------
        // Add a MaxEnveloppes tag with a value set to 1 according to the TR-069 document (2007)
        // ---------------------------------------------------------------------
        snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%d", MaxEnveloppes);
        xmlAddNode(SoapMsg.pParser,     // XML Doc
                   pRefInformTag,       // XML Parent Node
                   INFORM_MAXENVELOPES, // Node Name
                   NULL,                // Attribute Name
                   NULL,                // Attribute Value
                   pTmpBuffer);         // Node Value

        // ---------------------------------------------------------------------
        // Add a CurrentTime tag
        // ---------------------------------------------------------------------
        sTime = DM_ENG_dateTimeToString(CurrentTime);

        xmlAddNode(SoapMsg.pParser,    // XML Doc
                   pRefInformTag,      // XML Parent Node
                   INFORM_CURRENTTIME, // Node Name
                   NULL,               // Attribute Name
                   NULL,               // Attribute Value
                   sTime);             // Node Value
        free(sTime);

        // ---------------------------------------------------------------------
        // Add a RetryCount tag
        // ---------------------------------------------------------------------
        snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%d", RetryCounts);

        xmlAddNode(SoapMsg.pParser,   // XML Doc
                   pRefInformTag,     // XML Parent Node
                   INFORM_RETRYCOUNT, // Node Name
                   NULL,              // Attribute Name
                   NULL,              // Attribute Value
                   pTmpBuffer);       // Node Value
        // ---------------------------------------------------------------------
        // Add a parameterList tag and sub-tag ParameterValueStruct
        // ---------------------------------------------------------------------------
        // ---------------------------------------------------------------------------
        // Count how many parametervaluestruct there is in the list
        // ---------------------------------------------------------------------------
        nNbParameterValueStruct = DM_ENG_tablen((void**) ParameterList);
        if(nNbParameterValueStruct < 10) {
            snprintf(pTmpBuffer, TMPBUFFER_SIZE, INFORM_PARAMETERLIST_ATTR_VAL1, nNbParameterValueStruct);
        } else {
            snprintf(pTmpBuffer, TMPBUFFER_SIZE, INFORM_PARAMETERLIST_ATTR_VAL2, nNbParameterValueStruct);
        }


        GenericXmlNodePtr pRefParamListTag = xmlAddNode(SoapMsg.pParser,          // XML Doc
                                                        pRefInformTag,            // XML Parent Node
                                                        INFORM_PARAMETERLIST,     // Node Name
                                                        DM_COM_ArrayTypeAttrName, // Attribute Name
                                                        pTmpBuffer,               // Attribute Value
                                                        NULL);                    // Node Value


        GenericXmlNodePtr pRefParamStruct = NULL;
        GenericXmlNodePtr pRefParamValue = NULL;
        // char      * lanIpAddressStr = NULL;
        for(nI = 0; nI < nNbParameterValueStruct; nI++) {

            pRefParamStruct = xmlAddNode(SoapMsg.pParser,             // doc
                                         pRefParamListTag,            // parent node
                                         INFORM_PARAMETERVALUESTRUCT, // new node tag
                                         NULL,                        // Attribute Name
                                         NULL,                        // Attribute Value
                                         NULL);                       // Node value

            xmlAddNode(SoapMsg.pParser,                               // doc
                       pRefParamStruct,                               // parent node
                       INFORM_NAME,                                   // new node tag
                       NULL,                                          // Attribute Name
                       NULL,                                          // Attribute Value
                       ParameterList[nI]->parameterName);             // Node value

            memset((void*) pTmpValueBuffer, 0x00, TMPBUFFER_SIZE);
            if(ParameterList[nI]->value != NULL) {
                strncpy(pTmpValueBuffer, ParameterList[nI]->value, TMPBUFFER_SIZE - 1);
                SAH_TRACEZ_INFO("DM_COM", "ParameterList[%d]->value: %s", nI, ParameterList[nI]->value);
            } else {
                SAH_TRACEZ_INFO("DM_COM", "ParameterList[%d]->value is NULL", nI);
            }

            // pRefParamValue = DM_AddElementWithValue( pRefParamStruct, INFORM_VALUE, lanIpAddressStr );

            switch(ParameterList[nI]->type) {
            case DM_ENG_ParameterType_INT:     snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%s", XSD_INT);         break;
            case DM_ENG_ParameterType_UINT:    snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%s", XSD_UNSIGNEDINT); break;
            case DM_ENG_ParameterType_LONG:    snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%s", XSD_LONG);        break;
            case DM_ENG_ParameterType_BOOLEAN: snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%s", XSD_BOOLEAN);     break;
            case DM_ENG_ParameterType_DATE:    snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%s", XSD_DATETIME);    break;
            case DM_ENG_ParameterType_STRING:  snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%s", XSD_STRING);      break;
            case DM_ENG_ParameterType_STATISTICS:
            case DM_ENG_ParameterType_ANY:     snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%s", XSD_ANY);         break;
            case DM_ENG_ParameterType_UNDEFINED: SAH_TRACEZ_WARNING("DM_COM", "Undefined type value!!");           break;
            default:
            {
                SAH_TRACEZ_ERROR("DM_COM", "Unknown type value for a parameter (%d) ",
                                 ParameterList[nI]->type);
            }
            break;
            }

            pRefParamValue = xmlAddNode(SoapMsg.pParser,   // doc
                                        pRefParamStruct,   // parent node
                                        INFORM_VALUE,      // new node tag
                                        INFORM_VALUE_ATTR, // Attribute Name
                                        pTmpBuffer,        // Attribute Value
                                        pTmpValueBuffer);  // Node value
            (void) pRefParamValue;                         //avoid clang warnings

        } // end for

        // ---------------------------------------------------------------------------
        // Remake the XML buffer then send it to ACS server
        // ---------------------------------------------------------------------------
        if(SoapMsg.pRoot != NULL) {
            httpMsgString = DM_RemakeXMLBufferACS(SoapMsg.pRoot);
            if(httpMsgString != NULL) {
                // Send the HTTP message
                if(client_startSession() == 0) {
                    if(DM_SendHttpMessage(httpMsgString) == DM_OK) {
                        SAH_TRACEZ_INFO("DM_COM", "Inform - Sending http message : OK");
                        nRet = DM_ENG_SESSION_OPENING;
                    } else {
                        DM_RemoveHeaderIDFromTab(pUniqueHeaderID);
                        SAH_TRACEZ_ERROR("DM_COM", "Inform - Sending http message : NOK");
                        SAH_TRACEZ_INFO("DM_COM", "DMCOM ACS RPC 623 close ACS session");
                        _closeACSSession(false);
                    }
                } else {
                    DM_RemoveHeaderIDFromTab(pUniqueHeaderID);
                    SAH_TRACEZ_ERROR("DM_COM", "Inform - Could not connect the http client");
                    SAH_TRACEZ_INFO("DM_COM", "DMCOM ACS RPC 632 close ACS session");
                    _closeACSSession(false);
                }

                // Free the HTTP Message
                free(httpMsgString);
            }
        }
    } else {
        SAH_TRACEZ_ERROR("DM_COM", ERROR_INVALID_PARAMETERS);
    }

    if(NULL != SoapMsg.pParser) {
        // Free xml document
        xmlDocumentFree(SoapMsg.pParser);
        SoapMsg.pParser = NULL;

    }

    return( nRet );
} /* DM_ACS_Inform */

/**
 * @brief Inform the ACS server to the end of a download or upload
 *
 * @param CommandKey    Set to the value of the commandkey argument passed to the CPE in the download or
 *          upload method call that initiated the transfert.
 *
 * @param FaultStruct      If the transfert was successfully, the faultcode is set to zero. Otherwise, a non-zero
 *          faultcode is specified along with a faultstring indicating the faillure reason.
 *
 * @param StartTime     The date and time transfert was started in UTC. The CPE SOULD record  this information
 *          and report it in this argument, but if this information is not available, the value
 *          of this argument MUST be set to the unknown time value.
 *
 * @param CompleteTime     The date and time transfert completed in UTC. The CPE SOULD record  this information
 *          and report it in this argument, but if this information is not available, the value
 *          of this argument MUST be set to the unknown time value.
 *
 * @return see the TR069's RPC return code
 *
 */
int
DM_ACS_TransfertCompleteCallback(DM_ENG_TransferCompleteStruct* tcs) {
    int nRet = DM_ENG_CANCELLED;
    FaultStruct fs;
    time_t completeTime;

    // Check parameter
    if(tcs != NULL) {
        SAH_TRACEZ_INFO("DM_COM", "Send an 'TransfertComplete()' message to the ACS (from the callback) %p", tcs);
        SAH_TRACEZ_INFO("DM_COM", "[%d] : '%s' ", tcs->faultCode, tcs->faultString);

        // ---------------------------------------------------------------------------
        // Set the FaultStruct data structure before
        // ---------------------------------------------------------------------------
        fs.FaultCode = tcs->faultCode;
        if(tcs->faultString != NULL) {
            strncpy(fs.FaultString, tcs->faultString, SIZE_FAULTSTRUCT_STRING);
            fs.FaultString[SIZE_FAULTSTRUCT_STRING - 1] = '\0';
        } else {
            strncpy(fs.FaultString, NO_CONTENT, SIZE_FAULTSTRUCT_STRING);
            fs.FaultString[SIZE_FAULTSTRUCT_STRING - 1] = '\0';
        }

        // ---------------------------------------------------------------------------
        // if unknown time, the transfer probably finished when time synchronization was not yet finished.
        // fill in the current time
        // ---------------------------------------------------------------------------
        char* ntpStatus = NULL;
        completeTime = tcs->completeTime;
        if(DM_ENG_GetManagementServerValue(DM_ENG_EntityType_SYSTEM, DM_ENG_NTPSTATUS, &ntpStatus) == 0) {
            if(ntpStatus && ( strcmp(ntpStatus, "Synchronized") == 0)) {
                SAH_TRACEZ_ERROR("DM_COM", "Time synced, update complete time");
                if(tcs->completeTime < tcs->startTime) {
                    time(&completeTime);
                }
            }
            free(ntpStatus);
        }

        // ---------------------------------------------------------------------------
        // Translation with the internal DM_ACS_Transfertcomplete function
        // ---------------------------------------------------------------------------
        nRet = DM_ACS_TransfertComplete(tcs->commandKey, &fs, tcs->startTime, tcs->completeTime);
        if(nRet == DM_ENG_SESSION_OPENING) {
            SAH_TRACEZ_INFO("DM_COM", "Transfert Complete : OK");


        } else {
            SAH_TRACEZ_ERROR("DM_COM", "Transfert Complete : NOK (%d) ", nRet);
        }

    } else {
        SAH_TRACEZ_ERROR("DM_COM", ERROR_INVALID_PARAMETERS);
    }


    return( nRet );
} /* DM_ACS_TransfertCompleteCallback */

/**
 * @brief Beside to "DM_ACS_TransfertComplete_Callback", this function will follow the TR-069 definition
 *
 * Example of an SOAP TransfertComplete message :
 *
 * <soap-env:Envelope
 * soap-env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
 * xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"
 * xmlns:soap-enc="http://schemas.xmlsoap.org/soap/encoding/"
 * xmlns:cwmp="urn:dslforum-org:cwmp-1-0"
 * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 * xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 * <soap-env:Header>
 * <cwmp:NoMoreRequests
 *    xsi:type="xsd:string"
 *    xmlns:cwmp="urn:dslforum-org:cwmp-1-0">0</cwmp:NoMoreRequests>
 * <cwmp:ID soap-env:mustUnderstand="1"
 *    xsi:type="xsd:string"
 *    xmlns:cwmp="urn:dslforum-org:cwmp-1-0">OpenSTB-FTRD-9117</cwmp:ID>
 * </soap-env:Header>
 * <soap-env:Body>
 * <cwmp:TransfertComplete>
 *    <CommandKey>241</CommandKey>
 *    <FaultStruct>
 *          <FaultCode>0</FaultCode>
 *       <FaultString></FaultString>
 *    </FaultStruct>
 *    <StartTime>2001-08-08T05:35:14</StartTime>
 *    <CompleteTime>2001-08-08T05:42:59</CompleteTime>
 * </cwmp:TransfertComplete>
 * </soap-env:Body>
 * </soap-env:Envelope>
 */
int
DM_ACS_TransfertComplete(
    char* CommandKey,
    FaultStruct* fs,
    dateTime StartTime,
    dateTime CompleteTime) {
    int nRet = DM_ENG_CANCELLED;
    DM_SoapXml SoapMsg;
    char* httpMsgString = NULL;

    memset((void*) &SoapMsg, 0x00, sizeof(SoapMsg));

    // Check parameter
    if(CommandKey != NULL) {
        char pUniqueHeaderID[HEADER_ID_SIZE];
        GenericXmlNodePtr pRefTranfertCompleteTag = NULL;
        char pTmpBuffer[TMPBUFFER_SIZE];
        char* sTime = NULL;


        SAH_TRACEZ_INFO("DM_COM", "Send a 'TranfertComplete()' message to the ACS (from the callback) ");

        // ---------------------------------------------------------------------------
        // Initialize the SOAP data structure then create the XML/SOAP response
        // ---------------------------------------------------------------------------
        DM_InitSoapMsgReceived(&SoapMsg);
        DM_AnalyseSoapMessage(&SoapMsg, NULL, TYPE_CPE, false);
        memset((void*) pUniqueHeaderID, 0x00, HEADER_ID_SIZE);

        // ---------------------------------------------------------------------------
        // Update the SOAP HEADER
        // ---------------------------------------------------------------------------
        if(DM_GenerateUniqueHeaderID(pUniqueHeaderID) == DM_OK) {
            DM_AddSoapHeaderID(pUniqueHeaderID, &SoapMsg);
        }

        // ---------------------------------------------------------------------------
        // Save the current SOAPID
        // ---------------------------------------------------------------------------
        //strcpy( g_DmComData.DmSynchroCallback, pUniqueHeaderID );

        // ---------------------------------------------------------------------------
        // Update the SOAP BODY
        // ---------------------------------------------------------------------------
        if(SoapMsg.pBody != NULL) {
            // ---------------------------------------------------------------------
            // Add a Transfertcomplete Tag
            // ---------------------------------------------------------------------

            pRefTranfertCompleteTag = xmlAddNode(SoapMsg.pParser,   // doc
                                                 SoapMsg.pBody,     // parent node
                                                 TRANSFERTCOMPLETE, // new node tag
                                                 NULL,              // Attribute Name
                                                 NULL,              // Attribute Value
                                                 NULL);             // Node value

            // ---------------------------------------------------------------------
            // Add a command key
            // ---------------------------------------------------------------------
            if(strlen(CommandKey) <= SIZE_COMMANDKEY) {

                xmlAddNode(SoapMsg.pParser,             // doc
                           pRefTranfertCompleteTag,     // parent node
                           TRANSFERTCOMPLETECOMMANDKEY, // new node tag
                           NULL,                        // Attribute Name
                           NULL,                        // Attribute Value
                           CommandKey);                 // Node value

            } else {
                SAH_TRACEZ_ERROR("DM_COM", "Command key bigger than %d!!", SIZE_COMMANDKEY);
            }

            // ---------------------------------------------------------------------
            // Add a faultstruct and its two elements
            // ---------------------------------------------------------------------
            if(fs) {
                GenericXmlNodePtr pRefFaultStruct = xmlAddNode(SoapMsg.pParser,              // DOC
                                                               pRefTranfertCompleteTag,      // Parent Node
                                                               TRANSFERTCOMPLETEFAULTSTRUCT, // Node Name
                                                               NULL,                         // Attribute Name
                                                               NULL,                         // Attribute Value
                                                               NULL);                        // Node Value


                snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%d", (int) fs->FaultCode);

                xmlAddNode(SoapMsg.pParser,            // DOC
                           pRefFaultStruct,            // Parent Node
                           TRANSFERTCOMPLETEFAULTCODE, // Node Name
                           NULL,                       // Attribute Name
                           NULL,                       // Attribute Value
                           pTmpBuffer);                // Node Value


                xmlAddNode(SoapMsg.pParser,              // DOC
                           pRefFaultStruct,              // Parent Node
                           TRANSFERTCOMPLETEFAULTSTRING, // Node Name
                           NULL,                         // Attribute Name
                           NULL,                         // Attribute Value
                           fs->FaultString);             // Node Value
            }
            // ---------------------------------------------------------------------
            // Add a start time
            // ---------------------------------------------------------------------
            sTime = DM_ENG_dateTimeToString(StartTime);
            SAH_TRACEZ_INFO("DM_COM", "Start time = %s ", sTime);

            xmlAddNode(SoapMsg.pParser,            // DOC
                       pRefTranfertCompleteTag,    // Parent Node
                       TRANSFERTCOMPLETESTARTTIME, // Node Name
                       NULL,                       // Attribute Name
                       NULL,                       // Attribute Value
                       sTime);                     // Node Value
            free(sTime);

            // ---------------------------------------------------------------------
            // Add a complete time (end time)
            // ---------------------------------------------------------------------
            sTime = DM_ENG_dateTimeToString(CompleteTime);
            SAH_TRACEZ_INFO("DM_COM", "Complete/End time = %s ", sTime);

            xmlAddNode(SoapMsg.pParser,               // DOC
                       pRefTranfertCompleteTag,       // Parent Node
                       TRANSFERTCOMPLETECOMPLETETIME, // Node Name
                       NULL,                          // Attribute Name
                       NULL,                          // Attribute Value
                       sTime);                        // Node Value
            free(sTime);

        }

        // ---------------------------------------------------------------------------
        // Remake the XML buffer then send it to DM_SendHttpMessage
        // ---------------------------------------------------------------------------
        if(SoapMsg.pRoot != NULL) {
            httpMsgString = DM_RemakeXMLBufferACS(SoapMsg.pRoot);
            if(httpMsgString != NULL) {
                // Send the message
                if(DM_SendHttpMessage(httpMsgString) == DM_OK) {
                    SAH_TRACEZ_INFO("DM_COM", "TransfertComplete - Sending http message : OK");
                } else {
                    DM_RemoveHeaderIDFromTab(pUniqueHeaderID);
                    SAH_TRACEZ_ERROR("DM_COM", "TransfertComplete - Sending http message : NOK");
                }
            }
            free(httpMsgString);
        }

        nRet = DM_ENG_SESSION_OPENING;
    } else {
        SAH_TRACEZ_ERROR("DM_COM", ERROR_INVALID_PARAMETERS);
    }

    if(NULL != SoapMsg.pParser) {
        // Free xml document
        xmlDocumentFree(SoapMsg.pParser);
        SoapMsg.pParser = NULL;

    }

    return( nRet );
} /* DM_ACS_TransfertComplete */

/**
 * @brief Ask to download a file to the ACS server
 *
 * @param FileType   This is the file type being requested:
 *
 *          1 : Firmware upgrade image
 *          2 : Web content
 *          3 : Vendor configuration file
 *
 * @param FileTypeArg   Array of zero or more additional arguments where each argument is a structure of
 *                      name-value pairs. The use of the additional arguments depend of the file specified:
 *
 *          FileType FiletypeArg Name
 *             1        (none)
 *             2        "Version"
 *             3        (none)
 *
 * @return see the TR069's RPC return code
 *
 */
int
DM_ACS_RequestDownloadCallback(const char* FileType,
                               DM_ENG_ArgStruct* FileTypeArg[]) {

    DM_SoapXml SoapMsg;
    char* httpMsgString = NULL;
    GenericXmlNodePtr pRefRequestDownloadTag = NULL;
    GenericXmlNodePtr pRefFileTypeArgTag = NULL;
    GenericXmlNodePtr pRefArgStructTag = NULL;
    unsigned int argStructArraySize = 0;
    unsigned int n;
    char tmpStr[SIZE_FAULTSTRUCT_STRING];
    char pUniqueHeaderID[HEADER_ID_SIZE];
    int nRet = DM_ENG_CANCELLED;


    memset((void*) &SoapMsg, 0x00, sizeof(SoapMsg));

    // Check parameter
    if(FileType != NULL) {

        SAH_TRACEZ_NOTICE("DM_COM", "Send a 'RequestDownload()' message to the ACS (from the callback) ");
        // ---------------------------------------------------------------------------
        // Initialize the SOAP data structure then create the XML/SOAP response
        // ---------------------------------------------------------------------------
        DM_InitSoapMsgReceived(&SoapMsg);
        DM_AnalyseSoapMessage(&SoapMsg, NULL, TYPE_CPE, false);
        memset((void*) pUniqueHeaderID, 0x00, HEADER_ID_SIZE);

        // ---------------------------------------------------------------------------
        // Update the SOAP HEADER
        // ---------------------------------------------------------------------------
        if(DM_GenerateUniqueHeaderID(pUniqueHeaderID) == DM_OK) {
            DM_AddSoapHeaderID(pUniqueHeaderID, &SoapMsg);
        }

        // ---------------------------------------------------------------------------
        // Update the SOAP BODY
        // ---------------------------------------------------------------------------
        if(SoapMsg.pBody != NULL) {
            // ---------------------------------------------------------------------
            // Add a REQUESTDOWNLOAD Tag
            // ---------------------------------------------------------------------

            pRefRequestDownloadTag = xmlAddNode(SoapMsg.pParser, // doc
                                                SoapMsg.pBody,   // parent node
                                                REQUESTDOWNLOAD, // new node tag
                                                NULL,            // Attribute Name
                                                NULL,            // Attribute Value
                                                NULL);           // Node value
        }

        // Add the FileType Node
        xmlAddNode(SoapMsg.pParser,          // doc
                   pRefRequestDownloadTag,   // parent node
                   REQUESTDOWNLOAD_FILETYPE, // new node tag
                   NULL,                     // Attribute Name
                   NULL,                     // Attribute Value
                   FileType);                // Node value

        // Compute the number of element in the array
        argStructArraySize = DM_ENG_tablen((void**) FileTypeArg);

        SAH_TRACEZ_INFO("DM_COM", "Number of element in the Array = %d", argStructArraySize);

        // Build the string
        sprintf(tmpStr, REQUESTDOWNLOAD_CWMP_ARG_STRUCT, argStructArraySize);

        // Add The Node
        pRefFileTypeArgTag = xmlAddNode(SoapMsg.pParser,             // doc
                                        pRefRequestDownloadTag,      // parent node
                                        REQUESTDOWNLOAD_FILETYPEARG, // new node tag
                                        DM_COM_ArrayTypeAttrName,    // Attribute Name
                                        tmpStr,                      // Attribute Value
                                        NULL);                       // Node value

        // Add all the ArgStruct
        for(n = 0; n < argStructArraySize; n++) {
            // Add thhe
            pRefArgStructTag = xmlAddNode(SoapMsg.pParser,           // doc
                                          pRefFileTypeArgTag,        // parent node
                                          REQUESTDOWNLOAD_ARGSTRUCT, // new node tag
                                          NULL,                      // Attribute Name
                                          NULL,                      // Attribute Value
                                          NULL);                     // Node value
            // Add the Name
            xmlAddNode(SoapMsg.pParser,                              // doc
                       pRefArgStructTag,                             // parent node
                       PARAM_NAME,                                   // new node tag
                       NULL,                                         // Attribute Name
                       NULL,                                         // Attribute Value
                       FileTypeArg[n]->name);                        // Node value

            // Add the Value
            xmlAddNode(SoapMsg.pParser,        // doc
                       pRefArgStructTag,       // parent node
                       PARAM_VALUE,            // new node tag
                       NULL,                   // Attribute Name
                       NULL,                   // Attribute Value
                       FileTypeArg[n]->value); // Node value
        }



        // ---------------------------------------------------------------------------
        // Remake the XML buffer then send it to DM_SendHttpMessage
        // ---------------------------------------------------------------------------
        if(SoapMsg.pRoot != NULL) {
            httpMsgString = DM_RemakeXMLBufferACS(SoapMsg.pRoot);
            if(httpMsgString != NULL) {
                // Send the message
                if(DM_SendHttpMessage(httpMsgString) == DM_OK) {
                    SAH_TRACEZ_INFO("DM_COM", "TransfertComplete - Sending http message : OK");
                } else {
                    DM_RemoveHeaderIDFromTab(pUniqueHeaderID);
                    SAH_TRACEZ_ERROR("DM_COM", "TransfertComplete - Sending http message : NOK");
                }
            }
            free(httpMsgString);
        }

        nRet = DM_ENG_SESSION_OPENING;
    } else {
        SAH_TRACEZ_ERROR("DM_COM", ERROR_INVALID_PARAMETERS);
    }

    if(NULL != SoapMsg.pParser) {
        // Free xml document
        xmlDocumentFree(SoapMsg.pParser);
        SoapMsg.pParser = NULL;

    }

    return( nRet );

} /* DM_ACS_RequestDownloadCallback */

/**
 * @brief Function which make and send a soap message to the acs when need to be kicked
 *
 * @param Command Generic argument that MAY be used by the ACS for identification or other purpose
 *
 * @param Referer The content of the 'Referer' HTTP header sent to the CPE when it was kicked
 *
 * @param Arg     Generic argument that MAY be used by the ACS for identification or other purpose
 *
 * @param Next    The URL that the ACS SHOULD return in the method response under normal conditions
 *
 * @return see the TR069's RPC return code
 *
 */
int
DM_ACS_KickedCallback(
    char* Command,
    char* Referer,
    char* Arg,
    char* Next) {


    int nRet = DM_ENG_CANCELLED;
    if((NULL == Command) || (NULL == Referer) || (NULL == Arg) || (NULL == Next)) {
        SAH_TRACEZ_ERROR("DM_COM", "Invalid Pointer");
    }
    return( nRet );
} /* DM_ACS_KickedCallback */

/**
 * @brief Notify the DM_COM module to configure the HTTP Client Connection Data
 *        This callback function is used when the application is started and when
 *        the ACS URL, Username or password is changed.
 *
 * @param url - The URL of the ACS
 *
 * @param username - The username to used to perform authentication (could be NULL if no authentication is required)
 *
 * @param password - The password to used to perform authentication (could be NULL if no authentication is required)
 *
 * @return void
 *
 */
void DM_ACS_updateAscParameters(char* url,
                                char* username,
                                char* password) {
    SAH_TRACEZ_NOTICE("DM_COM", "Configure the HTTP Client Connection Data");
    SAH_TRACEZ_NOTICE("DM_COM", "ACS url      = %s", ((NULL != url)      ? url      : "None"));
    SAH_TRACEZ_NOTICE("DM_COM", "ACS username = %s", ((NULL != username) ? username : "None"));
    SAH_TRACEZ_NOTICE("DM_COM", "ACS password = %s", ((NULL != password) ? password : "None"));

    DM_COM_ConfigureHttpClient(url, username, password);

}

/**
 * @brief function which remake the xml buffer
 *
 * @param pElement  Current element of the XML tree (
 *
 * @Return A pointer on the string (or NULL on ERROR)
 *
 * @Remarks for a complete tree, pElement = pRoot
 */
char*
DM_RemakeXMLBufferACS(GenericXmlNodePtr pElement) {
    char* httpMessageStr = NULL;

    if(NULL == pElement) {
        return (httpMessageStr);
    }

    httpMessageStr = xmlDocumentToStringBuffer(xmlNodeToDocument(pElement));

    return httpMessageStr;
} /* DM_RemakeXMLBufferACS */

int DM_ACS_DUStateChangeCompleteCallback(dscc_t* dscc) {
    int nRet = DM_ENG_CANCELLED;
    if(dscc != NULL) {
        nRet = DM_ACS_DUStateChangeComplete(dscc);
        if(nRet == DM_ENG_SESSION_OPENING) {
            SAH_TRACEZ_INFO("DM_COM", "DUStateChangeComplete : OK");
        } else {
            SAH_TRACEZ_ERROR("DM_COM", "DUStateChangeComplete : NOK (%d) ", nRet);
        }
    } else {
        SAH_TRACEZ_ERROR("DM_COM", ERROR_INVALID_PARAMETERS);
    }
    return nRet;
}

int DM_ACS_DUStateChangeComplete(dscc_t* dscc) {
    int nRet = DM_ENG_CANCELLED;
    DM_SoapXml SoapMsg;
    char* httpMsgString = NULL;
    dscc_result_t* dscc_result = NULL;

    if(dscc == NULL) {
        SAH_TRACEZ_ERROR("DM_COM", "Invalid arg(s)");
        return nRet;
    }

    dscc_result = amxc_container_of(amxc_llist_get_first(&dscc->results), dscc_result_t, it);

    if(dscc_result == NULL) {
        SAH_TRACEZ_ERROR("DM_COM", "Invalid DSCC's result");
        return nRet;
    }

    memset((void*) &SoapMsg, 0x00, sizeof(SoapMsg));

    if((dscc != NULL) && (dscc->commandKey != NULL)) {
        char pUniqueHeaderID[HEADER_ID_SIZE];
        char pTmpBuffer[TMPBUFFER_SIZE];
        char* tmp_time = NULL;

        SAH_TRACEZ_INFO("DM_COM", "Send DUStateChangeComplete message to the ACS");

        DM_InitSoapMsgReceived(&SoapMsg);
        DM_AnalyseSoapMessage(&SoapMsg, NULL, TYPE_CPE, false);
        memset((void*) pUniqueHeaderID, 0x00, HEADER_ID_SIZE);

        if(DM_GenerateUniqueHeaderID(pUniqueHeaderID) == DM_OK) {
            DM_AddSoapHeaderID(pUniqueHeaderID, &SoapMsg);
        }

        if(SoapMsg.pBody != NULL) {
            GenericXmlNodePtr dustatechangecomplete_node = NULL;
            GenericXmlNodePtr results_node = NULL;
            GenericXmlNodePtr fault_node = NULL;

            dustatechangecomplete_node = xmlAddNode(SoapMsg.pParser, SoapMsg.pBody, DUSTATECHANGECOMPLETE,
                                                    NULL, NULL, NULL);

            results_node = xmlAddNode(SoapMsg.pParser, dustatechangecomplete_node, DUSTATECHANGECOMPLETE_RESULTS,
                                      ATTR_TYPE, DUSTATECHANGECOMPLETE_OPRESULTSTRUCT_TYPE, NULL);

            xmlAddNode(SoapMsg.pParser, results_node, DUSTATECHANGECOMPLETE_UUID,
                       NULL, NULL, dscc_result->uuid);

            xmlAddNode(SoapMsg.pParser, results_node, DUSTATECHANGECOMPLETE_DEPLOYMENTUNITREF,
                       NULL, NULL, dscc_result->deploymentUnitRef);

            xmlAddNode(SoapMsg.pParser, results_node, DUSTATECHANGECOMPLETE_VERSION,
                       NULL, NULL, dscc_result->version);

            xmlAddNode(SoapMsg.pParser, results_node, DUSTATECHANGECOMPLETE_CURRENTSTATE,
                       NULL, NULL, dscc_result->currentState);

            xmlAddNode(SoapMsg.pParser, results_node, DUSTATECHANGECOMPLETE_RESOLVED,
                       NULL, NULL, dscc_result->resolved? "1":"0");

            xmlAddNode(SoapMsg.pParser, results_node, DUSTATECHANGECOMPLETE_EXECUTIONUNITREFLIST,
                       NULL, NULL, dscc_result->executionUnitRefList);


            tmp_time = DM_ENG_dateTimeToString(dscc_result->startTime);
            xmlAddNode(SoapMsg.pParser, results_node, DUSTATECHANGECOMPLETE_STARTTIME,
                       NULL, NULL, tmp_time);
            free(tmp_time);
            tmp_time = DM_ENG_dateTimeToString(dscc_result->completeTime);
            xmlAddNode(SoapMsg.pParser, results_node, DUSTATECHANGECOMPLETE_COMPLETETIME,
                       NULL, NULL, tmp_time);
            free(tmp_time);

            fault_node = xmlAddNode(SoapMsg.pParser, results_node, DUSTATECHANGECOMPLETE_FAULT,
                                    NULL, NULL, NULL);
            snprintf(pTmpBuffer, TMPBUFFER_SIZE, "%u", dscc_result->faultCode);
            xmlAddNode(SoapMsg.pParser, fault_node, DUSTATECHANGECOMPLETE_FAULTCODE,
                       NULL, NULL, pTmpBuffer);
            xmlAddNode(SoapMsg.pParser, fault_node, DUSTATECHANGECOMPLETE_FAULTSTRING,
                       NULL, NULL, dscc_result->faultString);

            xmlAddNode(SoapMsg.pParser, dustatechangecomplete_node, DUSTATECHANGECOMPLETE_COMMANDKEY,
                       NULL, NULL, dscc->commandKey);
        }

        // ---------------------------------------------------------------------------
        // Remake the XML buffer then send it to DM_SendHttpMessage
        // ---------------------------------------------------------------------------
        if(SoapMsg.pRoot != NULL) {
            httpMsgString = DM_RemakeXMLBufferACS(SoapMsg.pRoot);
            if(httpMsgString != NULL) {
                if(DM_SendHttpMessage(httpMsgString) == DM_OK) {
                    SAH_TRACEZ_INFO("DM_COM", "DUStateChangeComplete - Sending http message : OK");
                } else {
                    DM_RemoveHeaderIDFromTab(pUniqueHeaderID);
                    SAH_TRACEZ_ERROR("DM_COM", "DUStateChangeComplete - Sending http message : NOK");
                }
            }
            free(httpMsgString);
        }

        nRet = DM_ENG_SESSION_OPENING;
    } else {
        SAH_TRACEZ_ERROR("DM_COM", ERROR_INVALID_PARAMETERS);
    }

    if(NULL != SoapMsg.pParser) {
        xmlDocumentFree(SoapMsg.pParser);
        SoapMsg.pParser = NULL;

    }
    return nRet;
}


#endif /* _DM_COM_RPC_ACS_H_ */

