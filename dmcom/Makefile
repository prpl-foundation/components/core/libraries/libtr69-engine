include ../makefile.inc

COMPONENT = libdmcom
# deb package name
PACKAGE_NAME = libtr69-engine

# build destination directories
OBJDIR = ../output/$(MACHINE)/$(COMPONENT)

# directories
# source directories
SRCDIR = ./src
INCDIR_PRIV = ./include_priv
INCDIRS = $(INCDIR_PRIV) -I./include -I../dmengine/include -I../dmcommon/include -I/include \
		  $(if $(STAGINGDIR), -I$(STAGINGDIR)/include) \
		  $(if $(STAGINGDIR), -I$(STAGINGDIR)/usr/include)

STAGING_LIBDIR = $(if $(STAGINGDIR), -L$(STAGINGDIR)/lib) \
				 $(if $(STAGINGDIR), -L$(STAGINGDIR)/usr/lib) \

OUTPUT_DIR = ../output
# files
SOURCES = $(wildcard $(SRCDIR)/*.c)
OBJECTS = $(addprefix $(OBJDIR)/,$(notdir $(SOURCES:.c=.o)))

# TARGETS
TARGET_SO = $(OBJDIR)/$(COMPONENT).so.$(VERSION)

CFLAGS += -Wall -Wextra -Wno-strict-aliasing \
		  -fPIC -g3 -I$(INCDIRS) -D_GNU_SOURCE \
		  -Wredundant-decls -DSAHTRACES_ENABLED \
		  $(shell pkg-config --cflags libxml-2.0) \
		  -DSAHTRACES_LEVEL_DEFAULT=700

ifeq ($(CC_NAME),g++)
	CFLAGS += -std=c++2a
else
	CFLAGS += -std=c11
endif

LDFLAGS += $(STAGING_LIBDIR) -shared -fPIC -lsahtrace -lxml2

ifeq ($(COVERAGE), y)
  CFLAGS += -fprofile-arcs -ftest-coverage
  LDFLAGS += -fprofile-arcs
endif

all: bannerCompile $(TARGET_SO)

bannerCompile:
	@echo "*********************** Building $(COMPONENT).so *********************************"

$(TARGET_SO): $(OBJECTS)
	$(CC) -Wl,-soname,$(COMPONENT).so -shared -o $@ $(OBJECTS) $(LDFLAGS)

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	$(MKDIR) -p $@

clean:
	rm -rf ../output/ ../$(PACKAGE_NAME)-*.* ../$(PACKAGE_NAME)_*.*

.PHONY: all bannerCompile clean
