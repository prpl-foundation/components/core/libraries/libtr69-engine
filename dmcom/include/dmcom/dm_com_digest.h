/*---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright (C) 2014 Orange
 *
 * This software is distributed under the terms and conditions of the 'Apache-2.0'
 * license which can be found in the file 'LICENSE.txt' in this package distribution
 * or at 'http://www.apache.org/licenses/LICENSE-2.0'.
 *
 *---------------------------------------------------------------------------
 * File        : dm_com_digest.h
 *
 * Created     : 2008/06/05
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file dm_com_digest.h
 *
 *
 */
#ifndef __DM_COM_DIGEST__
#define __DM_COM_DIGEST__

#ifdef __cplusplus
extern "C"
{
#endif

#include <dmcom/md5.h>
#include <dmcom/dm_com.h>


#define AuthorizationToken   "Authorization:"
#define DigestUsernameToken  "Digest username="
#define realmToken           "realm="
#define nonceToken           "nonce="
#define uriToken             "uri="
#define responseToken        "response="
#define opaqueToken          "opaque="
#define qopToken             "qop="
#define ncToken              "nc="
#define cnonceToken          "cnonce="

#define defaultCpeUsername "lbOui-liveBoxProductClass-123456789"

#define MAXTOKENSIZE        (300)
#define HA1_MAXTOKENSIZE    (3 * MAXTOKENSIZE)
#define HA2_MAXTOKENSIZE    (2 * MAXTOKENSIZE)
#define TOTAL_MAXTOKENSIZE  (6 * MAXTOKENSIZE)
#define SIGNATURE_SIZE      (16)
#define MAX_RESPONSE_SIZE   (616) //Max is 616

#define requestDigestAuthStr "Digest realm=\"sahrealm\", qop=\"auth\", nonce=\"%s\", opaque=\"%s\""

#define replyDigestAuthStr "Digest username=\"%s\",realm=\"%s\",uri=\"%s\",nonce=\"%s\",nc=%08x,cnonce=\"%s\",qop=auth,algorithm=MD5,response=\"%s\""

#define replyDigestAuthStr1 "Digest username=\"%s\", realm=\"%s\", uri=\"%s\", nonce=\"%s\", nc=%08x, cnonce=\"%s\", qop=auth, algorithm=MD5, response=\"%s\", opaque=\"%s\""

bool
DM_COM_CheckDigestAuthMessageContent(const char* _str);

bool
DM_COM_PerformClientDigestAuthentication(const char* _str,
                                         char* randomNonceStr,
                                         char* randomOpaqueStr,
                                         const char* cpeUsername,
                                         const char* cpeConnectionRequestPassword);

char*
DM_COM_GenerateRequestDigestMsg(char* randomNonceStr, char* randomOpaqueStr);

char*
DM_COM_GenerateDigestResponse_MD5(const char* auth,
                                  const char* cnonce,
                                  const char* path,
                                  const char* method,
                                  const char* username,
                                  const char* passwd);

#ifdef __cplusplus
}
#endif

#endif


