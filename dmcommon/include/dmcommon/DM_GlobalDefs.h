/*---------------------------------------------------------------------------
 * FT R&D
 *---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright France Telecom 2008, All Rights Reserved.
 *
 * This software is the confidential and proprietary information of France
 * Telecom. You shall not disclose such confidential information and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with France Telecom.
 *
 *---------------------------------------------------------------------------
 * File        : DM_GlobalDefs.h
 *
 * Created     : 2008/06/05
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_GlobalDefs.h
 *
 * @brief main header to tune target specific parameters parameter
 *
 **/



#ifndef _DM_GLOBALDEFS_H_
#define _DM_GLOBALDEFS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <dmcommon/CMN_Type_Def.h>

#ifndef _InternetGatewayDevice_ /* This flag is set in the main Makefile when DEVICE_TYPE is specified (IGD or D) */
/* Prefix type is Device*/
#define DM_PREFIX "Device."
#else
/* Prefix type is InternetGatewayDevice*/
#define DM_PREFIX "InternetGatewayDevice."
#endif

// Suppress the definition of DM_COM_CLOSE_ON_HTTP_200_ALLOWED to prohibit the session closure with HTTP 200
#define DM_COM_CLOSE_ON_HTTP_200_ALLOWED

#define DM_TR106_MAXCONNECTIONREQUEST       DM_PREFIX "ManagementServer.X_ORANGE-COM_MaxConnectionRequest"
#define DM_TR106_MAXCONNECTIONFREQ          DM_PREFIX "ManagementServer.X_ORANGE-COM_FreqConnectionRequest"
// Key to retrieve the WAN Ip address from the datamodel.
#define   DM_TR106_WANIP_ADDRESS            DM_PREFIX "Services.X_ORANGE-COM_Internet.WANIPConnection"
#define   DM_TR106_WANIP_EXTERNALIPADDRESS  DM_PREFIX "WANDevice.1.WANConnectionDevice.1.WANPPPConnection.1.ExternalIPAddress"

extern char* DATA_PATH;

// Size of the CPE URL
#define _CPE_URL_SIZE  16
// CPE PORT VALUE - Port assigned by IANA for CWMP : 7547
#define _CPE_PORT      50805

/* --------- Supported feature declaration ----------------- */
#define _CONNECTION_REQUEST_ALLOWED              true // An http server must be started or not for connection request
#define _CONNECTION_REQUEST_SUPPORTED            true // An http server must be started or not for connection request
#define _GETRPCMETHODS_RPC_SUPPORTED             true
#define _SETPARAMETERVALUES_RPC_SUPPORTED        true
#define _GETPARAMETERVALUES_RPC_SUPPORTED        true
#define _SETPARAMETERATTRIBUTES_RPC_SUPPORTED    true
#define _GETPARAMETERATTRIBUTES_RPC_SUPPORTED    true
#define _GETPARAMETERNAMES_RPC_SUPPORTED         true
#define _ADDOBJECT_RPC_SUPPORTED                 true
#define _DELETEOBJECT_RPC_SUPPORTED              true
#define _REBOOT_RPC_SUPPORTED                    true
#define _DOWNLOAD_RPC_SUPPORTED                  true
#define _FACTORYRESET_RPC_SUPPORTED              true
#define _SCHEDULEINFORM_RPC_SUPPORTED            true
#define _UPLOAD_RPC_SUPPORTED                    true
#define _GETQUEUEDTRANSFERS_RPC_SUPPORTED        false  // Not implemented
#define _GETALLQUEUEDTRANSFERS_RPC_SUPPORTED     true
#define _SETVOUCHERS_RPC_SUPPORTED               false  // Not implemented
#define _GETOPTIONS_RPC_SUPPORTED                false  // Not implemented

#define _FORCE_CWMP_1_0_USAGE                    true   // Karma supports only cwmp-1-0


/**
 * @brief Private routine used to generate a random stream
 *
 * @param A pointer on the stream to generate (the memory must be
 *        allocated by the calling routine)
         The size of the random stream to generate
 *
 * @Return TRUE if the stream is generated
 *         FALSE otherwise
 *
 */
bool DM_COMMON_generateRandomString(char* strToGenerate, int strSizeToGenerate);

// The routine below is used to retrieve the cwmp-1-X (CWMP Namespace Identifier) default flag used in
// the Soap message (X = 0 or 1). If the cwmp-1-1 flag is the default value, the CPE uses this flag and check if the
// ACS supports it (controling ACS response). If the ACS does not support the version, the CPE will decide to use the cwmp-1-0.
bool force_Cwmp_1_0_Usage();

/* ------ Define this macro to prevent the DM Agent to implement the persistence of the scheduled inform ------- */
// #define NO_PERSISTENT_SCHEDULED_INFORM 1

char* DM_COMMON_base64Decode(char* input);
char* DM_COMMON_base64Encode(const unsigned char* input);

void DM_COMMON_rpc_remove(const char* persistentRPCPath, const char* type);
void DM_COMMON_rpc_close(FILE* pFile, const char* persistentRPCPath, const char* type);
int DM_COMMON_rpc_read_scheduleinform(FILE* pFile, char** commandKey, time_t* time);
int DM_COMMON_rpc_write_scheduleinform(FILE* pFile, const char* commandKey, time_t time);
int DM_COMMON_rpc_read_subscription(FILE* pFile, char** path, char** value, uint32_t* type, char** accesslist);
int DM_COMMON_rpc_write_subscription(FILE* pFile, const char* path, const char* value, uint32_t type, const char* accesslist);
FILE* DM_COMMON_rpc_open_read(const char* persistentRPCPath, const char* type);
FILE* DM_COMMON_rpc_open_write(const char* persistentRPCPath, const char* type);

#ifdef __cplusplus
}
#endif

#endif /* _DM_GLOBALDEFS_H_ */
