/*---------------------------------------------------------------------------
 * FT R&D
 *---------------------------------------------------------------------------
 * Project     : TR069 Generic Agent
 *
 * Copyright France Telecom 2008, All Rights Reserved.
 *
 * This software is the confidential and proprietary information of France
 * Telecom. You shall not disclose such confidential information and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with France Telecom.
 *
 *---------------------------------------------------------------------------
 * File        : DM_GlobalUtils.c
 *
 * Created     : 22/05/2008
 * Author      :
 *
 *---------------------------------------------------------------------------
 * $Id$
 *
 *---------------------------------------------------------------------------
 * $Log$
 *
 */

/**
 * @file DM_GlobalUtils.c
 *
 * @brief
 *
 **/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE /* See feature_test_macros(7) */
#endif

#include <dmcommon/DM_GlobalDefs.h>
#include <debug/sahtrace.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>

#include <errno.h>

const bool DM_ENG_IS_GETRPCMETHODS_SUPPORTED = _GETRPCMETHODS_RPC_SUPPORTED;
const bool DM_ENG_IS_SETPARAMETERVALUES_SUPPORTED = _SETPARAMETERVALUES_RPC_SUPPORTED;
const bool DM_ENG_IS_GETPARAMETERVALUES_SUPPORTED = _GETPARAMETERVALUES_RPC_SUPPORTED;
const bool DM_ENG_IS_GETPARAMETERNAMES_SUPPORTED = _GETPARAMETERNAMES_RPC_SUPPORTED;
const bool DM_ENG_IS_SETPARAMETERATTRIBUTES_SUPPORTED = _SETPARAMETERATTRIBUTES_RPC_SUPPORTED;
const bool DM_ENG_IS_GETPARAMETERATTRIBUTES_SUPPORTED = _GETPARAMETERATTRIBUTES_RPC_SUPPORTED;
const bool DM_ENG_IS_ADDOBJECT_SUPPORTED = _ADDOBJECT_RPC_SUPPORTED;
const bool DM_ENG_IS_DELETEOBJECT_SUPPORTED = _DELETEOBJECT_RPC_SUPPORTED;
const bool DM_ENG_IS_REBOOT_SUPPORTED = _REBOOT_RPC_SUPPORTED;
const bool DM_ENG_IS_DOWNLOAD_SUPPORTED = _DOWNLOAD_RPC_SUPPORTED;
const bool DM_ENG_IS_UPLOAD_SUPPORTED = _UPLOAD_RPC_SUPPORTED;
const bool DM_ENG_IS_FACTORYRESET_SUPPORTED = _FACTORYRESET_RPC_SUPPORTED;
const bool DM_ENG_IS_GETQUEUEDTRANSFERS_SUPPORTED = _GETQUEUEDTRANSFERS_RPC_SUPPORTED;
const bool DM_ENG_IS_GETALLQUEUEDTRANSFERS_SUPPORTED = _GETALLQUEUEDTRANSFERS_RPC_SUPPORTED;
const bool DM_ENG_IS_SCHEDULEINFORM_SUPPORTED = _SCHEDULEINFORM_RPC_SUPPORTED;
const bool DM_ENG_IS_SETVOUCHERS_SUPPORTED = _SETVOUCHERS_RPC_SUPPORTED;
const bool DM_ENG_IS_GETOPTIONS_SUPPORTED = _GETOPTIONS_RPC_SUPPORTED;

const bool DM_ENG_IS_CONNECTION_REQUEST_ALLOWED = _CONNECTION_REQUEST_ALLOWED;

const bool DM_COM_IS_FORCED_CWMP_1_0_USAGE = _FORCE_CWMP_1_0_USAGE;

/////////////////////////////////////////////////////////////////
//   PARAMETER NAMES
/////////////////////////////////////////////////////////////////

const char* DM_ENG_PARAMETER_PREFIX = DM_PREFIX;

// Parameter names definitions
const char* DM_TR106_ACS_URL = DM_PREFIX "ManagementServer.URL";
const char* DM_TR106_ACS_USERNAME = DM_PREFIX "ManagementServer.Username";
const char* DM_TR106_ACS_PASSWORD = DM_PREFIX "ManagementServer.Password";
const char* DM_TR106_CONNECTIONREQUESTURL = DM_PREFIX "ManagementServer.ConnectionRequestURL";
const char* DM_TR106_CONNECTIONREQUESTUSERNAME = DM_PREFIX "ManagementServer.ConnectionRequestUsername";
const char* DM_TR106_CONNECTIONREQUESTPASSWORD = DM_PREFIX "ManagementServer.ConnectionRequestPassword";
const char* DM_TR106_STUN_PASSWORD = DM_PREFIX "ManagementServer.STUNPassword";

const char* DM_ENG_LAN_IP_ADDRESS_NAME = DM_PREFIX "LAN.IPAddress";
const char* DM_ENG_PARAMETER_KEY_NAME = DM_PREFIX "ManagementServer.ParameterKey";
const char* DM_ENG_DEVICE_STATUS_PARAMETER_NAME = DM_PREFIX "DeviceInfo.DeviceStatus";

const char* DM_ENG_PERIODIC_INFORM_ENABLE_NAME = DM_PREFIX "ManagementServer.PeriodicInformEnable";
const char* DM_ENG_PERIODIC_INFORM_INTERVAL_NAME = DM_PREFIX "ManagementServer.PeriodicInformInterval";
const char* DM_ENG_PERIODIC_INFORM_TIME_NAME = DM_PREFIX "ManagementServer.PeriodicInformTime";

const char* DM_ENG_DEVICE_MANUFACTURER_NAME = DM_PREFIX "DeviceInfo.Manufacturer";
const char* DM_ENG_DEVICE_MANUFACTURER_OUI_NAME = DM_PREFIX "DeviceInfo.ManufacturerOUI";
const char* DM_ENG_DEVICE_PRODUCT_CLASS_NAME = DM_PREFIX "DeviceInfo.ProductClass";
const char* DM_ENG_DEVICE_SERIAL_NUMBER_NAME = DM_PREFIX "DeviceInfo.SerialNumber";

/////////////////////////////////////////////////////////////////
//   CONNECTION REQUEST URL
/////////////////////////////////////////////////////////////////

const int CPE_URL_SIZE = _CPE_URL_SIZE;
const int CPE_PORT = _CPE_PORT;


bool force_Cwmp_1_0_Usage() {
    return _FORCE_CWMP_1_0_USAGE;
}

/**
 * @brief Private routine used to generate a random stream
 *
 * @param A pointer on the stream to generate (the memory must be
 *        allocated by the calling routine)
          The size of the random stream to generate
 *
 * @Return TRUE if the stream is generated
 *         FALSE otherwise
 *
 */
bool
DM_COMMON_generateRandomString(char* strToGenerate,
                               int strSizeToGenerate) {

    int r, x;
    char* tmpStr;
    char _dico[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    // Check parameter
    if(NULL == strToGenerate) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Invalid Parameters");
        return false;
    }

    memset((void*) strToGenerate, 0x00, strSizeToGenerate);
    tmpStr = strToGenerate;

    for(x = 0; x < strSizeToGenerate; x++) {
        int randomData = open("/dev/urandom", O_RDONLY);
        size_t bytes = 0;

        if(randomData < 0) {
            SAH_TRACEZ_INFO("DM_COMMON", "Failed to read /dev/urandom");
            continue;
        }

        bytes = read(randomData, &r, sizeof r);
        if(bytes == 0) {
            SAH_TRACEZ_INFO("DM_COMMON", "Failed to read /dev/urandom");
        }
        // you now have a random integer!
        r %= strlen(_dico);
        close(randomData);

        sprintf(tmpStr, "%c", _dico[r]);
        tmpStr++;
    }

    // Add \0 at the end
    memcpy((void*) tmpStr, "\0", 1);

    SAH_TRACEZ_INFO("DM_COMMON", "GENERATED RANDOM STRING:%s", strToGenerate);

    return true;
}

static const char* to_b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

//---------------------------------------------------------------------------------------------
/**
   @brief
   Base 64 encode the specified string.

   @details
   Base 64 encode the specified string.
   The calling routine is responsible for freeing the result.

   @param input The string to encode

   @return
   The base 64 encoded version of the string
 */
char* DM_COMMON_base64Encode(const unsigned char* input) {
    if(!input) {
        return NULL;
    }

    int length = strlen((char*) input);
    int div = length / 3;
    int rem = length % 3;
    int reqlen = div * 4 + (rem ? 4 : 0) + 1;
    char* pdst = NULL;
    const unsigned char* psrc = NULL;
    char* res = NULL;

    res = (char*) calloc(reqlen + 2, sizeof(char));
    if(!res) {
        return NULL;
    }
    pdst = res;
    psrc = input;

    while(div > 0) {
        pdst[0] = to_b64[ (psrc[0] >> 2) & 0x3f];
        pdst[1] = to_b64[((psrc[0] << 4) & 0x30) + ((psrc[1] >> 4) & 0xf)];
        pdst[2] = to_b64[((psrc[1] << 2) & 0x3c) + ((psrc[2] >> 6) & 0x3)];
        pdst[3] = to_b64[  psrc[2] & 0x3f];
        psrc += 3;
        pdst += 4;
        div--;
    }

    switch(rem) {
    case 2:
        pdst[0] = to_b64[ (psrc[0] >> 2) & 0x3f];
        pdst[1] = to_b64[((psrc[0] << 4) & 0x30) + ((psrc[1] >> 4) & 0xf)];
        pdst[2] = to_b64[ (psrc[1] << 2) & 0x3c];
        pdst[3] = '=';
        pdst += 4;
        break;
    case 1:
        pdst[0] = to_b64[ (psrc[0] >> 2) & 0x3f];
        pdst[1] = to_b64[ (psrc[0] << 4) & 0x30];
        pdst[2] = '=';
        pdst[3] = '=';
        pdst += 4;
        break;
    }
    *pdst = '\0';
    return res; /* success */
}

//---------------------------------------------------------------------------------------------
/**
   @brief
   Base 64 decoded the specified string.

   @details
   Base 64 decoded the specified string.
   The calling routine is responsible for freeing the result.

   @param input The string to decoded

   @return
   The base 64 decoded version of the string
 */
char* DM_COMMON_base64Decode(char* input) {
    if(input == 0) {
        return NULL;
    }

    int count = 0, rem = 0;
    const char* tmp = input;
    int stringlen = strlen(input);
    unsigned int reqbuf;
    char* res = NULL;



    // Check if string is valid
    while(stringlen > 0) {
        int skip = strspn(tmp, to_b64);
        count += skip;
        stringlen -= skip;
        tmp += skip;
        if(stringlen > 0) {
            int i, pos = strcspn(tmp, to_b64);

            for(i = 0; i < pos; i++) {
                if((tmp[i] == '\r') || (tmp[i] == '\n')) {
                    continue;
                }

                if(tmp[i] == '=') {
                    /* we should check if we're close to the end of the string */
                    rem = count % 4;

                    /* rem must be either 2 or 3, otherwise no '=' should be here */
                    if(rem < 2) {
                        return NULL;
                    }
                    /* end-of-message recognized */
                    break;
                } else {
                    /* Transmission error; RFC tells us to ignore this, but:
                     *  - the rest of the message is going to even more corrupt since we're sliding bits out of place
                     * If a message is corrupt, it should be dropped. Period.
                     */

                    return NULL;
                }
            }

            stringlen -= pos;
            tmp += pos;
        }
    }

    // Decode string
    reqbuf = ((count / 4) * 3 + (rem ? (rem - 1) : 0));

    res = (char*) calloc(reqbuf + 2, sizeof(char));
    if(!res) {
        return NULL;
    }

    if(count > 0) {
        int i, qw = 0, tw = 0;

        stringlen = strlen(input);

        for(i = 0; i < stringlen; i++) {
            char ch = input[i], bits;

            if((ch == '\r') || (ch == '\n')) {
                continue;
            }

            bits = 0;
            if((ch >= 'A') && (ch <= 'Z')) {
                bits = (char) (ch - 'A');
            } else if((ch >= 'a') && (ch <= 'z')) {
                bits = (char) (ch - 'a' + 26);
            } else if((ch >= '0') && (ch <= '9')) {
                bits = (char) (ch - '0' + 52);
            } else if(ch == '+') {
                bits = 62;
            } else if(ch == '/') {
                bits = 63;
            } else if(ch == '=') {
                break;
            }

            switch(qw++) {
            case 0:
                res[tw + 0] = (bits << 2) & 0xfc;
                break;
            case 1:
                res[tw + 0] |= (bits >> 4) & 0x03;
                res[tw + 1] = (bits << 4) & 0xf0;
                break;
            case 2:
                res[tw + 1] |= (bits >> 2) & 0x0f;
                res[tw + 2] = (bits << 6) & 0xc0;
                break;
            case 3:
                res[tw + 2] |= bits & 0x3f;
                break;
            }

            if(qw == 4) {
                qw = 0;
                tw += 3;
            }
        }
    }
    return res;
}

/**
 * Safely close a file, make sure it is flushed to persistent storage
 *
 * @param fp The open file pointer
 * @param fullfilepath The full file path
 *
 */
static void DM_COMMON_Device_safeClose(FILE* fp, char* fullfilepath) {
    int fd, dir_fd;
    char* containing_dir;

    /*
     * After the fwrite call returns, the data is in libc's stdio
     * buffer (still in the application's address space).  So, the
     * next thing we want to do is flush that buffer.
     */
    if(fflush(fp) != 0) {
        SAH_TRACEZ_ERROR("DM_COMMON", "fflush error");
    }

    /*
     * Now the data is in the kernel's page cache.  The next steps
     * flush the page cache for this file to disk.
     */
    fd = fileno(fp);
    if(fd == -1) {
        SAH_TRACEZ_ERROR("DM_COMMON", "fileno error");
    } else {
        fsync(fd);
    }
    /*
     * Because we just created this file, we also need to ensure that
     * the new directory entry gets flushed to disk.
     */
    /*
     * Basename and dirname may modify the string passed in. Therefore
     * strdup the file path
     * it.
     */
    char* tmp = strdup(fullfilepath);
    containing_dir = dirname(tmp);

    /*
     * You can't write directly to a directory.  fsync, however
     * is allowed on the directory, even when opened read-only.
     */
    dir_fd = open(containing_dir, O_RDONLY);
    if(dir_fd < 0) {
        SAH_TRACEZ_ERROR("DM_COMMON", "open dir error");
    } else {
        fsync(dir_fd);
        /*
         * There really shouldn't be any errors returned from close,
         * here.  However, in the case of memory corruption
         * (overwriting the dir_fd, for example), you can get a failure.
         * Also, the close call can be interrupted, which we don't
         * specifically handle.  The exit will take care of finishing
         * the job.
         */
        if(close(dir_fd) < 0) {
            SAH_TRACEZ_ERROR("DM_COMMON", "fclose error");
        }
    }
    free(tmp);


    if(fclose(fp) < 0) {
        SAH_TRACEZ_ERROR("DM_COMMON", "fclose error");
    }

}

/**
 * Safely rename a file, make sure it is flushed to persistent storage
 *
 * @param source The full file path of the file to rename
 * @param destination The full file path of the destination file
 *
 */
static void DM_COMMON_Device_safeRename(const char* source, const char* destination) {
    char* containing_dir;
    char* dest = strdup(destination);
    containing_dir = dirname(dest);

    if(rename(source, destination) == -1) {
        SAH_TRACEZ_INFO("DM_COMMON", "Could not rename %s to %s", source, destination);
    }
    /*
     * Because we just created this file, we also need to ensure that
     * the new directory entry gets flushed to disk.
     */
    int dir_fd = open(containing_dir, O_RDONLY);
    if(dir_fd < 0) {
        SAH_TRACEZ_ERROR("DM_COMMON", "could not get the directory descriptor");
    } else {
        if(fsync(dir_fd) < 0) {
            SAH_TRACEZ_ERROR("DM_COMMON", "could not sync the directory descriptor");
        }
        close(dir_fd);
    }

    sync();
    free(dest);
}

/**
 * Create rpc filenames based on rpc path and type
 *
 * @param persistentRPCPath The path
 * @param type The filename
 * @param cwmp_rpc_file The resulting path
 * @param cwmp_tmp_rpc_file The resulting temp path
 *
 * @return -1 on error, 0 on success
 *
 */
static int DM_COMMON_rpc_filenames(const char* persistentRPCPath, const char* type, char** cwmp_rpc_file, char** cwmp_tmp_rpc_file) {
    *cwmp_rpc_file = NULL;
    *cwmp_tmp_rpc_file = NULL;
    if(asprintf(cwmp_rpc_file, "%s/%s.txt", persistentRPCPath, type) == -1) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Failed to create tmp config file location string");
        return -1;
    }
    if(asprintf(cwmp_tmp_rpc_file, "%s/%s_tmp.txt", persistentRPCPath, type) == -1) {
        free(*cwmp_rpc_file);
        *cwmp_rpc_file = NULL;
        SAH_TRACEZ_ERROR("DM_COMMON", "Failed to create tmp config file location string");
        return -1;
    }
    return 0;
}

/**
 * Open the specified rpc file for write
 *
 * @param persistentRPCPath The path
 * @param type The filename
 *
 * @return NULL on error, file stream on success
 *
 */
FILE* DM_COMMON_rpc_open_write(const char* persistentRPCPath, const char* type) {
    SAH_TRACEZ_IN("DM_COMMON");
    char* cwmp_rpc_file = NULL;
    char* cwmp_tmp_rpc_file = NULL;
    FILE* pFile = NULL;

    if(DM_COMMON_rpc_filenames(persistentRPCPath, type, &cwmp_rpc_file, &cwmp_tmp_rpc_file) == -1) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Could not get file names");
        goto done;
    }

    pFile = fopen(cwmp_tmp_rpc_file, "w");
    if(pFile == NULL) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Failed to open tmp file [%s] [%s]", cwmp_tmp_rpc_file, strerror(errno));
        goto done;
    }
done:
    free(cwmp_rpc_file);
    free(cwmp_tmp_rpc_file);
    SAH_TRACEZ_OUT("DM_COMMON");
    return pFile;
}

/**
 * Open the specified rpc file for read
 *
 * @param persistentRPCPath The path
 * @param type The filename
 *
 * @return NULL on error, file stream on success
 *
 */
FILE* DM_COMMON_rpc_open_read(const char* persistentRPCPath, const char* type) {
    SAH_TRACEZ_IN("DM_ENGINE");
    char* cwmp_rpc_file = NULL;
    char* cwmp_tmp_rpc_file = NULL;
    FILE* pFile = NULL;
    if(DM_COMMON_rpc_filenames(persistentRPCPath, type, &cwmp_rpc_file, &cwmp_tmp_rpc_file) == -1) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Could not get file names");
        goto done;
    }

    pFile = fopen(cwmp_rpc_file, "r");
    if(pFile == NULL) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Failed to open file [%s] [%s]", cwmp_rpc_file, strerror(errno));
        goto done;
    }
done:
    free(cwmp_rpc_file);
    free(cwmp_tmp_rpc_file);
    SAH_TRACEZ_OUT("DM_ENGINE");
    return pFile;
}

/**
 * Write a subscription to the specified file stream
 *
 * @param pFile The file stream
 * @param path The path
 * @param value The value
 * @param type The type 0 == "Off"; 1 == "Passive"; 2 == "Active"; 3 == "Forced"
 * @param accesslist The acceslist
 *
 * @return -1 on error, 0 on success
 *
 */
int DM_COMMON_rpc_write_subscription(FILE* pFile, const char* path, const char* value, uint32_t type, const char* accesslist) {
    SAH_TRACEZ_IN("DM_COMMON");
    if(!pFile || !path) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Open file, path are mandatory");
        return -1;
    }

    // SAH_TRACEZ_INFO("DM_COMMON", "%s,%d,%s,%s\n", path, type, encodedValue, accesslist ? accesslist[0] : "NULL");
    char* encodedValue = NULL;
    if(value) {
        SAH_TRACEZ_INFO("DM_COMMON", "encoding");
        encodedValue = DM_COMMON_base64Encode((unsigned char*) value);
    } else {
        SAH_TRACEZ_INFO("DM_COMMON", "duping");
        encodedValue = strdup("");
    }
    fprintf(pFile, "%s,%d,%s,%s\n", path, type, encodedValue, accesslist ? accesslist : "NULL");
    free(encodedValue);
    SAH_TRACEZ_OUT("DM_COMMON");
    return 0;
}

/**
 * Read a subscription from the specified file stream
 *
 * @param pFile The file stream
 * @param path The path
 * @param value The value
 * @param type The type 0 == "Off"; 1 == "Passive"; 2 == "Active"; 3 == "Forced"
 * @param accesslist The acceslist
 *
 * @return -1 on error, 0 on success
 *
 */
int DM_COMMON_rpc_read_subscription(FILE* pFile, char** path, char** value, uint32_t* type, char** accesslist) {
    SAH_TRACEZ_IN("DM_COMMON");
    char tmp[1000];
    char* pathstr;
    char* notificationstr;
    char* valuestr;
    char* accessliststr;
    char* ptr;
    int len;

    *path = NULL;
    *value = NULL;
    *type = 4;
    *accesslist = NULL;

    if(!pFile) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Open file parameter is mandatory");
        return -1;
    }

    /* read a line */
    if(fgets(tmp, sizeof(tmp), pFile) == 0) {
        return -1;
    }

    /* remove newline */
    len = strlen(tmp);
    if(tmp[len - 1] == '\n') {
        tmp[len - 1] = 0;
    }

    /* parse a line */
    SAH_TRACEZ_INFO("DM_COMMON", "Line: %s", tmp);
    pathstr = tmp;
    for(ptr = tmp; *ptr != '\0'; ptr++) {
        if(*ptr == ',') {
            *ptr = '\0';
            ptr++;
            break;
        }
    }
    notificationstr = ptr;
    for(; *ptr != '\0'; ptr++) {
        if(*ptr == ',') {
            *ptr = '\0';
            ptr++;
            break;
        }
    }
    valuestr = ptr;
    for(; *ptr != '\0'; ptr++) {
        if(*ptr == ',') {
            *ptr = '\0';
            ptr++;
            break;
        }
    }
    accessliststr = ptr;

    /* interprete the values*/
    if((strlen(pathstr) == 0) || (strlen(notificationstr) == 0)) {
        return -1;
    }
    *path = strdup(pathstr);
    *type = atoi(notificationstr);

    if(valuestr && (strlen(valuestr) > 0)) {
        *value = DM_COMMON_base64Decode(valuestr);
    } else {
        *value = NULL;
    }

    if(accessliststr) {
        *accesslist = strdup("NULL");
    }
    SAH_TRACEZ_OUT("DM_COMMON");
    return 0;
}

/**
 * Write a schedule inform event to the specified file stream
 *
 * @param pFile The file stream
 * @param commandKey The command key
 * @param time The time
 *
 * @return -1 on error, 0 on success
 *
 */
int DM_COMMON_rpc_write_scheduleinform(FILE* pFile, const char* commandKey, time_t time) {
    SAH_TRACEZ_IN("DM_COMMON");
    if(!pFile || !commandKey) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Open file, path are mandatory");
        return -1;
    }

    SAH_TRACEZ_INFO("DM_COMMON", "%s,%ld", commandKey, time);

    fprintf(pFile, "%s\n%ld\n", commandKey, time);
    SAH_TRACEZ_OUT("DM_COMMON");
    return 0;
}

/**
 * Read a schedule inform event from the specified file stream
 *
 * @param pFile The file stream
 * @param commandKey The command key
 * @param time The time
 *
 * @return -1 on error, 0 on success
 *
 */
int DM_COMMON_rpc_read_scheduleinform(FILE* pFile, char** commandKey, time_t* time) {
    char cmdkeystr[200];
    char timestr[64];
    int len;

    *commandKey = NULL;
    *time = 0;

    if(!pFile) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Open file parameter is mandatory");
        return -1;
    }
    if(fgets(cmdkeystr, sizeof(cmdkeystr), pFile) == NULL) {
        return -1;
    }

    /* remove newline */
    len = strlen(cmdkeystr);
    if(cmdkeystr[len - 1] == '\n') {
        cmdkeystr[len - 1] = 0;
    }

    if(fgets(timestr, sizeof(timestr), pFile) == NULL) {
        return -1;
    }

    /* remove newline */
    len = strlen(timestr);
    if(timestr[len - 1] == '\n') {
        timestr[len - 1] = 0;
    }

    cmdkeystr[strlen(cmdkeystr)] = '\0'; // remove /n
    sscanf(timestr, "%32ld\n", time);
    *commandKey = strdup(cmdkeystr);

    return 0;
}

/**
 * Safely close the specified file stream (e.g. move everthing from the tmp file to the real backup file in an atomic operation)
 *
 * @param pFile The file stream
 * @param persistentRPCPath The path
 * @param type The type
 *
 */
void DM_COMMON_rpc_close(FILE* pFile, const char* persistentRPCPath, const char* type) {
    char* cwmp_rpc_file = NULL;
    char* cwmp_tmp_rpc_file = NULL;
    if(DM_COMMON_rpc_filenames(persistentRPCPath, type, &cwmp_rpc_file, &cwmp_tmp_rpc_file) == -1) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Could not get file names");
        return;
    }

    DM_COMMON_Device_safeClose(pFile, cwmp_tmp_rpc_file);
    DM_COMMON_Device_safeRename(cwmp_tmp_rpc_file, cwmp_rpc_file);
    free(cwmp_tmp_rpc_file);
    free(cwmp_rpc_file);
}

/**
 * Remove the the specified file
 *
 * @param pFile The file stream
 * @param persistentRPCPath The path
 * @param type The type
 *
 */
void DM_COMMON_rpc_remove(const char* persistentRPCPath, const char* type) {
    char* cwmp_rpc_file = NULL;
    char* cwmp_tmp_rpc_file = NULL;
    if(DM_COMMON_rpc_filenames(persistentRPCPath, type, &cwmp_rpc_file, &cwmp_tmp_rpc_file) == -1) {
        SAH_TRACEZ_ERROR("DM_COMMON", "Could not get file names");
        return;
    }

    if(cwmp_tmp_rpc_file) {
        if(remove(cwmp_tmp_rpc_file) == -1) {
            SAH_TRACEZ_INFO("DM_COMMON", "Could not remove %s", cwmp_tmp_rpc_file);
        }
    }
    if(cwmp_rpc_file) {
        if(remove(cwmp_rpc_file) == -1) {
            SAH_TRACEZ_INFO("DM_COMMON", "Could not remove %s", cwmp_rpc_file);
        }
    }
    sync();
    free(cwmp_tmp_rpc_file);
    free(cwmp_rpc_file);
}


