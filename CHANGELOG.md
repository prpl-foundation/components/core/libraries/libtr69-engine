# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.9.20 - 2024-12-10(12:04:12 +0000)

### Other

- Rework adapter cnx to the bus

## Release v0.9.19 - 2024-12-06(13:29:31 +0000)

### Other

- Rework qos classification

## Release v0.9.18 - 2024-11-12(13:47:53 +0000)

### Other

- Scheduled Download is not working with WindowMode  At any time

## Release v0.9.17 - 2024-10-24(10:52:15 +0000)

### Other

- GPV not adhering to the standard for partial paths and wildcard

## Release v0.9.16 - 2024-10-16(15:44:24 +0000)

## Release v0.9.15 - 2024-10-10(18:11:47 +0000)

### Other

- - [tr069-manager] 'Reboot.CurrentBootCycle' parameter should be...

## Release v0.9.14 - 2024-10-03(13:25:54 +0000)

### Other

- Optimize GPA when used to get all datamodel

## Release v0.9.13 - 2024-09-24(09:05:57 +0000)

### Other

- The box is not correctly handling the largest ManagementServer credentials

## Release v0.9.12 - 2024-09-20(08:47:54 +0000)

### Other

- [TR-069][QoS]Management traffic initiated from the HGW is with wrong DSCP

## Release v0.9.11 - 2024-09-11(16:00:51 +0000)

### Other

- Change Logs visibility for value change of Periodic Inform Interval

## Release v0.9.10 - 2024-09-03(11:17:34 +0000)

### Other

- PeriodicInformCounter parameter is not reset after 0 Bootstrap (ACS URL modif)

## Release v0.9.9 - 2024-08-16(12:33:09 +0000)

### Other

- Add SMM Mapping and Fix issues on TR157

## Release v0.9.8 - 2024-08-09(12:46:20 +0000)

### Other

- Enable Translation for GPA, GPN, and GPV

## Release v0.9.7 - 2024-08-02(15:00:22 +0000)

### Other

- Add dedicated log for value change of Periodic Inform Interval

## Release v0.9.6 - 2024-07-30(13:44:29 +0000)

### Other

- Add counter of Inform retries

## Release v0.9.5 - 2024-07-24(09:50:12 +0000)

### Other

- [TR-069] Add possibility to disable strict type checking for boolean parameters

## Release v0.9.4 - 2024-07-03(08:49:23 +0000)

### Other

- Optimize GPV when used to get all datamodel

## Release v0.9.3 - 2024-06-11(06:50:01 +0000)

### Other

- [TR069] ManagementServer.PeriodicInformCounter increments...

## Release v0.9.2 - 2024-06-07(15:16:55 +0000)

### Other

- Upload RPC: Username and Password NULL when not provided by the ACS

## Release v0.9.1 - 2024-06-04(14:19:41 +0000)

### Other

- remove debug stuff

## Release v0.9.0 - 2024-05-31(11:36:19 +0000)

### Other

- Implement ChangeDUState

## Release v0.8.16 - 2024-05-27(08:49:01 +0000)

### Other

- XMPP support - 2nd implementation

## Release v0.8.15 - 2024-05-22(07:13:31 +0000)

### Other

- CLONE - [Random][cwmpd] Crash of cwmpd after factory reset

## Release v0.8.14 - 2024-05-03(14:51:32 +0000)

### Other

- XMPP support - 1st implementation

## Release v0.8.13 - 2024-04-10(11:46:41 +0000)

### Other

- Add ScheduleDownload RPC

## Release v0.8.12 - 2024-04-05(13:10:31 +0000)

### Fixes

- firmware upgrade isn't working anymore

## Release v0.8.11 - 2024-04-04(16:24:47 +0000)

### Other

- DUT doesn't send '8 Diagnostics Complete' to the ACS after spv list fails

## Release v0.8.10 - 2024-04-03(15:30:08 +0000)

### Fixes

- DUT doesn't send '8 Diagnostics Complete' to the ACS

## Release v0.8.9 - 2024-03-22(08:27:38 +0000)

### Other

- [TR-069] Ability to configure default subscriptions under...

## Release v0.8.8 - 2024-03-19(09:44:17 +0000)

### Other

- TR69 Unitary Test: Heartbeat Inform

## Release v0.8.7 - 2024-03-15(15:58:04 +0000)

### Other

- [TR069] missing parameters in the Inform (PERIODIC)

## Release v0.8.6 - 2024-02-29(10:18:33 +0000)

### Other

- Add support of Heartbeat Inform

## Release v0.8.5 - 2024-02-02(17:37:29 +0000)

### Other

- LastSession not getting updated properly

## Release v0.8.4 - 2024-01-23(17:42:40 +0000)

### Fixes

- GPValues on empty Device.NAT.PortMapping. fails

## Release v0.8.3 - 2024-01-19(17:03:13 +0000)

### Other

- ACS Session lasts 30s more than expected

## Release v0.8.2 - 2024-01-12(16:31:50 +0000)

### Fixes

- inform sent every 12 sec

## Release v0.8.1 - 2024-01-11(14:13:29 +0000)

### Other

- ManagementServer.State.LastSession is not updated correctly after upgrade

## Release v0.8.0 - 2023-06-21(16:16:10 +0000)

### New

- use acls permissions for cwmp user

## Release v0.7.10 - 2023-02-21(09:31:14 +0000)

### Other

- Implementation of InstanceMode="InstanceAlias" [New]

## Release v0.7.9 - 2022-12-08(17:36:34 +0000)

### Fixes

- Fix transferComplete event

## Release v0.7.8 - 2022-10-05(13:51:36 +0000)

### Fixes

- latest versions break digest authentication

## Release v0.7.7 - 2022-08-11(10:21:39 +0000)

### Fixes

- Fix Digest Authentication when qop or nc headers values are quoted

## Release v0.7.6 - 2022-08-05(13:00:26 +0000)

### Fixes

- Fix cwmpd crash when SetParameterAttributes

## Release v0.7.5 - 2022-07-29(12:52:53 +0000)

### Fixes

- Fix InformMessageScheduler cleanup before initialzing

## Release v0.7.4 - 2022-07-04(15:27:15 +0000)

### Fixes

- warning on some parameter types

## Release v0.7.3 - 2022-06-08(15:03:46 +0000)

### Fixes

- Fix saving attributes cache and schedulerinform

## Release v0.7.2 - 2022-05-23(07:41:51 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.7.1 - 2022-04-29(14:20:21 +0000)

### Fixes

- failed to generate auth headers

## Release v0.7.0 - 2022-03-29(11:25:52 +0000)

### New

- Add support for digest auth

## Release v0.6.0 - 2022-02-25(09:02:09 +0000)

### New

- Make DNS fully async

## Release v0.5.0 - 2022-02-18(13:11:39 +0000)

### New

- - Add notification mode to engine

## Release v0.4.2 - 2022-02-16(14:57:54 +0000)

### Fixes

- Avoid casting from const char* to char*

## Release v0.4.1 - 2021-11-22(16:09:05 +0000)

### Changes

- replace printf by sahtrace

## Release v0.4.0 - 2021-11-22(15:39:03 +0000)

### New

- Add support for ACS Address Family

## Release v0.3.1 - 2021-11-22(09:05:54 +0000)

### Fixes

- Isssue: PCF-483 Fix make clean target to remove deb package

## Release v0.3.0 - 2021-11-10(11:42:31 +0000)

### New

- Add cache file option

## Release v0.2.1 - 2021-11-08(09:38:54 +0000)

### Fixes

- missing const blocks tr069-manager tests from compiling

## Release v0.2.0 - 2021-10-15(09:39:58 +0000)

### New

- Change functions using char* to const char* to avoid const cast

## Release v0.1.6 - 2021-09-29(14:50:14 +0000)

### Fixes

- remove redundant function declaration

## Release v0.1.5 - 2021-09-29(09:24:58 +0000)

### Fixes

- remove redundant function declaration

## Release v0.1.4 - 2021-09-24(15:14:15 +0000)

## Release v0.1.3 - 2021-09-24(09:02:18 +0000)

### Fixes

- GPN on empty object should return empty

## Release v0.1.2 - 2021-09-23(07:20:13 +0000)

### Fixes

- Avoid cast from const char* to char*
- Fix GPN with Device. and nextlevel true

## Release v0.1.1 - 2021-09-15(14:08:58 +0000)

### Other

- Fix generating a .deb package for libtr69-engine

## Release v0.1.0 - 2021-09-14(06:50:59 +0000)

### New

- Issue : PCF-150 [TR181 CWMP Plugin] First implementation for TR-69 stack (dmcom,dmcommon,dmengine)

